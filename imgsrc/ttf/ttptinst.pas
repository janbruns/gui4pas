{$mode objfpc}
{$unitpath .}
{$COPERATORS ON}

UNIT ttptinst;


{ The more font-data/render-engine specific parts of the
  TrueType code interpreter, things that don't look like
  a simple stack-machine.



  
  
}

interface
uses 
  ttstack,
  ttprog,
  ttcalc,
  ttdata,
  fontfile;

type
Tpointinstructor = class(TTexecenv)
  constructor create();
  destructor destroy(); override;

  function get_zonepoint_x(ptidx,zone : longint) : longint; override;
  function get_zonepoint_y(ptidx,zone : longint) : longint; override;
  procedure do_MDRP_instr(b : byte); override;
  procedure do_MIRP_instr(b : byte); override;
  procedure do_readwrite_store(b : byte); override;
  procedure do_ALPIGNPTS(b : byte); override;
  procedure do_UTP(b : byte); override;
  procedure do_MDAP(b : byte); override;
  procedure do_IUP(b : byte); override;
  procedure do_SHP(b : byte); override;
  procedure do_SHC(b : byte); override;
  procedure do_SHZ(b : byte); override;
  procedure do_SHPIX(b : byte); override;
  procedure do_IP(b : byte); override;
  procedure do_MSIRP(b : byte); override;
  procedure do_ALIGNRP(b : byte); override;
  procedure do_MIAP(b : byte); override;
  procedure do_RCVT(b : byte); override;
  procedure do_GC(b : byte); override;
  procedure do_SCFS(b : byte); override;
  procedure do_MD(b : byte); override;
  procedure do_SDPVTL(b : byte); override;
  procedure do_ISECT(b : byte); override;

  procedure switch_to_dv;
  procedure switch_to_pv;
  function nonzero_fv() : boolean;
  function nonzero_pv() : boolean;
  function nonzero_PV_dot_fV() : boolean;
  function nonzero_v() : boolean;



  function get_org_zonepoint_x(ptidx,zone : longint) : longint; virtual; abstract;
  function get_org_zonepoint_y(ptidx,zone : longint) : longint; virtual; abstract;
  function get_zonepoint_x(ptidx,zone : longint) : longint; virtual; abstract;
  function get_zonepoint_y(ptidx,zone : longint) : longint; virtual; abstract;
  procedure set_zonepoint_x(ptidx,zone,v : longint); virtual; abstract;
  procedure set_zonepoint_y(ptidx,zone,v : longint); virtual; abstract;
  procedure set_org_zonepoint_x(ptidx,zone,v : longint); virtual; abstract;
  procedure set_org_zonepoint_y(ptidx,zone,v : longint); virtual; abstract;
  procedure set_touched_z1x(p : longint; t : boolean); virtual; abstract;
  procedure set_touched_z1y(p : longint; t : boolean); virtual; abstract;
  function is_touched_z1x(p : longint) : boolean; virtual; abstract;
  function is_touched_z1y(p : longint) : boolean; virtual; abstract;
  function get_last_zone_point_idx(z : longint) : longint; virtual; abstract;

  procedure translate_point_x(k,zone,a : longint);
  procedure translate_point_y(k,zone,a : longint);
  procedure mov_point_fv(k,zone,a : longint);
  procedure mov_point_fv_by_dist_X_pvdotfv(k,zone : longint; a : F26DOT6);
  procedure mov_point_fv_by_dist_div_pvdotfv(k,zone : longint; a : F26DOT6);
  procedure mov_org_point_fv_by_dist_div_pvdotfv(k,zone : longint; a : F26DOT6);

  
  procedure translate_org_point_x(k,zone : longint; a : F26DOT6);
  procedure translate_org_point_y(k,zone : longint; a : F26DOT6);
  procedure mov_org_point_fv(k,zone : longint; a : F26DOT6);


  function pv_dot_fv() : F2DOT14; 
  procedure mov_cvt(k : longint; a : F26DOT6);
  function do_minimum_distance(d : F26DOT6) : F26DOT6;
  procedure mul_FV(d : F26DOT6; var x,y : F26DOT6);
  function dot_PV(x,y : F26DOT6) : F26DOT6; // [x,y].pv
  function dot_dPV(x,y : F26DOT6) : F26DOT6; // [x,y].dpv
  function do_project(x1,y1,x2,y2 : F26DOT6) : F26DOT6;
  function do_dproject(x1,y1,x2,y2 : F26DOT6) : F26DOT6;

  function divide_by_PV_dot_fV(d : F26DOT6) : F26DOT6; // d / (pv.fv)
  function do_cutin(d,ci,v : F26DOT6) : F26DOT6;
  function do_single_cutin(d : F26DOT6) : F26DOT6;
  function do_cvt_cutin(d : F26DOT6; c : longint) : F26DOT6;
  procedure interpolate_point(q1,z1,q2,z2,q3,z3 : longint);
  function is_touched_pv(i : longint) : boolean;
  function search_touched(cs,ce,i,di : longint) : longint;
  procedure interpol(p1,p2,p3 : longint); virtual;
  procedure do_IUP_contour(cs,ce : longint);



  function get_glyph_string(g : TTglyph; s1,s2 : ansistring) : ansistring;

  procedure setAcceptablePVFVmismatch(degree : double);


  protected
  saved_pvx, saved_pvy : F2DOT14;

  zone1glyph,mainglyph : TTglyph; // used to look up contour data
  cvt, progstorage : Ttdatastore;
  allow_mindist_autoflip : boolean; // see constructor
  min_acceptable_pv_dot_fv : F2dot14;



end;



implementation
uses sysutils,math;


function Tpointinstructor.nonzero_pv() : boolean;
begin
  nonzero_pv := not((gs.pvx=0)and(gs.pvy=0));
end;

function Tpointinstructor.nonzero_fv() : boolean;
begin
  nonzero_fv := not((gs.fvx=0)and(gs.fvy=0));
end;

function Tpointinstructor.nonzero_PV_dot_fV() : boolean;
var q : F2DOT14;
begin
  q := F2DOT14_MUL_F2DOT14_F2DOT14(gs.fvx,gs.pvx)
      +F2DOT14_MUL_F2DOT14_F2DOT14(gs.fvy,gs.pvy);
  nonzero_PV_dot_fV := (abs(q)>min_acceptable_pv_dot_fv); 
end;

function Tpointinstructor.nonzero_v() : boolean;
begin
  nonzero_v := nonzero_pv() and nonzero_fv() and nonzero_PV_dot_fV();
end;

procedure Tpointinstructor.setAcceptablePVFVmismatch(degree : double);
begin
  min_acceptable_pv_dot_fv := round(16384*abs(cos(degree*PI/180)));
end;


function Tpointinstructor.get_glyph_string(g : TTglyph; s1,s2 : ansistring) : ansistring;
begin
  if not(g=nil) then get_glyph_string := s1+inttostr(g.idx)+s2
                else get_glyph_string := '';
end;

procedure Tpointinstructor.mul_FV(d : F26DOT6; var x,y : F26DOT6);
begin
  x := MUL_F26DOT6_F2DOT14(d,gs.fvx);
  y := MUL_F26DOT6_F2DOT14(d,gs.fvy);
end;

function Tpointinstructor.pv_dot_fv() : F2DOT14; 
begin
  pv_dot_fv := F2DOT14_MUL_F2DOT14_F2DOT14(gs.fvx,gs.pvx)
              +F2DOT14_MUL_F2DOT14_F2DOT14(gs.fvy,gs.pvy);
end;


function Tpointinstructor.dot_PV(x,y : F26DOT6) : F26DOT6; // [x,y].pv
begin
  dot_pv := MUL_F26DOT6_F2DOT14(x,gs.pvx)
           +MUL_F26DOT6_F2DOT14(y,gs.pvy);
end;

function Tpointinstructor.dot_dPV(x,y : F26DOT6) : F26DOT6; // [x,y].dpv
begin
  dot_dpv := MUL_F26DOT6_F2DOT14(x,gs.dvx)
            +MUL_F26DOT6_F2DOT14(y,gs.dvy);
end;

function Tpointinstructor.do_project(x1,y1,x2,y2 : F26DOT6) : F26DOT6;
begin
  do_project := dot_PV(x1-x2,y1-y2);
end;

function Tpointinstructor.do_dproject(x1,y1,x2,y2 : F26DOT6) : F26DOT6;
begin
  do_dproject := dot_dPV(x1-x2,y1-y2);
end;

function Tpointinstructor.divide_by_PV_dot_fV(d : F26DOT6) : F26DOT6; // d / (pv.fv)
var q : F2DOT14; r : F26DOT6;
begin
  q := F2DOT14_MUL_F2DOT14_F2DOT14(gs.fvx,gs.pvx)
      +F2DOT14_MUL_F2DOT14_F2DOT14(gs.fvy,gs.pvy);
  r := DIVF26DOT6_F2DOT14(d,q);
  divide_by_PV_dot_fV := r;
end;


procedure Tpointinstructor.translate_point_x(k,zone : longint; a : F26DOT6);
begin
  set_zonepoint_x(k,zone,get_zonepoint_x(k,zone)+a);
end;

procedure Tpointinstructor.translate_point_y(k,zone : longint; a : F26DOT6);
begin
  set_zonepoint_y(k,zone,get_zonepoint_y(k,zone)+a);
end;

{ tanslate a point by a times FV }
procedure Tpointinstructor.mov_point_fv(k,zone : longint; a : F26DOT6);
var dx,dy : F26DOT6;
begin
  mul_FV(a,dx,dy);
  translate_point_x(k,zone,dx);
  translate_point_y(k,zone,dy);
end;

procedure Tpointinstructor.mov_point_fv_by_dist_X_pvdotfv(k,zone : longint; a : F26DOT6);
var dx,dy : F26DOT6;
begin
  a := MUL_F26DOT6_F2DOT14(a,pv_dot_fv());
  mul_FV(a,dx,dy);
  translate_point_x(k,zone,dx);
  translate_point_y(k,zone,dy);
end;

procedure Tpointinstructor.mov_point_fv_by_dist_div_pvdotfv(k,zone : longint; a : F26DOT6);
var dx,dy : F26DOT6;
begin
  a := divide_by_PV_dot_fV(a);
  mul_FV(a,dx,dy);
  translate_point_x(k,zone,dx);
  translate_point_y(k,zone,dy);
end;

procedure Tpointinstructor.mov_org_point_fv_by_dist_div_pvdotfv(k,zone : longint; a : F26DOT6);
var dx,dy : F26DOT6;
begin
  a := divide_by_PV_dot_fV(a);
  mul_FV(a,dx,dy);
  translate_org_point_x(k,zone,dx);
  translate_org_point_y(k,zone,dy);
end;

procedure Tpointinstructor.translate_org_point_x(k,zone : longint; a : F26DOT6);
begin
  set_org_zonepoint_x(k,zone,get_org_zonepoint_x(k,zone)+a);
end;

procedure Tpointinstructor.translate_org_point_y(k,zone : longint; a : F26DOT6);
begin
  set_org_zonepoint_y(k,zone,get_org_zonepoint_y(k,zone)+a);
end;

{ tanslate a point by a times FV }
procedure Tpointinstructor.mov_org_point_fv(k,zone : longint; a : F26DOT6);
var dx,dy : F26DOT6;
begin
  mul_FV(a,dx,dy);
  translate_org_point_x(k,zone,dx);
  translate_org_point_y(k,zone,dy);
end;


procedure Tpointinstructor.mov_cvt(k : longint; a : F26DOT6);
var x : F26DOT6;
begin
  x := cvt.getValue(k);
  x := x + a;
  cvt.setValue(k,x);
end;





function Tpointinstructor.do_cutin(d,ci,v : F26DOT6) : F26DOT6;
begin
  if (gs.auto_flip) then begin
    v := abs(v);
    if (abs(abs(d)-v)<ci) then begin
      if (d<0) then do_cutin := -abs(v)
               else do_cutin :=  abs(v);
    end else do_cutin := d;
  end else begin
    if (abs(d-v)<ci) then begin
      do_cutin := v;
    end else do_cutin := d;
  end;
end;

function Tpointinstructor.do_minimum_distance(d : F26DOT6) : F26DOT6;
begin
  if (gs.auto_flip and allow_mindist_autoflip) then begin
    if (abs(d)<abs(gs.minimum_distance)) then begin
      if (d<0) then d := -abs(gs.minimum_distance)
               else d :=  abs(gs.minimum_distance);
    end;
  end else begin
    if (d<gs.minimum_distance) then d := gs.minimum_distance;
  end;
  do_minimum_distance := d;
end;

function Tpointinstructor.do_single_cutin(d : F26DOT6) : F26DOT6;
var cv,ci : F26DOT6;
begin
  ci := gs.single_width_cut_in;
  cv := gs.single_width_value;
  do_single_cutin := do_cutin(d,ci,cv);
end;

function Tpointinstructor.do_cvt_cutin(d : F26DOT6; c : longint) : F26DOT6;
var cv,ci : F26DOT6;
begin
  ci := gs.control_value_cut_in;
  cv := cvt.getValue(c);
  do_cvt_cutin := do_cutin(d,ci,cv);
end;

function keep_sign(d,d_ref : F26DOT6) : F26DOT6;
begin
  if (d_ref>=0) then keep_sign :=  abs(d)
                else keep_sign := -abs(d);
end;





{ GC(pointindex)

  Get the (projected) Coordinate of a point. 

}


procedure Tpointinstructor.do_GC(b : byte);
var i : longint; dx,dy : F26DOT6;
begin
  i := pop_stack();
  if (b=$46) then begin
    dx := get_zonepoint_x(i,gs.zp2);
    dy := get_zonepoint_y(i,gs.zp2);
    push_stack( dot_PV(dx,dy) );
  end else begin
    dx := get_org_zonepoint_x(i,gs.zp2);
    dy := get_org_zonepoint_y(i,gs.zp2);
    push_stack( dot_dPV(dx,dy) );
  end;
end;




procedure Tpointinstructor.do_MD(b : byte);
var i,j : longint; dx,dy : F26DOT6;
begin
  i := pop_stack();
  j := pop_stack();
  if (b=$49) then begin
    dx := get_zonepoint_x(i,gs.zp1);
    dy := get_zonepoint_y(i,gs.zp1);
    dx -= get_zonepoint_x(j,gs.zp0);
    dy -= get_zonepoint_y(j,gs.zp0);
    push_stack(-dot_PV(dx,dy));
  end else begin
    dx := get_org_zonepoint_x(i,gs.zp1);
    dy := get_org_zonepoint_y(i,gs.zp1);
    dx -= get_org_zonepoint_x(j,gs.zp0);
    dy -= get_org_zonepoint_y(j,gs.zp0);
    push_stack(-dot_dPV(dx,dy));
  end;
end;


{ 
}

procedure Tpointinstructor.do_MIAP(b : byte);
var i,c : longint; d,d0,delta,dx,dy : F26DOT6;
begin
  c := pop_stack();
  i := pop_stack();

  d := cvt.getValue(c);

  if (gs.zp0=0) then begin
    mul_FV(d,dx,dy);
    set_org_zonepoint_x(i,0,dx);
    set_org_zonepoint_y(i,0,dy);
    set_zonepoint_x(i,0,dx);
    set_zonepoint_y(i,0,dy);   
  end;
 
  dx := get_zonepoint_x(i,gs.zp0);
  dy := get_zonepoint_y(i,gs.zp0);
  d0 := dot_PV(dx,dy);
  

  if (b and 1)>0 then begin
    delta := abs(d-d0);
    if (delta>gs.control_value_cut_in) then d := d0;
    d := dorounding(apply_engine_compensation(d,0));

  end;

  mov_point_fv_by_dist_div_pvdotfv(i,gs.zp0,d-d0);
  gs.rp0 := i;
  gs.rp1 := i;  
end;


{ 
}

procedure Tpointinstructor.do_MDRP_instr(b : byte);
var i : longint; d,dold,dx,dy : F26DOT6; 
begin 
  i := pop_stack();
  
{  
  write('refpoint rp0(=',gs.rp0,'):');
  write(get_org_zonepoint_x(gs.rp0,gs.zp0),'/', get_org_zonepoint_y(gs.rp0,gs.zp0));
  write(' => ');
  writeln(get_zonepoint_x(gs.rp0,gs.zp0),'/', get_zonepoint_y(gs.rp0,gs.zp0));

  write('point from stack(=',i,'):');
  write(get_org_zonepoint_x( i,gs.zp1),'/', get_org_zonepoint_y( i,gs.zp1));
  write(' => ');
  writeln(get_zonepoint_x( i,gs.zp1),'/', get_zonepoint_y( i,gs.zp1));
}  
  
  dx := get_org_zonepoint_x( i    ,gs.zp1);
  dy := get_org_zonepoint_y( i    ,gs.zp1);
  dx -= get_org_zonepoint_x(gs.rp0,gs.zp0);
  dy -= get_org_zonepoint_y(gs.rp0,gs.zp0);
  dold := dot_dPV(dx,dy); 

//writeln('dold_init=',dold,' (dx:',dx,' dy:',dy,') dvx:',gs.dvx,' dvy:',gs.dvy,' ',dot_dPV(dx,dy),' ',dot_PV(dx,dy));

  if  (gs.single_width_cut_in>0)
  and (dold < (gs.single_width_value+gs.single_width_cut_in))
  and (dold > (gs.single_width_value-gs.single_width_cut_in))
  then begin
    if (dold<0) then dold := -gs.single_width_value
                else dold :=  gs.single_width_value;
  end;

//writeln('dold_after_sw=',dold);

  d := apply_engine_compensation(dold,b and 3);
  if ((b and 4)>0) then d := dorounding(d);
  if ((b and 8)>0) then begin
    if (dold>=0) then begin
      if (d<gs.minimum_distance) then d := gs.minimum_distance;;
    end else begin
      if (d>-gs.minimum_distance) then d := -gs.minimum_distance;;
    end;
  end;

//writeln('rounded:',d);

  dx := get_zonepoint_x( i    ,gs.zp1);
  dy := get_zonepoint_y( i    ,gs.zp1);
  dx -= get_zonepoint_x(gs.rp0,gs.zp0);
  dy -= get_zonepoint_y(gs.rp0,gs.zp0);
  dold := dot_PV(dx,dy);
//writeln('curdist:',dold);
//writeln('moveby:',d-dold);

  mov_point_fv_by_dist_div_pvdotfv(i,gs.zp1,d-dold);

  gs.rp1 := gs.rp0;
  gs.rp2 := i;
  if ((b and 16)>0) then begin
    gs.rp0 := i;
  end;
end;




{ 
}

procedure Tpointinstructor.do_MSIRP(b : byte);
var i : longint; v,d0,dx,dy : F26DOT6;
begin
  v := pop_stack();
  i := pop_stack();
  if (gs.zp1=0) then begin
    set_org_zonepoint_x(i,gs.zp1,get_org_zonepoint_x(gs.rp0,gs.zp0));
    set_org_zonepoint_y(i,gs.zp1,get_org_zonepoint_y(gs.rp0,gs.zp0));
    mov_org_point_fv_by_dist_div_pvdotfv(i,gs.zp1,v);
    set_zonepoint_x(i,gs.zp1,get_org_zonepoint_x(i,gs.zp1));
    set_zonepoint_y(i,gs.zp1,get_org_zonepoint_y(i,gs.zp1));   
  end;
  dx := get_zonepoint_x(i,gs.zp1);
  dy := get_zonepoint_y(i,gs.zp1);
  dx -= get_zonepoint_x(gs.rp0,gs.zp0);
  dy -= get_zonepoint_y(gs.rp0,gs.zp0);
  d0 := dot_PV(dx,dy);
  //mov_point_fv(i,gs.zp1,v-d0);
  mov_point_fv_by_dist_div_pvdotfv(i,gs.zp1,v-d0);
  gs.rp1 := gs.rp0;
  gs.rp2 := i;
  if ((b and 1)>0) then begin
    gs.rp0 := i;
  end;
end;




{ 
}


procedure Tpointinstructor.do_MIRP_instr(b : byte);
var delta,cvtval,org_dist,cur_dist,dist : F26Dot6; cvtidx,ptidx : longint;
begin
  cvtidx := pop_stack();
  ptidx := pop_stack();
  if (cvtidx<0) then cvtval := 0 else cvtval := cvt.getValue(cvtidx);
  delta := abs(cvtval-gs.single_width_value);

//  writeln('cvtval=',cvtval);

  if (delta<gs.single_width_cut_in) then begin
    if (cvtval>=0) then begin
      cvtval := gs.single_width_value;
    end else begin
      cvtval := -gs.single_width_value;
    end;
  end;

//  writeln('cvtval=',cvtval);



  if (gs.zp1=0) then begin
    set_org_zonepoint_x(ptidx,gs.zp1,get_org_zonepoint_x(gs.rp0,gs.zp0));
    set_org_zonepoint_y(ptidx,gs.zp1,get_org_zonepoint_y(gs.rp0,gs.zp0));
    mov_org_point_fv(ptidx,gs.zp1,cvtval);
    set_zonepoint_x(ptidx,gs.zp1,get_org_zonepoint_x(ptidx,gs.zp1));
    set_zonepoint_y(ptidx,gs.zp1,get_org_zonepoint_y(ptidx,gs.zp1));  
  end;
{
  write('refpoint rp0(=',gs.rp0,'):');
  write(get_org_zonepoint_x(gs.rp0,gs.zp0),'/', get_org_zonepoint_y(gs.rp0,gs.zp0));
  write(' => ');
  writeln(get_zonepoint_x(gs.rp0,gs.zp0),'/', get_zonepoint_y(gs.rp0,gs.zp0));

  write('point from stack(=',ptidx,'):');
  write(get_org_zonepoint_x( ptidx,gs.zp1),'/', get_org_zonepoint_y( ptidx,gs.zp1));
  write(' => ');
  writeln(get_zonepoint_x( ptidx,gs.zp1),'/', get_zonepoint_y( ptidx,gs.zp1));
}  
  org_dist := do_dproject( get_org_zonepoint_x( ptidx,gs.zp1), get_org_zonepoint_y( ptidx,gs.zp1),
                           get_org_zonepoint_x(gs.rp0,gs.zp0), get_org_zonepoint_y(gs.rp0,gs.zp0)
                         );
			 
  cur_dist := do_project(  get_zonepoint_x( ptidx,gs.zp1), get_zonepoint_y( ptidx,gs.zp1),
                           get_zonepoint_x(gs.rp0,gs.zp0), get_zonepoint_y(gs.rp0,gs.zp0)
                         );

//write('final_cvt: ',cvtval);
  if gs.auto_flip then begin
    if ((org_dist xor cvtval)<0) then cvtval := -cvtval;
  end;
//writeln(' ',cvtval);

  if (b and 4)>0 then begin
    dist := cvtval;
    if (gs.zp0=gs.zp1) then begin
      delta := abs(cvtval-org_dist);
      if (delta>gs.control_value_cut_in) then dist := org_dist;
    end;
    dist := apply_engine_compensation(dist,b and 3);
    dist := dorounding(dist);
  end else begin
    dist := apply_engine_compensation(cvtval,b and 3);
  end;

//writeln('dist:',dist);

  if (b and 8)>0 then begin
    if (org_dist>=0) then begin
      if (dist<gs.minimum_distance) then dist := gs.minimum_distance;
    end else begin
      if (dist>-gs.minimum_distance) then dist := -gs.minimum_distance;
    end;
  end;
//writeln('after mindist: ',dist);
//writeln('curdist:',cur_dist);
//writeln('final: ',dist-cur_dist);
  mov_point_fv_by_dist_div_pvdotfv(ptidx,gs.zp1,dist-cur_dist);
  gs.rp1 := gs.rp0;
  gs.rp2 := ptidx;
  if ((b and 16)>0) then begin
    gs.rp0 := ptidx;
  end;  
end;






{ MDAP[]: Touch a point (on nonzero axes of freedom vector). 
}

procedure Tpointinstructor.do_MDAP(b : byte);
var i : longint; d,dx,dy : F26DOT6;
begin
  i := pop_stack();
  dx := get_zonepoint_x(i,gs.zp0);
  dy := get_zonepoint_y(i,gs.zp0);
  d := 0;
  if ((b and 1)>0) then begin
    d := dot_PV(dx,dy);
    d := dorounding(d)-d;
  end;
  mov_point_fv_by_dist_div_pvdotfv(i,gs.zp0,d);
  gs.rp0 := i;
  gs.rp1 := i;
end;



procedure Tpointinstructor.do_ALIGNRP(b : byte);
var i : longint; d,dx,dy : F26DOT6;
begin
  while (gs.loopcount>0) do begin
    i := pop_stack();
    dx := get_zonepoint_x(gs.rp0,gs.zp0);
    dy := get_zonepoint_y(gs.rp0,gs.zp0);
    dx -= get_zonepoint_x(i,gs.zp1);
    dy -= get_zonepoint_y(i,gs.zp1);
    d := dot_PV(dx,dy); 
    mov_point_fv_by_dist_div_pvdotfv(i,gs.zp1,d);
    dec(gs.loopcount);
  end;
  gs.loopcount := 1;
end;




procedure Tpointinstructor.do_ALPIGNPTS(b : byte);
var i,j : longint; d,dx,dy : F26DOT6;
begin
  i := pop_stack();
  j := pop_stack();
  if nonzero_v() then begin
    dx := get_zonepoint_x(j,gs.zp0) -get_zonepoint_x(i,gs.zp1);
    dy := get_zonepoint_y(j,gs.zp0) -get_zonepoint_y(i,gs.zp1);
    d := dot_PV(dx,dy);
    mov_point_fv_by_dist_div_pvdotfv(i,gs.zp1, d div 2);
    mov_point_fv_by_dist_div_pvdotfv(j,gs.zp0,-d div 2);
  end;
end;


procedure Tpointinstructor.interpolate_point(q1,z1,q2,z2,q3,z3 : longint);
var p1,p2,p3,p4,p5,p6,d : F26DOT6;
begin
  p1 := dot_PV( get_org_zonepoint_x(q1,z1),
                get_org_zonepoint_y(q1,z1) );
  p2 := dot_PV( get_org_zonepoint_x(q2,z2),
                get_org_zonepoint_y(q2,z2) );
  p3 := dot_PV( get_org_zonepoint_x(q3,z3),
                get_org_zonepoint_y(q3,z3) );
  p4 := dot_PV( get_zonepoint_x(q1,z1),
                get_zonepoint_y(q1,z1) );
  p5 := dot_PV( get_zonepoint_x(q2,z2),
                get_zonepoint_y(q2,z2) );
  p6 := dot_PV( get_zonepoint_x(q3,z3),
                get_zonepoint_y(q3,z3) );
// (p5-p4)*(p3-p1)/(p2-p1) = p6_soll -p4
  d := MULF26DOT6((p5-p4),(p3-p1));
  d := DIVF26DOT6(d,(p2-p1))+p4;
  d := d-p6;
  //d := divide_by_PV_dot_FV(d);
  mov_point_fv_by_dist_div_pvdotfv(q3,z3,d);
end;
{
                 
                p3
     p1 |<------x-------------->| p2
        |       |               |
        |<----->|<------------->|
        |  d13        d32       |
        |<--------------------->|
                 D12

                     
                        P6
            P4 |<--------x---------------------->| P5
               |         |                       |
               |<------->|<--------------------->|
               |   D46              D65          |
               |<------------------------------->|
                             D45


}

procedure Tpointinstructor.do_IP(b : byte);
var i : longint;
begin
  switch_to_dv;
  while (gs.loopcount>0) do begin
    i := pop_stack();
    if nonzero_v() then begin
      interpolate_point( gs.rp1, gs.zp0,
                         gs.rp2, gs.zp1,
                          i    , gs.zp2 );
    end;
    dec(gs.loopcount);
  end;
  gs.loopcount := 1;
  switch_to_pv;
end;



function Tpointinstructor.is_touched_pv(i : longint) : boolean;
begin
  if (gs.pvx=0) then begin
    is_touched_pv := is_touched_z1y(i);
  end else begin
    is_touched_pv := is_touched_z1x(i);
  end;
end;

function Tpointinstructor.search_touched(cs,ce,i,di : longint) : longint;
var j : longint;
begin
  j := i+di;
  repeat
    if (j>ce) then j := cs;
    if (j<cs) then j := ce;
    if is_touched_pv(j) then i := j
    else j := j+di;
  until (j=i);
  search_touched := i;
end;


procedure Tpointinstructor.interpol(p1,p2,p3 : longint);
var q1,q2,q3,dx,dy,d : F26DOT6; tx,ty : boolean;
begin
  tx := is_touched_z1x(p3);
  ty := is_touched_z1y(p3);
  q1 := dot_PV( get_org_zonepoint_x(p1,1),
                get_org_zonepoint_y(p1,1) );
  q2 := dot_PV( get_org_zonepoint_x(p2,1),
                get_org_zonepoint_y(p2,1) );
  q3 := dot_PV( get_org_zonepoint_x(p3,1),
                get_org_zonepoint_y(p3,1) );
  if ((q1<q3)and(q3<q2))
  or ((q1>q3)and(q3>q2)) then begin
    interpolate_point(p1,1,p2,1,p3,1);
  end else begin
    if (abs(q3-q2)<abs(q3-q1)) then p1 := p2;
    dx := get_zonepoint_x(p1,1);
    dy := get_zonepoint_y(p1,1);
    dx -= get_org_zonepoint_x(p1,1);
    dy -= get_org_zonepoint_y(p1,1);
    d := dot_PV(dx,dy);
    //d div pv.fv = d div 1
    mov_point_fv_by_dist_div_pvdotfv(p3,1,d);
  end;
  set_touched_z1x(p3,tx);
  set_touched_z1x(p3,ty);
end;

procedure Tpointinstructor.do_IUP_contour(cs,ce : longint);
var i,lp,rp : longint;
begin
  lp := 0;
  for i := cs to ce do begin
    if is_touched_pv(i) then inc(lp);
  end;
  if (lp<2) then exit;
  for i := cs to ce do begin
    if not(is_touched_pv(i)) then begin
      lp := search_touched(cs,ce,i,1);
      rp := search_touched(cs,ce,i,-1);
      interpol(lp,rp,i);
    end;
  end;
end;

procedure Tpointinstructor.do_IUP(b : byte);
var c,i,lastp,firstp : longint; pvx,fvx,pvy,fvy : F2DOT14;
begin
  if (zone1glyph=nil) 
  then raise Tttprogexception.create('IUP-instruction depends on glyph data');
  pvx := gs.pvx;
  pvy := gs.pvy;
  fvx := gs.fvx;
  fvy := gs.fvy;
  if (b=$31) then begin
    gs.pvx := $4000;
    gs.pvy := 0;
  end else begin
    gs.pvx := 0;
    gs.pvy := $4000;
  end;
  gs.fvx := gs.pvx;
  gs.fvy := gs.pvy;
  i := 0;
  c := 0;
  while (c<zone1glyph.numberOfContours) do begin
    firstp := i;
    lastp  := zone1glyph.endPtsOfContours[c];
    if nonzero_v() then begin
      do_IUP_contour(firstp,lastp);
    end;
    i := lastp+1;
    inc(c);
  end;
  gs.pvx := pvx;
  gs.pvy := pvy;
  gs.fvx := fvx;
  gs.fvy := fvy;
end;




procedure Tpointinstructor.do_readwrite_store(b : byte);
var v,i : dword;
begin
  if (b=$42) then begin
    v := pop_stack();
    i := pop_stack();
    //writeln('storage[',hexstr(i,4),'] <= ',hexstr(v,8),' ',v);
    progstorage.setValue(i,v);
  end else begin
    i := pop_stack();
    v := progstorage.getValue(i);
    push_stack(v);
  end;
end;


procedure Tpointinstructor.do_UTP(b : byte);
var i : longint;
begin
  i := pop_stack();
  if (gs.zp0=1) then begin
    if not(gs.pvx=0) then set_touched_z1x(i,false);
    if not(gs.pvy=0) then set_touched_z1y(i,false);
  end;
end;

procedure Tpointinstructor.do_SHZ(b : byte);
var z,rp,rz,i,j : longint; d,dx,dy : F26DOT6;
begin
  z := pop_stack();
  if ((b and 1)=0) then begin
    rp := gs.rp2;
    rz := gs.zp1;
  end else begin
    rp := gs.rp1;
    rz := gs.zp0;
  end;
  dx := get_zonepoint_x(rp,rz) -get_org_zonepoint_x(rp,rz);
  dy := get_zonepoint_y(rp,rz) -get_org_zonepoint_y(rp,rz);
  d := dot_PV(dx,dy);
  //d := divide_by_PV_dot_FV(d);
  mul_FV(d,dx,dy);
  j := get_last_zone_point_idx(z);
  for i := 0 to j do begin
    translate_point_x(i,z,dx);
    translate_point_y(i,z,dy);
  end;
end;

procedure Tpointinstructor.do_SHPIX(b : byte);
var i : longint; d : F26DOT6;
begin
  d := pop_stack();
  while (gs.loopcount>0) do begin
    i := pop_stack();
    mov_point_fv(i,gs.zp2,d);
    dec(gs.loopcount);
  end;
  gs.loopcount := 1;
end;

procedure Tpointinstructor.do_SHP(b : byte);
var rp,rz,i : longint; d,dx,dy : F26DOT6;
begin
  if ((b and 1)=0) then begin
    rp := gs.rp2;
    rz := gs.zp1;
  end else begin
    rp := gs.rp1;
    rz := gs.zp0;
  end;
  dx := get_zonepoint_x(rp,rz) -get_org_zonepoint_x(rp,rz);
  dy := get_zonepoint_y(rp,rz) -get_org_zonepoint_y(rp,rz);
  d := dot_PV(dx,dy);
  //d := divide_by_PV_dot_FV(d);
  while (gs.loopcount>0) do begin
    i := pop_stack();
    mov_point_fv_by_dist_div_pvdotfv(i,gs.zp2,d);
    dec(gs.loopcount);
  end;
  gs.loopcount := 1;
end;

procedure Tpointinstructor.switch_to_dv;
begin
  saved_pvx := gs.pvx; 
  saved_pvy := gs.pvy;
  gs.pvx := gs.dvx;
  gs.pvy := gs.dvy; 
end;

procedure Tpointinstructor.switch_to_pv;
begin
  gs.pvx := saved_pvx;
  gs.pvy := saved_pvy;
end;


procedure Tpointinstructor.do_SHC(b : byte);
var c,a,o,i,rp,rz : longint; d,dx,dy : F26DOT6;
begin
  c := pop_stack();
  if (zone1glyph=nil) then begin
    raise Tttprogexception.create('SHC-instruction depends on glyph data'); 
  end;
  if (c>=zone1glyph.numberOfContours)or(c<0) then begin
    raise Tttprogexception.create('SHC-instruction called with invalid contour number');
  end;
  if ((b and 1)=0) then begin
    rp := gs.rp2; rz := gs.zp1;
  end else begin
    rp := gs.rp1; rz := gs.zp0;
  end;
  dx := get_zonepoint_x(rp,rz) -get_org_zonepoint_x(rp,rz);
  dy := get_zonepoint_y(rp,rz) -get_org_zonepoint_y(rp,rz);
  d := dot_PV(dx,dy);
  //d := divide_by_PV_dot_FV(d);
  a := 0;
  o := zone1glyph.endPtsOfContours[c];
  if (c>0) then a := zone1glyph.endPtsOfContours[c-1]+1;
  for i := a to o do begin
    if not(i=rp) then mov_point_fv_by_dist_div_pvdotfv(i,gs.zp2,d);
  end;
end;


procedure Tpointinstructor.do_RCVT(b : byte);
var i,v : dword;
begin
  i := pop_stack();
  v := cvt.getValue(i);
  push_stack(v);
end;


procedure Tpointinstructor.do_SCFS(b : byte);
var i,v : dword;  d,dx,dy : F26DOT6;
begin
  v := pop_stack();
  i := pop_stack();
  dx := get_zonepoint_x(i,gs.zp2);
  dy := get_zonepoint_y(i,gs.zp2);
  d := dot_PV(dx,dy);
  //d := divide_by_PV_dot_FV(v-d);
  mov_point_fv_by_dist_div_pvdotfv(i,gs.zp2,v-d);
end;

procedure Tpointinstructor.do_SDPVTL(b : byte);
var p1,p2 : longint; a,b_,c : longint;
begin
  p1 := pop_stack();
  p2 := pop_stack();
  
  a := get_org_zonepoint_x(p2,gs.zp1) -get_org_zonepoint_x(p1,gs.zp2);
  b_ := get_org_zonepoint_y(p2,gs.zp1) -get_org_zonepoint_y(p1,gs.zp2);
  if (a=0)and(b=0) then begin
    a := 16384;   
    b_ := 0;
    b := 0;
  end;
  if ((b and 1)>0) then begin
    c := b_;
    b_ := a;
    a := -c;
  end;
  F2DOT14_normalize_F26DOT6(a,b_);
  gs.dvx := a;
  gs.dvy := b_; 
    
  a := get_zonepoint_x(p2,gs.zp1) -get_zonepoint_x(p1,gs.zp2);
  b_ := get_zonepoint_y(p2,gs.zp1) -get_zonepoint_y(p1,gs.zp2);
  if (a=0)and(b=0) then begin
    a := 16384;   
    b_ := 0;
    b := 0;
  end;
  if ((b and 1)>0) then begin
    c := b_;
    b_ := a;
    a := -c;
  end;
  F2DOT14_normalize_F26DOT6(a,b_);
  gs.pvx := a;
  gs.pvy := b_;  

end;



procedure Tpointinstructor.do_ISECT(b : byte);
begin
  raise Tttprogexception.create('ISECT currently no implemented'); 
  pop_stack();
  pop_stack();
  pop_stack();
  pop_stack();
  pop_stack();
end;




constructor Tpointinstructor.create();
begin
  inherited create();
  allow_mindist_autoflip := true;
end;

destructor Tpointinstructor.destroy();
begin
  inherited destroy();
end;



begin
end.
