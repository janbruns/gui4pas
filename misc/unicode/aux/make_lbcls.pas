{$mode objfpc}
{$unitpath ..}
uses unicode_UTF8, unicode_tabinit,
unicode_lbcls; // for consts lbcls_*


var bidicls : array of byte;
var sinctxt : ansistring;
var sincfirst : boolean;
var sinclines : longint;

const



missingvals : array[0..1*3-1] of dword = (
lbcls_XX, $0000,$10FFFF
);


procedure apply_block(t,a,b : longint);
begin
  if b<=high(bidicls) then begin
    b := 1+b-a;
    fillbyte(bidicls[a],b,t);
  end else begin
    writeln('some range out of bounds');
    halt();
  end;
end;

procedure do_init();
var i,b : longint;
begin
  i := 0; b := 0;
  while (i<high(missingvals)) do begin
    if missingvals[i+2]>b then b := missingvals[i+2];
    i += 3;
  end;
  setlength(bidicls,b+1);
  i := 0;
  while (i<high(missingvals)) do begin
    apply_block(missingvals[i],missingvals[i+1],missingvals[i+2]);
    i += 3;
  end;
end;

function test_block_required(t,a,b : longint) : boolean;
var i : longint; r : boolean;
begin
  r := false;
  for i := a to b do begin
    if bidicls[i]<>t then r := true;
  end;
  test_block_required := r;
end;



function test_and_apply_block(t,a,b : longint) : boolean;
begin
  test_and_apply_block := false;
  if test_block_required(t,a,b) then begin
    apply_block(t,a,b);
    if not sincfirst then sinctxt += ',';
    sinctxt += #10;
    sinctxt += '  $'+hexstr(t,2)+',';
    sinctxt += '$'+hexstr(a,6)+',';
    sinctxt += '$'+hexstr(b,6);
    sincfirst := false;
    sinclines += 1;
  end;
end;

procedure do_block(s,a,b : ansistring);
var t : byte; w1,w2 : word; av,bv : longint;
begin
  if s='XX'      then t := lbcls_XX
  else if s='BK' then t := lbcls_BK
  else if s='CM' then t := lbcls_CM
  else if s='CR' then t := lbcls_CR
  else if s='GL' then t := lbcls_GL
  else if s='LF' then t := lbcls_LF
  else if s='NL' then t := lbcls_NL
  else if s='SP' then t := lbcls_SP
  else if s='WJ' then t := lbcls_WJ
  else if s='ZW' then t := lbcls_ZW
  else if s='ZWJ' then t := lbcls_ZWJ
  else if s='AI' then t := lbcls_AI
  else if s='AL' then t := lbcls_AL
  else if s='B2' then t := lbcls_B2
  else if s='BA' then t := lbcls_BA
  else if s='BB' then t := lbcls_BB
  else if s='CB' then t := lbcls_CB
  else if s='CJ' then t := lbcls_CJ
  else if s='CL' then t := lbcls_CL
  else if s='CP' then t := lbcls_CP
  else if s='EB' then t := lbcls_EB
  else if s='EM' then t := lbcls_EM
  else if s='EX' then t := lbcls_EX
  else if s='H2' then t := lbcls_H2
  else if s='H3' then t := lbcls_H3
  else if s='HL' then t := lbcls_HL
  else if s='HY' then t := lbcls_HY
  else if s='ID' then t := lbcls_ID
  else if s='IN' then t := lbcls_IN
  else if s='IS' then t := lbcls_IS
  else if s='JL' then t := lbcls_JL
  else if s='JT' then t := lbcls_JT
  else if s='JV' then t := lbcls_JV
  else if s='NS' then t := lbcls_NS
  else if s='NU' then t := lbcls_NU
  else if s='OP' then t := lbcls_OP
  else if s='PO' then t := lbcls_PO
  else if s='PR' then t := lbcls_PR
  else if s='QU' then t := lbcls_QU
  else if s='RI' then t := lbcls_RI
  else if s='SA' then t := lbcls_SA
  else if s='SG' then t := lbcls_SG
  else if s='SY' then t := lbcls_SY
  else begin
    writeln('UNKNOWN LB-type: ',s);
    halt();
  end;
  val(a,av,w1);
  val(b,bv,w2);
  if (w1=0)and(w2=0) then begin
    test_and_apply_block(t,av,bv);
  end else begin
    writeln('prob with value');
    halt();
  end;
end;



function todot(var s : ansistring; const dot : shortstring) : ansistring;
var i : longint;
begin
  i := pos(dot,s);
  if (i>0) then begin
    result := copy(s,1,i-1);
    s := copy(s,i+length(dot));
  end else begin
    result := s;
    s := '';
  end;
end;

function fromdoton(const s : ansistring; const dot : shortstring) : ansistring;
var i : longint;
begin
  i := pos(dot,s);
  if (i>0) then begin
    result := copy(s,i+length(dot));
  end else begin
    result := '';
  end;
end;

procedure removechar(var s : ansistring; c : char);
var i : longint;
begin
  i := 1;
  while (i<=length(s)) do begin
    if (s[i]=c) then delete(s,i,1) else i += 1;
  end;
end;


function encode(t,l : dword) : ansistring;
var s : ansistring; tc0,lc0 : longint;
begin
  s := '';
  if (t>=15) then tc0 := 15 else tc0 := t;
  if (l>=7)  then lc0 := 7  else lc0 := l;
  s += UTF8_codepoint_as_UTF8(tc0+lc0*16);
  if (t>=15) then s += UTF8_codepoint_as_UTF8(t);
  if (l>=7)  then s += UTF8_codepoint_as_UTF8(l);
  encode := s;
end;





var
symbfreq : array[0..255] of longint;
arr2 : Tbarr;

procedure analyse_block();
var cnt,i,j : longint; cur : byte; s : ansistring;
arr : array of dword;
begin
  for i := 0 to 255 do symbfreq[i] := 0;
  j := 0;
  cur := bidicls[0];
  cnt := 0;
  for i := 1 to high(bidicls) do begin
    if (bidicls[i]<>cur) then begin
      cur := bidicls[i];
      cnt := 0;
      j += 1;
      inc(symbfreq[cur and 255]);
    end else cnt += 1;
  end;
  j +=1;
  inc(symbfreq[cur and 255]);

  setlength(arr,j*3);
  j := 0;
  cur := bidicls[0];
  arr[3*j+1] := 0;
  arr[3*j+0] := cur;
  cnt := 0;
  for i := 1 to high(bidicls) do begin
    if (bidicls[i]<>cur) then begin
      cur := bidicls[i];
      cnt := 0;
      arr[3*j+2] := i-1;
      j += 1;
      arr[3*j+1] := i;
      arr[3*j+0] := cur;
    end else cnt += 1;
  end;
  arr[3*j+2] := high(bidicls);
  s := '';
  i := 0;
  while i <high(arr) do begin
    s += encode(arr[i],arr[i+2]-arr[i+1]);
    i += 3;
  end;
  for i := 0 to 255 do begin
//    writeln(i,':',symbfreq[i]);
  end;
//  writeln('len=',length(s));
  setlength(arr2,0);
  unicode_initvaldecode(s,arr2);
  for i := 0 to high(bidicls) do begin
    if bidicls[i]<>arr2[i] then writeln('ERROR',i);
  end;
  writeln('sinc : array[0..',length(s)-1,'] of byte = (');
  write('  ');
  for i := 1 to length(s)-1 do begin
    write('$',hexstr(ord(s[i]),2),', ');
    if (((i-1) and 15)=15) then begin
      writeln();
      write('  ');
    end;
  end;
  writeln('$',hexstr(ord(s[i]),2),');');
end;


var
f : text;
s,s2,s3 : ansistring;

begin
  do_init();
  sincfirst := true;
  sinctxt := '';
  
  assign(f,'LineBreak.txt');
  reset(f);
  repeat 
    readln(f,s);
    s := todot(s,'#');
    if s<>'' then begin
      removechar(s,' ');
      s2 := todot(s,';');
      s3 := todot(s2,'..');
      if s2='' then s2 := s3;
      do_block(s,'$'+s3,'$'+s2);
    end;
  until eof(f);
  close(f);
//  write('dinc : array[0..',sinclines*3-1,'] of dword = (');
//  writeln(sinctxt);
//  writeln(');');
  analyse_block();
  writeln;
end.
