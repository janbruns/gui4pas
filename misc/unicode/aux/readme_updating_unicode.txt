Updating unicode version:

Copy the following files from www.unicode.org to ./aux subdir:

DerivedBidiClass.txt
BidiBrackets.txt
LineBreak.txt
UnicodeData.txt
Scripts.txt

In the aux dir. Go to cmd line there and do (roundabout):

fpc -B make_gencat
fpc -B make_bidicls
fpc -B make_lbcls
fpc -B make_bracketincs
fpc -B make_scr

./make_bidicls >bidicls.inc
./make_lbcls >linebreak.inc

./make_bracketincs o >openbracks.txt
./make_bracketincs c >closebracks.txt

use a text edit to manually put these values into unicode_bidir_brackets.pas

The tool just extracts the values in the order found. It is however important 
that the matching pairs-parts get the same index in the array.

finally (after that), try

cd ..
fpc -B unicode_bidir_brackets
cd aux
fpc -B make_brackinc
./make_brackinc >brack_canonical.txt

To see wether UnicodeData.txt has fresh canonical eq bracket mappings to also be added to

function canonical_bracketeq(v : dword) : dword;

in unicode_bidir_brackets.pas



Updating the script-list will most likely require manual add of the new scripts to 

../unicode_scripts.pas

To find out about which scripts are new, add some writeln() in make_scr.pas





