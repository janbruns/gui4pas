{$mode objfpc}
{$unitpath .}
unit unicode_tabinit;

interface

type Tbarr = array of byte;

procedure unicode_initvaldecode(const s : ansistring; var b : Tbarr);


implementation
uses unicode_UTF8;

procedure unicode_initvaldecode(const s : ansistring; var b : Tbarr);
var i,t,v,m : dword; l : longint;
begin
  m := 0;
  i := 1;
  while (i<=length(s)) do begin
    v := UTF8_getcodepoint_autoinc(s,i);
    t := v and 15;
    l := v shr 4;
    if t=15 then t := UTF8_getcodepoint_autoinc(s,i);
    if l=7  then l := UTF8_getcodepoint_autoinc(s,i);
    m += 1+l;
  end;
  setlength(b,m);
  m := 0;
  i := 1;
  while (i<=length(s)) do begin
    v := UTF8_getcodepoint_autoinc(s,i);
    t := v and 15;
    l := v shr 4;
    if t=15 then t := UTF8_getcodepoint_autoinc(s,i);
    if l=7  then l := UTF8_getcodepoint_autoinc(s,i);
    while (l>=0) do begin
      b[m] := t;
      inc(m);
      dec(l);
    end;
  end;
end;


begin
end.
