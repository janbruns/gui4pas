# About the font render engine

It's a "Microsoft TrueType/OpenType font file"-reader, combined with some algorithms to identify glyphs for unicode-points, rasterize+manage glyph data.

Unimplemented functionalty:
1. Raster Font data (legacy, no plans to support)
2. SVG Font data, colored fonts (might be an idea to add)
3. Adobe style glyph data (often misspelled OpenType)
4. Font conainers (ttc files, probably should be supprted)
5. VAR fonts (nice idea, but at the time I wrote most parts of this, I didn't have a single font file with VAR data, so the decision against the pain of supporting the more abstracte, complicated data structures was fixed)

So most of the time, supported vs. unsupported files can be identified simply be file extension (supported files almost always have the ttf extension, whereas otf files typically make use of some of the unimplemented functionality).


# Implementation

Font rendering can be initiated by calling

function get_glybmp(g : TTglyph; ppemx,ppemy : longint; gf : boolean) : Pglyphbmp;

in tttile.pas, which creates a Tglyphbmp filled with a pointer to a 1 byte per pixel greyscale representattion of glyph g, where subsequent such calls with the same parameters lead to the same Pglyphbmp pointer. The most significant bit ($80) in the greyscale pixel data is explicitly set to what the engine thinks would be the best monochrome result. 

ppemx and ppemy define the pixel size of the em square, and typically should or (for some reasons often must) be equal. Fonts typically keep the glyphs limited to 1 em size (for things like like latin letters), but the technical limit might be a bit more (was it 4em?). The raster data is destroyed together with the glyph. The flag gf tells wether or not to use instructed gridfitting.

To get a specific TTglyph from a font, call

function TTfont.getGlyph(n : longint) : TTglyph;

where n is a not so meaningful, font-internal glyph index.

The returned TTglyph shouldn't be destroyed! This would be the job of some currently empty

procedure TTfont.releaseGlyph(g : TTglyph);

The reason is that the glyph might be needed for other composite glyphs, so it typically doesn't make much sense to delete it. Currently it gets deleted only on TTfont destruction.

And a TTfont as defined in fontfile.pas should be created to load a single stream filled with the ttf file:

procedure TTfont.load(s : Tstream);



To map unicode to glyph indices, there is a 

TOTcmapfont = class(TTfont)

class defined in unit ot_cmap.pas, which knows a method

function TOTcmapfont.UnicodeToGlyphIndex(unicodeval : dword) : longint;


There are some advanced extensions to TTfont to extract opentype data like scripting data in GSUB tables.


# Gridfit

Gridfitting using the instructed glyph "VM" is supported. This VM is typically invoked when a glyph needs to be rasterized to some spefcific pixel size to attempt to move the scaled points around in way that font designers (probably) intended.

This often leaves to much better appearence in the lower range of scales (8..20 pixel per em), where the small amount of pixels per em-square can only be inappropriate to show all the details of a glyph and where the exact positioning of lines relative to the pixel raster is of importance. However, the glyph instructions are typically not made for even smaller scales. 

An alternative to using a VM to adjust scaled glyph data could be adjustment tables (the range 8..20 pixel doesn't cover so many integer scales, so even streaming full precision glyph data wouldn't be very problematic, today as well as maybe 20 yrs. back, eben with square that num of table entries for separate x/y scales). On the other hand, the VM was designed for that purpose (applying adjustment tables to points), so it shouldn't make a difference.

But it does: The VM has lots of specialized point move instructions, and the specification of what exactly the effective outcome of a point move instruction should be was held very vague and very far from just allowing just a single interpretation. 

So after all, my personal impression is it is absolutely *inappropriate* to describe scale-dependent glyph data adjustments today. The world needs a better solution!

Even with freetype as an open source reference implementation providing a unique interpretation this topic is full of useless specifics that effectively provide nothing but things that could go wrong. 

This implementation of the gridfit VM is (almost) freetype compatible. There's however a shortcoming with composite glyph handling. IIRC my (in this case probably also formally in addition to practically wrong) interpretation about how to deal with that initially was reverse to what ft does, and a complete fix would require some releatively deep changes, so only an incomplete fix was implemented.




# License

All the files here (pascal source code) have been written by Jan Bruns (see license.txt in the toplevel dir).



