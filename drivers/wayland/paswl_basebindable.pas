{$mode objfpc}
{$unitpath .}
unit paswl_basebindable;

interface
uses wl_basic_types,wl_core,wl_libwl;


type
Tpaswl_basebindable=class;
Tpaswl_basebindable_cls = class of Tpaswl_basebindable;
Tpaswl_basebindable = class
  public
  constructor create(); virtual;
 
  procedure onBind(); virtual;
  procedure onUnbind(); virtual;

  public
  display : Tpaswl_basebindable;
  wlo : wl_object;
  wlversion : dword;
  
  public
  class function wl_ifdef() : Pwl_interface; virtual; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; virtual; // wl_ifdef()^.version at the time the pas object code was written
end;

Tpaswl_regbasebindable = class(Tpaswl_basebindable)
  public
  wl_regname : dword;
end;



implementation


constructor Tpaswl_basebindable.create();
begin
end;

procedure Tpaswl_basebindable.onBind();
begin
end;

procedure Tpaswl_basebindable.onUnbind();
begin
end;



class function Tpaswl_basebindable.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := nil;
end;

class function Tpaswl_basebindable.wl_ifver() : dword;
begin
  wl_ifver := 0;
end;


initialization
begin
end;

finalization
begin
end;

end.



