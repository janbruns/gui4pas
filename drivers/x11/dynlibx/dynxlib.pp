{$unitpath .}
unit dynxlib;
interface
{$mode objfpc}
uses
  ctypes,x,dynlibs;
{$define MACROS}

const
  libX11='libX11.so';


type
  PPcint = ^Pcint;
  PPcuchar = ^Pcuchar;
{
  Automatically converted by H2Pas 0.99.15 from xlib.h
  The following command line parameters were used:
    -p
    -T
    -S
    -d
    -c
    xlib.h
}


function XlibAvailable() : boolean;
function XlibLibHandle() : TLibHandle;


{$PACKRECORDS C}


const
   XlibSpecificationRelease = 6;
type

   PXPointer = ^TXPointer;
   TXPointer = ^char;
   PBool = ^TBool;
   { We cannot use TBool = LongBool, because Longbool(True)=-1, 
     and that is not handled well by X. So we leave TBool=cint; 
     and make overloaded calls with a boolean parameter. 
     For function results, longbool is OK, since everything <>0 
     is interpreted as true, so we introduce TBoolResult. }
   TBool = cint;
   TBoolResult = longbool;
   PStatus = ^TStatus;
   TStatus = cint;

const
   QueuedAlready = 0;
   QueuedAfterReading = 1;
   QueuedAfterFlush = 2;

type

   PPXExtData = ^PXExtData;
   PXExtData = ^TXExtData;
   TXExtData = record
        number : cint;
        next : PXExtData;
        free_private : function (extension:PXExtData):cint;cdecl;
        private_data : TXPointer;
     end;

   PXExtCodes = ^TXExtCodes;
   TXExtCodes = record
        extension : cint;
        major_opcode : cint;
        first_event : cint;
        first_error : cint;
     end;

   PXPixmapFormatValues = ^TXPixmapFormatValues;
   TXPixmapFormatValues = record
        depth : cint;
        bits_per_pixel : cint;
        scanline_pad : cint;
     end;

   PXGCValues = ^TXGCValues;
   TXGCValues = record
        _function : cint;
        plane_mask : culong;
        foreground : culong;
        background : culong;
        line_width : cint;
        line_style : cint;
        cap_style : cint;
        join_style : cint;
        fill_style : cint;
        fill_rule : cint;
        arc_mode : cint;
        tile : TPixmap;
        stipple : TPixmap;
        ts_x_origin : cint;
        ts_y_origin : cint;
        font : TFont;
        subwindow_mode : cint;
        graphics_exposures : TBool;
        clip_x_origin : cint;
        clip_y_origin : cint;
        clip_mask : TPixmap;
        dash_offset : cint;
        dashes : cchar;
     end;

   PXGC = ^TXGC;
   TXGC = record
     end;
   TGC = PXGC;
   PGC = ^TGC;

   PVisual = ^TVisual;
   TVisual = record
        ext_data : PXExtData;
        visualid : TVisualID;
        c_class : cint;
        red_mask, green_mask, blue_mask : culong;
        bits_per_rgb : cint;
        map_entries : cint;
     end;

   PDepth = ^TDepth;
   TDepth = record
        depth : cint;
        nvisuals : cint;
        visuals : PVisual;
     end;
   PXDisplay = ^TXDisplay;
   TXDisplay = record
     end;


   PScreen = ^TScreen;
   TScreen = record
        ext_data : PXExtData;
        display : PXDisplay;
        root : TWindow;
        width, height : cint;
        mwidth, mheight : cint;
        ndepths : cint;
        depths : PDepth;
        root_depth : cint;
        root_visual : PVisual;
        default_gc : TGC;
        cmap : TColormap;
        white_pixel : culong;
        black_pixel : culong;
        max_maps, min_maps : cint;
        backing_store : cint;
        save_unders : TBool;
        root_input_mask : clong;
     end;

   PScreenFormat = ^TScreenFormat;
   TScreenFormat = record
        ext_data : PXExtData;
        depth : cint;
        bits_per_pixel : cint;
        scanline_pad : cint;
     end;

   PXSetWindowAttributes = ^TXSetWindowAttributes;
   TXSetWindowAttributes = record
        background_pixmap : TPixmap;
        background_pixel : culong;
        border_pixmap : TPixmap;
        border_pixel : culong;
        bit_gravity : cint;
        win_gravity : cint;
        backing_store : cint;
        backing_planes : culong;
        backing_pixel : culong;
        save_under : TBool;
        event_mask : clong;
        do_not_propagate_mask : clong;
        override_redirect : TBool;
        colormap : TColormap;
        cursor : TCursor;
     end;

   PXWindowAttributes = ^TXWindowAttributes;
   TXWindowAttributes = record
        x, y : cint;
        width, height : cint;
        border_width : cint;
        depth : cint;
        visual : PVisual;
        root : TWindow;
        c_class : cint;
        bit_gravity : cint;
        win_gravity : cint;
        backing_store : cint;
        backing_planes : culong;
        backing_pixel : culong;
        save_under : TBool;
        colormap : TColormap;
        map_installed : TBool;
        map_state : cint;
        all_event_masks : clong;
        your_event_mask : clong;
        do_not_propagate_mask : clong;
        override_redirect : TBool;
        screen : PScreen;
     end;

   PXHostAddress = ^TXHostAddress;
   TXHostAddress = record
        family : cint;
        length : cint;
        address : Pchar;
     end;

   PXServerInterpretedAddress = ^TXServerInterpretedAddress;
   TXServerInterpretedAddress = record
        typelength : cint;
        valuelength : cint;
        _type : Pchar;
        value : Pchar;
     end;

   PXImage = ^TXImage;
   TXImage = record
        width, height : cint;
        xoffset : cint;
        format : cint;
        data : Pchar;
        byte_order : cint;
        bitmap_unit : cint;
        bitmap_bit_order : cint;
        bitmap_pad : cint;
        depth : cint;
        bytes_per_line : cint;
        bits_per_pixel : cint;
        red_mask : culong;
        green_mask : culong;
        blue_mask : culong;
        obdata : TXPointer;
        f : record
             create_image : function (para1:PXDisplay; para2:PVisual; para3:cuint; para4:cint; para5:cint;
                          para6:Pchar; para7:cuint; para8:cuint; para9:cint; para10:cint):PXImage;cdecl;
             destroy_image : function (para1:PXImage):cint;cdecl;
             get_pixel : function (para1:PXImage; para2:cint; para3:cint):culong;cdecl;
             put_pixel : function (para1:PXImage; para2:cint; para3:cint; para4:culong):cint;cdecl;
             sub_image : function (para1:PXImage; para2:cint; para3:cint; para4:cuint; para5:cuint):PXImage;cdecl;
             add_pixel : function (para1:PXImage; para2:clong):cint;cdecl;
          end;
     end;

   PXWindowChanges = ^TXWindowChanges;
   TXWindowChanges = record
        x, y : cint;
        width, height : cint;
        border_width : cint;
        sibling : TWindow;
        stack_mode : cint;
     end;

   PXColor = ^TXColor;
   TXColor = record
        pixel : culong;
        red, green, blue : cushort;
        flags : cchar;
        pad : cchar;
     end;

   PXSegment = ^TXSegment;
   TXSegment = record
        x1, y1, x2, y2 : cshort;
     end;

   PXPoint = ^TXPoint;
   TXPoint = record
        x, y : cshort;
     end;

   PXRectangle = ^TXRectangle;
   TXRectangle = record
        x, y : cshort;
        width, height : cushort;
     end;

   PXArc = ^TXArc;
   TXArc = record
        x, y : cshort;
        width, height : cushort;
        angle1, angle2 : cshort;
     end;

   PXKeyboardControl = ^TXKeyboardControl;
   TXKeyboardControl = record
        key_click_percent : cint;
        bell_percent : cint;
        bell_pitch : cint;
        bell_duration : cint;
        led : cint;
        led_mode : cint;
        key : cint;
        auto_repeat_mode : cint;
     end;

   PXKeyboardState = ^TXKeyboardState;
   TXKeyboardState = record
        key_click_percent : cint;
        bell_percent : cint;
        bell_pitch, bell_duration : cuint;
        led_mask : culong;
        global_auto_repeat : cint;
        auto_repeats : array[0..31] of cchar;
     end;

   PXTimeCoord = ^TXTimeCoord;
   TXTimeCoord = record
        time : TTime;
        x, y : cshort;
     end;

   PXModifierKeymap = ^TXModifierKeymap;
   TXModifierKeymap = record
        max_keypermod : cint;
        modifiermap : PKeyCode;
     end;

   PDisplay = ^TDisplay;
   TDisplay = TXDisplay;

   PXPrivate = ^TXPrivate;
   TXPrivate = record
     end;

   PXrmHashBucketRec = ^TXrmHashBucketRec;
   TXrmHashBucketRec = record
     end;


   PXPrivDisplay = ^TXPrivDisplay;
   TXPrivDisplay = record
        ext_data : PXExtData;
        private1 : PXPrivate;
        fd : cint;
        private2 : cint;
        proto_major_version : cint;
        proto_minor_version : cint;
        vendor : Pchar;
        private3 : TXID;
        private4 : TXID;
        private5 : TXID;
        private6 : cint;
        resource_alloc : function (para1:PXDisplay):TXID;cdecl;
        byte_order : cint;
        bitmap_unit : cint;
        bitmap_pad : cint;
        bitmap_bit_order : cint;
        nformats : cint;
        pixmap_format : PScreenFormat;
        private8 : cint;
        release : cint;
        private9, private10 : PXPrivate;
        qlen : cint;
        last_request_read : culong;
        request : culong;
        private11 : TXPointer;
        private12 : TXPointer;
        private13 : TXPointer;
        private14 : TXPointer;
        max_request_size : cunsigned;
        db : PXrmHashBucketRec;
        private15 : function (para1:PXDisplay):cint;cdecl;
        display_name : Pchar;
        default_screen : cint;
        nscreens : cint;
        screens : PScreen;
        motion_buffer : culong;
        private16 : culong;
        min_keycode : cint;
        max_keycode : cint;
        private17 : TXPointer;
        private18 : TXPointer;
        private19 : cint;
        xdefaults : Pchar;
     end;

   PXKeyEvent = ^TXKeyEvent;
   TXKeyEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        root : TWindow;
        subwindow : TWindow;
        time : TTime;
        x, y : cint;
        x_root, y_root : cint;
        state : cuint;
        keycode : cuint;
        same_screen : TBool;
     end;

   PXKeyPressedEvent = ^TXKeyPressedEvent;
   TXKeyPressedEvent = TXKeyEvent;

   PXKeyReleasedEvent = ^TXKeyReleasedEvent;
   TXKeyReleasedEvent = TXKeyEvent;

   PXButtonEvent = ^TXButtonEvent;
   TXButtonEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        root : TWindow;
        subwindow : TWindow;
        time : TTime;
        x, y : cint;
        x_root, y_root : cint;
        state : cuint;
        button : cuint;
        same_screen : TBool;
     end;

   PXButtonPressedEvent = ^TXButtonPressedEvent;
   TXButtonPressedEvent = TXButtonEvent;

   PXButtonReleasedEvent = ^TXButtonReleasedEvent;
   TXButtonReleasedEvent = TXButtonEvent;

   PXMotionEvent = ^TXMotionEvent;
   TXMotionEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        root : TWindow;
        subwindow : TWindow;
        time : TTime;
        x, y : cint;
        x_root, y_root : cint;
        state : cuint;
        is_hint : cchar;
        same_screen : TBool;
     end;

   PXPointerMovedEvent = ^TXPointerMovedEvent;
   TXPointerMovedEvent = TXMotionEvent;

   PXCrossingEvent = ^TXCrossingEvent;
   TXCrossingEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        root : TWindow;
        subwindow : TWindow;
        time : TTime;
        x, y : cint;
        x_root, y_root : cint;
        mode : cint;
        detail : cint;
        same_screen : TBool;
        focus : TBool;
        state : cuint;
     end;

   PXEnterWindowEvent = ^TXEnterWindowEvent;
   TXEnterWindowEvent = TXCrossingEvent;

   PXLeaveWindowEvent = ^TXLeaveWindowEvent;
   TXLeaveWindowEvent = TXCrossingEvent;

   PXFocusChangeEvent = ^TXFocusChangeEvent;
   TXFocusChangeEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        mode : cint;
        detail : cint;
     end;

   PXFocusInEvent = ^TXFocusInEvent;
   TXFocusInEvent = TXFocusChangeEvent;

   PXFocusOutEvent = ^TXFocusOutEvent;
   TXFocusOutEvent = TXFocusChangeEvent;

   PXKeymapEvent = ^TXKeymapEvent;
   TXKeymapEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        key_vector : array[0..31] of cchar;
     end;

   PXExposeEvent = ^TXExposeEvent;
   TXExposeEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        x, y : cint;
        width, height : cint;
        count : cint;
     end;

   PXGraphicsExposeEvent = ^TXGraphicsExposeEvent;
   TXGraphicsExposeEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        drawable : TDrawable;
        x, y : cint;
        width, height : cint;
        count : cint;
        major_code : cint;
        minor_code : cint;
     end;

   PXNoExposeEvent = ^TXNoExposeEvent;
   TXNoExposeEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        drawable : TDrawable;
        major_code : cint;
        minor_code : cint;
     end;

   PXVisibilityEvent = ^TXVisibilityEvent;
   TXVisibilityEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        state : cint;
     end;

   PXCreateWindowEvent = ^TXCreateWindowEvent;
   TXCreateWindowEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        parent : TWindow;
        window : TWindow;
        x, y : cint;
        width, height : cint;
        border_width : cint;
        override_redirect : TBool;
     end;

   PXDestroyWindowEvent = ^TXDestroyWindowEvent;
   TXDestroyWindowEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        event : TWindow;
        window : TWindow;
     end;

   PXUnmapEvent = ^TXUnmapEvent;
   TXUnmapEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        event : TWindow;
        window : TWindow;
        from_configure : TBool;
     end;

   PXMapEvent = ^TXMapEvent;
   TXMapEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        event : TWindow;
        window : TWindow;
        override_redirect : TBool;
     end;

   PXMapRequestEvent = ^TXMapRequestEvent;
   TXMapRequestEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        parent : TWindow;
        window : TWindow;
     end;

   PXReparentEvent = ^TXReparentEvent;
   TXReparentEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        event : TWindow;
        window : TWindow;
        parent : TWindow;
        x, y : cint;
        override_redirect : TBool;
     end;

   PXConfigureEvent = ^TXConfigureEvent;
   TXConfigureEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        event : TWindow;
        window : TWindow;
        x, y : cint;
        width, height : cint;
        border_width : cint;
        above : TWindow;
        override_redirect : TBool;
     end;

   PXGravityEvent = ^TXGravityEvent;
   TXGravityEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        event : TWindow;
        window : TWindow;
        x, y : cint;
     end;

   PXResizeRequestEvent = ^TXResizeRequestEvent;
   TXResizeRequestEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        width, height : cint;
     end;

   PXConfigureRequestEvent = ^TXConfigureRequestEvent;
   TXConfigureRequestEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        parent : TWindow;
        window : TWindow;
        x, y : cint;
        width, height : cint;
        border_width : cint;
        above : TWindow;
        detail : cint;
        value_mask : culong;
     end;

   PXCirculateEvent = ^TXCirculateEvent;
   TXCirculateEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        event : TWindow;
        window : TWindow;
        place : cint;
     end;

   PXCirculateRequestEvent = ^TXCirculateRequestEvent;
   TXCirculateRequestEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        parent : TWindow;
        window : TWindow;
        place : cint;
     end;

   PXPropertyEvent = ^TXPropertyEvent;
   TXPropertyEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        atom : TAtom;
        time : TTime;
        state : cint;
     end;

   PXSelectionClearEvent = ^TXSelectionClearEvent;
   TXSelectionClearEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        selection : TAtom;
        time : TTime;
     end;

   PXSelectionRequestEvent = ^TXSelectionRequestEvent;
   TXSelectionRequestEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        owner : TWindow;
        requestor : TWindow;
        selection : TAtom;
        target : TAtom;
        _property : TAtom;
        time : TTime;
     end;

   PXSelectionEvent = ^TXSelectionEvent;
   TXSelectionEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        requestor : TWindow;
        selection : TAtom;
        target : TAtom;
        _property : TAtom;
        time : TTime;
     end;

   PXColormapEvent = ^TXColormapEvent;
   TXColormapEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        colormap : TColormap;
        c_new : TBool;
        state : cint;
     end;

   PXClientMessageEvent = ^TXClientMessageEvent;
   TXClientMessageEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        message_type : TAtom;
        format : cint;
        data : record
            case longint of
               0 : ( b : array[0..19] of cchar );
               1 : ( s : array[0..9] of cshort );
               2 : ( l : array[0..4] of clong );
            end;
     end;

   PXMappingEvent = ^TXMappingEvent;
   TXMappingEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
        request : cint;
        first_keycode : cint;
        count : cint;
     end;

   PXErrorEvent = ^TXErrorEvent;
   TXErrorEvent = record
        _type : cint;
        display : PDisplay;
        resourceid : TXID;
        serial : culong;
        error_code : cuchar;
        request_code : cuchar;
        minor_code : cuchar;
     end;

   PXAnyEvent = ^TXAnyEvent;
   TXAnyEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        window : TWindow;
     end;

   (***************************************************************
    *
    * GenericEvent.  This event is the standard event for all newer extensions.
    *)

   PXGenericEvent = ^TXGenericEvent;
   TXGenericEvent = record
        _type: cint;                 { of event. Always GenericEvent }
        serial: culong;              { # of last request processed }
        send_event: TBool;           { true if from SendEvent request }
        display: PDisplay;           { Display the event was read from }
        extension: cint;             { major opcode of extension that caused the event }
        evtype: cint;                { actual event type. }
     end;

   PXGenericEventCookie = ^TXGenericEventCookie;
   TXGenericEventCookie = record
        _type: cint;                 { of event. Always GenericEvent }
        serial: culong;              { # of last request processed }
        send_event: TBool;           { true if from SendEvent request }
        display: PDisplay;           { Display the event was read from }
        extension: cint;             { major opcode of extension that caused the event }
        evtype: cint;                { actual event type. }
        cookie: cuint;
        data: pointer;
     end;

   PXEvent = ^TXEvent;
   TXEvent = record
       case longint of
          0 : ( _type : cint );
          1 : ( xany : TXAnyEvent );
          2 : ( xkey : TXKeyEvent );
          3 : ( xbutton : TXButtonEvent );
          4 : ( xmotion : TXMotionEvent );
          5 : ( xcrossing : TXCrossingEvent );
          6 : ( xfocus : TXFocusChangeEvent );
          7 : ( xexpose : TXExposeEvent );
          8 : ( xgraphicsexpose : TXGraphicsExposeEvent );
          9 : ( xnoexpose : TXNoExposeEvent );
          10 : ( xvisibility : TXVisibilityEvent );
          11 : ( xcreatewindow : TXCreateWindowEvent );
          12 : ( xdestroywindow : TXDestroyWindowEvent );
          13 : ( xunmap : TXUnmapEvent );
          14 : ( xmap : TXMapEvent );
          15 : ( xmaprequest : TXMapRequestEvent );
          16 : ( xreparent : TXReparentEvent );
          17 : ( xconfigure : TXConfigureEvent );
          18 : ( xgravity : TXGravityEvent );
          19 : ( xresizerequest : TXResizeRequestEvent );
          20 : ( xconfigurerequest : TXConfigureRequestEvent );
          21 : ( xcirculate : TXCirculateEvent );
          22 : ( xcirculaterequest : TXCirculateRequestEvent );
          23 : ( xproperty : TXPropertyEvent );
          24 : ( xselectionclear : TXSelectionClearEvent );
          25 : ( xselectionrequest : TXSelectionRequestEvent );
          26 : ( xselection : TXSelectionEvent );
          27 : ( xcolormap : TXColormapEvent );
          28 : ( xclient : TXClientMessageEvent );
          29 : ( xmapping : TXMappingEvent );
          30 : ( xerror : TXErrorEvent );
          31 : ( xkeymap : TXKeymapEvent );
          32 : ( xgeneric : TXGenericEvent );
          33 : ( xcookie : TXGenericEventCookie );
          34 : ( pad : array[0..23] of clong );
       end;

type

   PXCharStruct = ^TXCharStruct;
   TXCharStruct = record
        lbearing : cshort;
        rbearing : cshort;
        width : cshort;
        ascent : cshort;
        descent : cshort;
        attributes : cushort;
     end;

   PXFontProp = ^TXFontProp;
   TXFontProp = record
        name : TAtom;
        card32 : culong;
     end;

   PPPXFontStruct = ^PPXFontStruct;
   PPXFontStruct = ^PXFontStruct;
   PXFontStruct = ^TXFontStruct;
   TXFontStruct = record
        ext_data : PXExtData;
        fid : TFont;
        direction : cunsigned;
        min_char_or_byte2 : cunsigned;
        max_char_or_byte2 : cunsigned;
        min_byte1 : cunsigned;
        max_byte1 : cunsigned;
        all_chars_exist : TBool;
        default_char : cunsigned;
        n_properties : cint;
        properties : PXFontProp;
        min_bounds : TXCharStruct;
        max_bounds : TXCharStruct;
        per_char : PXCharStruct;
        ascent : cint;
        descent : cint;
     end;

   PXTextItem = ^TXTextItem;
   TXTextItem = record
        chars : Pchar;
        nchars : cint;
        delta : cint;
        font : TFont;
     end;

   PXChar2b = ^TXChar2b;
   TXChar2b = record
        byte1 : cuchar;
        byte2 : cuchar;
     end;

   PXTextItem16 = ^TXTextItem16;
   TXTextItem16 = record
        chars : PXChar2b;
        nchars : cint;
        delta : cint;
        font : TFont;
     end;

   PXEDataObject = ^TXEDataObject;
   TXEDataObject = record
       case longint of
          0 : ( display : PDisplay );
          1 : ( gc : TGC );
          2 : ( visual : PVisual );
          3 : ( screen : PScreen );
          4 : ( pixmap_format : PScreenFormat );
          5 : ( font : PXFontStruct );
       end;

   PXFontSetExtents = ^TXFontSetExtents;
   TXFontSetExtents = record
        max_ink_extent : TXRectangle;
        max_logical_extent : TXRectangle;
     end;

   PXOM = ^TXOM;
   TXOM = record
     end;

   PXOC = ^TXOC;
   TXOC = record
     end;
   TXFontSet = PXOC;
   PXFontSet = ^TXFontSet;

   PXmbTextItem = ^TXmbTextItem;
   TXmbTextItem = record
        chars : Pchar;
        nchars : cint;
        delta : cint;
        font_set : TXFontSet;
     end;

   PXwcTextItem = ^TXwcTextItem;
   TXwcTextItem = record
        chars : PWideChar; {wchar_t*}
        nchars : cint;
        delta : cint;
        font_set : TXFontSet;
     end;

const
   XNRequiredCharSet = 'requiredCharSet';
   XNQueryOrientation = 'queryOrientation';
   XNBaseFontName = 'baseFontName';
   XNOMAutomatic = 'omAutomatic';
   XNMissingCharSet = 'missingCharSet';
   XNDefaultString = 'defaultString';
   XNOrientation = 'orientation';
   XNDirectionalDependentDrawing = 'directionalDependentDrawing';
   XNContextualDrawing = 'contextualDrawing';
   XNFontInfo = 'fontInfo';
type

   PXOMCharSetList = ^TXOMCharSetList;
   TXOMCharSetList = record
        charset_count : cint;
        charset_list : PPChar;
     end;

   PXOrientation = ^TXOrientation;
   TXOrientation = (XOMOrientation_LTR_TTB,XOMOrientation_RTL_TTB,
     XOMOrientation_TTB_LTR,XOMOrientation_TTB_RTL,
     XOMOrientation_Context);

   PXOMOrientation = ^TXOMOrientation;
   TXOMOrientation = record
        num_orientation : cint;
        orientation : PXOrientation;
     end;

   PXOMFontInfo = ^TXOMFontInfo;
   TXOMFontInfo = record
        num_font : cint;
        font_struct_list : ^PXFontStruct;
        font_name_list : PPChar;
     end;

   PXIM = ^TXIM;
   TXIM = record
     end;

   PXIC = ^TXIC;
   TXIC = record
     end;

   TXIMProc = procedure (para1:PXIM; para2:TXPointer; para3:TXPointer);cdecl;

   TXICProc = function (para1:PXIC; para2:TXPointer; para3:TXPointer):TBoolResult;cdecl;

   TXIDProc = procedure (para1:PDisplay; para2:TXPointer; para3:TXPointer);cdecl;

   PXIMStyle = ^TXIMStyle;
   TXIMStyle = culong;

   PXIMStyles = ^TXIMStyles;
   TXIMStyles = record
        count_styles : cushort;
        supported_styles : PXIMStyle;
     end;

const
   XIMPreeditArea = $0001;
   XIMPreeditCallbacks = $0002;
   XIMPreeditPosition = $0004;
   XIMPreeditNothing = $0008;
   XIMPreeditNone = $0010;
   XIMStatusArea = $0100;
   XIMStatusCallbacks = $0200;
   XIMStatusNothing = $0400;
   XIMStatusNone = $0800;
   XNVaNestedList = 'XNVaNestedList';
   XNQueryInputStyle = 'queryInputStyle';
   XNClientWindow = 'clientWindow';
   XNInputStyle = 'inputStyle';
   XNFocusWindow = 'focusWindow';
   XNResourceName = 'resourceName';
   XNResourceClass = 'resourceClass';
   XNGeometryCallback = 'geometryCallback';
   XNDestroyCallback = 'destroyCallback';
   XNFilterEvents = 'filterEvents';
   XNPreeditStartCallback = 'preeditStartCallback';
   XNPreeditDoneCallback = 'preeditDoneCallback';
   XNPreeditDrawCallback = 'preeditDrawCallback';
   XNPreeditCaretCallback = 'preeditCaretCallback';
   XNPreeditStateNotifyCallback = 'preeditStateNotifyCallback';
   XNPreeditAttributes = 'preeditAttributes';
   XNStatusStartCallback = 'statusStartCallback';
   XNStatusDoneCallback = 'statusDoneCallback';
   XNStatusDrawCallback = 'statusDrawCallback';
   XNStatusAttributes = 'statusAttributes';
   XNArea = 'area';
   XNAreaNeeded = 'areaNeeded';
   XNSpotLocation = 'spotLocation';
   XNColormap = 'colorMap';
   XNStdColormap = 'stdColorMap';
   XNForeground = 'foreground';
   XNBackground = 'background';
   XNBackgroundPixmap = 'backgroundPixmap';
   XNFontSet = 'fontSet';
   XNLineSpace = 'lineSpace';
   XNCursor = 'cursor';
   XNQueryIMValuesList = 'queryIMValuesList';
   XNQueryICValuesList = 'queryICValuesList';
   XNVisiblePosition = 'visiblePosition';
   XNR6PreeditCallback = 'r6PreeditCallback';
   XNStringConversionCallback = 'stringConversionCallback';
   XNStringConversion = 'stringConversion';
   XNResetState = 'resetState';
   XNHotKey = 'hotKey';
   XNHotKeyState = 'hotKeyState';
   XNPreeditState = 'preeditState';
   XNSeparatorofNestedList = 'separatorofNestedList';
   XBufferOverflow = -(1);
   XLookupNone = 1;
   XLookupChars = 2;
   XLookupKeySymVal = 3;
   XLookupBoth = 4;
type

   PXVaNestedList = ^TXVaNestedList;
   TXVaNestedList = pointer;

   PXIMCallback = ^TXIMCallback;
   TXIMCallback = record
        client_data : TXPointer;
        callback : TXIMProc;
     end;

   PXICCallback = ^TXICCallback;
   TXICCallback = record
        client_data : TXPointer;
        callback : TXICProc;
     end;

   PXIMFeedback = ^TXIMFeedback;
   TXIMFeedback = culong;

const
   XIMReverse = 1;
   XIMUnderline = 1 shl 1;
   XIMHighlight = 1 shl 2;
   XIMPrimary = 1 shl 5;
   XIMSecondary = 1 shl 6;
   XIMTertiary = 1 shl 7;
   XIMVisibleToForward = 1 shl 8;
   XIMVisibleToBackword = 1 shl 9;
   XIMVisibleToCenter = 1 shl 10;
type

   PXIMText = ^TXIMText;
   TXIMText = record
        length : cushort;
        feedback : PXIMFeedback;
        encoding_is_wchar : TBool;
        _string : record
            case longint of
               0 : ( multi_byte : Pchar );
               1 : ( wide_char : PWideChar ); {wchar_t*}
            end;
     end;

   PXIMPreeditState = ^TXIMPreeditState;
   TXIMPreeditState = culong;

const
   XIMPreeditUnKnown = 0;
   XIMPreeditEnable = 1;
   XIMPreeditDisable = 1 shl 1;
type

   PXIMPreeditStateNotifyCallbackStruct = ^TXIMPreeditStateNotifyCallbackStruct;
   TXIMPreeditStateNotifyCallbackStruct = record
        state : TXIMPreeditState;
     end;

   PXIMResetState = ^TXIMResetState;
   TXIMResetState = culong;

const
   XIMInitialState = 1;
   XIMPreserveState = 1 shl 1;
type

   PXIMStringConversionFeedback = ^TXIMStringConversionFeedback;
   TXIMStringConversionFeedback = culong;

const
   XIMStringConversionLeftEdge = $00000001;
   XIMStringConversionRightEdge = $00000002;
   XIMStringConversionTopEdge = $00000004;
   XIMStringConversionBottomEdge = $00000008;
   XIMStringConversionConcealed = $00000010;
   XIMStringConversionWrapped = $00000020;
type

   PXIMStringConversionText = ^TXIMStringConversionText;
   TXIMStringConversionText = record
        length : cushort;
        feedback : PXIMStringConversionFeedback;
        encoding_is_wchar : TBool;
        _string : record
            case longint of
               0 : ( mbs : Pchar );
               1 : ( wcs : PWideChar ); {wchar_t*}
            end;
     end;

   PXIMStringConversionPosition = ^TXIMStringConversionPosition;
   TXIMStringConversionPosition = cushort;

   PXIMStringConversionType = ^TXIMStringConversionType;
   TXIMStringConversionType = cushort;

const
   XIMStringConversionBuffer = $0001;
   XIMStringConversionLine = $0002;
   XIMStringConversionWord = $0003;
   XIMStringConversionChar = $0004;
type

   PXIMStringConversionOperation = ^TXIMStringConversionOperation;
   TXIMStringConversionOperation = cushort;

const
   XIMStringConversionSubstitution = $0001;
   XIMStringConversionRetrieval = $0002;
type

   PXIMCaretDirection = ^TXIMCaretDirection;
   TXIMCaretDirection = (XIMForwardChar,XIMBackwardChar,XIMForwardWord,
     XIMBackwardWord,XIMCaretUp,XIMCaretDown,
     XIMNextLine,XIMPreviousLine,XIMLineStart,
     XIMLineEnd,XIMAbsolutePosition,XIMDontChange
     );

   PXIMStringConversionCallbackStruct = ^TXIMStringConversionCallbackStruct;
   TXIMStringConversionCallbackStruct = record
        position : TXIMStringConversionPosition;
        direction : TXIMCaretDirection;
        operation : TXIMStringConversionOperation;
        factor : cushort;
        text : PXIMStringConversionText;
     end;

   PXIMPreeditDrawCallbackStruct = ^TXIMPreeditDrawCallbackStruct;
   TXIMPreeditDrawCallbackStruct = record
        caret : cint;
        chg_first : cint;
        chg_length : cint;
        text : PXIMText;
     end;

   PXIMCaretStyle = ^TXIMCaretStyle;
   TXIMCaretStyle = (XIMIsInvisible,XIMIsPrimary,XIMIsSecondary
     );

   PXIMPreeditCaretCallbackStruct = ^TXIMPreeditCaretCallbackStruct;
   TXIMPreeditCaretCallbackStruct = record
        position : cint;
        direction : TXIMCaretDirection;
        style : TXIMCaretStyle;
     end;

   PXIMStatusDataType = ^TXIMStatusDataType;
   TXIMStatusDataType = (XIMTextType,XIMBitmapType);

   PXIMStatusDrawCallbackStruct = ^TXIMStatusDrawCallbackStruct;
   TXIMStatusDrawCallbackStruct = record
        _type : TXIMStatusDataType;
        data : record
            case longint of
               0 : ( text : PXIMText );
               1 : ( bitmap : TPixmap );
            end;
     end;

   PXIMHotKeyTrigger = ^TXIMHotKeyTrigger;
   TXIMHotKeyTrigger = record
        keysym : TKeySym;
        modifier : cint;
        modifier_mask : cint;
     end;

   PXIMHotKeyTriggers = ^TXIMHotKeyTriggers;
   TXIMHotKeyTriggers = record
        num_hot_key : cint;
        key : PXIMHotKeyTrigger;
     end;

   PXIMHotKeyState = ^TXIMHotKeyState;
   TXIMHotKeyState = culong;

const
   XIMHotKeyStateON = $0001;
   XIMHotKeyStateOFF = $0002;
type

   PXIMValuesList = ^TXIMValuesList;
   TXIMValuesList = record
        count_values : cushort;
        supported_values : PPChar;
     end;
{_$warning _Xdebug dropped for dynXlib }

type
  funcdisp    = function(display:PDisplay):cint;cdecl;
  funcifevent = function(display:PDisplay; event:PXEvent; p : TXPointer):TBoolResult;cdecl;
  chararr32   = array[0..31] of char;
  pchararr32  = chararr32;

const
  AllPlanes : culong = culong(not 0);

type

   TXErrorHandler = function (para1:PDisplay; para2:PXErrorEvent):cint;cdecl;
   TXIOErrorHandler = function (para1:PDisplay):cint;cdecl;
   TXConnectionWatchProc = procedure (para1:PDisplay; para2:TXPointer; para3:cint; para4:TBool; para5:PXPointer);cdecl;



type
T_XLoadQueryFont=function(para1:PDisplay; para2:Pchar):PXFontStruct;cdecl;
T_XQueryFont=function(para1:PDisplay; para2:TXID):PXFontStruct;cdecl;
T_XGetMotionEvents=function(para1:PDisplay; para2:TWindow; para3:TTime; para4:TTime; para5:Pcint):PXTimeCoord;cdecl;
T_XDeleteModifiermapEntry=function(para1:PXModifierKeymap; para2:TKeyCode; para3:cint):PXModifierKeymap;cdecl;
T_XGetModifierMapping=function(para1:PDisplay):PXModifierKeymap;cdecl;
T_XInsertModifiermapEntry=function(para1:PXModifierKeymap; para2:TKeyCode; para3:cint):PXModifierKeymap;cdecl;
T_XNewModifiermap=function(para1:cint):PXModifierKeymap;cdecl;
T_XCreateImage=function(para1:PDisplay; para2:PVisual; para3:cuint; para4:cint; para5:cint; para6:Pchar; para7:cuint; para8:cuint; para9:cint; para10:cint):PXImage;cdecl;
T_XInitImage=function(para1:PXImage):TStatus;cdecl;
T_XGetImage=function(para1:PDisplay; para2:TDrawable; para3:cint; para4:cint; para5:cuint; para6:cuint; para7:culong; para8:cint):PXImage;cdecl;
T_XGetSubImage=function(para1:PDisplay; para2:TDrawable; para3:cint; para4:cint; para5:cuint; para6:cuint; para7:culong; para8:cint; para9:PXImage; para10:cint; para11:cint):PXImage;cdecl;
T_XOpenDisplay=function(para1:Pchar):PDisplay;cdecl;
T_XFetchBytes=function(para1:PDisplay; para2:Pcint):Pchar;cdecl;
T_XFetchBuffer=function(para1:PDisplay; para2:Pcint; para3:cint):Pchar;cdecl;
T_XGetAtomName=function(para1:PDisplay; para2:TAtom):Pchar;cdecl;
T_XGetAtomNames=function(para1:PDisplay; para2:PAtom; para3:cint; para4:PPchar):TStatus;cdecl;
T_XGetDefault=function(para1:PDisplay; para2:Pchar; para3:Pchar):Pchar;cdecl;
T_XDisplayName=function(para1:Pchar):Pchar;cdecl;
T_XKeysymToString=function(para1:TKeySym):Pchar;cdecl;
T_XSynchronize=function(para1:PDisplay; para2:TBool):funcdisp;cdecl;
T_XSetAfterFunction=function(para1:PDisplay; para2:funcdisp):funcdisp;cdecl;
T_XInternAtom=function(para1:PDisplay; para2:Pchar; para3:TBool):TAtom;cdecl;
T_XInternAtoms=function(para1:PDisplay; para2:PPchar; para3:cint; para4:TBool; para5:PAtom):TStatus;cdecl;
T_XCopyColormapAndFree=function(para1:PDisplay; para2:TColormap):TColormap;cdecl;
T_XCreateColormap=function(para1:PDisplay; para2:TWindow; para3:PVisual; para4:cint):TColormap;cdecl;
T_XCreatePixmapCursor=function(ADisplay:PDisplay; ASource:TPixmap; AMask:TPixmap; AForegroundColor:PXColor; ABackgroundColor:PXColor; AX:cuint; AY:cuint):TCursor;cdecl;
T_XCreateGlyphCursor=function(ADisplay:PDisplay; ASourceFont:TFont; AMaskFont:TFont; ASourceChar:cuint; AMaskChar:cuint; AForegroundColor:PXColor; ABackgroundColor:PXColor):TCursor;cdecl;
T_XCreateFontCursor=function(ADisplay:PDisplay; AShape:cuint):TCursor;cdecl;
T_XLoadFont=function(para1:PDisplay; para2:Pchar):TFont;cdecl;
T_XCreateGC=function(para1:PDisplay; para2:TDrawable; para3:culong; para4:PXGCValues):TGC;cdecl;
T_XGContextFromGC=function(para1:TGC):TGContext;cdecl;
T_XFlushGC=procedure(para1:PDisplay; para2:TGC);cdecl;
T_XCreatePixmap=function(ADisplay:PDisplay; ADrawable:TDrawable; AWidth:cuint; AHeight:cuint; ADepth:cuint):TPixmap;cdecl;
T_XCreateBitmapFromData=function(ADiplay:PDisplay; ADrawable:TDrawable; AData:Pchar; AWidth:cuint; AHeight:cuint):TPixmap;cdecl;
T_XCreatePixmapFromBitmapData=function(para1:PDisplay; para2:TDrawable; para3:Pchar; para4:cuint; para5:cuint; para6:culong; para7:culong; para8:cuint):TPixmap;cdecl;
T_XCreateSimpleWindow=function(ADisplay:PDisplay; AParent:TWindow; AX:cint; AY:cint; AWidth:cuint; AHeight:cuint; ABorderWidth:cuint; ABorder:culong; ABackground:culong):TWindow;cdecl;
T_XGetSelectionOwner=function(para1:PDisplay; para2:TAtom):TWindow;cdecl;
T_XCreateWindow=function(ADisplay:PDisplay; AParent:TWindow; AX:cint; AY:cint; AWidth:cuint; AHeight:cuint; ABorderWidth:cuint; ADepth:cint; AClass:cuint; AVisual:PVisual; AValueMask:culong; AAttributes:PXSetWindowAttributes):TWindow;cdecl;
T_XListInstalledColormaps=function(para1:PDisplay; para2:TWindow; para3:Pcint):PColormap;cdecl;
T_XListFonts=function(para1:PDisplay; para2:Pchar; para3:cint; para4:Pcint):PPChar;cdecl;
T_XListFontsWithInfo=function(para1:PDisplay; para2:Pchar; para3:cint; para4:Pcint; para5:PPXFontStruct):PPChar;cdecl;
T_XGetFontPath=function(para1:PDisplay; para2:Pcint):PPChar;cdecl;
T_XListExtensions=function(para1:PDisplay; para2:Pcint):PPChar;cdecl;
T_XListProperties=function(para1:PDisplay; para2:TWindow; para3:Pcint):PAtom;cdecl;
T_XListHosts=function(para1:PDisplay; para2:Pcint; para3:PBool):PXHostAddress;cdecl;
T_XKeycodeToKeysym=function(para1:PDisplay; para2:TKeyCode; para3:cint):TKeySym;cdecl;
T_XLookupKeysym=function(para1:PXKeyEvent; para2:cint):TKeySym;cdecl;
T_XGetKeyboardMapping=function(para1:PDisplay; para2:TKeyCode; para3:cint; para4:Pcint):PKeySym;cdecl;
T_XStringToKeysym=function(para1:Pchar):TKeySym;cdecl;
T_XMaxRequestSize=function(para1:PDisplay):clong;cdecl;
T_XExtendedMaxRequestSize=function(para1:PDisplay):clong;cdecl;
T_XResourceManagerString=function(para1:PDisplay):Pchar;cdecl;
T_XScreenResourceString=function(para1:PScreen):Pchar;cdecl;
T_XDisplayMotionBufferSize=function(para1:PDisplay):culong;cdecl;
T_XVisualIDFromVisual=function(para1:PVisual):TVisualID;cdecl;
T_XLockDisplay=procedure(para1:PDisplay);cdecl;
T_XUnlockDisplay=procedure(para1:PDisplay);cdecl;
T_XInitExtension=function(para1:PDisplay; para2:Pchar):PXExtCodes;cdecl;
T_XAddExtension=function(para1:PDisplay):PXExtCodes;cdecl;
T_XFindOnExtensionList=function(para1:PPXExtData; para2:cint):PXExtData;cdecl;
T_XEHeadOfExtensionList=function(para1:TXEDataObject):PPXExtData;cdecl;
T_XRootWindow=function(ADisplay:PDisplay; AScreenNumber:cint):TWindow;cdecl;
T_XDefaultRootWindow=function(ADisplay:PDisplay):TWindow;cdecl;
T_XRootWindowOfScreen=function(para1:PScreen):TWindow;cdecl;
T_XDefaultVisual=function(para1:PDisplay; para2:cint):PVisual;cdecl;
T_XDefaultVisualOfScreen=function(para1:PScreen):PVisual;cdecl;
T_XDefaultGC=function(para1:PDisplay; para2:cint):TGC;cdecl;
T_XDefaultGCOfScreen=function(para1:PScreen):TGC;cdecl;
T_XBlackPixel=function(ADisplay:PDisplay; AScreenNumber:cint):culong;cdecl;
T_XWhitePixel=function(ADisplay:PDisplay; AScreenNumber:cint):culong;cdecl;
T_XBlackPixelOfScreen=function(para1:PScreen):culong;cdecl;
T_XWhitePixelOfScreen=function(para1:PScreen):culong;cdecl;
T_XNextRequest=function(para1:PDisplay):culong;cdecl;
T_XLastKnownRequestProcessed=function(para1:PDisplay):culong;cdecl;
T_XServerVendor=function(para1:PDisplay):Pchar;cdecl;
T_XDisplayString=function(para1:PDisplay):Pchar;cdecl;
T_XDefaultColormap=function(para1:PDisplay; para2:cint):TColormap;cdecl;
T_XDefaultColormapOfScreen=function(para1:PScreen):TColormap;cdecl;
T_XDisplayOfScreen=function(para1:PScreen):PDisplay;cdecl;
T_XScreenOfDisplay=function(para1:PDisplay; para2:cint):PScreen;cdecl;
T_XDefaultScreenOfDisplay=function(para1:PDisplay):PScreen;cdecl;
T_XEventMaskOfScreen=function(para1:PScreen):clong;cdecl;
T_XScreenNumberOfScreen=function(para1:PScreen):cint;cdecl;
T_XSetErrorHandler=function(para1:TXErrorHandler):TXErrorHandler;cdecl;
T_XSetIOErrorHandler=function(para1:TXIOErrorHandler):TXIOErrorHandler;cdecl;
T_XListPixmapFormats=function(para1:PDisplay; para2:Pcint):PXPixmapFormatValues;cdecl;
T_XListDepths=function(para1:PDisplay; para2:cint; para3:Pcint):Pcint;cdecl;
T_XReconfigureWMWindow=function(para1:PDisplay; para2:TWindow; para3:cint; para4:cuint; para5:PXWindowChanges):TStatus;cdecl;
T_XGetWMProtocols=function(para1:PDisplay; para2:TWindow; para3:PPAtom; para4:Pcint):TStatus;cdecl;
T_XSetWMProtocols=function(para1:PDisplay; para2:TWindow; para3:PAtom; para4:cint):TStatus;cdecl;
T_XIconifyWindow=function(para1:PDisplay; para2:TWindow; para3:cint):TStatus;cdecl;
T_XWithdrawWindow=function(para1:PDisplay; para2:TWindow; para3:cint):TStatus;cdecl;
T_XGetCommand=function(para1:PDisplay; para2:TWindow; para3:PPPchar; para4:Pcint):TStatus;cdecl;
T_XGetWMColormapWindows=function(para1:PDisplay; para2:TWindow; para3:PPWindow; para4:Pcint):TStatus;cdecl;
T_XSetWMColormapWindows=function(para1:PDisplay; para2:TWindow; para3:PWindow; para4:cint):TStatus;cdecl;
T_XFreeStringList=procedure(para1:PPchar);cdecl;
T_XSetTransientForHint=function(ADisplay:PDisplay; AWindow:TWindow; APropWindow:TWindow):cint;cdecl;
T_XActivateScreenSaver=function(para1:PDisplay):cint;cdecl;
T_XAddHost=function(para1:PDisplay; para2:PXHostAddress):cint;cdecl;
T_XAddHosts=function(para1:PDisplay; para2:PXHostAddress; para3:cint):cint;cdecl;
T_XAddToExtensionList=function(para1:PPXExtData; para2:PXExtData):cint;cdecl;
T_XAddToSaveSet=function(para1:PDisplay; para2:TWindow):cint;cdecl;
T_XAllocColor=function(para1:PDisplay; para2:TColormap; para3:PXColor):TStatus;cdecl;
T_XAllocColorCells=function(para1:PDisplay; para2:TColormap; para3:TBool; para4:Pculong; para5:cuint; para6:Pculong; para7:cuint):TStatus;cdecl;
T_XAllocColorPlanes=function(para1:PDisplay; para2:TColormap; para3:TBool; para4:Pculong; para5:cint; para6:cint; para7:cint; para8:cint; para9:Pculong; para10:Pculong; para11:Pculong):TStatus;cdecl;
T_XAllocNamedColor=function(para1:PDisplay; para2:TColormap; para3:Pchar; para4:PXColor; para5:PXColor):TStatus;cdecl;
T_XAllowEvents=function(para1:PDisplay; para2:cint; para3:TTime):cint;cdecl;
T_XAutoRepeatOff=function(para1:PDisplay):cint;cdecl;
T_XAutoRepeatOn=function(para1:PDisplay):cint;cdecl;
T_XBell=function(para1:PDisplay; para2:cint):cint;cdecl;
T_XBitmapBitOrder=function(para1:PDisplay):cint;cdecl;
T_XBitmapPad=function(para1:PDisplay):cint;cdecl;
T_XBitmapUnit=function(para1:PDisplay):cint;cdecl;
T_XCellsOfScreen=function(para1:PScreen):cint;cdecl;
T_XChangeActivePointerGrab=function(para1:PDisplay; para2:cuint; para3:TCursor; para4:TTime):cint;cdecl;
T_XChangeGC=function(para1:PDisplay; para2:TGC; para3:culong; para4:PXGCValues):cint;cdecl;
T_XChangeKeyboardControl=function(para1:PDisplay; para2:culong; para3:PXKeyboardControl):cint;cdecl;
T_XChangeKeyboardMapping=function(para1:PDisplay; para2:cint; para3:cint; para4:PKeySym; para5:cint):cint;cdecl;
T_XChangePointerControl=function(para1:PDisplay; para2:TBool; para3:TBool; para4:cint; para5:cint; para6:cint):cint;cdecl;
T_XChangeProperty=function(para1:PDisplay; para2:TWindow; para3:TAtom; para4:TAtom; para5:cint; para6:cint; para7:Pcuchar; para8:cint):cint;cdecl;
T_XChangeSaveSet=function(para1:PDisplay; para2:TWindow; para3:cint):cint;cdecl;
T_XChangeWindowAttributes=function(para1:PDisplay; para2:TWindow; para3:culong; para4:PXSetWindowAttributes):cint;cdecl;
T_XCheckIfEvent=function(para1:PDisplay; para2:PXEvent; para3:funcifevent; para4:TXPointer):TBoolResult;cdecl;
T_XCheckMaskEvent=function(para1:PDisplay; para2:clong; para3:PXEvent):TBoolResult;cdecl;
T_XCheckTypedEvent=function(para1:PDisplay; para2:cint; para3:PXEvent):TBoolResult;cdecl;
T_XCheckTypedWindowEvent=function(para1:PDisplay; para2:TWindow; para3:cint; para4:PXEvent):TBoolResult;cdecl;
T_XCheckWindowEvent=function(para1:PDisplay; para2:TWindow; para3:clong; para4:PXEvent):TBoolResult;cdecl;
T_XCirculateSubwindows=function(para1:PDisplay; para2:TWindow; para3:cint):cint;cdecl;
T_XCirculateSubwindowsDown=function(para1:PDisplay; para2:TWindow):cint;cdecl;
T_XCirculateSubwindowsUp=function(para1:PDisplay; para2:TWindow):cint;cdecl;
T_XClearArea=function(para1:PDisplay; para2:TWindow; para3:cint; para4:cint; para5:cuint; para6:cuint; para7:TBool):cint;cdecl;
T_XClearWindow=function(para1:PDisplay; para2:TWindow):cint;cdecl;
T_XCloseDisplay=function(para1:PDisplay):cint;cdecl;
T_XConfigureWindow=function(para1:PDisplay; para2:TWindow; para3:cuint; para4:PXWindowChanges):cint;cdecl;
T_XConnectionNumber=function(para1:PDisplay):cint;cdecl;
T_XConvertSelection=function(para1:PDisplay; para2:TAtom; para3:TAtom; para4:TAtom; para5:TWindow; para6:TTime):cint;cdecl;
T_XCopyArea=function(para1:PDisplay; para2:TDrawable; para3:TDrawable; para4:TGC; para5:cint; para6:cint; para7:cuint; para8:cuint; para9:cint; para10:cint):cint;cdecl;
T_XCopyGC=function(para1:PDisplay; para2:TGC; para3:culong; para4:TGC):cint;cdecl;
T_XCopyPlane=function(para1:PDisplay; para2:TDrawable; para3:TDrawable; para4:TGC; para5:cint; para6:cint; para7:cuint; para8:cuint; para9:cint; para10:cint; para11:culong):cint;cdecl;
T_XDefaultDepth=function(para1:PDisplay; para2:cint):cint;cdecl;
T_XDefaultDepthOfScreen=function(para1:PScreen):cint;cdecl;
T_XDefaultScreen=function(para1:PDisplay):cint;cdecl;
T_XDefineCursor=function(ADisplay:PDisplay; AWindow:TWindow; ACursor:TCursor):cint;cdecl;
T_XDeleteProperty=function(para1:PDisplay; para2:TWindow; para3:TAtom):cint;cdecl;
T_XDestroyWindow=function(ADisplay:PDisplay; AWindow:TWindow):cint;cdecl;
T_XDestroySubwindows=function(ADisplay:PDisplay; AWindow:TWindow):cint;cdecl;
T_XDoesBackingStore=function(para1:PScreen):cint;cdecl;
T_XDoesSaveUnders=function(para1:PScreen):TBoolResult;cdecl;
T_XDisableAccessControl=function(para1:PDisplay):cint;cdecl;
T_XDisplayCells=function(para1:PDisplay; para2:cint):cint;cdecl;
T_XDisplayHeight=function(para1:PDisplay; para2:cint):cint;cdecl;
T_XDisplayHeightMM=function(para1:PDisplay; para2:cint):cint;cdecl;
T_XDisplayKeycodes=function(para1:PDisplay; para2:Pcint; para3:Pcint):cint;cdecl;
T_XDisplayPlanes=function(para1:PDisplay; para2:cint):cint;cdecl;
T_XDisplayWidth=function(para1:PDisplay; para2:cint):cint;cdecl;
T_XDisplayWidthMM=function(para1:PDisplay; para2:cint):cint;cdecl;
T_XDrawArc=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:cuint; para7:cuint; para8:cint; para9:cint):cint;cdecl;
T_XDrawArcs=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:PXArc; para5:cint):cint;cdecl;
T_XDrawImageString=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:Pchar; para7:cint):cint;cdecl;
T_XDrawImageString16=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:PXChar2b; para7:cint):cint;cdecl;
T_XDrawLine=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:cint; para7:cint):cint;cdecl;
T_XDrawLines=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:PXPoint; para5:cint; para6:cint):cint;cdecl;
T_XDrawPoint=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint):cint;cdecl;
T_XDrawPoints=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:PXPoint; para5:cint; para6:cint):cint;cdecl;
T_XDrawRectangle=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:cuint; para7:cuint):cint;cdecl;
T_XDrawRectangles=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:PXRectangle; para5:cint):cint;cdecl;
T_XDrawSegments=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:PXSegment; para5:cint):cint;cdecl;
T_XDrawString=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:Pchar; para7:cint):cint;cdecl;
T_XDrawString16=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:PXChar2b; para7:cint):cint;cdecl;
T_XDrawText=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:PXTextItem; para7:cint):cint;cdecl;
T_XDrawText16=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:PXTextItem16; para7:cint):cint;cdecl;
T_XEnableAccessControl=function(para1:PDisplay):cint;cdecl;
T_XEventsQueued=function(para1:PDisplay; para2:cint):cint;cdecl;
T_XFetchName=function(para1:PDisplay; para2:TWindow; para3:PPchar):TStatus;cdecl;
T_XFillArc=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:cuint; para7:cuint; para8:cint; para9:cint):cint;cdecl;
T_XFillArcs=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:PXArc; para5:cint):cint;cdecl;
T_XFillPolygon=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:PXPoint; para5:cint; para6:cint; para7:cint):cint;cdecl;
T_XFillRectangle=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:cuint; para7:cuint):cint;cdecl;
T_XFillRectangles=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:PXRectangle; para5:cint):cint;cdecl;
T_XFlush=function(para1:PDisplay):cint;cdecl;
T_XForceScreenSaver=function(para1:PDisplay; para2:cint):cint;cdecl;
T_XFree=function(para1:pointer):cint;cdecl;
T_XFreeColormap=function(para1:PDisplay; para2:TColormap):cint;cdecl;
T_XFreeColors=function(para1:PDisplay; para2:TColormap; para3:Pculong; para4:cint; para5:culong):cint;cdecl;
T_XFreeCursor=function(ADisplay:PDisplay; ACursor:TCursor):cint;cdecl;
T_XFreeExtensionList=function(para1:PPchar):cint;cdecl;
T_XFreeFont=function(para1:PDisplay; para2:PXFontStruct):cint;cdecl;
T_XFreeFontInfo=function(para1:PPchar; para2:PXFontStruct; para3:cint):cint;cdecl;
T_XFreeFontNames=function(para1:PPchar):cint;cdecl;
T_XFreeFontPath=function(para1:PPchar):cint;cdecl;
T_XFreeGC=function(para1:PDisplay; para2:TGC):cint;cdecl;
T_XFreeModifiermap=function(para1:PXModifierKeymap):cint;cdecl;
T_XFreePixmap=function(para1:PDisplay; para2:TPixmap):cint;cdecl;
T_XGeometry=function(para1:PDisplay; para2:cint; para3:Pchar; para4:Pchar; para5:cuint; para6:cuint; para7:cuint; para8:cint; para9:cint; para10:Pcint; para11:Pcint; para12:Pcint; para13:Pcint):cint;cdecl;
T_XGetErrorDatabaseText=function(para1:PDisplay; para2:Pchar; para3:Pchar; para4:Pchar; para5:Pchar; para6:cint):cint;cdecl;
T_XGetErrorText=function(para1:PDisplay; para2:cint; para3:Pchar; para4:cint):cint;cdecl;
T_XGetFontProperty=function(para1:PXFontStruct; para2:TAtom; para3:Pculong):TBoolResult;cdecl;
T_XGetGCValues=function(para1:PDisplay; para2:TGC; para3:culong; para4:PXGCValues):TStatus;cdecl;
T_XGetGeometry=function(para1:PDisplay; para2:TDrawable; para3:PWindow; para4:Pcint; para5:Pcint; para6:Pcuint; para7:Pcuint; para8:Pcuint; para9:Pcuint):TStatus;cdecl;
T_XGetIconName=function(para1:PDisplay; para2:TWindow; para3:PPchar):TStatus;cdecl;
T_XGetInputFocus=function(para1:PDisplay; para2:PWindow; para3:Pcint):cint;cdecl;
T_XGetKeyboardControl=function(para1:PDisplay; para2:PXKeyboardState):cint;cdecl;
T_XGetPointerControl=function(para1:PDisplay; para2:Pcint; para3:Pcint; para4:Pcint):cint;cdecl;
T_XGetPointerMapping=function(para1:PDisplay; para2:Pcuchar; para3:cint):cint;cdecl;
T_XGetScreenSaver=function(para1:PDisplay; para2:Pcint; para3:Pcint; para4:Pcint; para5:Pcint):cint;cdecl;
T_XGetTransientForHint=function(para1:PDisplay; para2:TWindow; para3:PWindow):TStatus;cdecl;
T_XGetWindowProperty=function(para1:PDisplay; para2:TWindow; para3:TAtom; para4:clong; para5:clong; para6:TBool; para7:TAtom; para8:PAtom; para9:Pcint; para10:Pculong; para11:Pculong; para12:PPcuchar):cint;cdecl;
T_XGetWindowAttributes=function(para1:PDisplay; para2:TWindow; para3:PXWindowAttributes):TStatus;cdecl;
T_XGrabButton=function(para1:PDisplay; para2:cuint; para3:cuint; para4:TWindow; para5:TBool; para6:cuint; para7:cint; para8:cint; para9:TWindow; para10:TCursor):cint;cdecl;
T_XGrabKey=function(para1:PDisplay; para2:cint; para3:cuint; para4:TWindow; para5:TBool; para6:cint; para7:cint):cint;cdecl;
T_XGrabKeyboard=function(para1:PDisplay; para2:TWindow; para3:TBool; para4:cint; para5:cint; para6:TTime):cint;cdecl;
T_XGrabPointer=function(para1:PDisplay; para2:TWindow; para3:TBool; para4:cuint; para5:cint; para6:cint; para7:TWindow; para8:TCursor; para9:TTime):cint;cdecl;
T_XGrabServer=function(para1:PDisplay):cint;cdecl;
T_XHeightMMOfScreen=function(para1:PScreen):cint;cdecl;
T_XHeightOfScreen=function(para1:PScreen):cint;cdecl;
T_XIfEvent=function(para1:PDisplay; para2:PXEvent; para3:funcifevent; para4:TXPointer):cint;cdecl;
T_XImageByteOrder=function(para1:PDisplay):cint;cdecl;
T_XInstallColormap=function(para1:PDisplay; para2:TColormap):cint;cdecl;
T_XKeysymToKeycode=function(para1:PDisplay; para2:TKeySym):TKeyCode;cdecl;
T_XKillClient=function(para1:PDisplay; para2:TXID):cint;cdecl;
T_XLookupColor=function(para1:PDisplay; para2:TColormap; para3:Pchar; para4:PXColor; para5:PXColor):TStatus;cdecl;
T_XLowerWindow=function(ADisplay:PDisplay; AWindow:TWindow):cint;cdecl;
T_XMapRaised=function(ADisplay:PDisplay; AWindow:TWindow):cint;cdecl;
T_XMapSubwindows=function(ADisplay:PDisplay; AWindow:TWindow):cint;cdecl;
T_XMapWindow=function(ADisplay:PDisplay; AWindow:TWindow):cint;cdecl;
T_XMaskEvent=function(para1:PDisplay; para2:clong; para3:PXEvent):cint;cdecl;
T_XMaxCmapsOfScreen=function(para1:PScreen):cint;cdecl;
T_XMinCmapsOfScreen=function(para1:PScreen):cint;cdecl;
T_XMoveResizeWindow=function(ADisplay:PDisplay; AWindow:TWindow; AX:cint; AY:cint; AWidth:cuint; AHeight:cuint):cint;cdecl;
T_XMoveWindow=function(ADisplay:PDisplay; AWindow:TWindow; AX:cint; AY:cint):cint;cdecl;
T_XNextEvent=function(ADisplay:PDisplay; AEvent:PXEvent):cint;cdecl;
T_XNoOp=function(para1:PDisplay):cint;cdecl;
T_XParseColor=function(para1:PDisplay; para2:TColormap; para3:Pchar; para4:PXColor):TStatus;cdecl;
T_XParseGeometry=function(para1:Pchar; para2:Pcint; para3:Pcint; para4:Pcuint; para5:Pcuint):cint;cdecl;
T_XPeekEvent=function(ADisplay:PDisplay; AEvent:PXEvent):cint;cdecl;
T_XPeekIfEvent=function(para1:PDisplay; para2:PXEvent; para3:funcifevent; para4:TXPointer):cint;cdecl;
T_XPending=function(para1:PDisplay):cint;cdecl;
T_XPlanesOfScreen=function(para1:PScreen):cint;cdecl;
T_XProtocolRevision=function(para1:PDisplay):cint;cdecl;
T_XProtocolVersion=function(para1:PDisplay):cint;cdecl;
T_XPutBackEvent=function(para1:PDisplay; para2:PXEvent):cint;cdecl;
T_XPutImage=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:PXImage; para5:cint; para6:cint; para7:cint; para8:cint; para9:cuint; para10:cuint):cint;cdecl;
T_XQLength=function(para1:PDisplay):cint;cdecl;
T_XQueryBestCursor=function(para1:PDisplay; para2:TDrawable; para3:cuint; para4:cuint; para5:Pcuint; para6:Pcuint):TStatus;cdecl;
T_XQueryBestSize=function(para1:PDisplay; para2:cint; para3:TDrawable; para4:cuint; para5:cuint; para6:Pcuint; para7:Pcuint):TStatus;cdecl;
T_XQueryBestStipple=function(para1:PDisplay; para2:TDrawable; para3:cuint; para4:cuint; para5:Pcuint; para6:Pcuint):TStatus;cdecl;
T_XQueryBestTile=function(para1:PDisplay; para2:TDrawable; para3:cuint; para4:cuint; para5:Pcuint; para6:Pcuint):TStatus;cdecl;
T_XQueryColor=function(para1:PDisplay; para2:TColormap; para3:PXColor):cint;cdecl;
T_XQueryColors=function(para1:PDisplay; para2:TColormap; para3:PXColor; para4:cint):cint;cdecl;
T_XQueryExtension=function(para1:PDisplay; para2:Pchar; para3:Pcint; para4:Pcint; para5:Pcint):TBoolResult;cdecl;
T_XQueryKeymap=function(para1:PDisplay; para2:pchararr32):cint;cdecl;
T_XQueryPointer=function(para1:PDisplay; para2:TWindow; para3:PWindow; para4:PWindow; para5:Pcint; para6:Pcint; para7:Pcint; para8:Pcint; para9:Pcuint):TBoolResult;cdecl;
T_XQueryTextExtents=function(para1:PDisplay; para2:TXID; para3:Pchar; para4:cint; para5:Pcint; para6:Pcint; para7:Pcint; para8:PXCharStruct):cint;cdecl;
T_XQueryTextExtents16=function(para1:PDisplay; para2:TXID; para3:PXChar2b; para4:cint; para5:Pcint; para6:Pcint; para7:Pcint; para8:PXCharStruct):cint;cdecl;
T_XQueryTree=function(para1:PDisplay; para2:TWindow; para3:PWindow; para4:PWindow; para5:PPWindow; para6:Pcuint):TStatus;cdecl;
T_XRaiseWindow=function(para1:PDisplay; para2:TWindow):cint;cdecl;
T_XReadBitmapFile=function(para1:PDisplay; para2:TDrawable; para3:Pchar; para4:Pcuint; para5:Pcuint; para6:PPixmap; para7:Pcint; para8:Pcint):cint;cdecl;
T_XReadBitmapFileData=function(para1:Pchar; para2:Pcuint; para3:Pcuint; para4:PPcuchar; para5:Pcint; para6:Pcint):cint;cdecl;
T_XRebindKeysym=function(para1:PDisplay; para2:TKeySym; para3:PKeySym; para4:cint; para5:Pcuchar; para6:cint):cint;cdecl;
T_XRecolorCursor=function(para1:PDisplay; para2:TCursor; para3:PXColor; para4:PXColor):cint;cdecl;
T_XRefreshKeyboardMapping=function(para1:PXMappingEvent):cint;cdecl;
T_XRemoveFromSaveSet=function(para1:PDisplay; para2:TWindow):cint;cdecl;
T_XRemoveHost=function(para1:PDisplay; para2:PXHostAddress):cint;cdecl;
T_XRemoveHosts=function(para1:PDisplay; para2:PXHostAddress; para3:cint):cint;cdecl;
T_XReparentWindow=function(para1:PDisplay; para2:TWindow; para3:TWindow; para4:cint; para5:cint):cint;cdecl;
T_XResetScreenSaver=function(para1:PDisplay):cint;cdecl;
T_XResizeWindow=function(para1:PDisplay; para2:TWindow; para3:cuint; para4:cuint):cint;cdecl;
T_XRestackWindows=function(para1:PDisplay; para2:PWindow; para3:cint):cint;cdecl;
T_XRotateBuffers=function(para1:PDisplay; para2:cint):cint;cdecl;
T_XRotateWindowProperties=function(para1:PDisplay; para2:TWindow; para3:PAtom; para4:cint; para5:cint):cint;cdecl;
T_XScreenCount=function(para1:PDisplay):cint;cdecl;
T_XSelectInput=function(ADisplay:PDisplay; AWindow:TWindow; AEventMask:clong):cint;cdecl;
T_XSendEvent=function(para1:PDisplay; para2:TWindow; para3:TBool; para4:clong; para5:PXEvent):TStatus;cdecl;
T_XSetAccessControl=function(para1:PDisplay; para2:cint):cint;cdecl;
T_XSetArcMode=function(para1:PDisplay; para2:TGC; para3:cint):cint;cdecl;
T_XSetBackground=function(para1:PDisplay; para2:TGC; para3:culong):cint;cdecl;
T_XSetClipMask=function(para1:PDisplay; para2:TGC; para3:TPixmap):cint;cdecl;
T_XSetClipOrigin=function(para1:PDisplay; para2:TGC; para3:cint; para4:cint):cint;cdecl;
T_XSetClipRectangles=function(para1:PDisplay; para2:TGC; para3:cint; para4:cint; para5:PXRectangle; para6:cint; para7:cint):cint;cdecl;
T_XSetCloseDownMode=function(para1:PDisplay; para2:cint):cint;cdecl;
T_XSetCommand=function(para1:PDisplay; para2:TWindow; para3:PPchar; para4:cint):cint;cdecl;
T_XSetDashes=function(para1:PDisplay; para2:TGC; para3:cint; para4:Pchar; para5:cint):cint;cdecl;
T_XSetFillRule=function(para1:PDisplay; para2:TGC; para3:cint):cint;cdecl;
T_XSetFillStyle=function(para1:PDisplay; para2:TGC; para3:cint):cint;cdecl;
T_XSetFont=function(para1:PDisplay; para2:TGC; para3:TFont):cint;cdecl;
T_XSetFontPath=function(para1:PDisplay; para2:PPchar; para3:cint):cint;cdecl;
T_XSetForeground=function(para1:PDisplay; para2:TGC; para3:culong):cint;cdecl;
T_XSetFunction=function(para1:PDisplay; para2:TGC; para3:cint):cint;cdecl;
T_XSetGraphicsExposures=function(para1:PDisplay; para2:TGC; para3:TBool):cint;cdecl;
T_XSetIconName=function(para1:PDisplay; para2:TWindow; para3:Pchar):cint;cdecl;
T_XSetInputFocus=function(para1:PDisplay; para2:TWindow; para3:cint; para4:TTime):cint;cdecl;
T_XSetLineAttributes=function(para1:PDisplay; para2:TGC; para3:cuint; para4:cint; para5:cint; para6:cint):cint;cdecl;
T_XSetModifierMapping=function(para1:PDisplay; para2:PXModifierKeymap):cint;cdecl;
T_XSetPlaneMask=function(para1:PDisplay; para2:TGC; para3:culong):cint;cdecl;
T_XSetPointerMapping=function(para1:PDisplay; para2:Pcuchar; para3:cint):cint;cdecl;
T_XSetScreenSaver=function(para1:PDisplay; para2:cint; para3:cint; para4:cint; para5:cint):cint;cdecl;
T_XSetSelectionOwner=function(para1:PDisplay; para2:TAtom; para3:TWindow; para4:TTime):cint;cdecl;
T_XSetState=function(para1:PDisplay; para2:TGC; para3:culong; para4:culong; para5:cint; para6:culong):cint;cdecl;
T_XSetStipple=function(para1:PDisplay; para2:TGC; para3:TPixmap):cint;cdecl;
T_XSetSubwindowMode=function(para1:PDisplay; para2:TGC; para3:cint):cint;cdecl;
T_XSetTSOrigin=function(para1:PDisplay; para2:TGC; para3:cint; para4:cint):cint;cdecl;
T_XSetTile=function(para1:PDisplay; para2:TGC; para3:TPixmap):cint;cdecl;
T_XSetWindowBackground=function(para1:PDisplay; para2:TWindow; para3:culong):cint;cdecl;
T_XSetWindowBackgroundPixmap=function(para1:PDisplay; para2:TWindow; para3:TPixmap):cint;cdecl;
T_XSetWindowBorder=function(para1:PDisplay; para2:TWindow; para3:culong):cint;cdecl;
T_XSetWindowBorderPixmap=function(para1:PDisplay; para2:TWindow; para3:TPixmap):cint;cdecl;
T_XSetWindowBorderWidth=function(para1:PDisplay; para2:TWindow; para3:cuint):cint;cdecl;
T_XSetWindowColormap=function(para1:PDisplay; para2:TWindow; para3:TColormap):cint;cdecl;
T_XStoreBuffer=function(para1:PDisplay; para2:Pchar; para3:cint; para4:cint):cint;cdecl;
T_XStoreBytes=function(para1:PDisplay; para2:Pchar; para3:cint):cint;cdecl;
T_XStoreColor=function(para1:PDisplay; para2:TColormap; para3:PXColor):cint;cdecl;
T_XStoreColors=function(para1:PDisplay; para2:TColormap; para3:PXColor; para4:cint):cint;cdecl;
T_XStoreName=function(para1:PDisplay; para2:TWindow; para3:Pchar):cint;cdecl;
T_XStoreNamedColor=function(para1:PDisplay; para2:TColormap; para3:Pchar; para4:culong; para5:cint):cint;cdecl;
T_XSync=function(para1:PDisplay; para2:TBool):cint;cdecl;
T_XTextExtents=function(para1:PXFontStruct; para2:Pchar; para3:cint; para4:Pcint; para5:Pcint; para6:Pcint; para7:PXCharStruct):cint;cdecl;
T_XTextExtents16=function(para1:PXFontStruct; para2:PXChar2b; para3:cint; para4:Pcint; para5:Pcint; para6:Pcint; para7:PXCharStruct):cint;cdecl;
T_XTextWidth=function(para1:PXFontStruct; para2:Pchar; para3:cint):cint;cdecl;
T_XTextWidth16=function(para1:PXFontStruct; para2:PXChar2b; para3:cint):cint;cdecl;
T_XTranslateCoordinates=function(ADisplay:PDisplay; ASrcWindow:TWindow; ADestWindow:TWindow; ASrcX:cint; ASrcY:cint; ADestXReturn:Pcint; ADestYReturn:Pcint; AChildReturn:PWindow):TBool;cdecl;
T_XUndefineCursor=function(para1:PDisplay; para2:TWindow):cint;cdecl;
T_XUngrabButton=function(para1:PDisplay; para2:cuint; para3:cuint; para4:TWindow):cint;cdecl;
T_XUngrabKey=function(para1:PDisplay; para2:cint; para3:cuint; para4:TWindow):cint;cdecl;
T_XUngrabKeyboard=function(para1:PDisplay; para2:TTime):cint;cdecl;
T_XUngrabPointer=function(para1:PDisplay; para2:TTime):cint;cdecl;
T_XUngrabServer=function(para1:PDisplay):cint;cdecl;
T_XUninstallColormap=function(para1:PDisplay; para2:TColormap):cint;cdecl;
T_XUnloadFont=function(para1:PDisplay; para2:TFont):cint;cdecl;
T_XUnmapSubwindows=function(ADisplay:PDisplay; AWindow:TWindow):cint;cdecl;
T_XUnmapWindow=function(ADisplay:PDisplay; AWindow:TWindow):cint;cdecl;
T_XVendorRelease=function(para1:PDisplay):cint;cdecl;
T_XWarpPointer=function(para1:PDisplay; para2:TWindow; para3:TWindow; para4:cint; para5:cint; para6:cuint; para7:cuint; para8:cint; para9:cint):cint;cdecl;
T_XWidthMMOfScreen=function(para1:PScreen):cint;cdecl;
T_XWidthOfScreen=function(para1:PScreen):cint;cdecl;
T_XWindowEvent=function(para1:PDisplay; para2:TWindow; para3:clong; para4:PXEvent):cint;cdecl;
T_XWriteBitmapFile=function(para1:PDisplay; para2:Pchar; para3:TPixmap; para4:cuint; para5:cuint; para6:cint; para7:cint):cint;cdecl;
T_XSetLocaleModifiers=function(para1:Pchar):Pchar;cdecl;
T_XOpenOM=function(para1:PDisplay; para2:PXrmHashBucketRec; para3:Pchar; para4:Pchar):TXOM;cdecl;
T_XCloseOM=function(para1:TXOM):TStatus;cdecl;
T_XSetOMValues=function(para1:TXOM; dotdotdot:array of const):Pchar;cdecl;
T_XGetOMValues=function(para1:TXOM; dotdotdot:array of const):Pchar;cdecl;
T_XDisplayOfOM=function(para1:TXOM):PDisplay;cdecl;
T_XLocaleOfOM=function(para1:TXOM):Pchar;cdecl;
T_XCreateOC=function(para1:TXOM; dotdotdot:array of const):TXOC;cdecl;
T_XDestroyOC=procedure(para1:TXOC);cdecl;
T_XOMOfOC=function(para1:TXOC):TXOM;cdecl;
T_XSetOCValues=function(para1:TXOC; dotdotdot:array of const):Pchar;cdecl;
T_XGetOCValues=function(para1:TXOC; dotdotdot:array of const):Pchar;cdecl;
T_XCreateFontSet=function(para1:PDisplay; para2:Pchar; para3:PPPchar; para4:Pcint; para5:PPchar):TXFontSet;cdecl;
T_XFreeFontSet=procedure(para1:PDisplay; para2:TXFontSet);cdecl;
T_XFontsOfFontSet=function(para1:TXFontSet; para2:PPPXFontStruct; para3:PPPchar):cint;cdecl;
T_XBaseFontNameListOfFontSet=function(para1:TXFontSet):Pchar;cdecl;
T_XLocaleOfFontSet=function(para1:TXFontSet):Pchar;cdecl;
T_XContextDependentDrawing=function(para1:TXFontSet):TBoolResult;cdecl;
T_XDirectionalDependentDrawing=function(para1:TXFontSet):TBoolResult;cdecl;
T_XContextualDrawing=function(para1:TXFontSet):TBoolResult;cdecl;
T_XExtentsOfFontSet=function(para1:TXFontSet):PXFontSetExtents;cdecl;
T_XmbTextEscapement=function(para1:TXFontSet; para2:Pchar; para3:cint):cint;cdecl;
T_XwcTextEscapement=function(para1:TXFontSet; para2:PWideChar; para3:cint):cint;cdecl;
T_Xutf8TextEscapement=function(para1:TXFontSet; para2:Pchar; para3:cint):cint;cdecl;
T_XmbTextExtents=function(para1:TXFontSet; para2:Pchar; para3:cint; para4:PXRectangle; para5:PXRectangle):cint;cdecl;
T_XwcTextExtents=function(para1:TXFontSet; para2:PWideChar; para3:cint; para4:PXRectangle; para5:PXRectangle):cint;cdecl;
T_Xutf8TextExtents=function(para1:TXFontSet; para2:Pchar; para3:cint; para4:PXRectangle; para5:PXRectangle):cint;cdecl;
T_XmbTextPerCharExtents=function(para1:TXFontSet; para2:Pchar; para3:cint; para4:PXRectangle; para5:PXRectangle; para6:cint; para7:Pcint; para8:PXRectangle; para9:PXRectangle):TStatus;cdecl;
T_XwcTextPerCharExtents=function(para1:TXFontSet; para2:PWideChar; para3:cint; para4:PXRectangle; para5:PXRectangle; para6:cint; para7:Pcint; para8:PXRectangle; para9:PXRectangle):TStatus;cdecl;
T_Xutf8TextPerCharExtents=function(para1:TXFontSet; para2:Pchar; para3:cint; para4:PXRectangle; para5:PXRectangle; para6:cint; para7:Pcint; para8:PXRectangle; para9:PXRectangle):TStatus;cdecl;
T_XmbDrawText=procedure(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:PXmbTextItem; para7:cint);cdecl;
T_XwcDrawText=procedure(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:PXwcTextItem; para7:cint);cdecl;
T_Xutf8DrawText=procedure(para1:PDisplay; para2:TDrawable; para3:TGC; para4:cint; para5:cint; para6:PXmbTextItem; para7:cint);cdecl;
T_XmbDrawString=procedure(para1:PDisplay; para2:TDrawable; para3:TXFontSet; para4:TGC; para5:cint; para6:cint; para7:Pchar; para8:cint);cdecl;
T_XwcDrawString=procedure(para1:PDisplay; para2:TDrawable; para3:TXFontSet; para4:TGC; para5:cint; para6:cint; para7:PWideChar; para8:cint);cdecl;
T_Xutf8DrawString=procedure(para1:PDisplay; para2:TDrawable; para3:TXFontSet; para4:TGC; para5:cint; para6:cint; para7:Pchar; para8:cint);cdecl;
T_XmbDrawImageString=procedure(para1:PDisplay; para2:TDrawable; para3:TXFontSet; para4:TGC; para5:cint; para6:cint; para7:Pchar; para8:cint);cdecl;
T_XwcDrawImageString=procedure(para1:PDisplay; para2:TDrawable; para3:TXFontSet; para4:TGC; para5:cint; para6:cint; para7:PWideChar; para8:cint);cdecl;
T_Xutf8DrawImageString=procedure(para1:PDisplay; para2:TDrawable; para3:TXFontSet; para4:TGC; para5:cint; para6:cint; para7:Pchar; para8:cint);cdecl;
T_XOpenIM=function(para1:PDisplay; para2:PXrmHashBucketRec; para3:Pchar; para4:Pchar):PXIM;cdecl;
T_XCloseIM=function(para1:PXIM):TStatus;cdecl;
T_XGetIMValues=function(para1:PXIM; dotdotdot:array of const):Pchar;cdecl;
T_XSetIMValues=function(para1:PXIM; dotdotdot:array of const):Pchar;cdecl;
T_XDisplayOfIM=function(para1:PXIM):PDisplay;cdecl;
T_XLocaleOfIM=function(para1:PXIM):Pchar;cdecl;
T_XCreateIC=function(para1:PXIM; dotdotdot:array of const):PXIC;cdecl;
T_XDestroyIC=procedure(para1:PXIC);cdecl;
T_XSetICFocus=procedure(para1:PXIC);cdecl;
T_XUnsetICFocus=procedure(para1:PXIC);cdecl;
T_XwcResetIC=function(para1:PXIC):PWideChar;cdecl;
T_XmbResetIC=function(para1:PXIC):Pchar;cdecl;
T_Xutf8ResetIC=function(para1:PXIC):Pchar;cdecl;
T_XSetICValues=function(para1:PXIC; dotdotdot:array of const):Pchar;cdecl;
T_XGetICValues=function(para1:PXIC; dotdotdot:array of const):Pchar;cdecl;
T_XIMOfIC=function(para1:PXIC):PXIM;cdecl;
T_XFilterEvent=function(para1:PXEvent; para2:TWindow):TBoolResult;cdecl;
T_XmbLookupString=function(para1:PXIC; para2:PXKeyPressedEvent; para3:Pchar; para4:cint; para5:PKeySym; para6:PStatus):cint;cdecl;
T_XwcLookupString=function(para1:PXIC; para2:PXKeyPressedEvent; para3:PWideChar; para4:cint; para5:PKeySym; para6:PStatus):cint;cdecl;
T_Xutf8LookupString=function(para1:PXIC; para2:PXKeyPressedEvent; para3:Pchar; para4:cint; para5:PKeySym; para6:PStatus):cint;cdecl;
T_XVaCreateNestedList=function(unused:cint; dotdotdot:array of const):TXVaNestedList;cdecl;
T_XRegisterIMInstantiateCallback=function(para1:PDisplay; para2:PXrmHashBucketRec; para3:Pchar; para4:Pchar; para5:TXIDProc; para6:TXPointer):TBoolResult;cdecl;
T_XUnregisterIMInstantiateCallback=function(para1:PDisplay; para2:PXrmHashBucketRec; para3:Pchar; para4:Pchar; para5:TXIDProc; para6:TXPointer):TBoolResult;cdecl;
T_XInternalConnectionNumbers=function(para1:PDisplay; para2:PPcint; para3:Pcint):TStatus;cdecl;
T_XProcessInternalConnection=procedure(para1:PDisplay; para2:cint);cdecl;
T_XAddConnectionWatch=function(para1:PDisplay; para2:TXConnectionWatchProc; para3:TXPointer):TStatus;cdecl;
T_XRemoveConnectionWatch=procedure(para1:PDisplay; para2:TXConnectionWatchProc; para3:TXPointer);cdecl;
T_XSetAuthorization=procedure(para1:Pchar; para2:cint; para3:Pchar; para4:cint);cdecl;
T_XGetEventData=function(dpy: PDisplay; cookie: PXGenericEventCookie) : TBoolResult;cdecl;
T_XFreeEventData=procedure(dpy: PDisplay; cookie: PXGenericEventCookie);cdecl;



var
XLoadQueryFont : T_XLoadQueryFont;
XQueryFont : T_XQueryFont;
XGetMotionEvents : T_XGetMotionEvents;
XDeleteModifiermapEntry : T_XDeleteModifiermapEntry;
XGetModifierMapping : T_XGetModifierMapping;
XInsertModifiermapEntry : T_XInsertModifiermapEntry;
XNewModifiermap : T_XNewModifiermap;
XCreateImage : T_XCreateImage;
XInitImage : T_XInitImage;
XGetImage : T_XGetImage;
XGetSubImage : T_XGetSubImage;
XOpenDisplay : T_XOpenDisplay;
XFetchBytes : T_XFetchBytes;
XFetchBuffer : T_XFetchBuffer;
XGetAtomName : T_XGetAtomName;
XGetAtomNames : T_XGetAtomNames;
XGetDefault : T_XGetDefault;
XDisplayName : T_XDisplayName;
XKeysymToString : T_XKeysymToString;
_XSynchronize : T_XSynchronize;
XSetAfterFunction : T_XSetAfterFunction;
_XInternAtom : T_XInternAtom;
_XInternAtoms : T_XInternAtoms;
XCopyColormapAndFree : T_XCopyColormapAndFree;
XCreateColormap : T_XCreateColormap;
XCreatePixmapCursor : T_XCreatePixmapCursor;
XCreateGlyphCursor : T_XCreateGlyphCursor;
XCreateFontCursor : T_XCreateFontCursor;
XLoadFont : T_XLoadFont;
XCreateGC : T_XCreateGC;
XGContextFromGC : T_XGContextFromGC;
XFlushGC : T_XFlushGC;
XCreatePixmap : T_XCreatePixmap;
XCreateBitmapFromData : T_XCreateBitmapFromData;
XCreatePixmapFromBitmapData : T_XCreatePixmapFromBitmapData;
XCreateSimpleWindow : T_XCreateSimpleWindow;
XGetSelectionOwner : T_XGetSelectionOwner;
XCreateWindow : T_XCreateWindow;
XListInstalledColormaps : T_XListInstalledColormaps;
XListFonts : T_XListFonts;
XListFontsWithInfo : T_XListFontsWithInfo;
XGetFontPath : T_XGetFontPath;
XListExtensions : T_XListExtensions;
XListProperties : T_XListProperties;
XListHosts : T_XListHosts;
XKeycodeToKeysym : T_XKeycodeToKeysym;
XLookupKeysym : T_XLookupKeysym;
XGetKeyboardMapping : T_XGetKeyboardMapping;
XStringToKeysym : T_XStringToKeysym;
XMaxRequestSize : T_XMaxRequestSize;
XExtendedMaxRequestSize : T_XExtendedMaxRequestSize;
XResourceManagerString : T_XResourceManagerString;
XScreenResourceString : T_XScreenResourceString;
XDisplayMotionBufferSize : T_XDisplayMotionBufferSize;
XVisualIDFromVisual : T_XVisualIDFromVisual;
XLockDisplay : T_XLockDisplay;
XUnlockDisplay : T_XUnlockDisplay;
XInitExtension : T_XInitExtension;
XAddExtension : T_XAddExtension;
XFindOnExtensionList : T_XFindOnExtensionList;
XEHeadOfExtensionList : T_XEHeadOfExtensionList;
XRootWindow : T_XRootWindow;
XDefaultRootWindow : T_XDefaultRootWindow;
XRootWindowOfScreen : T_XRootWindowOfScreen;
XDefaultVisual : T_XDefaultVisual;
XDefaultVisualOfScreen : T_XDefaultVisualOfScreen;
XDefaultGC : T_XDefaultGC;
XDefaultGCOfScreen : T_XDefaultGCOfScreen;
XBlackPixel : T_XBlackPixel;
XWhitePixel : T_XWhitePixel;
XBlackPixelOfScreen : T_XBlackPixelOfScreen;
XWhitePixelOfScreen : T_XWhitePixelOfScreen;
XNextRequest : T_XNextRequest;
XLastKnownRequestProcessed : T_XLastKnownRequestProcessed;
XServerVendor : T_XServerVendor;
XDisplayString : T_XDisplayString;
XDefaultColormap : T_XDefaultColormap;
XDefaultColormapOfScreen : T_XDefaultColormapOfScreen;
XDisplayOfScreen : T_XDisplayOfScreen;
XScreenOfDisplay : T_XScreenOfDisplay;
XDefaultScreenOfDisplay : T_XDefaultScreenOfDisplay;
XEventMaskOfScreen : T_XEventMaskOfScreen;
XScreenNumberOfScreen : T_XScreenNumberOfScreen;
XSetErrorHandler : T_XSetErrorHandler;
XSetIOErrorHandler : T_XSetIOErrorHandler;
XListPixmapFormats : T_XListPixmapFormats;
XListDepths : T_XListDepths;
XReconfigureWMWindow : T_XReconfigureWMWindow;
XGetWMProtocols : T_XGetWMProtocols;
XSetWMProtocols : T_XSetWMProtocols;
XIconifyWindow : T_XIconifyWindow;
XWithdrawWindow : T_XWithdrawWindow;
XGetCommand : T_XGetCommand;
XGetWMColormapWindows : T_XGetWMColormapWindows;
XSetWMColormapWindows : T_XSetWMColormapWindows;
XFreeStringList : T_XFreeStringList;
XSetTransientForHint : T_XSetTransientForHint;
XActivateScreenSaver : T_XActivateScreenSaver;
XAddHost : T_XAddHost;
XAddHosts : T_XAddHosts;
XAddToExtensionList : T_XAddToExtensionList;
XAddToSaveSet : T_XAddToSaveSet;
XAllocColor : T_XAllocColor;
XAllocColorCells : T_XAllocColorCells;
XAllocColorPlanes : T_XAllocColorPlanes;
XAllocNamedColor : T_XAllocNamedColor;
XAllowEvents : T_XAllowEvents;
XAutoRepeatOff : T_XAutoRepeatOff;
XAutoRepeatOn : T_XAutoRepeatOn;
XBell : T_XBell;
XBitmapBitOrder : T_XBitmapBitOrder;
XBitmapPad : T_XBitmapPad;
XBitmapUnit : T_XBitmapUnit;
XCellsOfScreen : T_XCellsOfScreen;
XChangeActivePointerGrab : T_XChangeActivePointerGrab;
XChangeGC : T_XChangeGC;
XChangeKeyboardControl : T_XChangeKeyboardControl;
XChangeKeyboardMapping : T_XChangeKeyboardMapping;
XChangePointerControl : T_XChangePointerControl;
XChangeProperty : T_XChangeProperty;
XChangeSaveSet : T_XChangeSaveSet;
XChangeWindowAttributes : T_XChangeWindowAttributes;
XCheckIfEvent : T_XCheckIfEvent;
XCheckMaskEvent : T_XCheckMaskEvent;
XCheckTypedEvent : T_XCheckTypedEvent;
XCheckTypedWindowEvent : T_XCheckTypedWindowEvent;
XCheckWindowEvent : T_XCheckWindowEvent;
XCirculateSubwindows : T_XCirculateSubwindows;
XCirculateSubwindowsDown : T_XCirculateSubwindowsDown;
XCirculateSubwindowsUp : T_XCirculateSubwindowsUp;
_XClearArea : T_XClearArea;
XClearWindow : T_XClearWindow;
XCloseDisplay : T_XCloseDisplay;
XConfigureWindow : T_XConfigureWindow;
XConnectionNumber : T_XConnectionNumber;
XConvertSelection : T_XConvertSelection;
XCopyArea : T_XCopyArea;
XCopyGC : T_XCopyGC;
XCopyPlane : T_XCopyPlane;
XDefaultDepth : T_XDefaultDepth;
XDefaultDepthOfScreen : T_XDefaultDepthOfScreen;
XDefaultScreen : T_XDefaultScreen;
XDefineCursor : T_XDefineCursor;
XDeleteProperty : T_XDeleteProperty;
XDestroyWindow : T_XDestroyWindow;
XDestroySubwindows : T_XDestroySubwindows;
XDoesBackingStore : T_XDoesBackingStore;
XDoesSaveUnders : T_XDoesSaveUnders;
XDisableAccessControl : T_XDisableAccessControl;
XDisplayCells : T_XDisplayCells;
XDisplayHeight : T_XDisplayHeight;
XDisplayHeightMM : T_XDisplayHeightMM;
XDisplayKeycodes : T_XDisplayKeycodes;
XDisplayPlanes : T_XDisplayPlanes;
XDisplayWidth : T_XDisplayWidth;
XDisplayWidthMM : T_XDisplayWidthMM;
XDrawArc : T_XDrawArc;
XDrawArcs : T_XDrawArcs;
XDrawImageString : T_XDrawImageString;
XDrawImageString16 : T_XDrawImageString16;
XDrawLine : T_XDrawLine;
XDrawLines : T_XDrawLines;
XDrawPoint : T_XDrawPoint;
XDrawPoints : T_XDrawPoints;
XDrawRectangle : T_XDrawRectangle;
XDrawRectangles : T_XDrawRectangles;
XDrawSegments : T_XDrawSegments;
XDrawString : T_XDrawString;
XDrawString16 : T_XDrawString16;
XDrawText : T_XDrawText;
XDrawText16 : T_XDrawText16;
XEnableAccessControl : T_XEnableAccessControl;
XEventsQueued : T_XEventsQueued;
XFetchName : T_XFetchName;
XFillArc : T_XFillArc;
XFillArcs : T_XFillArcs;
XFillPolygon : T_XFillPolygon;
XFillRectangle : T_XFillRectangle;
XFillRectangles : T_XFillRectangles;
XFlush : T_XFlush;
XForceScreenSaver : T_XForceScreenSaver;
XFree : T_XFree;
XFreeColormap : T_XFreeColormap;
XFreeColors : T_XFreeColors;
XFreeCursor : T_XFreeCursor;
XFreeExtensionList : T_XFreeExtensionList;
XFreeFont : T_XFreeFont;
XFreeFontInfo : T_XFreeFontInfo;
XFreeFontNames : T_XFreeFontNames;
XFreeFontPath : T_XFreeFontPath;
XFreeGC : T_XFreeGC;
XFreeModifiermap : T_XFreeModifiermap;
XFreePixmap : T_XFreePixmap;
XGeometry : T_XGeometry;
XGetErrorDatabaseText : T_XGetErrorDatabaseText;
XGetErrorText : T_XGetErrorText;
XGetFontProperty : T_XGetFontProperty;
XGetGCValues : T_XGetGCValues;
XGetGeometry : T_XGetGeometry;
XGetIconName : T_XGetIconName;
XGetInputFocus : T_XGetInputFocus;
XGetKeyboardControl : T_XGetKeyboardControl;
XGetPointerControl : T_XGetPointerControl;
XGetPointerMapping : T_XGetPointerMapping;
XGetScreenSaver : T_XGetScreenSaver;
XGetTransientForHint : T_XGetTransientForHint;
_XGetWindowProperty : T_XGetWindowProperty;
XGetWindowAttributes : T_XGetWindowAttributes;
XGrabButton : T_XGrabButton;
XGrabKey : T_XGrabKey;
_XGrabKeyboard : T_XGrabKeyboard;
_XGrabPointer : T_XGrabPointer;
XGrabServer : T_XGrabServer;
XHeightMMOfScreen : T_XHeightMMOfScreen;
XHeightOfScreen : T_XHeightOfScreen;
XIfEvent : T_XIfEvent;
XImageByteOrder : T_XImageByteOrder;
XInstallColormap : T_XInstallColormap;
XKeysymToKeycode : T_XKeysymToKeycode;
XKillClient : T_XKillClient;
XLookupColor : T_XLookupColor;
XLowerWindow : T_XLowerWindow;
XMapRaised : T_XMapRaised;
XMapSubwindows : T_XMapSubwindows;
XMapWindow : T_XMapWindow;
XMaskEvent : T_XMaskEvent;
XMaxCmapsOfScreen : T_XMaxCmapsOfScreen;
XMinCmapsOfScreen : T_XMinCmapsOfScreen;
XMoveResizeWindow : T_XMoveResizeWindow;
XMoveWindow : T_XMoveWindow;
XNextEvent : T_XNextEvent;
XNoOp : T_XNoOp;
XParseColor : T_XParseColor;
XParseGeometry : T_XParseGeometry;
XPeekEvent : T_XPeekEvent;
XPeekIfEvent : T_XPeekIfEvent;
XPending : T_XPending;
XPlanesOfScreen : T_XPlanesOfScreen;
XProtocolRevision : T_XProtocolRevision;
XProtocolVersion : T_XProtocolVersion;
XPutBackEvent : T_XPutBackEvent;
XPutImage : T_XPutImage;
XQLength : T_XQLength;
XQueryBestCursor : T_XQueryBestCursor;
XQueryBestSize : T_XQueryBestSize;
XQueryBestStipple : T_XQueryBestStipple;
XQueryBestTile : T_XQueryBestTile;
XQueryColor : T_XQueryColor;
XQueryColors : T_XQueryColors;
XQueryExtension : T_XQueryExtension;
XQueryKeymap : T_XQueryKeymap;
XQueryPointer : T_XQueryPointer;
XQueryTextExtents : T_XQueryTextExtents;
XQueryTextExtents16 : T_XQueryTextExtents16;
XQueryTree : T_XQueryTree;
XRaiseWindow : T_XRaiseWindow;
XReadBitmapFile : T_XReadBitmapFile;
XReadBitmapFileData : T_XReadBitmapFileData;
XRebindKeysym : T_XRebindKeysym;
XRecolorCursor : T_XRecolorCursor;
XRefreshKeyboardMapping : T_XRefreshKeyboardMapping;
XRemoveFromSaveSet : T_XRemoveFromSaveSet;
XRemoveHost : T_XRemoveHost;
XRemoveHosts : T_XRemoveHosts;
XReparentWindow : T_XReparentWindow;
XResetScreenSaver : T_XResetScreenSaver;
XResizeWindow : T_XResizeWindow;
XRestackWindows : T_XRestackWindows;
XRotateBuffers : T_XRotateBuffers;
XRotateWindowProperties : T_XRotateWindowProperties;
XScreenCount : T_XScreenCount;
XSelectInput : T_XSelectInput;
_XSendEvent : T_XSendEvent;
XSetAccessControl : T_XSetAccessControl;
XSetArcMode : T_XSetArcMode;
XSetBackground : T_XSetBackground;
XSetClipMask : T_XSetClipMask;
XSetClipOrigin : T_XSetClipOrigin;
XSetClipRectangles : T_XSetClipRectangles;
XSetCloseDownMode : T_XSetCloseDownMode;
XSetCommand : T_XSetCommand;
XSetDashes : T_XSetDashes;
XSetFillRule : T_XSetFillRule;
XSetFillStyle : T_XSetFillStyle;
XSetFont : T_XSetFont;
XSetFontPath : T_XSetFontPath;
XSetForeground : T_XSetForeground;
XSetFunction : T_XSetFunction;
_XSetGraphicsExposures : T_XSetGraphicsExposures;
XSetIconName : T_XSetIconName;
XSetInputFocus : T_XSetInputFocus;
XSetLineAttributes : T_XSetLineAttributes;
XSetModifierMapping : T_XSetModifierMapping;
XSetPlaneMask : T_XSetPlaneMask;
XSetPointerMapping : T_XSetPointerMapping;
XSetScreenSaver : T_XSetScreenSaver;
XSetSelectionOwner : T_XSetSelectionOwner;
XSetState : T_XSetState;
XSetStipple : T_XSetStipple;
XSetSubwindowMode : T_XSetSubwindowMode;
XSetTSOrigin : T_XSetTSOrigin;
XSetTile : T_XSetTile;
XSetWindowBackground : T_XSetWindowBackground;
XSetWindowBackgroundPixmap : T_XSetWindowBackgroundPixmap;
XSetWindowBorder : T_XSetWindowBorder;
XSetWindowBorderPixmap : T_XSetWindowBorderPixmap;
XSetWindowBorderWidth : T_XSetWindowBorderWidth;
XSetWindowColormap : T_XSetWindowColormap;
XStoreBuffer : T_XStoreBuffer;
XStoreBytes : T_XStoreBytes;
XStoreColor : T_XStoreColor;
XStoreColors : T_XStoreColors;
XStoreName : T_XStoreName;
XStoreNamedColor : T_XStoreNamedColor;
_XSync : T_XSync;
XTextExtents : T_XTextExtents;
XTextExtents16 : T_XTextExtents16;
XTextWidth : T_XTextWidth;
XTextWidth16 : T_XTextWidth16;
XTranslateCoordinates : T_XTranslateCoordinates;
XUndefineCursor : T_XUndefineCursor;
XUngrabButton : T_XUngrabButton;
XUngrabKey : T_XUngrabKey;
XUngrabKeyboard : T_XUngrabKeyboard;
XUngrabPointer : T_XUngrabPointer;
XUngrabServer : T_XUngrabServer;
XUninstallColormap : T_XUninstallColormap;
XUnloadFont : T_XUnloadFont;
XUnmapSubwindows : T_XUnmapSubwindows;
XUnmapWindow : T_XUnmapWindow;
XVendorRelease : T_XVendorRelease;
XWarpPointer : T_XWarpPointer;
XWidthMMOfScreen : T_XWidthMMOfScreen;
XWidthOfScreen : T_XWidthOfScreen;
XWindowEvent : T_XWindowEvent;
XWriteBitmapFile : T_XWriteBitmapFile;
XSetLocaleModifiers : T_XSetLocaleModifiers;
XOpenOM : T_XOpenOM;
XCloseOM : T_XCloseOM;
XSetOMValues : T_XSetOMValues;
XGetOMValues : T_XGetOMValues;
XDisplayOfOM : T_XDisplayOfOM;
XLocaleOfOM : T_XLocaleOfOM;
XCreateOC : T_XCreateOC;
XDestroyOC : T_XDestroyOC;
XOMOfOC : T_XOMOfOC;
XSetOCValues : T_XSetOCValues;
XGetOCValues : T_XGetOCValues;
XCreateFontSet : T_XCreateFontSet;
XFreeFontSet : T_XFreeFontSet;
XFontsOfFontSet : T_XFontsOfFontSet;
XBaseFontNameListOfFontSet : T_XBaseFontNameListOfFontSet;
XLocaleOfFontSet : T_XLocaleOfFontSet;
XContextDependentDrawing : T_XContextDependentDrawing;
XDirectionalDependentDrawing : T_XDirectionalDependentDrawing;
XContextualDrawing : T_XContextualDrawing;
XExtentsOfFontSet : T_XExtentsOfFontSet;
XmbTextEscapement : T_XmbTextEscapement;
XwcTextEscapement : T_XwcTextEscapement;
Xutf8TextEscapement : T_Xutf8TextEscapement;
XmbTextExtents : T_XmbTextExtents;
XwcTextExtents : T_XwcTextExtents;
Xutf8TextExtents : T_Xutf8TextExtents;
XmbTextPerCharExtents : T_XmbTextPerCharExtents;
XwcTextPerCharExtents : T_XwcTextPerCharExtents;
Xutf8TextPerCharExtents : T_Xutf8TextPerCharExtents;
XmbDrawText : T_XmbDrawText;
XwcDrawText : T_XwcDrawText;
Xutf8DrawText : T_Xutf8DrawText;
XmbDrawString : T_XmbDrawString;
XwcDrawString : T_XwcDrawString;
Xutf8DrawString : T_Xutf8DrawString;
XmbDrawImageString : T_XmbDrawImageString;
XwcDrawImageString : T_XwcDrawImageString;
Xutf8DrawImageString : T_Xutf8DrawImageString;
XOpenIM : T_XOpenIM;
XCloseIM : T_XCloseIM;
XGetIMValues : T_XGetIMValues;
XSetIMValues : T_XSetIMValues;
XDisplayOfIM : T_XDisplayOfIM;
XLocaleOfIM : T_XLocaleOfIM;
XCreateIC : T_XCreateIC;
XDestroyIC : T_XDestroyIC;
XSetICFocus : T_XSetICFocus;
XUnsetICFocus : T_XUnsetICFocus;
XwcResetIC : T_XwcResetIC;
XmbResetIC : T_XmbResetIC;
Xutf8ResetIC : T_Xutf8ResetIC;
XSetICValues : T_XSetICValues;
XGetICValues : T_XGetICValues;
XIMOfIC : T_XIMOfIC;
XFilterEvent : T_XFilterEvent;
XmbLookupString : T_XmbLookupString;
XwcLookupString : T_XwcLookupString;
Xutf8LookupString : T_Xutf8LookupString;
XVaCreateNestedList : T_XVaCreateNestedList;
XRegisterIMInstantiateCallback : T_XRegisterIMInstantiateCallback;
XUnregisterIMInstantiateCallback : T_XUnregisterIMInstantiateCallback;
XInternalConnectionNumbers : T_XInternalConnectionNumbers;
XProcessInternalConnection : T_XProcessInternalConnection;
XAddConnectionWatch : T_XAddConnectionWatch;
XRemoveConnectionWatch : T_XRemoveConnectionWatch;
XSetAuthorization : T_XSetAuthorization;
XGetEventData : T_XGetEventData;
XFreeEventData : T_XFreeEventData;





{$ifdef MACROS}
function ConnectionNumber(dpy : PDisplay) : cint;
function RootWindow(dpy : PDisplay; scr : cint) : TWindow;
function DefaultScreen(dpy : PDisplay) : cint;
function DefaultRootWindow(dpy : PDisplay) : TWindow;
function DefaultVisual(dpy : PDisplay; scr : cint) : PVisual;
function DefaultGC(dpy : PDisplay; scr : cint) : TGC;
function BlackPixel(dpy : PDisplay; scr : cint) : culong;
function WhitePixel(dpy : PDisplay; scr : cint) : culong;
function QLength(dpy : PDisplay) : cint;
function DisplayWidth(dpy : PDisplay; scr : cint) : cint;
function DisplayHeight(dpy : PDisplay; scr : cint) : cint;
function DisplayWidthMM(dpy : PDisplay; scr : cint) : cint;
function DisplayHeightMM(dpy : PDisplay; scr : cint) : cint;
function DisplayPlanes(dpy : PDisplay; scr : cint) : cint;
function DisplayCells(dpy : PDisplay; scr : cint) : cint;
function ScreenCount(dpy : PDisplay) : cint;
function ServerVendor(dpy : PDisplay) : Pchar;
function ProtocolVersion(dpy : PDisplay) : cint;
function ProtocolRevision(dpy : PDisplay) : cint;
function VendorRelease(dpy : PDisplay) : cint;
function DisplayString(dpy : PDisplay) : Pchar;
function DefaultDepth(dpy : PDisplay; scr : cint) : cint;
function DefaultColormap(dpy : PDisplay; scr : cint) : TColormap;
function BitmapUnit(dpy : PDisplay) : cint;
function BitmapBitOrder(dpy : PDisplay) : cint;
function BitmapPad(dpy : PDisplay) : cint;
function ImageByteOrder(dpy : PDisplay) : cint;
function NextRequest(dpy : PDisplay) : culong;
function LastKnownRequestProcessed(dpy : PDisplay) : culong;
function ScreenOfDisplay(dpy : PDisplay; scr : cint) : PScreen;
function DefaultScreenOfDisplay(dpy : PDisplay) : PScreen;
function DisplayOfScreen(s : PScreen) : PDisplay;
function RootWindowOfScreen(s : PScreen) : TWindow;
function BlackPixelOfScreen(s : PScreen) : culong;
function WhitePixelOfScreen(s : PScreen) : culong;
function DefaultColormapOfScreen(s : PScreen) : TColormap;
function DefaultDepthOfScreen(s : PScreen) : cint;
function DefaultGCOfScreen(s : PScreen) : TGC;
function DefaultVisualOfScreen(s : PScreen) : PVisual;
function WidthOfScreen(s : PScreen) : cint;
function HeightOfScreen(s : PScreen) : cint;
function WidthMMOfScreen(s : PScreen) : cint;
function HeightMMOfScreen(s : PScreen) : cint;
function PlanesOfScreen(s : PScreen) : cint;
function CellsOfScreen(s : PScreen) : cint;
function MinCmapsOfScreen(s : PScreen) : cint;
function MaxCmapsOfScreen(s : PScreen) : cint;
function DoesSaveUnders(s : PScreen) : TBool;
function DoesBackingStore(s : PScreen) : cint;
function EventMaskOfScreen(s : PScreen) : clong;
function XAllocID(dpy : PDisplay) : TXID;
{$endif MACROS}

// Overloaded functions to handle TBool parameters as actual booleans.
function XClearArea(para1:PDisplay; para2:TWindow; para3:cint; para4:cint; para5:cuint; para6:cuint; para7:Boolean):cint;
function XGetWindowProperty(para1:PDisplay; para2:TWindow; para3:TAtom; para4:clong; para5:clong;
           para6:Boolean; para7:TAtom; para8:PAtom; para9:Pcint; para10:Pculong;
           para11:Pculong; para12:PPcuchar):cint;
function XGrabKeyboard(para1:PDisplay; para2:TWindow; para3:Boolean; para4:cint; para5:cint; para6:TTime):cint;
function XGrabPointer(para1:PDisplay; para2:TWindow; para3:Boolean; para4:cuint; para5:cint;
           para6:cint; para7:TWindow; para8:TCursor; para9:TTime):cint;
function XInternAtom(para1:PDisplay; para2:Pchar; para3:Boolean):TAtom;
function XInternAtoms(para1:PDisplay; para2:PPchar; para3:cint; para4:Boolean; para5:PAtom):TStatus;
function XSendEvent(para1:PDisplay; para2:TWindow; para3:Boolean; para4:clong; para5:PXEvent):TStatus;
function XSetGraphicsExposures(para1:PDisplay; para2:TGC; para3:Boolean):cint;
function XSync(para1:PDisplay; para2:Boolean):cint;
function XSynchronize(para1:PDisplay; para2:Boolean):funcdisp;


implementation


{$ifdef MACROS}
function ConnectionNumber(dpy : PDisplay) : cint;
begin
   ConnectionNumber:=(PXPrivDisplay(dpy))^.fd;
end;

function RootWindow(dpy : PDisplay; scr : cint) : TWindow;
begin
   RootWindow:=(ScreenOfDisplay(dpy,scr))^.root;
end;

function DefaultScreen(dpy : PDisplay) : cint;
begin
   DefaultScreen:=(PXPrivDisplay(dpy))^.default_screen;
end;

function DefaultRootWindow(dpy : PDisplay) : TWindow;
begin
   DefaultRootWindow:=(ScreenOfDisplay(dpy,DefaultScreen(dpy)))^.root;
end;

function DefaultVisual(dpy : PDisplay; scr : cint) : PVisual;
begin
   DefaultVisual:=(ScreenOfDisplay(dpy,scr))^.root_visual;
end;

function DefaultGC(dpy : PDisplay; scr : cint) : TGC;
begin
   DefaultGC:=(ScreenOfDisplay(dpy,scr))^.default_gc;
end;

function BlackPixel(dpy : PDisplay; scr : cint) : culong;
begin
   BlackPixel:=(ScreenOfDisplay(dpy,scr))^.black_pixel;
end;

function WhitePixel(dpy : PDisplay; scr : cint) : culong;
begin
   WhitePixel:=(ScreenOfDisplay(dpy,scr))^.white_pixel;
end;

function QLength(dpy : PDisplay) : cint;
begin
   QLength:=(PXPrivDisplay(dpy))^.qlen;
end;

function DisplayWidth(dpy : PDisplay; scr : cint) : cint;
begin
   DisplayWidth:=(ScreenOfDisplay(dpy,scr))^.width;
end;

function DisplayHeight(dpy : PDisplay; scr : cint) : cint;
begin
   DisplayHeight:=(ScreenOfDisplay(dpy,scr))^.height;
end;

function DisplayWidthMM(dpy : PDisplay; scr : cint) : cint;
begin
   DisplayWidthMM:=(ScreenOfDisplay(dpy,scr))^.mwidth;
end;

function DisplayHeightMM(dpy : PDisplay; scr : cint) : cint;
begin
   DisplayHeightMM:=(ScreenOfDisplay(dpy,scr))^.mheight;
end;

function DisplayPlanes(dpy : PDisplay; scr : cint) : cint;
begin
   DisplayPlanes:=(ScreenOfDisplay(dpy,scr))^.root_depth;
end;

function DisplayCells(dpy : PDisplay; scr : cint) : cint;
begin
   DisplayCells:=(DefaultVisual(dpy,scr))^.map_entries;
end;

function ScreenCount(dpy : PDisplay) : cint;
begin
   ScreenCount:=(PXPrivDisplay(dpy))^.nscreens;
end;

function ServerVendor(dpy : PDisplay) : Pchar;
begin
   ServerVendor:=(PXPrivDisplay(dpy))^.vendor;
end;

function ProtocolVersion(dpy : PDisplay) : cint;
begin
   ProtocolVersion:=(PXPrivDisplay(dpy))^.proto_major_version;
end;

function ProtocolRevision(dpy : PDisplay) : cint;
begin
   ProtocolRevision:=(PXPrivDisplay(dpy))^.proto_minor_version;
end;

function VendorRelease(dpy : PDisplay) : cint;
begin
   VendorRelease:=(PXPrivDisplay(dpy))^.release;
end;

function DisplayString(dpy : PDisplay) : Pchar;
begin
   DisplayString:=(PXPrivDisplay(dpy))^.display_name;
end;

function DefaultDepth(dpy : PDisplay; scr : cint) : cint;
begin
   DefaultDepth:=(ScreenOfDisplay(dpy,scr))^.root_depth;
end;

function DefaultColormap(dpy : PDisplay; scr : cint) : TColormap;
begin
   DefaultColormap:=(ScreenOfDisplay(dpy,scr))^.cmap;
end;

function BitmapUnit(dpy : PDisplay) : cint;
begin
   BitmapUnit:=(PXPrivDisplay(dpy))^.bitmap_unit;
end;

function BitmapBitOrder(dpy : PDisplay) : cint;
begin
   BitmapBitOrder:=(PXPrivDisplay(dpy))^.bitmap_bit_order;
end;

function BitmapPad(dpy : PDisplay) : cint;
begin
   BitmapPad:=(PXPrivDisplay(dpy))^.bitmap_pad;
end;

function ImageByteOrder(dpy : PDisplay) : cint;
begin
   ImageByteOrder:=(PXPrivDisplay(dpy))^.byte_order;
end;

function NextRequest(dpy : PDisplay) : culong;
begin
   NextRequest:=((PXPrivDisplay(dpy))^.request) + 1;
end;

function LastKnownRequestProcessed(dpy : PDisplay) : culong;
begin
   LastKnownRequestProcessed:=(PXPrivDisplay(dpy))^.last_request_read;
end;

function ScreenOfDisplay(dpy : PDisplay; scr : cint) : PScreen;
begin
   ScreenOfDisplay:=@(((PXPrivDisplay(dpy))^.screens)[scr]);
end;

function DefaultScreenOfDisplay(dpy : PDisplay) : PScreen;
begin
   DefaultScreenOfDisplay:=ScreenOfDisplay(dpy,DefaultScreen(dpy));
end;

function DisplayOfScreen(s : PScreen) : PDisplay;
begin
   DisplayOfScreen:=s^.display;
end;

function RootWindowOfScreen(s : PScreen) : TWindow;
begin
   RootWindowOfScreen:=s^.root;
end;

function BlackPixelOfScreen(s : PScreen) : culong;
begin
   BlackPixelOfScreen:=s^.black_pixel;
end;

function WhitePixelOfScreen(s : PScreen) : culong;
begin
   WhitePixelOfScreen:=s^.white_pixel;
end;

function DefaultColormapOfScreen(s : PScreen) : TColormap;
begin
   DefaultColormapOfScreen:=s^.cmap;
end;

function DefaultDepthOfScreen(s : PScreen) : cint;
begin
   DefaultDepthOfScreen:=s^.root_depth;
end;

function DefaultGCOfScreen(s : PScreen) : TGC;
begin
   DefaultGCOfScreen:=s^.default_gc;
end;

function DefaultVisualOfScreen(s : PScreen) : PVisual;
begin
   DefaultVisualOfScreen:=s^.root_visual;
end;

function WidthOfScreen(s : PScreen) : cint;
begin
   WidthOfScreen:=s^.width;
end;

function HeightOfScreen(s : PScreen) : cint;
begin
   HeightOfScreen:=s^.height;
end;

function WidthMMOfScreen(s : PScreen) : cint;
begin
   WidthMMOfScreen:=s^.mwidth;
end;

function HeightMMOfScreen(s : PScreen) : cint;
begin
   HeightMMOfScreen:=s^.mheight;
end;

function PlanesOfScreen(s : PScreen) : cint;
begin
   PlanesOfScreen:=s^.root_depth;
end;

function CellsOfScreen(s : PScreen) : cint;
begin
   CellsOfScreen:=(DefaultVisualOfScreen(s))^.map_entries;
end;

function MinCmapsOfScreen(s : PScreen) : cint;
begin
   MinCmapsOfScreen:=s^.min_maps;
end;

function MaxCmapsOfScreen(s : PScreen) : cint;
begin
   MaxCmapsOfScreen:=s^.max_maps;
end;

function DoesSaveUnders(s : PScreen) : TBool;
begin
   DoesSaveUnders:=s^.save_unders;
end;

function DoesBackingStore(s : PScreen) : cint;
begin
   DoesBackingStore:=s^.backing_store;
end;

function EventMaskOfScreen(s : PScreen) : clong;
begin
   EventMaskOfScreen:=s^.root_input_mask;
end;

function XAllocID(dpy : PDisplay) : TXID;
begin
   XAllocID:=(PXPrivDisplay(dpy))^.resource_alloc(dpy);
end;
{$endif MACROS}

function XClearArea(para1:PDisplay; para2:TWindow; para3:cint; para4:cint; para5:cuint; para6:cuint; para7:Boolean):cint;

begin
  Result:=_XClearArea(para1,para2,para3,para4,para5,para6,Ord(Para7));
end;

function XGetWindowProperty(para1: PDisplay; para2: TWindow; para3: TAtom;
  para4: clong; para5: clong; para6: Boolean; para7: TAtom; para8: PAtom;
  para9: Pcint; para10: Pculong; para11: Pculong; para12: PPcuchar): cint;
begin
  Result := _XGetWindowProperty(para1,para2,para3,para4,para5,ord(para6),para7,para8,para9,para10,para11,para12);
end;

function XGrabKeyboard(para1: PDisplay; para2: TWindow; para3: Boolean;
  para4: cint; para5: cint; para6: TTime): cint;
begin
  Result:=_XGrabKeyboard(para1,para2,Ord(para3),para4,para5,para6);
end;

function XGrabPointer(para1: PDisplay; para2: TWindow; para3: Boolean;
  para4: cuint; para5: cint; para6: cint; para7: TWindow; para8: TCursor;
  para9: TTime): cint;
begin
  Result:=_XGrabPointer(para1,para2,Ord(para3),para4,para5,para6,para7,para8,para9);
end;

function XInternAtom(para1:PDisplay; para2:Pchar; para3:Boolean):TAtom;

begin
  Result:=_XInternAtom(para1,para2,Ord(para3));
end;

function XInternAtoms(para1:PDisplay; para2:PPchar; para3:cint; para4:Boolean; para5:PAtom):TStatus;

begin
  Result:=_XInternAtoms(para1,para2,para3,Ord(para4),para5);
end;

function XSendEvent(para1:PDisplay; para2:TWindow; para3:Boolean; para4:clong; para5:PXEvent):TStatus;

begin
  Result:=_XSendEvent(para1,para2,ord(Para3),para4,para5);
end;

function XSetGraphicsExposures(para1:PDisplay; para2:TGC; para3:Boolean):cint;

begin
  Result:=_XSetGraphicsExposures(Para1,para2,Ord(Para3));
end;

function XSync(para1:PDisplay; para2:Boolean):cint;

begin
  Result:=_XSync(Para1,Ord(Para2));
end;

function XSynchronize(para1:PDisplay; para2:boolean):funcdisp;

begin
  Result:=_XSynchronize(para1,Ord(para2));
end;




procedure init_procvars();
begin
  pointer(XLoadQueryFont) := nil;
  pointer(XQueryFont) := nil;
  pointer(XGetMotionEvents) := nil;
  pointer(XDeleteModifiermapEntry) := nil;
  pointer(XGetModifierMapping) := nil;
  pointer(XInsertModifiermapEntry) := nil;
  pointer(XNewModifiermap) := nil;
  pointer(XCreateImage) := nil;
  pointer(XInitImage) := nil;
  pointer(XGetImage) := nil;
  pointer(XGetSubImage) := nil;
  pointer(XOpenDisplay) := nil;
  pointer(XFetchBytes) := nil;
  pointer(XFetchBuffer) := nil;
  pointer(XGetAtomName) := nil;
  pointer(XGetAtomNames) := nil;
  pointer(XGetDefault) := nil;
  pointer(XDisplayName) := nil;
  pointer(XKeysymToString) := nil;
  pointer(_XSynchronize) := nil;
  pointer(XSetAfterFunction) := nil;
  pointer(_XInternAtom) := nil;
  pointer(_XInternAtoms) := nil;
  pointer(XCopyColormapAndFree) := nil;
  pointer(XCreateColormap) := nil;
  pointer(XCreatePixmapCursor) := nil;
  pointer(XCreateGlyphCursor) := nil;
  pointer(XCreateFontCursor) := nil;
  pointer(XLoadFont) := nil;
  pointer(XCreateGC) := nil;
  pointer(XGContextFromGC) := nil;
  pointer(XFlushGC) := nil;
  pointer(XCreatePixmap) := nil;
  pointer(XCreateBitmapFromData) := nil;
  pointer(XCreatePixmapFromBitmapData) := nil;
  pointer(XCreateSimpleWindow) := nil;
  pointer(XGetSelectionOwner) := nil;
  pointer(XCreateWindow) := nil;
  pointer(XListInstalledColormaps) := nil;
  pointer(XListFonts) := nil;
  pointer(XListFontsWithInfo) := nil;
  pointer(XGetFontPath) := nil;
  pointer(XListExtensions) := nil;
  pointer(XListProperties) := nil;
  pointer(XListHosts) := nil;
  pointer(XKeycodeToKeysym) := nil;
  pointer(XLookupKeysym) := nil;
  pointer(XGetKeyboardMapping) := nil;
  pointer(XStringToKeysym) := nil;
  pointer(XMaxRequestSize) := nil;
  pointer(XExtendedMaxRequestSize) := nil;
  pointer(XResourceManagerString) := nil;
  pointer(XScreenResourceString) := nil;
  pointer(XDisplayMotionBufferSize) := nil;
  pointer(XVisualIDFromVisual) := nil;
  pointer(XLockDisplay) := nil;
  pointer(XUnlockDisplay) := nil;
  pointer(XInitExtension) := nil;
  pointer(XAddExtension) := nil;
  pointer(XFindOnExtensionList) := nil;
  pointer(XEHeadOfExtensionList) := nil;
  pointer(XRootWindow) := nil;
  pointer(XDefaultRootWindow) := nil;
  pointer(XRootWindowOfScreen) := nil;
  pointer(XDefaultVisual) := nil;
  pointer(XDefaultVisualOfScreen) := nil;
  pointer(XDefaultGC) := nil;
  pointer(XDefaultGCOfScreen) := nil;
  pointer(XBlackPixel) := nil;
  pointer(XWhitePixel) := nil;
  pointer(XBlackPixelOfScreen) := nil;
  pointer(XWhitePixelOfScreen) := nil;
  pointer(XNextRequest) := nil;
  pointer(XLastKnownRequestProcessed) := nil;
  pointer(XServerVendor) := nil;
  pointer(XDisplayString) := nil;
  pointer(XDefaultColormap) := nil;
  pointer(XDefaultColormapOfScreen) := nil;
  pointer(XDisplayOfScreen) := nil;
  pointer(XScreenOfDisplay) := nil;
  pointer(XDefaultScreenOfDisplay) := nil;
  pointer(XEventMaskOfScreen) := nil;
  pointer(XScreenNumberOfScreen) := nil;
  pointer(XSetErrorHandler) := nil;
  pointer(XSetIOErrorHandler) := nil;
  pointer(XListPixmapFormats) := nil;
  pointer(XListDepths) := nil;
  pointer(XReconfigureWMWindow) := nil;
  pointer(XGetWMProtocols) := nil;
  pointer(XSetWMProtocols) := nil;
  pointer(XIconifyWindow) := nil;
  pointer(XWithdrawWindow) := nil;
  pointer(XGetCommand) := nil;
  pointer(XGetWMColormapWindows) := nil;
  pointer(XSetWMColormapWindows) := nil;
  pointer(XFreeStringList) := nil;
  pointer(XSetTransientForHint) := nil;
  pointer(XActivateScreenSaver) := nil;
  pointer(XAddHost) := nil;
  pointer(XAddHosts) := nil;
  pointer(XAddToExtensionList) := nil;
  pointer(XAddToSaveSet) := nil;
  pointer(XAllocColor) := nil;
  pointer(XAllocColorCells) := nil;
  pointer(XAllocColorPlanes) := nil;
  pointer(XAllocNamedColor) := nil;
  pointer(XAllowEvents) := nil;
  pointer(XAutoRepeatOff) := nil;
  pointer(XAutoRepeatOn) := nil;
  pointer(XBell) := nil;
  pointer(XBitmapBitOrder) := nil;
  pointer(XBitmapPad) := nil;
  pointer(XBitmapUnit) := nil;
  pointer(XCellsOfScreen) := nil;
  pointer(XChangeActivePointerGrab) := nil;
  pointer(XChangeGC) := nil;
  pointer(XChangeKeyboardControl) := nil;
  pointer(XChangeKeyboardMapping) := nil;
  pointer(XChangePointerControl) := nil;
  pointer(XChangeProperty) := nil;
  pointer(XChangeSaveSet) := nil;
  pointer(XChangeWindowAttributes) := nil;
  pointer(XCheckIfEvent) := nil;
  pointer(XCheckMaskEvent) := nil;
  pointer(XCheckTypedEvent) := nil;
  pointer(XCheckTypedWindowEvent) := nil;
  pointer(XCheckWindowEvent) := nil;
  pointer(XCirculateSubwindows) := nil;
  pointer(XCirculateSubwindowsDown) := nil;
  pointer(XCirculateSubwindowsUp) := nil;
  pointer(_XClearArea) := nil;
  pointer(XClearWindow) := nil;
  pointer(XCloseDisplay) := nil;
  pointer(XConfigureWindow) := nil;
  pointer(XConnectionNumber) := nil;
  pointer(XConvertSelection) := nil;
  pointer(XCopyArea) := nil;
  pointer(XCopyGC) := nil;
  pointer(XCopyPlane) := nil;
  pointer(XDefaultDepth) := nil;
  pointer(XDefaultDepthOfScreen) := nil;
  pointer(XDefaultScreen) := nil;
  pointer(XDefineCursor) := nil;
  pointer(XDeleteProperty) := nil;
  pointer(XDestroyWindow) := nil;
  pointer(XDestroySubwindows) := nil;
  pointer(XDoesBackingStore) := nil;
  pointer(XDoesSaveUnders) := nil;
  pointer(XDisableAccessControl) := nil;
  pointer(XDisplayCells) := nil;
  pointer(XDisplayHeight) := nil;
  pointer(XDisplayHeightMM) := nil;
  pointer(XDisplayKeycodes) := nil;
  pointer(XDisplayPlanes) := nil;
  pointer(XDisplayWidth) := nil;
  pointer(XDisplayWidthMM) := nil;
  pointer(XDrawArc) := nil;
  pointer(XDrawArcs) := nil;
  pointer(XDrawImageString) := nil;
  pointer(XDrawImageString16) := nil;
  pointer(XDrawLine) := nil;
  pointer(XDrawLines) := nil;
  pointer(XDrawPoint) := nil;
  pointer(XDrawPoints) := nil;
  pointer(XDrawRectangle) := nil;
  pointer(XDrawRectangles) := nil;
  pointer(XDrawSegments) := nil;
  pointer(XDrawString) := nil;
  pointer(XDrawString16) := nil;
  pointer(XDrawText) := nil;
  pointer(XDrawText16) := nil;
  pointer(XEnableAccessControl) := nil;
  pointer(XEventsQueued) := nil;
  pointer(XFetchName) := nil;
  pointer(XFillArc) := nil;
  pointer(XFillArcs) := nil;
  pointer(XFillPolygon) := nil;
  pointer(XFillRectangle) := nil;
  pointer(XFillRectangles) := nil;
  pointer(XFlush) := nil;
  pointer(XForceScreenSaver) := nil;
  pointer(XFree) := nil;
  pointer(XFreeColormap) := nil;
  pointer(XFreeColors) := nil;
  pointer(XFreeCursor) := nil;
  pointer(XFreeExtensionList) := nil;
  pointer(XFreeFont) := nil;
  pointer(XFreeFontInfo) := nil;
  pointer(XFreeFontNames) := nil;
  pointer(XFreeFontPath) := nil;
  pointer(XFreeGC) := nil;
  pointer(XFreeModifiermap) := nil;
  pointer(XFreePixmap) := nil;
  pointer(XGeometry) := nil;
  pointer(XGetErrorDatabaseText) := nil;
  pointer(XGetErrorText) := nil;
  pointer(XGetFontProperty) := nil;
  pointer(XGetGCValues) := nil;
  pointer(XGetGeometry) := nil;
  pointer(XGetIconName) := nil;
  pointer(XGetInputFocus) := nil;
  pointer(XGetKeyboardControl) := nil;
  pointer(XGetPointerControl) := nil;
  pointer(XGetPointerMapping) := nil;
  pointer(XGetScreenSaver) := nil;
  pointer(XGetTransientForHint) := nil;
  pointer(_XGetWindowProperty) := nil;
  pointer(XGetWindowAttributes) := nil;
  pointer(XGrabButton) := nil;
  pointer(XGrabKey) := nil;
  pointer(_XGrabKeyboard) := nil;
  pointer(_XGrabPointer) := nil;
  pointer(XGrabServer) := nil;
  pointer(XHeightMMOfScreen) := nil;
  pointer(XHeightOfScreen) := nil;
  pointer(XIfEvent) := nil;
  pointer(XImageByteOrder) := nil;
  pointer(XInstallColormap) := nil;
  pointer(XKeysymToKeycode) := nil;
  pointer(XKillClient) := nil;
  pointer(XLookupColor) := nil;
  pointer(XLowerWindow) := nil;
  pointer(XMapRaised) := nil;
  pointer(XMapSubwindows) := nil;
  pointer(XMapWindow) := nil;
  pointer(XMaskEvent) := nil;
  pointer(XMaxCmapsOfScreen) := nil;
  pointer(XMinCmapsOfScreen) := nil;
  pointer(XMoveResizeWindow) := nil;
  pointer(XMoveWindow) := nil;
  pointer(XNextEvent) := nil;
  pointer(XNoOp) := nil;
  pointer(XParseColor) := nil;
  pointer(XParseGeometry) := nil;
  pointer(XPeekEvent) := nil;
  pointer(XPeekIfEvent) := nil;
  pointer(XPending) := nil;
  pointer(XPlanesOfScreen) := nil;
  pointer(XProtocolRevision) := nil;
  pointer(XProtocolVersion) := nil;
  pointer(XPutBackEvent) := nil;
  pointer(XPutImage) := nil;
  pointer(XQLength) := nil;
  pointer(XQueryBestCursor) := nil;
  pointer(XQueryBestSize) := nil;
  pointer(XQueryBestStipple) := nil;
  pointer(XQueryBestTile) := nil;
  pointer(XQueryColor) := nil;
  pointer(XQueryColors) := nil;
  pointer(XQueryExtension) := nil;
  pointer(XQueryKeymap) := nil;
  pointer(XQueryPointer) := nil;
  pointer(XQueryTextExtents) := nil;
  pointer(XQueryTextExtents16) := nil;
  pointer(XQueryTree) := nil;
  pointer(XRaiseWindow) := nil;
  pointer(XReadBitmapFile) := nil;
  pointer(XReadBitmapFileData) := nil;
  pointer(XRebindKeysym) := nil;
  pointer(XRecolorCursor) := nil;
  pointer(XRefreshKeyboardMapping) := nil;
  pointer(XRemoveFromSaveSet) := nil;
  pointer(XRemoveHost) := nil;
  pointer(XRemoveHosts) := nil;
  pointer(XReparentWindow) := nil;
  pointer(XResetScreenSaver) := nil;
  pointer(XResizeWindow) := nil;
  pointer(XRestackWindows) := nil;
  pointer(XRotateBuffers) := nil;
  pointer(XRotateWindowProperties) := nil;
  pointer(XScreenCount) := nil;
  pointer(XSelectInput) := nil;
  pointer(_XSendEvent) := nil;
  pointer(XSetAccessControl) := nil;
  pointer(XSetArcMode) := nil;
  pointer(XSetBackground) := nil;
  pointer(XSetClipMask) := nil;
  pointer(XSetClipOrigin) := nil;
  pointer(XSetClipRectangles) := nil;
  pointer(XSetCloseDownMode) := nil;
  pointer(XSetCommand) := nil;
  pointer(XSetDashes) := nil;
  pointer(XSetFillRule) := nil;
  pointer(XSetFillStyle) := nil;
  pointer(XSetFont) := nil;
  pointer(XSetFontPath) := nil;
  pointer(XSetForeground) := nil;
  pointer(XSetFunction) := nil;
  pointer(_XSetGraphicsExposures) := nil;
  pointer(XSetIconName) := nil;
  pointer(XSetInputFocus) := nil;
  pointer(XSetLineAttributes) := nil;
  pointer(XSetModifierMapping) := nil;
  pointer(XSetPlaneMask) := nil;
  pointer(XSetPointerMapping) := nil;
  pointer(XSetScreenSaver) := nil;
  pointer(XSetSelectionOwner) := nil;
  pointer(XSetState) := nil;
  pointer(XSetStipple) := nil;
  pointer(XSetSubwindowMode) := nil;
  pointer(XSetTSOrigin) := nil;
  pointer(XSetTile) := nil;
  pointer(XSetWindowBackground) := nil;
  pointer(XSetWindowBackgroundPixmap) := nil;
  pointer(XSetWindowBorder) := nil;
  pointer(XSetWindowBorderPixmap) := nil;
  pointer(XSetWindowBorderWidth) := nil;
  pointer(XSetWindowColormap) := nil;
  pointer(XStoreBuffer) := nil;
  pointer(XStoreBytes) := nil;
  pointer(XStoreColor) := nil;
  pointer(XStoreColors) := nil;
  pointer(XStoreName) := nil;
  pointer(XStoreNamedColor) := nil;
  pointer(_XSync) := nil;
  pointer(XTextExtents) := nil;
  pointer(XTextExtents16) := nil;
  pointer(XTextWidth) := nil;
  pointer(XTextWidth16) := nil;
  pointer(XTranslateCoordinates) := nil;
  pointer(XUndefineCursor) := nil;
  pointer(XUngrabButton) := nil;
  pointer(XUngrabKey) := nil;
  pointer(XUngrabKeyboard) := nil;
  pointer(XUngrabPointer) := nil;
  pointer(XUngrabServer) := nil;
  pointer(XUninstallColormap) := nil;
  pointer(XUnloadFont) := nil;
  pointer(XUnmapSubwindows) := nil;
  pointer(XUnmapWindow) := nil;
  pointer(XVendorRelease) := nil;
  pointer(XWarpPointer) := nil;
  pointer(XWidthMMOfScreen) := nil;
  pointer(XWidthOfScreen) := nil;
  pointer(XWindowEvent) := nil;
  pointer(XWriteBitmapFile) := nil;
  pointer(XSetLocaleModifiers) := nil;
  pointer(XOpenOM) := nil;
  pointer(XCloseOM) := nil;
  pointer(XSetOMValues) := nil;
  pointer(XGetOMValues) := nil;
  pointer(XDisplayOfOM) := nil;
  pointer(XLocaleOfOM) := nil;
  pointer(XCreateOC) := nil;
  pointer(XDestroyOC) := nil;
  pointer(XOMOfOC) := nil;
  pointer(XSetOCValues) := nil;
  pointer(XGetOCValues) := nil;
  pointer(XCreateFontSet) := nil;
  pointer(XFreeFontSet) := nil;
  pointer(XFontsOfFontSet) := nil;
  pointer(XBaseFontNameListOfFontSet) := nil;
  pointer(XLocaleOfFontSet) := nil;
  pointer(XContextDependentDrawing) := nil;
  pointer(XDirectionalDependentDrawing) := nil;
  pointer(XContextualDrawing) := nil;
  pointer(XExtentsOfFontSet) := nil;
  pointer(XmbTextEscapement) := nil;
  pointer(XwcTextEscapement) := nil;
  pointer(Xutf8TextEscapement) := nil;
  pointer(XmbTextExtents) := nil;
  pointer(XwcTextExtents) := nil;
  pointer(Xutf8TextExtents) := nil;
  pointer(XmbTextPerCharExtents) := nil;
  pointer(XwcTextPerCharExtents) := nil;
  pointer(Xutf8TextPerCharExtents) := nil;
  pointer(XmbDrawText) := nil;
  pointer(XwcDrawText) := nil;
  pointer(Xutf8DrawText) := nil;
  pointer(XmbDrawString) := nil;
  pointer(XwcDrawString) := nil;
  pointer(Xutf8DrawString) := nil;
  pointer(XmbDrawImageString) := nil;
  pointer(XwcDrawImageString) := nil;
  pointer(Xutf8DrawImageString) := nil;
  pointer(XOpenIM) := nil;
  pointer(XCloseIM) := nil;
  pointer(XGetIMValues) := nil;
  pointer(XSetIMValues) := nil;
  pointer(XDisplayOfIM) := nil;
  pointer(XLocaleOfIM) := nil;
  pointer(XCreateIC) := nil;
  pointer(XDestroyIC) := nil;
  pointer(XSetICFocus) := nil;
  pointer(XUnsetICFocus) := nil;
  pointer(XwcResetIC) := nil;
  pointer(XmbResetIC) := nil;
  pointer(Xutf8ResetIC) := nil;
  pointer(XSetICValues) := nil;
  pointer(XGetICValues) := nil;
  pointer(XIMOfIC) := nil;
  pointer(XFilterEvent) := nil;
  pointer(XmbLookupString) := nil;
  pointer(XwcLookupString) := nil;
  pointer(Xutf8LookupString) := nil;
  pointer(XVaCreateNestedList) := nil;
  pointer(XRegisterIMInstantiateCallback) := nil;
  pointer(XUnregisterIMInstantiateCallback) := nil;
  pointer(XInternalConnectionNumbers) := nil;
  pointer(XProcessInternalConnection) := nil;
  pointer(XAddConnectionWatch) := nil;
  pointer(XRemoveConnectionWatch) := nil;
  pointer(XSetAuthorization) := nil;
  pointer(XGetEventData) := nil;
  pointer(XFreeEventData) := nil;
end;

procedure load_procvars(libh : TLibHandle);
begin
  pointer(XLoadQueryFont) := GetProcedureAddress(libh,'XLoadQueryFont');
  pointer(XQueryFont) := GetProcedureAddress(libh,'XQueryFont');
  pointer(XGetMotionEvents) := GetProcedureAddress(libh,'XGetMotionEvents');
  pointer(XDeleteModifiermapEntry) := GetProcedureAddress(libh,'XDeleteModifiermapEntry');
  pointer(XGetModifierMapping) := GetProcedureAddress(libh,'XGetModifierMapping');
  pointer(XInsertModifiermapEntry) := GetProcedureAddress(libh,'XInsertModifiermapEntry');
  pointer(XNewModifiermap) := GetProcedureAddress(libh,'XNewModifiermap');
  pointer(XCreateImage) := GetProcedureAddress(libh,'XCreateImage');
  pointer(XInitImage) := GetProcedureAddress(libh,'XInitImage');
  pointer(XGetImage) := GetProcedureAddress(libh,'XGetImage');
  pointer(XGetSubImage) := GetProcedureAddress(libh,'XGetSubImage');
  pointer(XOpenDisplay) := GetProcedureAddress(libh,'XOpenDisplay');
  pointer(XFetchBytes) := GetProcedureAddress(libh,'XFetchBytes');
  pointer(XFetchBuffer) := GetProcedureAddress(libh,'XFetchBuffer');
  pointer(XGetAtomName) := GetProcedureAddress(libh,'XGetAtomName');
  pointer(XGetAtomNames) := GetProcedureAddress(libh,'XGetAtomNames');
  pointer(XGetDefault) := GetProcedureAddress(libh,'XGetDefault');
  pointer(XDisplayName) := GetProcedureAddress(libh,'XDisplayName');
  pointer(XKeysymToString) := GetProcedureAddress(libh,'XKeysymToString');
  pointer(_XSynchronize) := GetProcedureAddress(libh,'XSynchronize');
  pointer(XSetAfterFunction) := GetProcedureAddress(libh,'XSetAfterFunction');
  pointer(_XInternAtom) := GetProcedureAddress(libh,'XInternAtom');
  pointer(_XInternAtoms) := GetProcedureAddress(libh,'XInternAtoms');
  pointer(XCopyColormapAndFree) := GetProcedureAddress(libh,'XCopyColormapAndFree');
  pointer(XCreateColormap) := GetProcedureAddress(libh,'XCreateColormap');
  pointer(XCreatePixmapCursor) := GetProcedureAddress(libh,'XCreatePixmapCursor');
  pointer(XCreateGlyphCursor) := GetProcedureAddress(libh,'XCreateGlyphCursor');
  pointer(XCreateFontCursor) := GetProcedureAddress(libh,'XCreateFontCursor');
  pointer(XLoadFont) := GetProcedureAddress(libh,'XLoadFont');
  pointer(XCreateGC) := GetProcedureAddress(libh,'XCreateGC');
  pointer(XGContextFromGC) := GetProcedureAddress(libh,'XGContextFromGC');
  pointer(XFlushGC) := GetProcedureAddress(libh,'XFlushGC');
  pointer(XCreatePixmap) := GetProcedureAddress(libh,'XCreatePixmap');
  pointer(XCreateBitmapFromData) := GetProcedureAddress(libh,'XCreateBitmapFromData');
  pointer(XCreatePixmapFromBitmapData) := GetProcedureAddress(libh,'XCreatePixmapFromBitmapData');
  pointer(XCreateSimpleWindow) := GetProcedureAddress(libh,'XCreateSimpleWindow');
  pointer(XGetSelectionOwner) := GetProcedureAddress(libh,'XGetSelectionOwner');
  pointer(XCreateWindow) := GetProcedureAddress(libh,'XCreateWindow');
  pointer(XListInstalledColormaps) := GetProcedureAddress(libh,'XListInstalledColormaps');
  pointer(XListFonts) := GetProcedureAddress(libh,'XListFonts');
  pointer(XListFontsWithInfo) := GetProcedureAddress(libh,'XListFontsWithInfo');
  pointer(XGetFontPath) := GetProcedureAddress(libh,'XGetFontPath');
  pointer(XListExtensions) := GetProcedureAddress(libh,'XListExtensions');
  pointer(XListProperties) := GetProcedureAddress(libh,'XListProperties');
  pointer(XListHosts) := GetProcedureAddress(libh,'XListHosts');
  pointer(XKeycodeToKeysym) := GetProcedureAddress(libh,'XKeycodeToKeysym');
  pointer(XLookupKeysym) := GetProcedureAddress(libh,'XLookupKeysym');
  pointer(XGetKeyboardMapping) := GetProcedureAddress(libh,'XGetKeyboardMapping');
  pointer(XStringToKeysym) := GetProcedureAddress(libh,'XStringToKeysym');
  pointer(XMaxRequestSize) := GetProcedureAddress(libh,'XMaxRequestSize');
  pointer(XExtendedMaxRequestSize) := GetProcedureAddress(libh,'XExtendedMaxRequestSize');
  pointer(XResourceManagerString) := GetProcedureAddress(libh,'XResourceManagerString');
  pointer(XScreenResourceString) := GetProcedureAddress(libh,'XScreenResourceString');
  pointer(XDisplayMotionBufferSize) := GetProcedureAddress(libh,'XDisplayMotionBufferSize');
  pointer(XVisualIDFromVisual) := GetProcedureAddress(libh,'XVisualIDFromVisual');
  pointer(XLockDisplay) := GetProcedureAddress(libh,'XLockDisplay');
  pointer(XUnlockDisplay) := GetProcedureAddress(libh,'XUnlockDisplay');
  pointer(XInitExtension) := GetProcedureAddress(libh,'XInitExtension');
  pointer(XAddExtension) := GetProcedureAddress(libh,'XAddExtension');
  pointer(XFindOnExtensionList) := GetProcedureAddress(libh,'XFindOnExtensionList');
  pointer(XEHeadOfExtensionList) := GetProcedureAddress(libh,'XEHeadOfExtensionList');
  pointer(XRootWindow) := GetProcedureAddress(libh,'XRootWindow');
  pointer(XDefaultRootWindow) := GetProcedureAddress(libh,'XDefaultRootWindow');
  pointer(XRootWindowOfScreen) := GetProcedureAddress(libh,'XRootWindowOfScreen');
  pointer(XDefaultVisual) := GetProcedureAddress(libh,'XDefaultVisual');
  pointer(XDefaultVisualOfScreen) := GetProcedureAddress(libh,'XDefaultVisualOfScreen');
  pointer(XDefaultGC) := GetProcedureAddress(libh,'XDefaultGC');
  pointer(XDefaultGCOfScreen) := GetProcedureAddress(libh,'XDefaultGCOfScreen');
  pointer(XBlackPixel) := GetProcedureAddress(libh,'XBlackPixel');
  pointer(XWhitePixel) := GetProcedureAddress(libh,'XWhitePixel');
  pointer(XBlackPixelOfScreen) := GetProcedureAddress(libh,'XBlackPixelOfScreen');
  pointer(XWhitePixelOfScreen) := GetProcedureAddress(libh,'XWhitePixelOfScreen');
  pointer(XNextRequest) := GetProcedureAddress(libh,'XNextRequest');
  pointer(XLastKnownRequestProcessed) := GetProcedureAddress(libh,'XLastKnownRequestProcessed');
  pointer(XServerVendor) := GetProcedureAddress(libh,'XServerVendor');
  pointer(XDisplayString) := GetProcedureAddress(libh,'XDisplayString');
  pointer(XDefaultColormap) := GetProcedureAddress(libh,'XDefaultColormap');
  pointer(XDefaultColormapOfScreen) := GetProcedureAddress(libh,'XDefaultColormapOfScreen');
  pointer(XDisplayOfScreen) := GetProcedureAddress(libh,'XDisplayOfScreen');
  pointer(XScreenOfDisplay) := GetProcedureAddress(libh,'XScreenOfDisplay');
  pointer(XDefaultScreenOfDisplay) := GetProcedureAddress(libh,'XDefaultScreenOfDisplay');
  pointer(XEventMaskOfScreen) := GetProcedureAddress(libh,'XEventMaskOfScreen');
  pointer(XScreenNumberOfScreen) := GetProcedureAddress(libh,'XScreenNumberOfScreen');
  pointer(XSetErrorHandler) := GetProcedureAddress(libh,'XSetErrorHandler');
  pointer(XSetIOErrorHandler) := GetProcedureAddress(libh,'XSetIOErrorHandler');
  pointer(XListPixmapFormats) := GetProcedureAddress(libh,'XListPixmapFormats');
  pointer(XListDepths) := GetProcedureAddress(libh,'XListDepths');
  pointer(XReconfigureWMWindow) := GetProcedureAddress(libh,'XReconfigureWMWindow');
  pointer(XGetWMProtocols) := GetProcedureAddress(libh,'XGetWMProtocols');
  pointer(XSetWMProtocols) := GetProcedureAddress(libh,'XSetWMProtocols');
  pointer(XIconifyWindow) := GetProcedureAddress(libh,'XIconifyWindow');
  pointer(XWithdrawWindow) := GetProcedureAddress(libh,'XWithdrawWindow');
  pointer(XGetCommand) := GetProcedureAddress(libh,'XGetCommand');
  pointer(XGetWMColormapWindows) := GetProcedureAddress(libh,'XGetWMColormapWindows');
  pointer(XSetWMColormapWindows) := GetProcedureAddress(libh,'XSetWMColormapWindows');
  pointer(XFreeStringList) := GetProcedureAddress(libh,'XFreeStringList');
  pointer(XSetTransientForHint) := GetProcedureAddress(libh,'XSetTransientForHint');
  pointer(XActivateScreenSaver) := GetProcedureAddress(libh,'XActivateScreenSaver');
  pointer(XAddHost) := GetProcedureAddress(libh,'XAddHost');
  pointer(XAddHosts) := GetProcedureAddress(libh,'XAddHosts');
  pointer(XAddToExtensionList) := GetProcedureAddress(libh,'XAddToExtensionList');
  pointer(XAddToSaveSet) := GetProcedureAddress(libh,'XAddToSaveSet');
  pointer(XAllocColor) := GetProcedureAddress(libh,'XAllocColor');
  pointer(XAllocColorCells) := GetProcedureAddress(libh,'XAllocColorCells');
  pointer(XAllocColorPlanes) := GetProcedureAddress(libh,'XAllocColorPlanes');
  pointer(XAllocNamedColor) := GetProcedureAddress(libh,'XAllocNamedColor');
  pointer(XAllowEvents) := GetProcedureAddress(libh,'XAllowEvents');
  pointer(XAutoRepeatOff) := GetProcedureAddress(libh,'XAutoRepeatOff');
  pointer(XAutoRepeatOn) := GetProcedureAddress(libh,'XAutoRepeatOn');
  pointer(XBell) := GetProcedureAddress(libh,'XBell');
  pointer(XBitmapBitOrder) := GetProcedureAddress(libh,'XBitmapBitOrder');
  pointer(XBitmapPad) := GetProcedureAddress(libh,'XBitmapPad');
  pointer(XBitmapUnit) := GetProcedureAddress(libh,'XBitmapUnit');
  pointer(XCellsOfScreen) := GetProcedureAddress(libh,'XCellsOfScreen');
  pointer(XChangeActivePointerGrab) := GetProcedureAddress(libh,'XChangeActivePointerGrab');
  pointer(XChangeGC) := GetProcedureAddress(libh,'XChangeGC');
  pointer(XChangeKeyboardControl) := GetProcedureAddress(libh,'XChangeKeyboardControl');
  pointer(XChangeKeyboardMapping) := GetProcedureAddress(libh,'XChangeKeyboardMapping');
  pointer(XChangePointerControl) := GetProcedureAddress(libh,'XChangePointerControl');
  pointer(XChangeProperty) := GetProcedureAddress(libh,'XChangeProperty');
  pointer(XChangeSaveSet) := GetProcedureAddress(libh,'XChangeSaveSet');
  pointer(XChangeWindowAttributes) := GetProcedureAddress(libh,'XChangeWindowAttributes');
  pointer(XCheckIfEvent) := GetProcedureAddress(libh,'XCheckIfEvent');
  pointer(XCheckMaskEvent) := GetProcedureAddress(libh,'XCheckMaskEvent');
  pointer(XCheckTypedEvent) := GetProcedureAddress(libh,'XCheckTypedEvent');
  pointer(XCheckTypedWindowEvent) := GetProcedureAddress(libh,'XCheckTypedWindowEvent');
  pointer(XCheckWindowEvent) := GetProcedureAddress(libh,'XCheckWindowEvent');
  pointer(XCirculateSubwindows) := GetProcedureAddress(libh,'XCirculateSubwindows');
  pointer(XCirculateSubwindowsDown) := GetProcedureAddress(libh,'XCirculateSubwindowsDown');
  pointer(XCirculateSubwindowsUp) := GetProcedureAddress(libh,'XCirculateSubwindowsUp');
  pointer(_XClearArea) := GetProcedureAddress(libh,'XClearArea');
  pointer(XClearWindow) := GetProcedureAddress(libh,'XClearWindow');
  pointer(XCloseDisplay) := GetProcedureAddress(libh,'XCloseDisplay');
  pointer(XConfigureWindow) := GetProcedureAddress(libh,'XConfigureWindow');
  pointer(XConnectionNumber) := GetProcedureAddress(libh,'XConnectionNumber');
  pointer(XConvertSelection) := GetProcedureAddress(libh,'XConvertSelection');
  pointer(XCopyArea) := GetProcedureAddress(libh,'XCopyArea');
  pointer(XCopyGC) := GetProcedureAddress(libh,'XCopyGC');
  pointer(XCopyPlane) := GetProcedureAddress(libh,'XCopyPlane');
  pointer(XDefaultDepth) := GetProcedureAddress(libh,'XDefaultDepth');
  pointer(XDefaultDepthOfScreen) := GetProcedureAddress(libh,'XDefaultDepthOfScreen');
  pointer(XDefaultScreen) := GetProcedureAddress(libh,'XDefaultScreen');
  pointer(XDefineCursor) := GetProcedureAddress(libh,'XDefineCursor');
  pointer(XDeleteProperty) := GetProcedureAddress(libh,'XDeleteProperty');
  pointer(XDestroyWindow) := GetProcedureAddress(libh,'XDestroyWindow');
  pointer(XDestroySubwindows) := GetProcedureAddress(libh,'XDestroySubwindows');
  pointer(XDoesBackingStore) := GetProcedureAddress(libh,'XDoesBackingStore');
  pointer(XDoesSaveUnders) := GetProcedureAddress(libh,'XDoesSaveUnders');
  pointer(XDisableAccessControl) := GetProcedureAddress(libh,'XDisableAccessControl');
  pointer(XDisplayCells) := GetProcedureAddress(libh,'XDisplayCells');
  pointer(XDisplayHeight) := GetProcedureAddress(libh,'XDisplayHeight');
  pointer(XDisplayHeightMM) := GetProcedureAddress(libh,'XDisplayHeightMM');
  pointer(XDisplayKeycodes) := GetProcedureAddress(libh,'XDisplayKeycodes');
  pointer(XDisplayPlanes) := GetProcedureAddress(libh,'XDisplayPlanes');
  pointer(XDisplayWidth) := GetProcedureAddress(libh,'XDisplayWidth');
  pointer(XDisplayWidthMM) := GetProcedureAddress(libh,'XDisplayWidthMM');
  pointer(XDrawArc) := GetProcedureAddress(libh,'XDrawArc');
  pointer(XDrawArcs) := GetProcedureAddress(libh,'XDrawArcs');
  pointer(XDrawImageString) := GetProcedureAddress(libh,'XDrawImageString');
  pointer(XDrawImageString16) := GetProcedureAddress(libh,'XDrawImageString16');
  pointer(XDrawLine) := GetProcedureAddress(libh,'XDrawLine');
  pointer(XDrawLines) := GetProcedureAddress(libh,'XDrawLines');
  pointer(XDrawPoint) := GetProcedureAddress(libh,'XDrawPoint');
  pointer(XDrawPoints) := GetProcedureAddress(libh,'XDrawPoints');
  pointer(XDrawRectangle) := GetProcedureAddress(libh,'XDrawRectangle');
  pointer(XDrawRectangles) := GetProcedureAddress(libh,'XDrawRectangles');
  pointer(XDrawSegments) := GetProcedureAddress(libh,'XDrawSegments');
  pointer(XDrawString) := GetProcedureAddress(libh,'XDrawString');
  pointer(XDrawString16) := GetProcedureAddress(libh,'XDrawString16');
  pointer(XDrawText) := GetProcedureAddress(libh,'XDrawText');
  pointer(XDrawText16) := GetProcedureAddress(libh,'XDrawText16');
  pointer(XEnableAccessControl) := GetProcedureAddress(libh,'XEnableAccessControl');
  pointer(XEventsQueued) := GetProcedureAddress(libh,'XEventsQueued');
  pointer(XFetchName) := GetProcedureAddress(libh,'XFetchName');
  pointer(XFillArc) := GetProcedureAddress(libh,'XFillArc');
  pointer(XFillArcs) := GetProcedureAddress(libh,'XFillArcs');
  pointer(XFillPolygon) := GetProcedureAddress(libh,'XFillPolygon');
  pointer(XFillRectangle) := GetProcedureAddress(libh,'XFillRectangle');
  pointer(XFillRectangles) := GetProcedureAddress(libh,'XFillRectangles');
  pointer(XFlush) := GetProcedureAddress(libh,'XFlush');
  pointer(XForceScreenSaver) := GetProcedureAddress(libh,'XForceScreenSaver');
  pointer(XFree) := GetProcedureAddress(libh,'XFree');
  pointer(XFreeColormap) := GetProcedureAddress(libh,'XFreeColormap');
  pointer(XFreeColors) := GetProcedureAddress(libh,'XFreeColors');
  pointer(XFreeCursor) := GetProcedureAddress(libh,'XFreeCursor');
  pointer(XFreeExtensionList) := GetProcedureAddress(libh,'XFreeExtensionList');
  pointer(XFreeFont) := GetProcedureAddress(libh,'XFreeFont');
  pointer(XFreeFontInfo) := GetProcedureAddress(libh,'XFreeFontInfo');
  pointer(XFreeFontNames) := GetProcedureAddress(libh,'XFreeFontNames');
  pointer(XFreeFontPath) := GetProcedureAddress(libh,'XFreeFontPath');
  pointer(XFreeGC) := GetProcedureAddress(libh,'XFreeGC');
  pointer(XFreeModifiermap) := GetProcedureAddress(libh,'XFreeModifiermap');
  pointer(XFreePixmap) := GetProcedureAddress(libh,'XFreePixmap');
  pointer(XGeometry) := GetProcedureAddress(libh,'XGeometry');
  pointer(XGetErrorDatabaseText) := GetProcedureAddress(libh,'XGetErrorDatabaseText');
  pointer(XGetErrorText) := GetProcedureAddress(libh,'XGetErrorText');
  pointer(XGetFontProperty) := GetProcedureAddress(libh,'XGetFontProperty');
  pointer(XGetGCValues) := GetProcedureAddress(libh,'XGetGCValues');
  pointer(XGetGeometry) := GetProcedureAddress(libh,'XGetGeometry');
  pointer(XGetIconName) := GetProcedureAddress(libh,'XGetIconName');
  pointer(XGetInputFocus) := GetProcedureAddress(libh,'XGetInputFocus');
  pointer(XGetKeyboardControl) := GetProcedureAddress(libh,'XGetKeyboardControl');
  pointer(XGetPointerControl) := GetProcedureAddress(libh,'XGetPointerControl');
  pointer(XGetPointerMapping) := GetProcedureAddress(libh,'XGetPointerMapping');
  pointer(XGetScreenSaver) := GetProcedureAddress(libh,'XGetScreenSaver');
  pointer(XGetTransientForHint) := GetProcedureAddress(libh,'XGetTransientForHint');
  pointer(_XGetWindowProperty) := GetProcedureAddress(libh,'XGetWindowProperty');
  pointer(XGetWindowAttributes) := GetProcedureAddress(libh,'XGetWindowAttributes');
  pointer(XGrabButton) := GetProcedureAddress(libh,'XGrabButton');
  pointer(XGrabKey) := GetProcedureAddress(libh,'XGrabKey');
  pointer(_XGrabKeyboard) := GetProcedureAddress(libh,'XGrabKeyboard');
  pointer(_XGrabPointer) := GetProcedureAddress(libh,'XGrabPointer');
  pointer(XGrabServer) := GetProcedureAddress(libh,'XGrabServer');
  pointer(XHeightMMOfScreen) := GetProcedureAddress(libh,'XHeightMMOfScreen');
  pointer(XHeightOfScreen) := GetProcedureAddress(libh,'XHeightOfScreen');
  pointer(XIfEvent) := GetProcedureAddress(libh,'XIfEvent');
  pointer(XImageByteOrder) := GetProcedureAddress(libh,'XImageByteOrder');
  pointer(XInstallColormap) := GetProcedureAddress(libh,'XInstallColormap');
  pointer(XKeysymToKeycode) := GetProcedureAddress(libh,'XKeysymToKeycode');
  pointer(XKillClient) := GetProcedureAddress(libh,'XKillClient');
  pointer(XLookupColor) := GetProcedureAddress(libh,'XLookupColor');
  pointer(XLowerWindow) := GetProcedureAddress(libh,'XLowerWindow');
  pointer(XMapRaised) := GetProcedureAddress(libh,'XMapRaised');
  pointer(XMapSubwindows) := GetProcedureAddress(libh,'XMapSubwindows');
  pointer(XMapWindow) := GetProcedureAddress(libh,'XMapWindow');
  pointer(XMaskEvent) := GetProcedureAddress(libh,'XMaskEvent');
  pointer(XMaxCmapsOfScreen) := GetProcedureAddress(libh,'XMaxCmapsOfScreen');
  pointer(XMinCmapsOfScreen) := GetProcedureAddress(libh,'XMinCmapsOfScreen');
  pointer(XMoveResizeWindow) := GetProcedureAddress(libh,'XMoveResizeWindow');
  pointer(XMoveWindow) := GetProcedureAddress(libh,'XMoveWindow');
  pointer(XNextEvent) := GetProcedureAddress(libh,'XNextEvent');
  pointer(XNoOp) := GetProcedureAddress(libh,'XNoOp');
  pointer(XParseColor) := GetProcedureAddress(libh,'XParseColor');
  pointer(XParseGeometry) := GetProcedureAddress(libh,'XParseGeometry');
  pointer(XPeekEvent) := GetProcedureAddress(libh,'XPeekEvent');
  pointer(XPeekIfEvent) := GetProcedureAddress(libh,'XPeekIfEvent');
  pointer(XPending) := GetProcedureAddress(libh,'XPending');
  pointer(XPlanesOfScreen) := GetProcedureAddress(libh,'XPlanesOfScreen');
  pointer(XProtocolRevision) := GetProcedureAddress(libh,'XProtocolRevision');
  pointer(XProtocolVersion) := GetProcedureAddress(libh,'XProtocolVersion');
  pointer(XPutBackEvent) := GetProcedureAddress(libh,'XPutBackEvent');
  pointer(XPutImage) := GetProcedureAddress(libh,'XPutImage');
  pointer(XQLength) := GetProcedureAddress(libh,'XQLength');
  pointer(XQueryBestCursor) := GetProcedureAddress(libh,'XQueryBestCursor');
  pointer(XQueryBestSize) := GetProcedureAddress(libh,'XQueryBestSize');
  pointer(XQueryBestStipple) := GetProcedureAddress(libh,'XQueryBestStipple');
  pointer(XQueryBestTile) := GetProcedureAddress(libh,'XQueryBestTile');
  pointer(XQueryColor) := GetProcedureAddress(libh,'XQueryColor');
  pointer(XQueryColors) := GetProcedureAddress(libh,'XQueryColors');
  pointer(XQueryExtension) := GetProcedureAddress(libh,'XQueryExtension');
  pointer(XQueryKeymap) := GetProcedureAddress(libh,'XQueryKeymap');
  pointer(XQueryPointer) := GetProcedureAddress(libh,'XQueryPointer');
  pointer(XQueryTextExtents) := GetProcedureAddress(libh,'XQueryTextExtents');
  pointer(XQueryTextExtents16) := GetProcedureAddress(libh,'XQueryTextExtents16');
  pointer(XQueryTree) := GetProcedureAddress(libh,'XQueryTree');
  pointer(XRaiseWindow) := GetProcedureAddress(libh,'XRaiseWindow');
  pointer(XReadBitmapFile) := GetProcedureAddress(libh,'XReadBitmapFile');
  pointer(XReadBitmapFileData) := GetProcedureAddress(libh,'XReadBitmapFileData');
  pointer(XRebindKeysym) := GetProcedureAddress(libh,'XRebindKeysym');
  pointer(XRecolorCursor) := GetProcedureAddress(libh,'XRecolorCursor');
  pointer(XRefreshKeyboardMapping) := GetProcedureAddress(libh,'XRefreshKeyboardMapping');
  pointer(XRemoveFromSaveSet) := GetProcedureAddress(libh,'XRemoveFromSaveSet');
  pointer(XRemoveHost) := GetProcedureAddress(libh,'XRemoveHost');
  pointer(XRemoveHosts) := GetProcedureAddress(libh,'XRemoveHosts');
  pointer(XReparentWindow) := GetProcedureAddress(libh,'XReparentWindow');
  pointer(XResetScreenSaver) := GetProcedureAddress(libh,'XResetScreenSaver');
  pointer(XResizeWindow) := GetProcedureAddress(libh,'XResizeWindow');
  pointer(XRestackWindows) := GetProcedureAddress(libh,'XRestackWindows');
  pointer(XRotateBuffers) := GetProcedureAddress(libh,'XRotateBuffers');
  pointer(XRotateWindowProperties) := GetProcedureAddress(libh,'XRotateWindowProperties');
  pointer(XScreenCount) := GetProcedureAddress(libh,'XScreenCount');
  pointer(XSelectInput) := GetProcedureAddress(libh,'XSelectInput');
  pointer(_XSendEvent) := GetProcedureAddress(libh,'XSendEvent');
  pointer(XSetAccessControl) := GetProcedureAddress(libh,'XSetAccessControl');
  pointer(XSetArcMode) := GetProcedureAddress(libh,'XSetArcMode');
  pointer(XSetBackground) := GetProcedureAddress(libh,'XSetBackground');
  pointer(XSetClipMask) := GetProcedureAddress(libh,'XSetClipMask');
  pointer(XSetClipOrigin) := GetProcedureAddress(libh,'XSetClipOrigin');
  pointer(XSetClipRectangles) := GetProcedureAddress(libh,'XSetClipRectangles');
  pointer(XSetCloseDownMode) := GetProcedureAddress(libh,'XSetCloseDownMode');
  pointer(XSetCommand) := GetProcedureAddress(libh,'XSetCommand');
  pointer(XSetDashes) := GetProcedureAddress(libh,'XSetDashes');
  pointer(XSetFillRule) := GetProcedureAddress(libh,'XSetFillRule');
  pointer(XSetFillStyle) := GetProcedureAddress(libh,'XSetFillStyle');
  pointer(XSetFont) := GetProcedureAddress(libh,'XSetFont');
  pointer(XSetFontPath) := GetProcedureAddress(libh,'XSetFontPath');
  pointer(XSetForeground) := GetProcedureAddress(libh,'XSetForeground');
  pointer(XSetFunction) := GetProcedureAddress(libh,'XSetFunction');
  pointer(_XSetGraphicsExposures) := GetProcedureAddress(libh,'XSetGraphicsExposures');
  pointer(XSetIconName) := GetProcedureAddress(libh,'XSetIconName');
  pointer(XSetInputFocus) := GetProcedureAddress(libh,'XSetInputFocus');
  pointer(XSetLineAttributes) := GetProcedureAddress(libh,'XSetLineAttributes');
  pointer(XSetModifierMapping) := GetProcedureAddress(libh,'XSetModifierMapping');
  pointer(XSetPlaneMask) := GetProcedureAddress(libh,'XSetPlaneMask');
  pointer(XSetPointerMapping) := GetProcedureAddress(libh,'XSetPointerMapping');
  pointer(XSetScreenSaver) := GetProcedureAddress(libh,'XSetScreenSaver');
  pointer(XSetSelectionOwner) := GetProcedureAddress(libh,'XSetSelectionOwner');
  pointer(XSetState) := GetProcedureAddress(libh,'XSetState');
  pointer(XSetStipple) := GetProcedureAddress(libh,'XSetStipple');
  pointer(XSetSubwindowMode) := GetProcedureAddress(libh,'XSetSubwindowMode');
  pointer(XSetTSOrigin) := GetProcedureAddress(libh,'XSetTSOrigin');
  pointer(XSetTile) := GetProcedureAddress(libh,'XSetTile');
  pointer(XSetWindowBackground) := GetProcedureAddress(libh,'XSetWindowBackground');
  pointer(XSetWindowBackgroundPixmap) := GetProcedureAddress(libh,'XSetWindowBackgroundPixmap');
  pointer(XSetWindowBorder) := GetProcedureAddress(libh,'XSetWindowBorder');
  pointer(XSetWindowBorderPixmap) := GetProcedureAddress(libh,'XSetWindowBorderPixmap');
  pointer(XSetWindowBorderWidth) := GetProcedureAddress(libh,'XSetWindowBorderWidth');
  pointer(XSetWindowColormap) := GetProcedureAddress(libh,'XSetWindowColormap');
  pointer(XStoreBuffer) := GetProcedureAddress(libh,'XStoreBuffer');
  pointer(XStoreBytes) := GetProcedureAddress(libh,'XStoreBytes');
  pointer(XStoreColor) := GetProcedureAddress(libh,'XStoreColor');
  pointer(XStoreColors) := GetProcedureAddress(libh,'XStoreColors');
  pointer(XStoreName) := GetProcedureAddress(libh,'XStoreName');
  pointer(XStoreNamedColor) := GetProcedureAddress(libh,'XStoreNamedColor');
  pointer(_XSync) := GetProcedureAddress(libh,'XSync');
  pointer(XTextExtents) := GetProcedureAddress(libh,'XTextExtents');
  pointer(XTextExtents16) := GetProcedureAddress(libh,'XTextExtents16');
  pointer(XTextWidth) := GetProcedureAddress(libh,'XTextWidth');
  pointer(XTextWidth16) := GetProcedureAddress(libh,'XTextWidth16');
  pointer(XTranslateCoordinates) := GetProcedureAddress(libh,'XTranslateCoordinates');
  pointer(XUndefineCursor) := GetProcedureAddress(libh,'XUndefineCursor');
  pointer(XUngrabButton) := GetProcedureAddress(libh,'XUngrabButton');
  pointer(XUngrabKey) := GetProcedureAddress(libh,'XUngrabKey');
  pointer(XUngrabKeyboard) := GetProcedureAddress(libh,'XUngrabKeyboard');
  pointer(XUngrabPointer) := GetProcedureAddress(libh,'XUngrabPointer');
  pointer(XUngrabServer) := GetProcedureAddress(libh,'XUngrabServer');
  pointer(XUninstallColormap) := GetProcedureAddress(libh,'XUninstallColormap');
  pointer(XUnloadFont) := GetProcedureAddress(libh,'XUnloadFont');
  pointer(XUnmapSubwindows) := GetProcedureAddress(libh,'XUnmapSubwindows');
  pointer(XUnmapWindow) := GetProcedureAddress(libh,'XUnmapWindow');
  pointer(XVendorRelease) := GetProcedureAddress(libh,'XVendorRelease');
  pointer(XWarpPointer) := GetProcedureAddress(libh,'XWarpPointer');
  pointer(XWidthMMOfScreen) := GetProcedureAddress(libh,'XWidthMMOfScreen');
  pointer(XWidthOfScreen) := GetProcedureAddress(libh,'XWidthOfScreen');
  pointer(XWindowEvent) := GetProcedureAddress(libh,'XWindowEvent');
  pointer(XWriteBitmapFile) := GetProcedureAddress(libh,'XWriteBitmapFile');
  pointer(XSetLocaleModifiers) := GetProcedureAddress(libh,'XSetLocaleModifiers');
  pointer(XOpenOM) := GetProcedureAddress(libh,'XOpenOM');
  pointer(XCloseOM) := GetProcedureAddress(libh,'XCloseOM');
  pointer(XSetOMValues) := GetProcedureAddress(libh,'XSetOMValues');
  pointer(XGetOMValues) := GetProcedureAddress(libh,'XGetOMValues');
  pointer(XDisplayOfOM) := GetProcedureAddress(libh,'XDisplayOfOM');
  pointer(XLocaleOfOM) := GetProcedureAddress(libh,'XLocaleOfOM');
  pointer(XCreateOC) := GetProcedureAddress(libh,'XCreateOC');
  pointer(XDestroyOC) := GetProcedureAddress(libh,'XDestroyOC');
  pointer(XOMOfOC) := GetProcedureAddress(libh,'XOMOfOC');
  pointer(XSetOCValues) := GetProcedureAddress(libh,'XSetOCValues');
  pointer(XGetOCValues) := GetProcedureAddress(libh,'XGetOCValues');
  pointer(XCreateFontSet) := GetProcedureAddress(libh,'XCreateFontSet');
  pointer(XFreeFontSet) := GetProcedureAddress(libh,'XFreeFontSet');
  pointer(XFontsOfFontSet) := GetProcedureAddress(libh,'XFontsOfFontSet');
  pointer(XBaseFontNameListOfFontSet) := GetProcedureAddress(libh,'XBaseFontNameListOfFontSet');
  pointer(XLocaleOfFontSet) := GetProcedureAddress(libh,'XLocaleOfFontSet');
  pointer(XContextDependentDrawing) := GetProcedureAddress(libh,'XContextDependentDrawing');
  pointer(XDirectionalDependentDrawing) := GetProcedureAddress(libh,'XDirectionalDependentDrawing');
  pointer(XContextualDrawing) := GetProcedureAddress(libh,'XContextualDrawing');
  pointer(XExtentsOfFontSet) := GetProcedureAddress(libh,'XExtentsOfFontSet');
  pointer(XmbTextEscapement) := GetProcedureAddress(libh,'XmbTextEscapement');
  pointer(XwcTextEscapement) := GetProcedureAddress(libh,'XwcTextEscapement');
  pointer(Xutf8TextEscapement) := GetProcedureAddress(libh,'Xutf8TextEscapement');
  pointer(XmbTextExtents) := GetProcedureAddress(libh,'XmbTextExtents');
  pointer(XwcTextExtents) := GetProcedureAddress(libh,'XwcTextExtents');
  pointer(Xutf8TextExtents) := GetProcedureAddress(libh,'Xutf8TextExtents');
  pointer(XmbTextPerCharExtents) := GetProcedureAddress(libh,'XmbTextPerCharExtents');
  pointer(XwcTextPerCharExtents) := GetProcedureAddress(libh,'XwcTextPerCharExtents');
  pointer(Xutf8TextPerCharExtents) := GetProcedureAddress(libh,'Xutf8TextPerCharExtents');
  pointer(XmbDrawText) := GetProcedureAddress(libh,'XmbDrawText');
  pointer(XwcDrawText) := GetProcedureAddress(libh,'XwcDrawText');
  pointer(Xutf8DrawText) := GetProcedureAddress(libh,'Xutf8DrawText');
  pointer(XmbDrawString) := GetProcedureAddress(libh,'XmbDrawString');
  pointer(XwcDrawString) := GetProcedureAddress(libh,'XwcDrawString');
  pointer(Xutf8DrawString) := GetProcedureAddress(libh,'Xutf8DrawString');
  pointer(XmbDrawImageString) := GetProcedureAddress(libh,'XmbDrawImageString');
  pointer(XwcDrawImageString) := GetProcedureAddress(libh,'XwcDrawImageString');
  pointer(Xutf8DrawImageString) := GetProcedureAddress(libh,'Xutf8DrawImageString');
  pointer(XOpenIM) := GetProcedureAddress(libh,'XOpenIM');
  pointer(XCloseIM) := GetProcedureAddress(libh,'XCloseIM');
  pointer(XGetIMValues) := GetProcedureAddress(libh,'XGetIMValues');
  pointer(XSetIMValues) := GetProcedureAddress(libh,'XSetIMValues');
  pointer(XDisplayOfIM) := GetProcedureAddress(libh,'XDisplayOfIM');
  pointer(XLocaleOfIM) := GetProcedureAddress(libh,'XLocaleOfIM');
  pointer(XCreateIC) := GetProcedureAddress(libh,'XCreateIC');
  pointer(XDestroyIC) := GetProcedureAddress(libh,'XDestroyIC');
  pointer(XSetICFocus) := GetProcedureAddress(libh,'XSetICFocus');
  pointer(XUnsetICFocus) := GetProcedureAddress(libh,'XUnsetICFocus');
  pointer(XwcResetIC) := GetProcedureAddress(libh,'XwcResetIC');
  pointer(XmbResetIC) := GetProcedureAddress(libh,'XmbResetIC');
  pointer(Xutf8ResetIC) := GetProcedureAddress(libh,'Xutf8ResetIC');
  pointer(XSetICValues) := GetProcedureAddress(libh,'XSetICValues');
  pointer(XGetICValues) := GetProcedureAddress(libh,'XGetICValues');
  pointer(XIMOfIC) := GetProcedureAddress(libh,'XIMOfIC');
  pointer(XFilterEvent) := GetProcedureAddress(libh,'XFilterEvent');
  pointer(XmbLookupString) := GetProcedureAddress(libh,'XmbLookupString');
  pointer(XwcLookupString) := GetProcedureAddress(libh,'XwcLookupString');
  pointer(Xutf8LookupString) := GetProcedureAddress(libh,'Xutf8LookupString');
  pointer(XVaCreateNestedList) := GetProcedureAddress(libh,'XVaCreateNestedList');
  pointer(XRegisterIMInstantiateCallback) := GetProcedureAddress(libh,'XRegisterIMInstantiateCallback');
  pointer(XUnregisterIMInstantiateCallback) := GetProcedureAddress(libh,'XUnregisterIMInstantiateCallback');
  pointer(XInternalConnectionNumbers) := GetProcedureAddress(libh,'XInternalConnectionNumbers');
  pointer(XProcessInternalConnection) := GetProcedureAddress(libh,'XProcessInternalConnection');
  pointer(XAddConnectionWatch) := GetProcedureAddress(libh,'XAddConnectionWatch');
  pointer(XRemoveConnectionWatch) := GetProcedureAddress(libh,'XRemoveConnectionWatch');
  pointer(XSetAuthorization) := GetProcedureAddress(libh,'XSetAuthorization');
  pointer(XGetEventData) := GetProcedureAddress(libh,'XGetEventData');
  pointer(XFreeEventData) := GetProcedureAddress(libh,'XFreeEventData');
end;


var 
lh : TLibHandle;

function XlibAvailable() : boolean;
begin
  XlibAvailable := (lh<>NilHandle);
end;

function XlibLibHandle() : TLibHandle;
begin
  XlibLibHandle := lh;
end;

procedure do_init();
begin
  init_procvars();
  if lh<>NilHandle then begin
    load_procvars(lh);
  end;
end;


initialization
begin
  lh := LoadLibrary(libX11);
writeln('loading Xlib @(',libX11,')',lh<>NilHandle);
  do_init();
end;

finalization
begin
  if lh<>NilHandle then UnloadLibrary(lh);
end;


end.
