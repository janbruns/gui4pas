UNIT ttdbggf;
{$mode objfpc}
{$COPERATORS ON}


interface
uses ttgridfit, ttptinst, ttcalc, ttprog, fontfile,ttdata;

type
Tdebuggridfitter = class(Tgridfitter)
  constructor create();
  destructor destroy(); override;

  procedure onBeforePointOperation(); virtual;
  procedure onEnterGlyphInstructions(g,g0 : TTglyph); override;

  procedure untake_glyphdata(g0,g : TTglyph; p0,np,c0,nc : longint); override;


  procedure do_GC(b : byte); override;
  procedure do_MD(b : byte); override;
  procedure do_MIAP(b : byte); override;
  procedure do_MDRP_instr(b : byte); override;
  procedure do_MSIRP(b : byte); override;
  procedure do_MIRP_instr(b : byte); override;
  procedure do_MDAP(b : byte); override;
  procedure do_ALIGNRP(b : byte); override;
  procedure do_ALPIGNPTS(b : byte); override;
  procedure do_IP(b : byte); override;
  procedure interpol(p1,p2,p3 : longint); override;
  procedure do_IUP(b : byte); override;
  procedure do_UTP(b : byte); override;
  procedure do_SHZ(b : byte); override;
  procedure do_SHPIX(b : byte); override;
  procedure do_SHP(b : byte); override;
  procedure do_SHC(b : byte); override;
  procedure do_SCFS(b : byte); override;

  procedure do_WCVTP(b : byte); override;
  procedure do_readwrite_store(b : byte); override;

  procedure print_gs();
  procedure print_gridfitdbg(); virtual;

  procedure set_verbosity(v : longint);

  procedure onExec(); override;

  procedure onInstructionExec(); override;
  procedure do_dump_callstack(msg : ansistring);
  procedure print_stack();
  function get_IP_string(p : TTprogram; ep : dword) : ansistring;
  procedure print_instruction();


  procedure make_svg(moved_point : longint);

  procedure set_zonepoint_x(ptidx,zone : longint; v : F26DOT6); override;
  procedure set_zonepoint_y(ptidx,zone : longint; v : F26DOT6); override;


  protected
  procedure setRefPoints(a,az,b,bz : longint); // only used for debug purposes
  function get_glyph_string(g : TTglyph; s1,s2 : ansistring) : ansistring;





  public 
  svg_prefix : string;

  print_state_before_point_operations : boolean;
  current_refpoint1, current_refpoint2 : longint;
  printcode : boolean;
  printstackelems : longint;
  print_pointmoves : boolean;
  print_svg : boolean;
  print_store : boolean;
  svgnum : longint;
end;




implementation
uses sysutils;


var instrname : array[0..255] of string;




procedure Tdebuggridfitter.set_verbosity(v : longint);
begin
  printstackelems  := (v and 7);
  printcode := ((v and 8)>0);
  print_pointmoves := ((v and 16)>0);
  print_state_before_point_operations := ((v and 32)>0);
  print_svg := ((v and 64)>0);
  print_store := ((v and 128)>0);
end;

procedure Tdebuggridfitter.print_gridfitdbg();
begin
end;

procedure Tdebuggridfitter.onEnterGlyphInstructions(g,g0 : TTglyph); 
begin
  inherited;
  if printcode then writeln('Enter instructions on ',g.idx,' for ',g0.idx);
  setRefPoints(-1,0,-1,0); if print_pointmoves then make_svg(-1);
end;

procedure Tdebuggridfitter.set_zonepoint_x(ptidx,zone : longint; v : F26DOT6);
var d : longint;
begin
  d := get_zonepoint_x(ptidx,zone);
  chkZonePtIndex(ptidx,zone);
  if (zone=0) then zone0curx[ptidx] := v
              else zone1curx[ptidx] := v;
  if (zone=1) then begin
    if not(gs.fvx=0) then zone1touchX[ptidx] := true;
  end;
  if print_pointmoves and (zone=1) then begin
    writeln('Point ',ptidx,' xmoved by ',v-d,' from ',d,' to ',v);
    if (v-d)<>0 then make_svg(ptidx);
  end;
end;

procedure Tdebuggridfitter.set_zonepoint_y(ptidx,zone : longint; v : F26DOT6);
var d : longint;
begin
  d := get_zonepoint_y(ptidx,zone);
  chkZonePtIndex(ptidx,zone);
  if (zone=0) then zone0cury[ptidx] := v
              else zone1cury[ptidx] := v;
  if (zone=1) then begin
    if not(gs.fvy=0) then zone1touchY[ptidx] := true;
  end;
  if print_pointmoves and (zone=1) then begin
    writeln('Point ',ptidx,' ymoved by ',v-d,' from ',d,' to ',v);
    if (v-d)<>0 then make_svg(ptidx);
  end;
end;




procedure Tdebuggridfitter.setRefPoints(a,az,b,bz : longint); // only used for debug purposes
begin
  if (az=0) then a := -1;
  if (bz=0) then b := -1;
  current_refpoint1 := a;
  current_refpoint2 := b;
end;



function Tdebuggridfitter.get_glyph_string(g : TTglyph; s1,s2 : ansistring) : ansistring;
begin
  if not(g=nil) then get_glyph_string := s1+inttostr(g.idx)+s2
                else get_glyph_string := '';
end;


procedure Tdebuggridfitter.do_WCVTP(b : byte);
var i,j,k : longint; 
begin
  i := pop_stack();
  j := pop_stack();
  push_stack(j);
  push_stack(i);
  if (b=$70) then begin
    i := doscale(i);
  end;
  k := cvt.getValue(j);
  if print_store then writeln('cvt[',hexstr(j,4),']: ',hexstr(k,8),' <= ',hexstr(i,8),' (',longint(i),')');
  inherited;
end;

procedure Tdebuggridfitter.do_readwrite_store(b : byte);
var v,i : dword;
begin
  if (b=$42) then begin
    v := pop_stack();
    i := pop_stack();
    push_stack(i);
    push_stack(v);
    if print_store then writeln('storage[',hexstr(i,4),']: ',hexstr(progstorage.getValue(i),8),' <= ',hexstr(v,8),' (',longint(v),')');
  end;
  inherited;
end;

procedure printstorage(st : Ttdatastore; s : string);
var i,j : longint;
begin
  j := st.getLength()-1;
  for i := 0 to j do begin
    writeln(s,'[',hexstr(i,4),']: ',hexstr(st.getValue(i),8),' (',longint(st.getValue(i)),')');
  end;
end;

procedure Tdebuggridfitter.onExec();
begin
  inherited;
  if print_store then begin
    printstorage(cvt,'cvt');
    printstorage(progstorage,'storage');
  end;
end;


procedure Tdebuggridfitter.do_GC(b : byte);
var i : longint; 
begin
  onBeforePointOperation();
  i := pop_stack();
  push_stack(i);
  inherited do_GC(b);
end;


procedure Tdebuggridfitter.do_MD(b : byte);
var i,j : longint; 
begin
  onBeforePointOperation();
  i := pop_stack();
  j := pop_stack();
  push_stack(j);
  push_stack(i);
  inherited do_MD(b);
end;


procedure Tdebuggridfitter.do_MIAP(b : byte);
var i,c : longint;
begin
  setRefPoints(-1,0,-1,0);
  onBeforePointOperation();
  c := pop_stack();
  i := pop_stack();
  push_stack(i);
  push_stack(c);
  inherited do_MIAP(b);
end;



procedure Tdebuggridfitter.do_MDRP_instr(b : byte);
var i : longint; 
begin
  setRefPoints(gs.rp0,gs.zp0,-1,0);
  onBeforePointOperation();
  i := pop_stack();
  push_stack(i);
  inherited do_MDRP_instr(b);
end;



procedure Tdebuggridfitter.do_MSIRP(b : byte);
var i,v : longint; 
begin
  setRefPoints(gs.rp0,gs.zp0,-1,0);
  onBeforePointOperation();
  v := pop_stack();
  i := pop_stack();
  push_stack(i);
  push_stack(v);
  inherited do_MSIRP(b);
end;


procedure Tdebuggridfitter.do_MIRP_instr(b : byte);
var i,c : longint; 
begin
  setRefPoints(gs.rp0,gs.zp0,-1,0);
  onBeforePointOperation();
  c := pop_stack();
  i := pop_stack();
  push_stack(i);
  push_stack(c);
  inherited do_MIRP_instr(b);
end;



procedure Tdebuggridfitter.do_MDAP(b : byte);
var i : longint; 
begin
  setRefPoints(-1,0,-1,0);
  onBeforePointOperation();
  i := pop_stack();
  push_stack(i);
  inherited do_MDAP(b);
end;



procedure Tdebuggridfitter.do_ALIGNRP(b : byte);
begin
  setRefPoints(gs.rp0,gs.zp0,-1,0);
  onBeforePointOperation();
  inherited do_ALIGNRP(b);
end;




procedure Tdebuggridfitter.do_ALPIGNPTS(b : byte);
var i,j : longint; 
begin
  onBeforePointOperation();
//  writeln('do_ALPIGNPTS');
  i := pop_stack();
  j := pop_stack();
  push_stack(j);
  push_stack(i);
  setRefPoints(j,gs.zp0,i,gs.zp1);
  inherited do_ALPIGNPTS(b);
end;


procedure Tdebuggridfitter.do_IP(b : byte);
begin
  setRefPoints(gs.rp1,gs.zp0,gs.rp2,gs.zp1);
  onBeforePointOperation();
  inherited do_IP(b);
end;



procedure Tdebuggridfitter.interpol(p1,p2,p3 : longint);
begin
  setRefPoints(p1,1,p2,1);
  inherited interpol(p1,p2,p3);
end;


procedure Tdebuggridfitter.do_IUP(b : byte);
begin
  onBeforePointOperation();
  inherited do_IUP(b);
end;




procedure Tdebuggridfitter.do_UTP(b : byte);
var i : longint;
begin
  onBeforePointOperation();
  i := pop_stack();
  push_stack(i);
  inherited do_UTP(b);
end;

procedure Tdebuggridfitter.do_SHZ(b : byte);
var z : longint; 
begin
  onBeforePointOperation();
  z := pop_stack();
  push_stack(z);
  inherited do_SHZ(b);
end;

procedure Tdebuggridfitter.do_SHPIX(b : byte);
var d : F26DOT6;
begin
  setRefPoints(-1,0,-1,0);
  onBeforePointOperation();
  d := pop_stack();
  push_stack(d);
  inherited do_SHPIX(b);
end;

procedure Tdebuggridfitter.do_SHP(b : byte);
var rp,rz : longint;
begin
  onBeforePointOperation();
  if ((b and 1)=0) then begin
    rp := gs.rp2;
    rz := gs.zp1;
  end else begin
    rp := gs.rp1;
    rz := gs.zp0;
  end;
  setRefPoints(rp,rz,-1,0);
  inherited do_SHP(b);
end;


procedure Tdebuggridfitter.do_SHC(b : byte);
var c : longint;
begin
  setRefPoints(-1,0,-1,0);
  onBeforePointOperation();
  c := pop_stack();
  push_stack(c);
  inherited do_SHC(b);
end;




procedure Tdebuggridfitter.do_SCFS(b : byte);
var i,v : dword;
begin
  setRefPoints(-1,0,-1,0);
  v := pop_stack();
  i := pop_stack();
  push_stack(i);
  push_stack(v);
  inherited do_SCFS(b);
end;


procedure Tdebuggridfitter.onBeforePointOperation();
begin
  if print_state_before_point_operations then print_gs();
end;


procedure Tdebuggridfitter.onInstructionExec();
begin
  if printcode then print_instruction();
  print_gridfitdbg();
end;


procedure Tdebuggridfitter.untake_glyphdata(g0,g : TTglyph; p0,np,c0,nc : longint);
var i,c,len : longint;
begin
  c := 0;
  // discard "ugly" computations
  len := high(g.coordx)+1;
  for i := 0 to len-1 do begin
    if (zone1curx[i]<-640*ppem_x)
    or (zone1cury[i]<-640*ppem_y)
    or (zone1curx[i]>640*ppem_x)
    or (zone1cury[i]>640*ppem_y) then begin
      writeln('ugly gridfit results on subglyph. will get discarded.');
    end;
  end;
  for i := 0 to len+3 do begin
    if print_pointmoves then begin
      write(i);
      if (i<len) then begin 
        write(' : ');
        write(g0.scoordx[i+p0],'/',g0.scoordy[i+p0]);
      end else begin
        write('F: ');
        write(zone1orgx[i],'/',zone1orgy[i]);
      end;
      write(' => ');
      write(zone1curx[i],'/',zone1cury[i]);
      write('  onoff: ', g0.sonoffcurve[i+p0],' => ',zone1onoff[i]);
      writeln;
    end;
  end;
  inherited untake_glyphdata(g0,g,p0,np,c0,nc);
end;



procedure print_rounding_test(rs : Trndstate);
var i : longint;
begin
  writeln('Roundstate: (peri:',rs.peri,', phase:',rs.phase,', thresh:',rs.thresh,')');
  for i := -15 to 15 do begin
    write(i*10,':',doround(i*10,rs),' ');
  end;
  writeln;
end;

procedure Tdebuggridfitter.print_gs();
begin
  with gs do begin
    write('p(',pvx,',',pvy,') ');
    write('f(',fvx,',',fvy,') ');
    write('d(',dvx,',',dvy,') ');
    write('zp(',zp0,zp1,zp2,') ');
    write('rp(',rp0,',',rp1,',',rp2,') ');
    write('sw(',single_width_value,',',single_width_cut_in,') ');
    write('ci:',control_value_cut_in, 'auto:',ord(auto_flip),' ');
    write('md:',minimum_distance,' ');
    writeln('dd(',delta_base,',',delta_shift,')');
  end;
end;


procedure Tdebuggridfitter.do_dump_callstack(msg : ansistring);
var p : TTprogram; ep : dword;
begin
  writeln(msg);
  writeln('call-stack:');
  writeln( get_IP_string( prg,ip),' <--' );
  while pop_ret_addr(p,ep) do begin
    writeln( get_IP_string( p,ep ),' (*)');
  end;
  writeln('(*) ret addr to next instruction');
  raise Tttprogexception.create(msg);
end;

procedure Tdebuggridfitter.print_stack();
var i,j : longint;
begin
  for i := printstackelems downto 1 do begin
    j := get_current_sp()-i;
    if (j>=0) then write( ' ',hexstr(get_stack_element(j),8) )
    else write(' --------');
  end;
end;

function count_parentp(p : TTprogram) : longint;
var i : longint;
begin
  i := 0;
  while not(p=nil) do begin
    p := p.parent;
    inc(i);
  end;
  count_parentp := i;
end;

function Tdebuggridfitter.get_IP_string(p : TTprogram; ep : dword) : ansistring;
begin
  get_IP_string := hexstr(count_parentp(p),1)+'-'+hexstr(ep,4);
end;

procedure Tdebuggridfitter.print_instruction();
var i : longint;
begin
  if ifskip then write('@') else write(' ');
  if (defskip>0) then write('# ') else write('  ');
  write( get_IP_string(prg,ip) );
  write(' : ',hexstr(get_current_sp(),3),' =');
  print_stack();
  write(' : ',instrname[prg.instructions[ip]],'[');
  for i := 0 to ilen-1 do write(' ',hexstr(prg.instructions[ip+i],2));
  write(']');
  writeln;
end;

function inttostring(a,n : longint) : string;
var s : string;
begin
  str(a,s);
  while length(s)<n do s := '0'+s;
  inttostring := s;
end;

procedure Tdebuggridfitter.make_svg(moved_point : longint);
var a,i,c,ps : longint; f : text; col : string;
begin
  ps := round(pointsize);
  if print_svg and not(zone1glyph=nil) then begin
    assign(f,svg_prefix+inttostring(svgnum,4)+'.svg');
    rewrite(f);
    writeln(f,'<svg width="1000" height="1000" xmlns="http://www.w3.org/2000/svg">');
    writeln(f,'<g transform="translate(500,500) scale(',(8/pointsize),',',(-8/pointsize),')">');
    for i := -ps to ps do begin
      writeln(f,'<line x1="',  i*64,'" x2="', i*64,'" y1="',-ps*64,'" y2="',ps*64,'" stroke-width="2" stroke="black"/>');
      writeln(f,'<line x1="',-ps*64,'" x2="',ps*64,'" y1="',  i*64,'" y2="', i*64,'" stroke-width="2" stroke="black"/>');
    end;
    a := 0;
    write(f,'<path d="');
    for c := 0 to zone1glyph.numberOfContours-1 do begin
      write(f,'M ',zone1curx[a],' ',zone1cury[a],' ');
      for i := a+1 to zone1glyph.endPtsOfContours[c] do begin
        write(f,'L ',zone1curx[i],' ',zone1cury[i],' ');
      end;
      a := zone1glyph.endPtsOfContours[c]+1;
    end;
    writeln(f,'"/>');
    for i := a-1 downto 0 do begin
      col := 'grey';
      if zone1onoff[i] then col := 'red';
      if (i=current_refpoint1) then col := 'pink';
      if (i=current_refpoint2) then col := 'plum';
      if (i=moved_point) then begin
        if (zone1onoff[i]) then col := 'yellow' else col := 'greenyellow';
      end;
      writeln(f,'<circle cx="',zone1curx[i],'" cy="',zone1cury[i],'" r="',round(pointsize/3),'" fill="',col,'"/>');
    end;
    writeln(f,'</g>');
    writeln(f,'</svg>');
    close(f);
    writeln('generated svg file: ','debug_pm_'+inttostring(svgnum,4)+'.svg');
    inc(svgnum);
  end;
end;



constructor Tdebuggridfitter.create();
begin
  inherited create();
  print_state_before_point_operations := false;
  printcode := false;
  printstackelems := 3;
  print_pointmoves := false;
  print_svg := false;
  svgnum := 0;
  svg_prefix := 'ttf_debug_';

end;

destructor Tdebuggridfitter.destroy();
begin
  inherited destroy();
end;





procedure set_instrnames();
var i : longint;
begin
  for i := 0 to 255 do instrname[i] := '???';

  instrname[$00] := 'SVTCA';
  instrname[$01] := 'SVTCA';
  instrname[$02] := 'SPVTCA';
  instrname[$03] := 'SPVTCA';
  instrname[$04] := 'SFVTCA';
  instrname[$05] := 'SFVTCA';
  instrname[$06] := 'SPVTL';
  instrname[$07] := 'SPVTL';
  instrname[$08] := 'SFVTL';
  instrname[$09] := 'SFVTL';
  instrname[$0a] := 'SPVFS';
  instrname[$0b] := 'SFVFS';
  instrname[$0c] := 'GPV';
  instrname[$0d] := 'GFV';
  instrname[$0e] := 'SFVTPV';
  instrname[$0f] := 'ISECT';
  instrname[$10] := 'SRP0';
  instrname[$11] := 'SRP1';
  instrname[$12] := 'SRP2';
  instrname[$13] := 'SZP0';
  instrname[$14] := 'SZP1';
  instrname[$15] := 'SZP2';
  instrname[$16] := 'SZPS';
  instrname[$17] := 'SLOOP';
  instrname[$18] := 'RTG';
  instrname[$19] := 'RTHG';
  instrname[$1a] := 'SMD';
  instrname[$1b] := 'ELSE';
  instrname[$1C] := 'JMPR';
  instrname[$1d] := 'SCVTCI';
  instrname[$1e] := 'SSWCI';
  instrname[$1f] := 'SSW'; 
  instrname[$20] := 'DUP';
  instrname[$21] := 'POP';
  instrname[$22] := 'CLEAR';
  instrname[$23] := 'SWAP';
  instrname[$24] := 'DEPTH';
  instrname[$25] := 'CINDEX';
  instrname[$26] := 'MINDEX';
  instrname[$27] := 'ALIGNPTS';

  instrname[$29] := 'UTP';
  instrname[$2A] := 'LOOPCALL';
  instrname[$2B] := 'CALL';
  instrname[$2C] := 'DEFFUNC';
  instrname[$2D] := 'ENDF';
  instrname[$2E] := 'MDAP';
  instrname[$2F] := 'MDAP';
  instrname[$30] := 'IUP';
  instrname[$31] := 'IUP';
  instrname[$32] := 'SHP';
  instrname[$33] := 'SHP';
  instrname[$34] := 'SHC';
  instrname[$35] := 'SHC';
  instrname[$36] := 'SHZ';
  instrname[$37] := 'SHZ';
  instrname[$38] := 'SHPIX';
  instrname[$39] := 'IP';
  for i := $3A to $3B do  instrname[i] := 'MSIRP';
  instrname[$3c] := 'ALIGNRP';
  instrname[$3D] := 'RTDG';
  for i := $3E to $3F do  instrname[i] := 'MIAP';
  instrname[$40] := 'NPUSH_BYTES';
  instrname[$41] := 'NPUSH_WORDS';
  instrname[$42] := 'WS';
  instrname[$43] := 'RS';
  instrname[$44] := 'WCVTP';
  instrname[$45] := 'RCVT';
  instrname[$46] := 'GC';
  instrname[$47] := 'GC';
  instrname[$48] := 'SCFS';
  instrname[$49] := 'MD';
  instrname[$4A] := 'MD';
  instrname[$4B] := 'MPPEM';
  instrname[$4C] := 'MPS';
  instrname[$4D] := 'FLIPON';
  instrname[$4E] := 'FLIPOFF';
  instrname[$4F] := 'DEBUG';
  instrname[$50] := 'LT';
  instrname[$51] := 'LTEQ';
  instrname[$52] := 'GT';
  instrname[$53] := 'GTEQ';
  instrname[$54] := 'EQ';
  instrname[$55] := 'NEQ';
  instrname[$56] := 'ODD';
  instrname[$56] := 'EVEN';
  instrname[$58] := 'IF';
  instrname[$59] := 'ENDIF';
  instrname[$5A] := 'AND';
  instrname[$5B] := 'OR';
  instrname[$5C] := 'NOT';
  instrname[$5D] := 'DELTAP1';
  instrname[$5E] := 'SDB';
  instrname[$5F] := 'SDS';
  instrname[$60] := 'ADD';
  instrname[$61] := 'SUB';
  instrname[$62] := 'DIV';
  instrname[$63] := 'MUL';
  instrname[$64] := 'ABS';
  instrname[$65] := 'NEG';
  instrname[$66] := 'FLOOR';
  instrname[$67] := 'CEILING';
  for i := $68 to $6b do begin
    instrname[i] := 'ROUND';
  end;
  for i := $6c to $6f do begin
    instrname[i] := 'NoROUND';
  end;
  instrname[$70] := 'WCVTF';
  instrname[$71] := 'DELTAP2';
  instrname[$72] := 'DELTAP3';
  instrname[$73] := 'DELTAC1';
  instrname[$74] := 'DELTAC2';
  instrname[$75] := 'DELTAC3';
  instrname[$76] := 'SPERROUND';
  instrname[$77] := 'SPERROUND45';  
  instrname[$78] := 'JMPRoT';
  instrname[$79] := 'JMPRoF';  
  instrname[$7A] := 'ROFF';

  instrname[$7C] := 'RUTG';
  instrname[$7D] := 'RDTG';
  instrname[$7E] := 'SANGW';
  instrname[$7F] := 'AA';
  instrname[$80] := 'FLIPPT';
  instrname[$81] := 'FLIPRGON';
  instrname[$82] := 'FLIPRGOFF';

  instrname[$85] := 'SCANCTRL';
  instrname[$86] := 'SDPVTL';
  instrname[$87] := 'SDPVTL';
  instrname[$88] := 'GETINFO';
  instrname[$89] := 'DEFINSTR';
  instrname[$8A] := 'ROLL';
  instrname[$8B] := 'MAX';
  instrname[$8c] := 'MIN';
  instrname[$8d] := 'SCANTYPE';
  instrname[$8e] := 'INSTCTRL'; 


  for i := 0 to 7 do begin
    instrname[i+$b0] := 'PUSH_'+inttostr(i+1)+'_BYTES';
    instrname[i+$b8] := 'PUSH_'+inttostr(i+1)+'_WORDS';
  end;
  for i := $C0 to $DF do  instrname[i] := 'MDRP';
  for i := $E0 to $FF do  instrname[i] := 'MIRP';
end;


begin
  set_instrnames();
end.

