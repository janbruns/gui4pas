{$mode objfpc}
{$unitpath .}
unit paswl_collection2;

interface
uses 
  paswl_shm,
  paswl_compositor,
  paswl_subcompositor,
  paswl_viewporter,
  paswl_output,
  paswl_seat,
  paswl_shell,
  paswl_xdg_wm_base,
  paswl_collection,
  paswl_bindable;


const
max_outputs = 4;

type
Tpaswl_collection2 = class(Tpaswl_collection)
  public
  constructor create(); override;
  destructor destroy(); override;
  procedure addAutoBinds(); virtual;


  public
  shm : Tpaswl_shm;
  compositor : Tpaswl_compositor;
  subcompositor : Tpaswl_subcompositor;
  viewporter : Tpaswl_viewporter;
  seat : Tpaswl_seat;
  xdg_wm_base : Tpaswl_xdg_wm_base;
  coreshell : Tpaswl_shell;
  outputs : array[0..max_outputs-1] of Tpaswl_output;
end;

Tpaswl_collection2_cls = class of Tpaswl_collection2;





implementation
uses sysutils,math;



constructor Tpaswl_collection2.create();
begin
  inherited;
  addAutoBinds();
end;

procedure Tpaswl_collection2.addAutoBinds();
var i : longint;
begin
  addAutobind(Tpaswl_shm, @shm, true);
  addAutobind(Tpaswl_compositor, @compositor, true);
  addAutobind(Tpaswl_subcompositor, @subcompositor, false);
  addAutobind(Tpaswl_viewporter, @viewporter, false);
  addAutobind(Tpaswl_seat, @seat, true);
  addAutobind(Tpaswl_xdg_wm_base, @xdg_wm_base, false);
  addAutobind(Tpaswl_shell, @coreshell, false);
  for i := 0 to high(outputs) do addAutobind(Tpaswl_output, @outputs[i], false);
end;


destructor Tpaswl_collection2.destroy();
var i : longint;
begin
  for i := 0 to high(outputs) do freeandnil(outputs[i]);
  freeandnil(xdg_wm_base);
  freeandnil(coreshell);
  freeandnil(seat);
  freeandnil(viewporter);
  freeandnil(subcompositor);
  freeandnil(compositor);
  freeandnil(shm);  
  inherited;
end;



initialization
begin

end;

finalization
begin
end;

end.



