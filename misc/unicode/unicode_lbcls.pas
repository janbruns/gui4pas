{$mode objfpc}
{$unitpath .}
unit unicode_lbcls;

interface

const
lbcls_XX=0;
lbcls_AL=1;
lbcls_H2=2;
lbcls_H3=3;
lbcls_CM=4;
lbcls_ID=5;
lbcls_AI=6;
lbcls_BA=7;
lbcls_OP=8;
lbcls_CL=9;
lbcls_NU=10;
lbcls_EB=11;
lbcls_SA=12;
lbcls_CJ=13;
lbcls_PO=14;
lbcls_PR=15;
lbcls_BB=16;
lbcls_QU=17;
lbcls_NS=18;
lbcls_IS=19;
lbcls_EX=20;
lbcls_GL=21;
lbcls_HL=22;
lbcls_ZWJ=23;
lbcls_NL=24;
lbcls_BK=25;
lbcls_B2=26;
lbcls_SP=27;
lbcls_WJ=28;
lbcls_CB=29;
lbcls_CP=30;
lbcls_EM=31;
lbcls_ZW=32;
lbcls_HY=33;
lbcls_LF=34;
lbcls_IN=35;
lbcls_JL=36;
lbcls_CR=37;
lbcls_JT=38;
lbcls_JV=39;
lbcls_RI=40;
lbcls_SG=41;
lbcls_SY=42;



function get_unicode_linebreak_class(cp : dword) : byte;

implementation
uses sysutils,unicode_tabinit;


const
{$include linebreak.inc}

var lbcls : array of byte;



function get_unicode_linebreak_class(cp : dword) : byte;
begin
  if cp<=high(lbcls) then result := lbcls[cp] else result := 0;
end;


procedure do_init();
var s : ansistring;
begin
  setlength(s,high(sinc)+1);
  move(sinc[0],s[1],high(sinc)+1);
  unicode_initvaldecode(s,lbcls);
end;


begin
  do_init();
end.
