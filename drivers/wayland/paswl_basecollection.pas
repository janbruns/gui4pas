{$mode objfpc}
{$unitpath .}
unit paswl_basecollection;

interface
uses wl_basic_types,wl_core,wl_libwl,
paswl_basebindable,
paswl_display, paswl_registry;


type
Tpaswl_basecollection = class
  public
  constructor create();
  destructor destroy(); override;
  procedure msgloop();
  function connect(p : Pchar) : boolean;


  procedure onOutputRemove(o : Tpaswl_basebindable); virtual;
  
  
  protected
  procedure cb_registry_global(name : dword; iface : Pchar; version : dword); virtual;
  procedure cb_registry_global_remove(name : dword); virtual;
  
  public
  display : Tpaswl_display;
  registry : Tpaswl_registry;
  alldone : boolean;
  
end;

Tpaswl_basecollection_cls = class of Tpaswl_basecollection;





implementation
uses sysutils;


type
Tpaswl_registry_paswl=class(Tpaswl_registry)
  public paswl : Tpaswl_basecollection;
  protected
  procedure cb_registry_global(name : dword; iface : Pchar; version : dword); override;
  procedure cb_registry_global_remove(name : dword); override;
end;

procedure Tpaswl_registry_paswl.cb_registry_global(name : dword; iface : Pchar; version : dword);
begin
  paswl.cb_registry_global(name,iface,version);
end;

procedure Tpaswl_registry_paswl.cb_registry_global_remove(name : dword);
begin
  paswl.cb_registry_global_remove(name);
end;




constructor Tpaswl_basecollection.create();
begin
end;

destructor Tpaswl_basecollection.destroy();
begin
  freeandnil(registry);
  freeandnil(display);
end;

function Tpaswl_basecollection.connect(p : Pchar) : boolean;
begin
  display := Tpaswl_display.create();
  if display.display_connect(p) then begin
    registry := display.display_get_registry(Tpaswl_registry_paswl);
    if registry<>nil then begin
      Tpaswl_registry_paswl(registry).paswl := self;
      registry.onBind();
      connect := true;
      display.display_roundtrip();
    end else begin
      connect := false;
      display.destroy();
    end;
  end else connect := false;
end;

procedure Tpaswl_basecollection.msgloop();
begin
  while not(alldone) do begin
    if (display.display_dispatch() < 0) then begin
      writeln('"Main loop error"');
      alldone := true;
    end;
  end;
end;

procedure Tpaswl_basecollection.cb_registry_global(name : dword; iface : Pchar; version : dword);
begin
  writeln('GLOBAL: ',name:5,iface);
end;

procedure Tpaswl_basecollection.cb_registry_global_remove(name : dword);
begin
  writeln('GLOBAL_REMOVE: ',name:5);
end;

procedure Tpaswl_basecollection.onOutputRemove(o : Tpaswl_basebindable);
begin
  writeln('on Output REMOVE ');
end;

initialization
begin

end;

finalization
begin
end;

end.



