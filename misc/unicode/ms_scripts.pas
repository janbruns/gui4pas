{$mode objfpc}
{$unitpath .}
unit ms_scripts;

interface
uses unicode_scripts;

type
Ttag = array[0..3] of char;

const
ms_script_tags : array[unicode_scr_Common..unicode_scr_Zanabazar_Square] of Ttag = (
'DFLT',//unicode_scr_Common = 0;
'dflt',//unicode_scr_Inherited = 1;
'adlm',//unicode_scr_Adlam = 2;
'ahom',//unicode_scr_Ahom = 3;
'hluw',//unicode_scr_Anatolian_Hieroglyphs = 4;
'arab',//unicode_scr_Arabic = 5;
'armn',//unicode_scr_Armenian = 6;
'avst',//unicode_scr_Avestan = 7;
'bali',//unicode_scr_Balinese = 8;
'bamu',//unicode_scr_Bamum = 9;
'bass',//unicode_scr_Bassa_Vah = 10;
'batk',//unicode_scr_Batak = 11;
//'beng',
'bng2',//unicode_scr_Bengali = 12;
'bhks',//unicode_scr_Bhaiksuki = 13;
'bopo',//unicode_scr_Bopomofo = 14;
'brah',//unicode_scr_Brahmi = 15;
'brai',//unicode_scr_Braille = 16;
'bugi',//unicode_scr_Buginese = 17;
'buhd',//unicode_scr_Buhid = 18;
//Byzantine Music 	'byzm' 	
'cans',//unicode_scr_Canadian_Aboriginal = 19;
'cari',//unicode_scr_Carian = 20;
'aghb',//unicode_scr_Caucasian_Albanian = 21;
'cakm',//unicode_scr_Chakma = 22;
'cham',//unicode_scr_Cham = 23;
'cher',//unicode_scr_Cherokee = 24;
'chrs',//unicode_scr_Chorasmian = 25;
'copt',//unicode_scr_Coptic = 26;
'xsux',//unicode_scr_Cuneiform = 27;
'cprt',//unicode_scr_Cypriot = 28;
'cpmn',//unicode_scr_Cypro_Minoan = 29;
'cyrl',//unicode_scr_Cyrillic = 30;
'dsrt',//unicode_scr_Deseret = 31;
//Devanagari 	'deva' 	
'dev2',//unicode_scr_Devanagari = 32;
'diak',//unicode_scr_Dives_Akuru = 33;
'dogr',//unicode_scr_Dogra = 34;
'dupl',//unicode_scr_Duployan = 35;
'egyp',//unicode_scr_Egyptian_Hieroglyphs = 36;
'elba',//unicode_scr_Elbasan = 37;
'elym',//unicode_scr_Elymaic = 38;
'ethi',//unicode_scr_Ethiopic = 39;
'geor',//unicode_scr_Georgian = 40;
'glag',//unicode_scr_Glagolitic = 41;
'goth',//unicode_scr_Gothic = 42;
'gran',//unicode_scr_Grantha = 43;
'grek',//unicode_scr_Greek = 44;
//'gujr',
'gjr2',//unicode_scr_Gujarati = 45;
'gong',//unicode_scr_Gunjala_Gondi = 46;
//Gurmukhi 	'guru' 	
'gur2',//unicode_scr_Gurmukhi = 47;
'hani',//unicode_scr_Han = 48;??
'hang',//unicode_scr_Hangul = 49;
'rohg',//unicode_scr_Hanifi_Rohingya = 50;
'hano',//unicode_scr_Hanunoo = 51;
'hatr',//unicode_scr_Hatran = 52;
'hebr',//unicode_scr_Hebrew = 53;
'kana',//unicode_scr_Hiragana = 54;
'armi',//unicode_scr_Imperial_Aramaic = 55;
'phli',//unicode_scr_Inscriptional_Pahlavi = 56;
'prti',//unicode_scr_Inscriptional_Parthian = 57;
'java',//unicode_scr_Javanese = 58;
'kthi',//unicode_scr_Kaithi = 59;
//'knda',
'knd2',//unicode_scr_Kannada = 60;
'kana',//unicode_scr_Katakana = 61;
'kawi',//unicode_scr_Kawi = 62; // NOT IN MICOROSOFT'S LIST
'kali',//unicode_scr_Kayah_Li = 63;
'khar',//unicode_scr_Kharoshthi = 64;
'kits',//unicode_scr_Khitan_Small_Script = 65;
'khmr',//unicode_scr_Khmer = 66;
'khoj',//unicode_scr_Khojki = 67;
'sind',//unicode_scr_Khudawadi = 68;
'lao ',//unicode_scr_Lao = 69;
'latn',//unicode_scr_Latin = 70;
'lepc',//unicode_scr_Lepcha = 71;
'limb',//unicode_scr_Limbu = 72;
'lina',//unicode_scr_Linear_A = 73;
'linb',//unicode_scr_Linear_B = 74;
'lisu',//unicode_scr_Lisu = 75;
'lyci',//unicode_scr_Lycian = 76;
'lydi',//unicode_scr_Lydian = 77;
'mahj',//unicode_scr_Mahajani = 78;
'maka',//unicode_scr_Makasar = 79;
//'mlym',
'mlm2',//unicode_scr_Malayalam = 80;
'mand',//unicode_scr_Mandaic = 81;
'mani',//unicode_scr_Manichaean = 82;
'marc',//unicode_scr_Marchen = 83;
'gonm',//unicode_scr_Masaram_Gondi = 84;
//Mathematical Alphanumeric Symbols 	'math' 	
'medf',//unicode_scr_Medefaidrin = 85;
'mtei',//unicode_scr_Meetei_Mayek = 86;
'mend',//unicode_scr_Mende_Kikakui = 87;
'merc',//unicode_scr_Meroitic_Cursive = 88;
'mero',//unicode_scr_Meroitic_Hieroglyphs = 89;
'plrd',//unicode_scr_Miao = 90;
'modi',//unicode_scr_Modi = 91;
'mong',//unicode_scr_Mongolian = 92;
'mroo',//unicode_scr_Mro = 93;
'mult',//unicode_scr_Multani = 94;
//'musc', 'mymr'
'mym2',//unicode_scr_Myanmar = 95;
'nbat',//unicode_scr_Nabataean = 96;
'nagm',//unicode_scr_Nag_Mundari = 97; // NOT IN MICROSOFT'S LIST!
'nand',//unicode_scr_Nandinagari = 98;
'newa',//unicode_scr_Newa = 99;
'talu',//unicode_scr_New_Tai_Lue = 100;
'nko ',//unicode_scr_Nko = 101;
'nshu',//unicode_scr_Nushu = 102;
'hmnp',//unicode_scr_Nyiakeng_Puachue_Hmong = 103;
'ogam',//unicode_scr_Ogham = 104;
'olck',//unicode_scr_Ol_Chiki = 105;
'hung',//unicode_scr_Old_Hungarian = 106;
'ital',//unicode_scr_Old_Italic = 107;
'narb',//unicode_scr_Old_North_Arabian = 108;
'perm',//unicode_scr_Old_Permic = 109;
'xpeo',//unicode_scr_Old_Persian = 110;
'sogo',//unicode_scr_Old_Sogdian = 111;
'sarb',//unicode_scr_Old_South_Arabian = 112;
'orkh',//unicode_scr_Old_Turkic = 113;
'ougr',//unicode_scr_Old_Uyghur = 114;
'ory2',//unicode_scr_Oriya = 115;
'osge',//unicode_scr_Osage = 116;
'osma',//unicode_scr_Osmanya = 117;
'hmng',//unicode_scr_Pahawh_Hmong = 118;
'palm',//unicode_scr_Palmyrene = 119;
'pauc',//unicode_scr_Pau_Cin_Hau = 120;
'phag',//unicode_scr_Phags_Pa = 121;
'phnx',//unicode_scr_Phoenician = 122;
'phlp',//unicode_scr_Psalter_Pahlavi = 123;
'rjng',//unicode_scr_Rejang = 124;
'runr',//unicode_scr_Runic = 125;
'samr',//unicode_scr_Samaritan = 126;
'saur',//unicode_scr_Saurashtra = 127;
'shrd',//unicode_scr_Sharada = 128;
'shaw',//unicode_scr_Shavian = 129;
'sidd',//unicode_scr_Siddham = 130;
'sgnw',//unicode_scr_SignWriting = 131;
'sinh',//unicode_scr_Sinhala = 132;
'sogd',//unicode_scr_Sogdian = 133;
'sora',//unicode_scr_Sora_Sompeng = 134;
'soyo',//unicode_scr_Soyombo = 135;
'sund',//unicode_scr_Sundanese = 136;
'sylo',//unicode_scr_Syloti_Nagri = 137;
'syrc',//unicode_scr_Syriac = 138;
'tglg',//unicode_scr_Tagalog = 139;
'tagb',//unicode_scr_Tagbanwa = 140;
'tale',//unicode_scr_Tai_Le = 141;
'lana',//unicode_scr_Tai_Tham = 142;
'tavt',//unicode_scr_Tai_Viet = 143;
'takr',//unicode_scr_Takri = 144;
//Tamil 	'taml' 	
'tml2',//unicode_scr_Tamil = 145;
'tnsa',//unicode_scr_Tangsa = 146;
'tang',//unicode_scr_Tangut = 147;
//'telu',
'tel2',//unicode_scr_Telugu = 148;
'thaa',//unicode_scr_Thaana = 149;
'thai',//unicode_scr_Thai = 150;
'tibt',//unicode_scr_Tibetan = 151;
'tfng',//unicode_scr_Tifinagh = 152;
'tirh',//unicode_scr_Tirhuta = 153;
'toto',//unicode_scr_Toto = 154;
'ugar',//unicode_scr_Ugaritic = 155;
'vai ',//unicode_scr_Vai = 156;
'vith',//unicode_scr_Vithkuqi = 157;
'wcho',//unicode_scr_Wancho = 158;
'wara',//unicode_scr_Warang_Citi = 159;
'yezi',//unicode_scr_Yezidi = 160;
'yi  ',//unicode_scr_Yi = 161;
'zanb' //unicode_scr_Zanabazar_Square = 162;
);



implementation



begin
end.
