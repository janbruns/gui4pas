{$mode objfpc}
{$unitpath ..}
uses 
  unicode_bidiralgo,
  unicode_utf8;


function todot(var s : ansistring; const dot : shortstring) : ansistring;
var i : longint;
begin
  i := pos(dot,s);
  if (i>0) then begin
    result := copy(s,1,i-1);
    s := copy(s,i+length(dot));
  end else begin
    result := s;
    s := '';
  end;
end;


var
lvlstr,ordstr,datstr : ansistring;
levelarr : array[0..1000] of longint;
orderarr : array[0..1000] of longint;
levelcnt : longint;
ordercnt : longint;
errcnt,goodcnt : longint;


procedure dotest(tst : ansistring; m,mv : longint);
var a : Tunicode_bidir_algo; i,j : longint; err : boolean;
begin
  a := Tunicode_bidir_algo.create();
  a.setting_embed := m;
  a.load_from_UTF8(tst);
  a.preprocess();
  a.do_L1(0,high(a.chars));
  err := false;
  for i := 0 to high(a.chars) do begin
    if (levelarr[i]=-1) and (a.chart[i]<>0) then err := true;
    if (levelarr[i]>-1) and (a.charl[i]<>levelarr[i]) then err := true;
  end;
  if not err then begin
    a.do_L2(0,high(a.chars));
    j := 0;
    for i := 0 to high(a.chars) do begin
      if a.charexists(i) then begin
        if a.charidx[i]<>orderarr[j] then err := true;
        inc(j);
      end;
    end;
  end;
  if err then begin
    write('err(m=',m,'),got:');
    for i := 0 to high(a.chars) do begin
      if a.chart[i]=0 then write('x ') else write(a.charl[i],' ');
    end;
    writeln;
    writeln('expected:    ',lvlstr);
    writeln('dat: ',datstr);
    for i := 0 to high(a.chars) do begin
      write(a.chart[i],' ');
    end;
    writeln('paragraph_embedding_level:',a.paragraph_embedding_level,'/',mv);
    writeln(tst);
    readln;
  end;
  if err then inc(errcnt) else inc(goodcnt);
  a.destroy();
end;




procedure do_levelsline(s : ansistring);
var w : word; s2 : ansistring; i : longint;
begin
  lvlstr := s;
  levelcnt := 0;
  while (s<>'') do begin
    s2 := todot(s,' ');
    if s2='x' 
    then levelarr[levelcnt] := -1
    else val(s2,levelarr[levelcnt],w);
    inc(levelcnt);
  end;
end;

procedure do_reorderline(s : ansistring);
var w : word; s2 : ansistring; i : longint;
begin
  ordstr := s;
  ordercnt := 0;
  while (s<>'') do begin
    s2 := todot(s,' ');
    if s2='x' 
    then orderarr[ordercnt] := -1
    else val(s2,orderarr[ordercnt],w);
    inc(ordercnt);
  end;
end;


procedure do_datline(s,sm,smv : ansistring);
var s2,s3,tst : ansistring; v : dword; m,w : word;
begin
  datstr := s;
  tst := '';
  s2 := todot(s,';');
  while s2<>'' do begin
    s3 := todot(s2,' ');
    s3 := '$'+s3;
    val(s3,v,w);
    tst += UTF8_codepoint_as_UTF8(v);
  end;
  val(sm,v,w);
  val(smv,m,w);
  v := (v+1)mod 3;
  
  dotest(tst,v,m);
end;


procedure do_line(s : ansistring);
var s2,s3,s4,s5 : ansistring;
begin
  s := todot(s,'#');
  if s<>'' then begin
    s2 := todot(s,';');
    s3 := todot(s,';');
    s4 := todot(s,';');
    s5 := todot(s,';');
    do_levelsline(s5);
    do_reorderline(s);
    do_datline(s2,s3,s4);
  end;
end;


var
f : text;
s : ansistring;

begin
  errcnt := 0; 
  goodcnt := 0;
  assign(f,'BidiCharacterTest.txt');
  reset(f);
  while not eof(f) do begin
    readln(f,s);
    do_line(s);
  end;
  close(f);
  writeln('errcnt: ',errcnt);
  writeln('goodcnt: ',goodcnt);
end.
