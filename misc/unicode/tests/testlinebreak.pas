{$mode objfpc}
{$unitpath ..}
uses 
linebreakalgo,
paragraphmanager,
unicode_bidiralgo,
unicode_UTF8;

function todot(var s : ansistring; const dot : shortstring) : ansistring;
var i : longint;
begin
  i := pos(dot,s);
  if (i>0) then begin
    result := copy(s,1,i-1);
    s := copy(s,i+length(dot));
  end else begin
    result := s;
    s := '';
  end;
end;

function dotest(s : ansistring) : ansistring;
var o : TparagraphManager; i : dword; r : ansistring;
begin
  r := '';
  o := TparagraphManager.create();
  o.make_bd(Tunicode_bidir_algo);
  o.make_lb(Tlinebreaker);
  o.load_from_UTF8(s);
  o.linebreaker.make_break();
  for i := 0 to high(o.linebreaker.breakstatus) do begin
    if (o.linebreaker.breakstatus[i] and break_allowed)>0 then begin
      r += '÷';
    end else begin
      r += '×';
    end;
  end;
  o.destroy();
  dotest := r;
end;


var
failcnt, okcnt : longint;

procedure doline(s0 : ansistring);
var cp,s,vs,rs,tst,sol,res : ansistring; v : dword; w : word; 
begin
  cp := s0;
  sol := '';
  tst := '';
  s := todot(s0,'#');
  if s<>'' then begin
    //writeln(s);
    todot(s,' ');
    repeat
      vs := '$'+todot(s,' ');
      rs := todot(s,' ');
      rs := UTF8_codepoint_as_UTF8(UTF8_getcodepoint(rs,1));
      sol += rs;
      val(vs,v,w);
      //writeln('use ',hexstr(v,6),' #',rs,'#');
      tst += UTF8_codepoint_as_UTF8(v);
    until s='';
    res := dotest(tst);
    if res<>sol then begin
      writeln('fail. got: ',res,' expected: ',sol);
      writeln(cp);
      writeln;
      inc(failcnt);
    end else begin
      inc(okcnt);
    end;
  end;
end;

var
o : Tlinebreaker;
f : text;
s : ansistring;

begin
  failcnt := 0;
  okcnt := 0;
  assign(f,'LineBreakTest.txt');
  reset(f);
  while not eof(f) do begin
    readln(f,s);
    doline(s);
  end;
  close(f);
  writeln(failcnt+okcnt,' tests done, ok:',okcnt,' fail:',failcnt);
end.
