{$mode objfpc}
{$unitpath .}
{$COPERATORS ON}
unit fontfile;

{ parse the font file tables,
  most notably the glyph-data.

  The TTfont and TTglyph classes.

  Currrently only bare font files supported, 
  no ttc container.

}


interface
uses 
ttflaten,
ttrender,
classes,sysutils;

{ includes:     ttcmap.pas
}


type
TTTFtag = array[0..3] of char;
Tcoord = smallint;
Tcoordarr = array of Tcoord;
Tbytearr = array of byte;
F2DOT14 = smallint;
Tttfontexception = Class(Exception);

Tunicodemapentry = packed record
  startCharCode, 
  endCharCode, 
  startGlyphID : dword;
  delta : longint;  // 1 for format12, 0 for format 13 cmap 
end;

Tmaxp_profile = record
  maxPoints,
  maxContours,
  maxCompositePoints,
  maxCompositeContours,
  maxZones,
  maxTwilightPoints,
  maxStorage,
  maxFunctionDefs,
  maxInstructionDefs,
  maxStackElements,
  maxSizeOfInstructions,
  maxComponentElements,
  maxComponentDepth : word;
end;

Thhea_tab = record
  { most values are 16 bit in fileformat, but we probably
    don't wanna care about signedness of these values when
    using them, so we'll do this in the file-reader }
  versh,versl : word;
  Ascender, 
  Descender, 
  LineGap, 
  advanceWidthMax, 
  minLeftSideBearing,
  minRightSideBearing,
  xMaxExtent,
  caretSlopeRise,
  caretSlopeRun,
  caretOffset,
  metricDataFormat,
  numberOfHMetrics : longint; 
  load_ok : boolean;
end;


TTglyph = class;
TTfont = class;

Ttiledestryfunc = procedure(g : TTglyph);
Ttilefdestryfunc = procedure(f : TTfont);
Tdwarr = array of dword;
Tsiarr = array of smallint;


TTfont = class
  constructor create();
  destructor destroy(); override;

  procedure load(s : Tstream);

  procedure throwerr(s : ansistring);


  function getGlyph(n : longint) : TTglyph;
  procedure releaseGlyph(g : TTglyph);


  function seek_tab(which : TTTFtag) : longint;
  function lookup_kern(gl,gr,cov : longint) : longint;

  protected
  procedure load_head();
  procedure load_loca();
  procedure load_glyph(i : longint);
  function load_hhea() : boolean;
  function load_kern() : boolean;
  function load_kern_subtab(sidx : longint) : boolean;



  function calc_tabsum(o,len : dword; ishead : boolean) : dword;
  function calc_checksum() : boolean;

  procedure load_gftabs();

  private
  tabstart : longint;
  numTables, searchRange, 
  entrySelector, rangeShift : longint; // values from file-header

  glyphs : array of TTglyph;
  locatab : array of longint;
  glyphstart,glyphtablen : longint;

  public 
  src : Tstream;
  vers_upper,vers_lower,
  fvers_upper,fvers_lower,
  hvers_upper,hvers_lower,
  hflags : word;
  unitsPerEm : word;
  date_modified,
  date_created : int64;
  xmin,xmax,ymin,ymax,numGlyphs : longint;
  macStyle, lowestRecPPEM : word;
  fontDirectionHint,
  indexToLocFormat,
  glyphDataFormat : smallint;
  default_scaleposition_order : longint;
  maxprofile : Tmaxp_profile;
  hhea : Thhea_tab;
  advanceWidth : array of longint;
  leftSideBearing : array of longint;
  fontprog, prepprog : array of byte;
  cvt : array of smallint;

  renderer : ttmultirenderer;
  gridfitter : pointer; // opaque here
  gfdestroy : Ttilefdestryfunc;

  kerntabs_gids : array of Tdwarr;
  kerntabs_vals : array of Tsiarr;
  kerntabs_covs : array of byte;
end;
Psubglyph = ^Tsubglyph;
Tsubglyph = record
  next : Psubglyph;
  flags : word;
  idx : longint;
  dx,dy : smallint;
  sx,sy : smallint;
  sxy,syx : smallint;
end;


TTglyph = class
  constructor create(fnt : TTfont);
  destructor destroy(); override;

  procedure print_subglyphs();
  procedure print();

  procedure read_subglyphs();
  procedure expand_subglyphs();
  procedure scale_subglyph(p : Psubglyph; first,len : longint);
  procedure scale_subglyphs();
  procedure translate_subglyph(first,len : longint; dx,dy : F2DOT14);
  procedure translate_subglyphs();
  procedure done_subglyphs();

  procedure scale_metrics(sx,sy,unitsPerEm : double);
  procedure scale(sx,sy,unitsPerEm : double);

  procedure make_contour(b : Tbezierstreamer; cs,ce : longint);
  procedure make_contours(b : Tbezierstreamer);


  protected
  procedure load();

  private
  procedure read_coords(var c : Tcoordarr; shift : longint);
  procedure read_flags();

  function read_subglyph() : Psubglyph;

  public
  idx : longint;
  indices : array of longint;
  indexcount : longint;
  font : TTfont; 
  subglyphs : Psubglyph;
  endPtsOfContours : array of word;
  coordx,
  coordy : Tcoordarr;
  instructions,
  flags  : Tbytearr;
  numberOfContours,
  xmin,xmax,ymin,ymax : longint;
  sxmin,sxmax,symin,symax,slsb,saw : longint;
  // scaled data
  scoordx,scoordy : array of longint;
  sonoffcurve     : array of boolean;

  is_loading : boolean;
  zeropoint : longint;

  tilesdestroy : Ttiledestryfunc;
  tiles : pointer; // opaque here
end;

function read_word(s : Tstream) : word;
function read_dword(s : Tstream) : dword;
function read_qword(s : Tstream) : qword;
function read_tag(s : Tstream) : TTTFtag;


const
testfont = '/usr/share/fonts/truetype/freefont/FreeSans.ttf';

//testfont = '/usr/share/fonts/truetype/ttf_janbruns/Font2.ttf';
F2DOT14_ONE = $4000;

implementation
uses math;


FUNCTION F2DOT14_MUL_F2DOT14_F2DOT14(a,b : F2DOT14) : F2DOT14;
VAR c : longint;
BEGIN
  c := longint(a)*longint(b);
  F2DOT14_MUL_F2DOT14_F2DOT14 := F2DOT14(SarLongint(c,14));
END;







function read_word(s : Tstream) : word;
begin
  read_word := (word(s.readbyte()) shl 8) or s.readbyte;
end;

function read_dword(s : Tstream) : dword;
begin
  read_dword := (dword(read_word(s)) shl 16) or read_word(s);
end;

function read_qword(s : Tstream) : qword;
begin
  read_qword := (qword(read_dword(s)) shl 32) or read_dword(s);
end;

function read_tag(s : Tstream) : TTTFtag;
var a : TTTFtag;
begin
  s.readbuffer(a[0],4);
  read_tag := a;
end;



procedure TTglyph.read_coords(var c : Tcoordarr; shift : longint);
var i,j,f : longint; s : Tstream;
begin
  s := font.src;
  j := high(c);
  for i := 0 to j do begin
    f := flags[i] shr shift;
    if (f and 1)>0 then begin
      c[i] := s.readbyte();
      if (f and 8)=0 then c[i] := -c[i];
    end else begin
      if (f and 8)=0 then begin
        c[i] := smallint(read_word(s));
      end else begin
        c[i] := 0;
      end;
    end;
  end;
  for i := 1 to j do c[i] += c[i-1];
end;

procedure TTglyph.read_flags();
var i,j,k : longint; s : Tstream;
begin
  s := font.src;
  k := high(flags);
  i := 0;
  while (i<=k) do begin
    flags[i] := s.readbyte();
    if (flags[i] and 8)=8 then begin
      j := s.readbyte();
      while (j>0)and(i<k) do begin
        inc(i);
        flags[i] := flags[i-1];
        dec(j);
      end;
      if (j>0) then font.throwerr('error reading glyph flags');
    end;
    inc(i);
  end;
end;

function TTglyph.read_subglyph() : Psubglyph;
var p : Psubglyph;
begin
  new(p);
  p^.next := nil;
  p^.flags := read_word(font.src);
  p^.idx   := read_word(font.src);
  if ((p^.flags and 1)>0) then begin
    p^.dx  := read_word(font.src);
    p^.dy  := read_word(font.src);
  end else begin
    if ((p^.flags and 2)>0) then begin
      p^.dx := shortint(font.src.readbyte());
      p^.dy := shortint(font.src.readbyte());
    end else begin
      p^.dx := font.src.readbyte();
      p^.dy := font.src.readbyte();
    end;
  end;
  p^.sx  := F2DOT14_ONE;
  p^.sy  := F2DOT14_ONE;
  p^.sxy := 0;
  p^.syx := 0;
  if ((p^.flags and 8)>0) then begin
    p^.sx  := read_word(font.src);
    p^.sy  := p^.sx;
  end else if ((p^.flags and 64)>0) then begin
    p^.sx  := read_word(font.src);
    p^.sy  := read_word(font.src);
  end else if ((p^.flags and 128)>0) then begin
    p^.sx  := read_word(font.src);
    p^.sxy := read_word(font.src);
    p^.syx := read_word(font.src);
    p^.sy  := read_word(font.src);
  end;
  if ((p^.flags and (2048+4096))=0) then begin
    p^.flags := p^.flags or font.default_scaleposition_order;
  end;
  read_subglyph := p;
end;

procedure TTglyph.read_subglyphs();
var p,prevp : Psubglyph; j : longint;
begin
  subglyphs := nil;
  prevp := nil;
  repeat
    p := read_subglyph();
    if (subglyphs=nil) then subglyphs := p;
    if not(prevp=nil) then prevp^.next := p;
    prevp := p;
  until ((p^.flags and 32)=0);
  if ((p^.flags and 256)>0) then begin
    j := read_word(font.src);
    setlength(instructions,j);
    font.src.readBuffer(instructions[0],j);
  end;
end;

procedure TTglyph.expand_subglyphs();
var p : Psubglyph; g : TTglyph; i,j,cc,pts : longint;
begin
  numberOfContours := 0;
  pts := 0;
  p := subglyphs;
  while not(p=nil) do begin
    g := font.getGlyph(p^.idx);
    numberOfContours += g.numberOfContours;
    pts += high(g.coordx)+1;
    p := p^.next;
  end;
  setlength(coordx,pts);
  setlength(coordy,pts);
  setlength(flags,pts);
  setlength(endPtsOfContours,numberOfContours);
  pts := 0;
  j := 0;
  p := subglyphs;
  while not(p=nil) do begin
    g := font.getGlyph(p^.idx);
    cc := high(g.coordx)+1;
    move(g.coordx[0],coordx[pts],cc*sizeof(Tcoord));
    move(g.coordy[0],coordy[pts],cc*sizeof(Tcoord));
    for i := 0 to cc-1 do begin
      { reuse "repeat"-flag to indicate positioning is done 
        for subglyph-points }
      flags[pts+i] := g.flags[i] and $F7; 
    end;
    for i := 0 to g.numberOfContours-1 do begin
      endPtsOfContours[j] := pts + g.endPtsOfContours[i];
      inc(j);
    end;
    pts += cc;
    p := p^.next;
  end;  
end;

procedure TTglyph.scale_subglyph(p : Psubglyph; first,len : longint);
var i : longint; x,y : F2DOT14;
begin
  for i := first+len-1 downto first do begin

    x :=  F2DOT14_MUL_F2DOT14_F2DOT14( coordx[i], p^.sx )
         +F2DOT14_MUL_F2DOT14_F2DOT14( coordy[i], p^.sxy);

    y :=  F2DOT14_MUL_F2DOT14_F2DOT14( coordx[i], p^.syx)
         +F2DOT14_MUL_F2DOT14_F2DOT14( coordy[i], p^.sy );

    coordx[i] := x;
    coordy[i] := y;
  end;
end;

procedure TTglyph.scale_subglyphs();
var p : Psubglyph; g : TTglyph; cc,pts : longint;
begin
  pts := 0;
  p := subglyphs;
  while not(p=nil) do begin
    g := font.getGlyph(p^.idx);
    cc := high(g.coordx)+1;
    scale_subglyph(p,pts,cc);
    pts += cc;
    p := p^.next;
  end;
end;

procedure TTglyph.translate_subglyph(first,len : longint; dx,dy : F2DOT14);
var i : longint;
begin
  for i := first+len-1 downto first do begin
    coordx[i] += dx;
    coordy[i] += dy;
    { reuse "repeat"-coord-flag to indicate positioning is done 
      for subglyph-points }
    flags[i] := flags[i] or 8;
  end;
end;

procedure TTglyph.translate_subglyphs();
var p : Psubglyph; 
    g : TTglyph; 
    cc,pts : longint; 
    dx,dy,dx2,dy2 : F2DOT14;
    nothingdone, stillproblematic : boolean;
begin
  pts := 0;
  { first, translate all subglyphs with direct xy values }
  p := subglyphs;
  while not(p=nil) do begin
    if ((p^.flags and 2)>0) then begin    
      g := font.getGlyph(p^.idx);
      cc := high(g.coordx)+1;
      dx := p^.dx;
      dy := p^.dy;
      { "SCALED_COMPONENT_OFFSET" }
      if ((p^.flags and 2048)>0) then begin
        dx2 :=  F2DOT14_MUL_F2DOT14_F2DOT14( dx, p^.sx )
               +F2DOT14_MUL_F2DOT14_F2DOT14( dy, p^.sxy);
        dy2 :=  F2DOT14_MUL_F2DOT14_F2DOT14( dx, p^.syx)
               +F2DOT14_MUL_F2DOT14_F2DOT14( dy, p^.sy );
        dx := dx2;
        dy := dy2;
      end;
      translate_subglyph(pts,cc,dx,dy);
      pts += cc;
    end;
    p := p^.next;
  end;
  { keep on positioning relative-placed components }
  repeat
    nothingdone := true;
    stillproblematic := false;
    p := subglyphs;
    pts := 0;
    while not(p=nil) do begin
      if ((p^.flags and 2)=0) then begin
        if (word(p^.dx)>high(coordx)) then font.throwerr('translation ref out of bounds');
        if (word(p^.dy)>high(coordx)) then font.throwerr('translation ref out of bounds');
        if ((flags[word(p^.dx)] and 8)>0) then begin
          nothingdone := false;
          g := font.getGlyph(p^.idx);
          cc := high(g.coordx)+1;
          dx := coordx[word(p^.dx)] -coordx[word(p^.dy)];
          dy := coordy[word(p^.dx)] -coordy[word(p^.dy)];
          translate_subglyph(pts,cc,dx,dy);
          pts += cc;
        end else stillproblematic := true;
      end;
      p := p^.next;
    end;
  until (nothingdone or not(stillproblematic));
  if stillproblematic then font.throwerr('circular translation-reference detected');
end;

procedure TTglyph.done_subglyphs();
var p : Psubglyph;
begin
  while not(subglyphs=nil) do begin 
    p := subglyphs^.next;
    dispose(subglyphs);
    subglyphs := p;
  end;
end;

procedure TTglyph.load();
var lc,i,nc,instructionLength : longint;
begin
//writeln;
//writeln('loading glyph ',idx);
  numberOfContours := smallint( read_word(font.src) );
//writeln('numberOfContours:',numberOfContours);
  xmin := smallint( read_word(font.src) );
  ymin := smallint( read_word(font.src) );
  xmax := smallint( read_word(font.src) );
  ymax := smallint( read_word(font.src) );
  if (numberOfContours>0) then begin
    setlength(endPtsOfContours, numberOfContours);
    lc := numberOfContours-1;
    for i := 0 to lc do begin
      endPtsOfContours[i] := read_word(font.src);
//writeln('endPointOfContour[',i,']=',endPtsOfContours[i]);
      if (i>0)and(endPtsOfContours[i]<endPtsOfContours[i-1]) 
      then halt(1);
    end;
    if (lc>=0) then nc := endPtsOfContours[lc]+1 else nc := 0;
    instructionLength := read_word(font.src);
    setlength(instructions,instructionLength);
    if (instructionLength>0) 
    then font.src.readbuffer(instructions[0],instructionLength);
    setlength(flags,nc);  
    read_flags();
    setlength(coordx,nc);
    setlength(coordy,nc);
    read_coords(coordx,1);
    read_coords(coordy,2);    
  end else begin 
    read_subglyphs();
    expand_subglyphs();
    scale_subglyphs();
    translate_subglyphs();
  end;
//  writeln('numberOfContours after loading:',numberOfContours);
end;







procedure TTglyph.make_contour(b : Tbezierstreamer; cs,ce : longint);
var i,j,k : longint;
  barr : array of boolean;
  carr : array of TOT_coord;
begin
  k := 0;
  for i := cs to ce do begin
    inc(k);
    if  not(sonoffcurve[i]) then begin
      j := i+1;
      if (j>ce) then j := cs;
      if not(sonoffcurve[j]) then inc(k);
    end;
  end;
  setlength(barr,k);
  setlength(carr,k);
  k := 0;
  for i := cs to ce do begin
    barr[k] := sonoffcurve[i];
    carr[k].x := scoordx[i];
    carr[k].y := scoordy[i];  
    if not(barr[k]) then begin
      j := i+1;
      if (j>ce) then j := cs;
      if not(sonoffcurve[j]) then begin
        inc(k);
        barr[k] := true;
        carr[k].x := (scoordx[i]+scoordx[j])*0.5;
        carr[k].y := (scoordy[i]+scoordy[j])*0.5;
      end;
    end;
    inc(k);
  end;
  for i := 0 to k-1 do begin
    if barr[i] then begin
      b.addBezierPoint(carr[i]);
      b.addBezierPoint(carr[(i+1) mod k]);
      if not(barr[(i+1) mod k]) then b.addBezierPoint(carr[(i+2) mod k]);
      b.done_curve();
    end;
  end;
  b.done_contour();
end;

procedure TTglyph.make_contours(b : Tbezierstreamer);
var cc,cs,ce : longint;
begin
  cc := 0;
  cs := 0;
  while (cc<numberOfContours) do begin
    ce := endPtsOfContours[cc];
    make_contour(b,cs,ce);
    cs := ce+1;
    inc(cc);
  end;
end;


procedure TTglyph.scale_metrics(sx,sy,unitsPerEm : double);
var xs,ys : extended; 
begin
  xs := 64*sx/unitsPerEm;
  ys := 64*sy/unitsPerEm;
  sxmin := round(xs*xmin);
  sxmax := round(xs*xmax);
  symin := round(ys*ymin);
  symax := round(ys*ymax);
  saw :=  round(xs*font.advanceWidth[idx]);
  slsb := round(xs*font.leftSideBearing[idx]);
end;



procedure TTglyph.scale(sx,sy,unitsPerEm : double);
var xs,ys : extended; len,i : longint;
begin
  xs := 64*sx/unitsPerEm;
  ys := 64*sy/unitsPerEm;
  len := high(coordx)+1;
  setlength(scoordx,len);
  setlength(scoordy,len);
  setlength(sonoffcurve,len);
  for i := 0 to len-1 do begin
    scoordx[i] := round(coordx[i]*xs);
    scoordy[i] := round(coordy[i]*ys);
    sonoffcurve[i] := ((flags[i] and 1)>0);
  end;
  scale_metrics(sx,sy,unitsPerEm);
end;



constructor TTglyph.create(fnt : TTfont);
begin
  inherited create();
  font := fnt;
  subglyphs := nil;
  is_loading := true;
  setlength(coordx,0);
  setlength(coordy,0);
  setlength(flags,0);
  setlength(indices,0);

  setlength(scoordx,0);
  setlength(scoordy,0);
  setlength(sonoffcurve,0);
  indexcount := 0;

  tiles := nil;
  tilesdestroy := nil;
end;


destructor TTglyph.destroy();
begin
  if (tilesdestroy<>nil) then tilesdestroy(self);
  done_subglyphs();
  inherited destroy();
end;





procedure print_maxp_profile(const p : Tmaxp_profile);
begin
  with p do begin
    writeln('maxPoints             ',maxPoints);
    writeln('maxContours           ',maxContours);
    writeln('maxCompositePoints    ',maxCompositePoints);
    writeln('maxCompositeContours  ',maxCompositeContours);
    writeln('maxZones              ',maxZones);
    writeln('maxTwilightPoints     ',maxTwilightPoints);
    writeln('maxStorage            ',maxStorage);
    writeln('maxFunctionDefs       ',maxFunctionDefs);
    writeln('maxInstructionDefs    ',maxInstructionDefs);
    writeln('maxStackElements      ',maxStackElements);
    writeln('maxSizeOfInstructions ',maxSizeOfInstructions);
    writeln('maxComponentElements  ',maxComponentElements);
    writeln('maxComponentDepth     ',maxComponentDepth);
  end;
end;



procedure TTfont.load_glyph(i : longint);
begin
  glyphs[i] := TTglyph.create(self);
  glyphs[i].idx := i;
  if (locatab[i]=locatab[i+1]) then begin
    glyphs[i].is_loading := false;
  end else begin
    src.seek(locatab[i]+glyphstart,soFromBeginning);
    glyphs[i].load();
    glyphs[i].is_loading := false;
  end;
end;

function TTfont.getGlyph(n : longint) : TTglyph;
begin
  getGlyph := nil;
  if (n>=numGlyphs)or(n<0) then exit;
  if (glyphs[n]=nil) then load_glyph(n);
  if glyphs[n].is_loading then throwerr('circular components');
  getGlyph := glyphs[n];
end;


procedure TTfont.load(s : Tstream);
var i : longint;
begin
  src := s;
  vers_upper := read_word(s);
  vers_lower := read_word(s);
  numTables := read_word(s);
  searchRange := read_word(s);
  entrySelector := read_word(s);
  rangeShift := read_word(s);
  tabstart := s.position;
  load_head();
  if not(calc_checksum()) then throwerr('checksum incorrect');

  setlength(glyphs,numGlyphs);

  for i := 1 to numGlyphs do glyphs[i-1] := nil;
  glyphtablen := seek_tab('glyf');
  if (glyphtablen>=0) then begin
    glyphstart := src.position;
    load_loca();
  end;
  load_hhea();
  load_gftabs();
  load_kern();
end;


constructor TTfont.create();
begin
  inherited create();
  setlength(advanceWidth,0);
  setlength(leftSideBearing,0);
  setlength(glyphs,0);
  setlength(locatab,0);
  default_scaleposition_order := 2048; // Apple
  default_scaleposition_order := 4096; // MS/OpenType
  renderer := ttmultirenderer.create();
  renderer.dbgprint := false;
  renderer.setres(64,64);
  renderer.setMultisample(1);
  gridfitter := nil;
  gfdestroy := nil;
  setlength(kerntabs_gids,0);
  setlength(kerntabs_vals,0);
  setlength(kerntabs_covs,0);
end;



destructor TTfont.destroy();
var i : longint;
begin
  for i := 0 to high(glyphs) do begin
    if not(glyphs[i]=nil) then freeandnil(glyphs[i]);
  end;
  renderer.destroy();

  if gfdestroy<>nil then gfdestroy(self);
  gridfitter := nil;
  inherited destroy();
end;






function TTfont.calc_tabsum(o,len : dword; ishead : boolean) : dword;
var x,i,s,j : longint; arr : array of byte;
begin
  x := src.position;
  src.seek(o,soFromBeginning);
  s := 0;
  len := (len + 3) div 4;
  if not ishead then begin
    setlength(arr,4*len);
    src.readBuffer(arr[0],4*len);
    j := 0;
    for i := 1 to len do begin
      s := s + (longint(arr[j  ]) shl 24);
      s := s + (longint(arr[j+1]) shl 16);
      s := s + (longint(arr[j+2]) shl  8);
      s := s + (longint(arr[j+3]) shl  0);
      j := j+4;
    end;
  end else begin
    s := s + read_dword(src);
    s := s + read_dword(src);
    read_dword(src);
    for i := 4 to len do begin     
      s := s + read_dword(src);
    end;
  end;
  src.seek(x,soFromBeginning);
  calc_tabsum := s;
end;

function TTfont.calc_checksum() : boolean;
var tag : TTTFtag; chks,o,len,sum,tabsum : dword; i : longint;
begin
  sum := 0;
  src.seek(tabstart,soFromBeginning);
  for i := 1 to numTables do begin
    tag := read_tag(src);
    chks := read_dword(src);
    o    := read_dword(src);
    len  := read_dword(src);
    tabsum := calc_tabsum(o,len,false);//tag='_head'
//    writeln(tag,' ',hexstr(chks,8),' ',hexstr(tabsum,8),' ',(tabsum=chks));
    sum := sum + tabsum;
  end;
  chks := $B1B0AFBA;
  tabsum := sum;
//  writeln('all ',hexstr(chks,8),' ',hexstr(tabsum,8),' ',(tabsum=chks));
  src.seek(tabstart,soFromBeginning);
  for i := 1 to numTables do begin
    tabsum := tabsum + read_dword(src);
    tabsum := tabsum + read_dword(src);
    tabsum := tabsum + read_dword(src);
    tabsum := tabsum + read_dword(src);
  end;
//  writeln('all ',hexstr(chks,8),' ',hexstr(tabsum,8),' ',(tabsum=chks));
  src.seek(0,soFromBeginning);
  tabsum := tabsum + read_dword(src);
  tabsum := tabsum + read_dword(src);
  tabsum := tabsum + read_dword(src);
//  writeln('all ',hexstr(chks,8),' ',hexstr(tabsum,8),' ',(tabsum=chks));
  calc_checksum := (tabsum=chks);
end;




procedure TTglyph.print_subglyphs();
var p : Psubglyph; g : TTglyph; j : longint;
begin
  j := 0;
  p := subglyphs;
  while not(p=nil) do begin
    g := font.getGlyph(p^.idx);
    writeln('printing ',j,'th subglyp with idx ',p^.idx);
    g.print();
    p := p^.next; 
    inc(j);
  end;
end;

procedure TTglyph.print();
var i : longint;
begin
  print_subglyphs();
  writeln('The Glyph ',idx,' here. These are the coords...');
  for i := 0 to high(coordx) do begin
    writeln(coordx[i],'/',coordy[i],' ',(flags[i] and 1)=1);
  end;
  writeln('nothing more to print');
end;



function tagseq(const a,b) : boolean;
begin
  tagseq := (dword(a)=dword(b));
end;

function TTfont.seek_tab(which : TTTFtag) : longint;
var tag : TTTFtag; o,len : dword; i : longint;
begin
  src.seek(tabstart,soFromBeginning);
  for i := 1 to numTables do begin
    tag := read_tag(src);
    read_dword(src); // don't care about checksum here
    o    := read_dword(src);
    len  := read_dword(src);
    if tagseq(tag,which) then begin
      seek_tab := len;
      src.seek(o,soFromBeginning);
      exit;
    end;
  end;
  seek_tab := -1;
end;

procedure TTfont.throwerr(s : ansistring);
begin
  raise Tttfontexception.create(s);
end;



procedure TTfont.load_head();
var magic,v : dword;
begin
  if ( seek_tab('head') <0 ) then throwerr('cannot find head-table in file');
  hvers_upper := read_word(src);
  hvers_lower := read_word(src);  
  fvers_upper := read_word(src);
  fvers_lower := read_word(src);
  read_dword(src);  // csa
  magic :=  read_dword(src);
  if not(magic=$5F0F3CF5) then throwerr('file magic number mismatch');
  hflags :=  read_word(src);
  unitsPerEm :=  read_word(src);
  date_created  := read_qword(src);
  date_modified := read_qword(src);
  xmin := smallint( read_word(src) );
  ymin := smallint( read_word(src) );
  xmax := smallint( read_word(src) );
  ymax := smallint( read_word(src) );
  macStyle := read_word(src);
  lowestRecPPEM := read_word(src);
  fontDirectionHint := read_word(src);
  indexToLocFormat := read_word(src);
  glyphDataFormat := read_word(src);
writeln('head.indexToLocFormat :',indexToLocFormat);
writeln('head.glyphDataFormat  :',glyphDataFormat);
writeln('head.fontDirectionHint:',fontDirectionHint);
writeln('head.lowestRecPPEM    :',lowestRecPPEM);
  seek_tab('maxp');
  v := read_dword(src);
  numGlyphs := read_word(src);
  if (v>=$00010000) then with maxprofile do begin
    maxPoints := read_word(src);
    maxContours := read_word(src);
    maxCompositePoints := read_word(src);
    maxCompositeContours := read_word(src);
    maxZones := read_word(src);
    maxTwilightPoints := read_word(src);
    maxStorage := read_word(src);
    maxFunctionDefs := read_word(src);
    maxInstructionDefs := read_word(src);
    maxStackElements := read_word(src);
    maxSizeOfInstructions := read_word(src);
    maxComponentElements := read_word(src);
    maxComponentDepth := read_word(src);
    print_maxp_profile(maxprofile);
  end;
writeln('numGlyphs=',numGlyphs);
end;


procedure TTfont.load_loca();
var  i : longint;
begin
  i := seek_tab('loca');
  if ( i <0 ) then throwerr('cannot find loca-table in file');
  setlength(locatab,numGlyphs+1);
  if (indexToLocFormat=0) then begin
    if ((numglyphs+1)*2>i) then throwerr('loca-table to short');
    for i := 0 to numGlyphs do locatab[i] := dword(2)*read_word(src);
  end else begin
    if ((numglyphs+1)*4>i) then throwerr('loca-table to short');
    for i := 0 to numGlyphs do locatab[i] := read_dword(src);
  end;
end;

function TTfont.load_kern() : boolean;
var i,vers,ntabs : longint;
begin
  load_kern := false;
  i := seek_tab('kern');
  if (i>0) then begin
    vers := read_word(src);
    nTabs := read_word(src);
    writeln('found kern v',vers,' with ',ntabs,' tables.');
    setlength(kerntabs_gids,nTabs);
    setlength(kerntabs_vals,nTabs);
    setlength(kerntabs_covs,nTabs);
    for i := 1 to nTabs do load_kern_subtab(i);
  end;
  writeln('done reading kern');
end;

function TTfont.load_kern_subtab(sidx : longint) : boolean;
var i,{vers,}len,cov : longint;
knPairs,{ksearchRange,kentrySelector,krangeShift,}l,r : longint;
begin
  
  {vers :=} read_word(src);
  len := read_word(src);
  cov := read_word(src);
  if (cov and $FF00)<>0 then begin
    setlength(kerntabs_gids[sidx-1],0);
    setlength(kerntabs_vals[sidx-1],0);
    kerntabs_covs[sidx-1] := 0;
    src.seek(len-6,soFromCurrent);
    exit;
  end;
  kerntabs_covs[sidx-1] := cov;
  
  knPairs := read_word(src);
  {ksearchRange :=} read_word(src);
  {kentrySelector :=} read_word(src);
  {krangeShift :=} read_word(src);
  setlength(kerntabs_gids[sidx-1],knpairs);
  setlength(kerntabs_vals[sidx-1],knpairs);
  for i := 1 to knPairs do begin
    l := read_word(src);
    r := read_word(src);
    kerntabs_gids[sidx-1][i-1] := (dword(l) shl 16) or r;
    kerntabs_vals[sidx-1][i-1] := read_word(src);
  end;
  load_kern_subtab := true;
end;


function binsearch(pl : Pdword; a,b : longint; ref : dword) : longint;
var c,d,i : longint;
begin
  c := a;
  d := b;
  while (c<d) do begin
    i := (c+d) div 2;
    if (pl[i]>ref) then begin
      d := i-1; 
    end else if (pl[i]<ref) then begin
      c := i+1;
    end else d := i;
  end;
  i := (c+d) div 2;
  if (pl[i]=ref) then begin
    binsearch := i;
  end else begin
    binsearch := -1;
  end;
end;

function TTfont.lookup_kern(gl,gr,cov : longint) : longint;
var idx : dword; v,tab,tcov,i : longint; 
begin
  idx := (gl shl 16) or gr;
  v := 0;
  for tab := 0 to high(kerntabs_covs) do begin
    tcov := kerntabs_covs[tab];
    if (tcov and 7)=cov then begin
      i := binsearch(@kerntabs_gids[tab][0],0,high(kerntabs_gids[tab]),idx);
      if i>=0 then begin
        if (tcov and 8)>0 then v := kerntabs_vals[tab][i] else v+= kerntabs_vals[tab][i];
      end; 
    end;
  end;
  lookup_kern := v;//590 593
end;

function TTfont.load_hhea() : boolean;
var i,j : longint;
begin
  hhea.load_ok := false;
  i := seek_tab('hhea');
  if (i>0) then with hhea do begin
    versh := read_word(src);
    versl := read_word(src);
    Ascender  := smallint(read_word(src));
    Descender := smallint(read_word(src));
    LineGap   := smallint(read_word(src));
    advanceWidthMax := read_word(src);
    minLeftSideBearing  := smallint(read_word(src));
    minRightSideBearing := smallint(read_word(src));
    xMaxExtent          := smallint(read_word(src));
    caretSlopeRise := smallint(read_word(src));
    caretSlopeRun  := smallint(read_word(src));
    caretOffset    := smallint(read_word(src));
    read_word(src);
    read_word(src);
    read_word(src);
    read_word(src);
    metricDataFormat := smallint(read_word(src));
    numberOfHMetrics := read_word(src);
  end;
  j := numGlyphs-hhea.numberOfHMetrics;
  if (j>=0) then begin
    j := j*2 +4*hhea.numberOfHMetrics;
    i := seek_tab('hmtx');    
    if (i>=j) then begin
      setlength(advanceWidth,numGlyphs);
      setlength(leftSideBearing,numGlyphs);
      j := 0;
      for i := 1 to hhea.numberOfHMetrics do begin
        advanceWidth[j]    := read_word(src);
        leftSideBearing[j] := smallint(read_word(src));
        inc(j);
      end;
      if (j>0) then begin
        for i := j to numGlyphs-1 do begin
          advanceWidth[i] := advanceWidth[j-1];
          leftSideBearing[i] := smallint(read_word(src));
        end;
        hhea.load_ok := true;
      end;
    end;
  end;
  load_hhea := hhea.load_ok;
end;



procedure TTfont.load_gftabs();
var i,j : longint;
begin
  setLength(cvt,0);
  i := seek_tab('cvt ');
  if (i>0) then begin
    j := i div 2;
    setLength(cvt,j);
    for i := 0 to j-1 do cvt[i] := read_word(src);
  end;
  setlength(prepprog,0);
  i := seek_tab('prep');
  if (i>0) then begin
    setlength(prepprog,i);
    src.readBuffer(prepprog[0],i);
  end;
  setlength(fontprog,0);
  i := seek_tab('fpgm');
  if (i>0) then begin
    setlength(fontprog,i);
    src.readBuffer(fontprog[0],i);
  end;
end;



procedure TTfont.releaseGlyph(g : TTglyph);
begin
end;


begin

end.











