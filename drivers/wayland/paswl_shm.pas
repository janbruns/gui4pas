{$mode objfpc}
{$unitpath .}
unit paswl_shm;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_bindable,paswl_shm_pool,baseunix;


type
Tpaswl_shm=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;
  
  function create_pool(s : Tsize) : Tpaswl_shm_pool;
  function create_managed_pool(s : Tsize) : Tpaswl_shm_managedpool;
  function create_pool(s : Tsize; t : T_paswl_shm_pool_cls) : T_paswl_shm_pool;
  
  protected
  procedure cb_shm_format(f : dword); virtual;
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;
Tpaswl_shm_cls = class of Tpaswl_shm;








implementation
uses strings,sysutils,math;




procedure shm_format(dat:pointer; o:wl_shm; format : dword); cdecl;
var d : Tpaswl_shm;
begin
  if dat<>nil then begin
    d := Tpaswl_shm(dat);
    if d.wlo=o then d.cb_shm_format(format)
    else ;//
  end;
end;

var
imp_if_shm_listener : wl_shm_listener =(
  format : @shm_format;
);


procedure Tpaswl_shm.cb_shm_format(f : dword);
begin
  writeln('shm_format: ',hexstr(f,8));
end;

function Tpaswl_shm.create_pool(s : Tsize) : Tpaswl_shm_pool;
begin
  create_pool := create_pool(s,Tpaswl_shm_pool) as Tpaswl_shm_pool;
end;

function Tpaswl_shm.create_managed_pool(s : Tsize) : Tpaswl_shm_managedpool;
begin
  create_managed_pool := create_pool(s,Tpaswl_shm_managedpool) as Tpaswl_shm_managedpool;
end;

function Tpaswl_shm.create_pool(s : Tsize; t : T_paswl_shm_pool_cls) : T_paswl_shm_pool;
var o : T_paswl_shm_pool;
begin
writeln(classname,' about to create pool of size ',s);
  o := t.create();
  if o<>nil then begin
    o.display := display;
    o.collection := collection;
    o.wlversion := wlversion;
    o.init_size(s);
    if (o.tmpf<>nil) then begin
      o.wlo := wl_shm_create_pool(wlo, o.tmpf.fd, s);
      if o.wlo=nil then freeandnil(o);
    end else freeandnil(o);
  end;
  create_pool := o;
end;

constructor Tpaswl_shm.create();
begin
  inherited create();
end;

destructor Tpaswl_shm.destroy();
begin
  { wl_shm doesn't have a destructor, so we cannot call one }
  { but since 2015, libwayland-client has wl_proxy_destroy that
    makes valgrind leak-check clean for the client side  }
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_shm.onBind();
begin
  inherited;
  wl_shm_add_listener(wl_shm(wlo),imp_if_shm_listener,self);
end;

procedure Tpaswl_shm.onUnbind();
begin
  inherited;
end;

class function Tpaswl_shm.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_shm_interface;
end;

class function Tpaswl_shm.wl_ifver() : dword;
begin
  wl_ifver := 1;
end;


initialization
begin
end;

finalization
begin
end;

end.



