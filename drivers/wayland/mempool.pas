{$mode objfpc}
{$unitpath .}
unit mempool;

{ A simple "memory manager" }

interface
uses dbllinkedlist,baseunix;

type
{ The mempool should hold a freelist with deallocations.
  For safety, I decided to not use the memblocks to implement the freelist.
  And given that, it'll be least error-prone to reuse the dbllinked list here.
}

Pmemfreelistentry = ^Tmemfreelistentry;
Tmemfreelistentry = packed record
  dbl : Tdbllinkedlist;
  o,s : Tsize;
end;

Tmempool=class
  public
  function alloc_from_pool(s : Tsize) : Tsize;
  procedure release_to_pool(o,s : Tsize);
  { should resize the pool by at least s elements }
  function grow_pool(s : Tsize) : boolean; virtual; abstract;
  constructor create();
  destructor destroy(); override;
  procedure print();

  private
  procedure try_combine(pref : Pmemfreelistentry);
  
  private
  freelist : Tdbllinkedlist;
end;


implementation

function memfree_make() : Pdbllinkedlist;
var p : Pdbllinkedlist;
begin
  getmem(p,sizeof(Tmemfreelistentry));
  memfree_make := p;
end;

procedure memfree_release(p : Pdbllinkedlist);
begin
  freemem(p,sizeof(Tmemfreelistentry));
end;

var
memfreelistfactory : Tfactoryif = ( make : @memfree_make; release : @memfree_release);




constructor Tmempool.create();
begin
  dblLinkedList_init(freelist,@memfreelistfactory);
end;

destructor Tmempool.destroy();
var p : Pdbllinkedlist;
begin
  repeat
    p := dblLinkedList_getFirst(freelist);
    if p<>nil then begin
      dblLinkedList_remove(p);
    end else break;
  until false;
end;
  
  
function dobettercheck(var ref : Pmemfreelistentry; cur : Pmemfreelistentry; n : Tsize) : boolean;
begin
  if cur^.s>=n then begin
    if (ref=nil) then begin
      ref := cur;
    end else begin
      if (ref^.s<cur^.s) then begin
        ref := cur;
      end;
    end;
    dobettercheck := (ref^.s=n);
  end else dobettercheck := false;
end;

function Tmempool.alloc_from_pool(s : Tsize) : Tsize;
var p : Pdbllinkedlist; best,p2 : Pmemfreelistentry;
begin
//writeln('alloc from pool: ',s);
//print();
  best := nil;
  p := dblLinkedList_getFirst(freelist);
  while p<>nil do begin
    if dobettercheck(best,pointer(p),s) then break;
    p := dblLinkedList_getNext(p);
  end;
  if best=nil then begin
    if grow_pool(s) 
    then alloc_from_pool := alloc_from_pool(s) 
    else alloc_from_pool := high(Tsize);
  end else begin
    p2 := pointer(best);
    alloc_from_pool := p2^.o;
    p2^.o += s;
    p2^.s -= s;
    if (p2^.s=0) then dblLinkedList_remove(pointer(best));
  end;
//print();
end;

procedure Tmempool.print();
var p : Pdbllinkedlist; p2 : Pmemfreelistentry;
begin
  writeln('current pool:');
  p := dblLinkedList_getFirst(freelist);
  while p<>nil do begin
    p2 := pointer(p);
    writeln('memblock @',p2^.o,' s=',p2^.s);
    p := dblLinkedList_getNext(p);
  end;
  writeln;
end;

procedure Tmempool.release_to_pool(o,s : Tsize);
var p : Pdbllinkedlist; p2 : Pmemfreelistentry;
begin
//writeln('release_to_pool: ',o,' / ',s);
  p := dblLinkedList_insertAfter(freelist,nil,nil);
  p2 := pointer(p);
  p2^.o := o;
  p2^.s := s;
  try_combine(p2);
end;


procedure Tmempool.try_combine(pref : Pmemfreelistentry);
var p : Pdbllinkedlist; p2 : Pmemfreelistentry; prefnext : Pmemfreelistentry;
begin
  while (pref<>nil) do begin
    prefnext := nil;
    p := dblLinkedList_getFirst(freelist);
    while (p<>nil) do begin
      p2 := pointer(p);
      if (p2^.o+p2^.s)=pref^.o then begin
        p2^.s += pref^.s;
        dblLinkedList_remove(@pref^);
        prefnext := p2;
        p := nil;
      end else if (pref^.o+pref^.s)=p2^.o then begin
        pref^.s += p2^.s;
        dblLinkedList_remove(p);
        prefnext := pref;
        p := nil;
      end else begin
        p := dblLinkedList_getNext(p);
      end;
    end;
    pref := prefnext;
  end;
  //print();
end;


begin
end.
