UNIT ttdata;
{$mode objfpc}

{ 
}


interface
uses ttprog;

type
Ttdatastore = class
  function getValue(i : longint) : dword;
  procedure setValue(i : longint; v : dword);

  procedure setValues(arr : array of dword);
  procedure copyFrom(src : Ttdatastore);

  function is_dirty() : boolean;
  procedure mark_clean();
  procedure mark_dirty();
  procedure initData(d : dword);
  procedure setLength(i : longint);
  function getLength() : longint;

  constructor create();
  destructor destroy(); override;

  protected
  dat : array of dword;
  dirty : boolean;  
end;


implementation


function Ttdatastore.getValue(i : longint) : dword;
begin
  if (i>=0)and(i<=high(dat)) then begin
    getValue := dat[i];
  end else begin
    raise Tttprogexception.create('read past end of TT-instruction storage area ');
  end;
end;

procedure Ttdatastore.setValue(i : longint; v : dword);
begin
  if (i>=0)and(i<=high(dat)) then begin
    dat[i] := v;
    dirty := true;
  end else begin
    raise Tttprogexception.create('write past end of TT-instruction storage area ');
  end;
end;

procedure Ttdatastore.setValues(arr : array of dword);
var i : longint;
begin
  self.setlength( high(arr)+1 );
  for i := 0 to high(dat) do dat[i] := arr[i];
end;

procedure Ttdatastore.copyFrom(src : Ttdatastore);
begin
  setValues(src.dat);
  dirty := false;
end;



procedure Ttdatastore.setLength(i : longint);
begin
  system.setlength(dat,i);
end;

function Ttdatastore.getLength() : longint;
begin
  getLength := high(dat)+1;
end;

procedure Ttdatastore.initData(d : dword);
var i : longint;
begin
  for i := 0 to high(dat) do dat[i] := d;
end;



function Ttdatastore.is_dirty() : boolean;
begin
  is_dirty := dirty;
end;

procedure Ttdatastore.mark_clean();
begin
  dirty := false;
end;

procedure Ttdatastore.mark_dirty();
begin
  dirty := true;
end;



constructor Ttdatastore.create();
begin
  inherited create();
  dirty := false;
  setLength(0);
end;

destructor Ttdatastore.destroy();
begin
  inherited destroy();
end;



begin
end.

