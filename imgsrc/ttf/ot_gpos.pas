unit ot_gpos;
{$mode objfpc}
{$COPERATORS ON}

{ 
}



interface
uses classes,sysutils,fontfile,ot_cmap,ot_gdef,ot_gsub,ot_comn;


type



TposValueRecordArr = array of TposValueRecord;


TanchorArr = array of Tanchor;
Tanchor2dArr = array of TanchorArr;
Tligatt = Tanchor2dArr;

TMarkRecord = record
  a : Tanchor;
  cls : word;
end;
TMarkArray = array of TMarkRecord;




TSinglePosFormat1 = class(TOTgposlookup)
  procedure load(s : Tstream); override;
  procedure adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); override;

  public
  coverage : Tcoverage;
  vr : TposValueRecord;
end;

TSinglePosFormat2 = class(TOTgposlookup)
  procedure load(s : Tstream); override;
  procedure adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); override;
  public
  coverage : Tcoverage;
  vrs : TposValueRecordArr;
end;

TPairValue = record
  SecondGlyph : Tglychar;
  v1,v2 : TposValueRecord;
end;
TPairSet = array of TPairValue;

TPairPosFormat1 = class(TOTgposlookup)
  procedure load(s : Tstream); override;
  procedure adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); override;

  public
  Coverage : Tcoverage;
  PairSets : array of TPairSet;
  f2 : longint;
end;


TClass2Record = record
  v1,v2 : TposValueRecord;
end;
TClass2Records = array of TClass2Record;

TPairPosFormat2 = class(TOTgposlookup)
  procedure load(s : Tstream); override;
  procedure adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); override;

  public
  Coverage : Tcoverage;
  cd1,cd2 : Tglycls;
  v : array of TClass2Records;
  f2 : longint;
end;

TCursivePosFormat1 = class(TOTgposlookup)
  procedure load(s : Tstream); override;
  procedure adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); override;

  public
  Coverage : Tcoverage;
  enex : array of Tanchor;
end;

TMarkBasePosFormat1 = class(TOTgposlookup)
  procedure load(s : Tstream); override;
  procedure adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); override;

  public
  MarkCoverage,
  BaseCoverage : Tcoverage;
  MarkArray : TMarkArray;
  BaseArray : Tanchor2dArr;
end;

TMarkLigPosFormat1 = class(TOTgposlookup)
  procedure load(s : Tstream); override;
  procedure adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); override;

  public
  MarkCoverage,
  LigatureCoverage : Tcoverage;
  MarkArray : TMarkArray;
  LigArray : array of Tligatt;
end;

TMarkMarkPosFormat1 = class(TMarkBasePosFormat1)
  procedure adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); override;
end;





TChainContextPosFormat1 = class(TOTgposlookup)
  procedure load(s : Tstream); override;
  procedure adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); override;

  public
  Coverage : Tcoverage;
  csrs : array of TchainsubruleArr;
end;

TChainContextPosFormat2 = class(TOTgposlookup)
  procedure load(s : Tstream); override;
  procedure adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); override;

  public
  cscs : array of TChainSubClassSet;
  coverage : Tcoverage;
  btcd,ipcd,lacd : Tglycls;
end;

TChainContextPosFormat3 = class(TOTgposlookup)
  procedure load(s : Tstream); override;
  procedure adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); override;

  public
  cbt,cin,cla : Tcoveragearr;
  sa : Tsubstarr;
end;


TContextPosFormat1 = class(TChainContextPosFormat1)
  procedure load(s : Tstream); override;

end;

TContextPosFormat2 = class(TChainContextPosFormat2)
  procedure load(s : Tstream); override;

end;

TContextPosFormat3 = class(TChainContextPosFormat3)
  procedure load(s : Tstream); override;
end;


TExtensionPosFormat1 = class(TOText32lookup)
end;






/////////////////////////////////////////////////////////////////
// the TTfont descendant
/////////////////////////////////////////////////////////////////


TOTgposfont = class(TOTgsubfont)
  constructor create();
  destructor destroy(); override;

  procedure adjustScript(s : Tglystr; var adj : Tglyphplacement; i : longint);
  procedure adjustFeature(s : Tglystr; var adj : Tglyphplacement; i : longint);
  procedure adjustLookup(s : Tglystr; var adj : Tglyphplacement; i : longint);

  procedure load_gpos();

  function findgposScript(nam : string) : longint;


  public
  gposScripts : TOTscriptList;
  gposFeaturelist : TOTfeaturelist;
  gposLookUps : Tlookuptabs;
end;


procedure init_adj(s : Tglystr; var adj : Tglyphplacement);




implementation
uses math;


procedure read_anchor(var a : Tanchor; s : Tstream);
begin
  with a do begin
    XCoordinate := 0;
    YCoordinate := 0;
    AnchorPoint := 65535;
    XDeviceTable := 0;
    YDeviceTable := 0;
    fmt := read_word(s);
    if (fmt>0)and(fmt<4) then begin
      XCoordinate := read_word(s);
      YCoordinate := read_word(s);
      if (fmt=2) then begin
        AnchorPoint := read_word(s);
      end else if (fmt=3) then begin
        XDeviceTable := read_word(s);
        YDeviceTable := read_word(s);
      end;
    end;
  end;
end;



procedure read_markrec(var r : TMarkRecord; i0 : longint; s : Tstream);
var o,i1 : longint;
begin
  r.cls := read_word(s);
  o := read_word(s);
  i1 := s.position;
  s.seek(o+i0,soFromBeginning);
  read_anchor(r.a,s);
  s.seek(i1,soFromBeginning);
end;

procedure read_markarr(var ma : TMarkArray; s : Tstream);
var i,i0,n : longint;
begin
  i0 := s.position;
  n := read_word(s);
  setlength(ma,n);
  for i := 0 to n-1 do begin
    read_markrec(ma[i],i0,s);
  end;
end;

procedure seekread_markarr(var ma : TMarkArray; i : longint; s : Tstream);
begin
  s.seek(i,soFromBeginning);
  read_markarr(ma,s);
end;


procedure apply_valrec(const src : TposValueRecord; var dst : TposValueRecord);
begin
  inc(dst.XPlacement,src.XPlacement);
  inc(dst.YPlacement,src.YPlacement);
  inc(dst.XAdvance  ,src.XAdvance  );
  inc(dst.YAdvance  ,src.YAdvance  );
  // ...
end;


procedure load_valrec(var vr : TposValueRecord; fmt : longint; s : Tstream);
begin
  fillbyte(vr,sizeof(vr),0);
  with vr do begin
    if (fmt and $0001)>0 then XPlacement := read_word(s);
    if (fmt and $0002)>0 then YPlacement := read_word(s);
    if (fmt and $0004)>0 then XAdvance   := read_word(s);
    if (fmt and $0008)>0 then YAdvance   := read_word(s);
    if (fmt and $0010)>0 then XPlaDevice := read_word(s);
    if (fmt and $0020)>0 then YPlaDevice := read_word(s);
    if (fmt and $0040)>0 then XAdvDevice := read_word(s);
    if (fmt and $0080)>0 then YAdvDevice := read_word(s);
  end;
end;

procedure load_valrecs(var vrs : TposValueRecordArr; fmt,n : longint; s : Tstream);
var i : longint;
begin
  setlength(vrs,n);
  for i := 0 to n-1 do begin
    load_valrec(vrs[i],fmt,s);
  end;
end;

procedure seekloadcoverage(var cov : Tcoverage; i : longint; s : Tstream);
begin
  s.seek(i ,soFromBeginning);
  load_coverage(cov,s);
end;

procedure easyloadcoverage(var cov : Tcoverage; i0 : longint; s : Tstream);
var o,i1 : longint;
begin
  o := read_word(s);
  i1 := s.position;
  seekloadcoverage(cov,i0+o,s);
  s.seek(i1 ,soFromBeginning);
end;






procedure TSinglePosFormat1.load(s : Tstream);
var fmt,i0 : longint;
begin
  i0 := s.position;
  read_word(s); // fmt = 1, already processed
  easyloadcoverage(coverage,i0,s);
  fmt := read_word(s);
  load_valrec(vr,fmt,s);
  writeln(classname,' loaded:',1);  
end;

procedure TSinglePosFormat2.load(s : Tstream);
var fmt,n,i0 : longint;
begin
  i0 := s.position;
  read_word(s); // fmt = 2, already processed
  easyloadcoverage(coverage,i0,s);
  fmt := read_word(s);
  n   := read_word(s);
  load_valrecs(vrs,fmt,n,s);
  n := min(length(coverage),high(vrs)+1);
  setlength(coverage,n);
  setlength(vrs,n);
  writeln(classname,' loaded:',n);  
end;



procedure load_pairset(var ps : TPairSet; fmt1,fmt2 : longint; s : Tstream);
var n,i : longint;
begin
  n := read_word(s);
  setlength(ps,n);
  for i := 0 to n-1 do begin
    ps[i].SecondGlyph := read_glychar(s);
    load_valrec(ps[i].v1,fmt1,s);
    load_valrec(ps[i].v2,fmt2,s);
  end;
end;

procedure TPairPosFormat1.load(s : Tstream);
var fmt1,fmt2,n,i0,i : longint; os : array of longint;
begin
  i0 := s.position;
  read_word(s); // fmt = 1 already processed
  easyloadcoverage(coverage,i0,s);
  fmt1 := read_word(s);
  fmt2 := read_word(s);
  f2 := fmt2;
  n    := read_word(s);
  setlength(os,n);
  for i := 0 to n-1 do os[i] := read_word(s);
  setlength(PairSets,n);
  for i := 0 to n-1 do begin
    s.seek(i0+os[i] ,soFromBeginning);
    load_pairset(PairSets[i],fmt1,fmt2,s);
  end;
  writeln(classname,' loaded:',n);  
end;

procedure TPairPosFormat2.load(s : Tstream);
var fmt1,fmt2,n1,n2,o1,o2,i0,i,j : longint; 
begin
  i0 := s.position;
  read_word(s); // fmt = 2, already processed
  easyloadcoverage(coverage,i0,s);
  fmt1 := read_word(s);
  fmt2 := read_word(s);
  f2 := fmt2;
  o1 := read_word(s);
  o2 := read_word(s);
  n1 := read_word(s);
  n2 := read_word(s);
  setlength(v,n1);
  for i := 0 to n1-1 do begin
    setlength(v[i],n2);
    for j := 0 to n2-1 do begin
      load_valrec(v[i][j].v1,fmt1,s);
      load_valrec(v[i][j].v2,fmt2,s);
    end;
  end;
  s.seek(i0+o1,soFromBeginning); load_glyphclassdef(cd1,s);
  s.seek(i0+o2,soFromBeginning); load_glyphclassdef(cd2,s);
  writeln(classname,' loaded:',n1,' , ',n2);  
end;

procedure TCursivePosFormat1.load(s : Tstream);
var n,i0,i : longint; os : array of longint;
begin
  i0 := s.position;
  read_word(s); // fmt = 1, already processed
  easyloadcoverage(coverage,i0,s);
  n := read_word(s);
  n := n*2;
  setlength(enex,n);
  setlength(os,n);
  for i := 0 to n-1 do begin
    os[i] := read_word(s);
  end;
  for i := 0 to n-1 do begin
    fillbyte(enex[i],sizeof(Tanchor),0);
    if not(os[i]=0) then begin
      s.seek(i0+os[i],soFromBeginning);
      read_anchor(enex[i],s);
    end;
  end;
  writeln(classname,' loaded:',n,' , ',n);  
end;


procedure TMarkBasePosFormat1.load(s : Tstream);
var i0,i1,i,j,cc,bc,ma,ba : longint; 
begin
  i0 := s.position;
  read_word(s); // fmt = 1, already processed
  easyloadcoverage(MarkCoverage,i0,s);
  easyloadcoverage(BaseCoverage,i0,s);
  cc := read_word(s);
  ma := read_word(s);
  ba := read_word(s);
  seekread_markarr(MarkArray,i0+ma,s);
  s.seek(i0+ba,soFromBeginning);
  bc := read_word(s);
  setlength(BaseArray,bc);
  i1 := s.position;
  for i := 0 to bc-1 do begin
    setlength(BaseArray[i],cc);
    for j := 0 to cc-1 do begin
      s.seek(i1,soFromBeginning);
      ma := read_word(s);
      i1 := s.position;
      s.seek(i0+ba+ma,soFromBeginning);
      read_anchor(BaseArray[i][j],s);
    end;
  end;
  writeln(classname,' loaded:',bc,' , ',cc);  
end;



procedure read_ligattay(var la : Tligatt; cc : longint; s : Tstream);
var compos,i,j,i0,i1,o : longint;
begin
  i0 := s.position;
  compos := read_word(s);
  setlength(la,compos);
  i1 := s.position;
  for i := 0 to compos-1 do begin
    setlength(la[i],cc);
    for j := 0 to cc-1 do begin
      s.seek(i1,soFromBeginning);
      o := read_word(s);
      i1 := s.position;
      if not(o=0) then begin
        s.seek(i0+o,soFromBeginning);
        read_anchor(la[i][j],s);
      end else fillbyte(la[i][j],sizeof(Tanchor),0);
    end;
  end;
end;

procedure TMarkLigPosFormat1.load(s : Tstream);
var i0,i,cc,lc,ma,la : longint; os : array of longint;
begin
  i0 := s.position;
  read_word(s); // fmt = 1, already processed
  easyloadcoverage(MarkCoverage,i0,s);
  easyloadcoverage(LigatureCoverage,i0,s);
  cc := read_word(s);
  ma := read_word(s);
  la := read_word(s);
  seekread_markarr(MarkArray,i0+ma,s);

  s.seek(i0+la,soFromBeginning);
  lc := read_word(s);
  setlength(LigArray,lc);
  setlength(os,lc);
  for i := 0 to lc-1 do begin
    os[i] := read_word(s);
  end;
  for i := 0 to lc-1 do begin
    s.seek(i0+la+os[i],soFromBeginning);
    read_ligattay(LigArray[i],cc,s);
  end;

  writeln(classname,' loaded:',lc,' , ',cc);  
end;













// context+chaincontext data structure loading identical to GSUB
// replicated here to keep inheritance clean and less misleading

procedure TContextPosFormat1.load(s : Tstream);
var i,i0,n,co : longint; os : array of longint;
begin
  i0 := s.position;
  read_word(s); // version, already processed
  co := read_word(s);
  n := read_word(s);
  setlength(os,n);
  for i := 0 to n-1 do os[i] := read_word(s);
  s.seek(i0+co,soFromBeginning);
  load_coverage(coverage,s);
  n := min(n,length(coverage));
  setlength(coverage,n);
  setlength(csrs,n);
  for i := 0 to n-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    load_srs(csrs[i],s);
  end; 
end;



procedure TContextPosFormat2.load(s : Tstream);
var ipo,co,cs,i,i0 : longint; os : array of longint;
begin
  i0 := s.position;
  read_word(s); // SubstFormat=2, already processed
  co  := read_word(s);
  ipo := read_word(s);
  cs  := read_word(s);
  setlength(os,cs);
  for i := 0 to cs-1 do os[i] := read_word(s);
  setlength(btcd,0);
  setlength(lacd,0);
  s.seek(i0+co ,soFromBeginning);  load_coverage(coverage,s);
  s.seek(i0+ipo,soFromBeginning);  load_glyphclassdef(ipcd,s);
  setlength(cscs,cs);
  for i := 0 to cs-1 do begin
    if not(os[i]=0) then begin
      s.seek(i0+os[i],soFromBeginning);
      read_SubClassSet(cscs[i],s);
    end else setlength(cscs[i],0)
  end;
  writeln(classname, ' loaded ');
end;


procedure TContextPosFormat3.load(s : Tstream);
var n,n2,i0 : longint;
begin
  setlength(cbt,0);
  setlength(cla,0);
  i0 := s.position;
  read_word(s); // SubstFormat
  n := read_word(s);
  n2 := read_word(s);
  load_covarr(n,i0,cin,s);
  load_substarr(n2,sa,s);
  writeln(classname, ' loaded ',high(cbt)+1,'[',high(cin)+1,']',high(cla)+1);
end;



procedure TChainContextPosFormat1.load(s : Tstream);
var i,i0,n,co : longint; os : array of longint;
begin
  i0 := s.position;
  read_word(s); // version, already processed
  co := read_word(s);
  n := read_word(s);
  setlength(os,n);
  for i := 0 to n-1 do os[i] := read_word(s);
  s.seek(i0+co,soFromBeginning);
  load_coverage(coverage,s);
  n := min(n,length(coverage));
  setlength(coverage,n);
  setlength(csrs,n);
  for i := 0 to n-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    load_csrs(csrs[i],s);
  end; 
  writeln(classname, ' loaded ');
end;


procedure TChainContextPosFormat2.load(s : Tstream);
var bto,ipo,lao,co,cs,i,i0 : longint; os : array of longint;
begin
  i0 := s.position;
  read_word(s); // SubstFormat=2, already processed
  co  := read_word(s);
  bto := read_word(s);
  ipo := read_word(s);
  lao := read_word(s);
  cs  := read_word(s);
  setlength(os,cs);
  for i := 0 to cs-1 do os[i] := read_word(s);
  s.seek(i0+co ,soFromBeginning);  load_coverage(coverage,s);
  s.seek(i0+ipo,soFromBeginning);  load_glyphclassdef(ipcd,s);
  if (bto=0) then setlength(btcd,0) else begin
    s.seek(i0+bto,soFromBeginning);  load_glyphclassdef(btcd,s);
  end;
  if (lao=0) then setlength(lacd,0) else begin
    s.seek(i0+lao,soFromBeginning);  load_glyphclassdef(lacd,s);
  end;
  setlength(cscs,cs);
  for i := 0 to cs-1 do begin
    if not(os[i]=0) then begin
      s.seek(i0+os[i],soFromBeginning);
      read_ChainSubClassSet(cscs[i],s);
    end else setlength(cscs[i],0)
  end;

  writeln(classname, ' loaded ',cs);
end;



procedure TChainContextPosFormat3.load(s : Tstream);
var n,i0 : longint;
begin
  i0 := s.position;
  read_word(s); // SubstFormat
  n := read_word(s);
  load_covarr(n,i0,cbt,s);
  n := read_word(s);
  load_covarr(n,i0,cin,s);
  n := read_word(s);
  load_covarr(n,i0,cla,s);
  n := read_word(s);
  load_substarr(n,sa,s);
  writeln(classname, ' loaded ',high(cbt)+1,'[',high(cin)+1,']',high(cla)+1);
end;




























procedure TSinglePosFormat1.adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); 
var i : longint;
begin
writeln(classname,'.adjust');
  b := min(b,length(s));
  for i := a to b do begin
    if covpos(s[i],coverage,tabs)>0 then begin
writeln(classname,' adjusted ',i);
      apply_valrec(vr,adj[i].vr);
    end;
  end;
end;

procedure TSinglePosFormat2.adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); 
var i,j : longint;
begin
writeln(classname,'.adjust');
  b := min(b,length(s));
  for i := a to b do begin
    j := covpos(s[i],coverage,tabs) -1;
    if (j>=0)and(j<=high(vrs)) then begin
writeln(classname,' adjusted ',i);
      apply_valrec(vrs[j],adj[i].vr);
    end;
  end;
end;

procedure TCursivePosFormat1.adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); 
var i,j : longint;
begin
writeln(classname,'.adjust');
  b := min(b,length(s));
  for i := a to b do begin
    j := covpos(s[i],coverage,tabs) -1;
    j := j*2;
    if (j>=0)and((j+1)<=high(enex)) then begin
writeln(classname,' adjusted ',i);
      adj[i].selfentry := enex[j];
      adj[i].selfexit  := enex[j+1];
    end;
  end;
end;



procedure TPairPosFormat1.adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs);
var i,j,k,upbd : longint; nextgly1,nextgly2 : Tglystr;
begin
writeln(classname,'.adjust');
  b := min(b,length(s));
  if tabs.use_long_distance_matching 
  then upbd := length(s)
  else upbd := b; 
  i := a;
  while (i<=b) do begin
    if tabs.dont_ignore_one 
    then j :=   cpos(s[i],Coverage)-1
    else j := covpos(s[i],Coverage,tabs)-1;
    if (j>=0) then begin
      nextgly1 := ignacopy(s,i+1,+1,1,tabs);
      if upbd>=(i+length(nextgly1)) then begin
        nextgly2 := ignocopy(nextgly1,1,+1,1,tabs);
        if length(nextgly2)=1 then begin
          if (j<=high(PairSets)) then begin
            for k := 0 to high(PairSets[j]) do with PairSets[j][k] do begin
              if SecondGlyph=nextgly2 then begin
writeln(classname,' adjusted ',i,' and ',i+length(nextgly1));
                apply_valrec(v1,adj[i].vr);
                apply_valrec(v2,adj[i+length(nextgly1)].vr);
                inc(i,length(nextgly1));
                if (f2=0) then dec(i);  // excpetion explicitly stated in specs
                break; // out of k-loop
              end;
            end;
          end;
        end;
      end;
    end;
    inc(i);
  end;
end;






procedure TPairPosFormat2.adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs);
var i,j,upbd,cv1,cv2 : longint; nextgly1,nextgly2 : Tglystr;
begin
writeln(classname,'.adjust');
  b := min(b,length(s));
  if tabs.use_long_distance_matching 
  then upbd := length(s)
  else upbd := b; 
  i := a;
  while (i<=b) do begin
    if tabs.dont_ignore_one 
    then j :=   cpos(s[i],Coverage)-1
    else j := covpos(s[i],Coverage,tabs)-1;
    if (j>=0) then begin
      nextgly1 := ignacopy(s,i+1,+1,1,tabs);
      if upbd>=(i+length(nextgly1)) then begin
        nextgly2 := ignocopy(nextgly1,1,+1,1,tabs);
        if length(nextgly2)=1 then begin
          cv1 := get_glyphclass(s[i],cd1);
          cv2 := get_glyphclass(nextgly2[1],cd2); 
          if (cv1<=high(v)) then begin
            if (cv2<=high(v[cv1])) then with v[cv1][cv2] do begin
writeln(classname,' adjusted ',i,' and ',i+length(nextgly1));
              apply_valrec(v1,adj[i].vr);
              apply_valrec(v2,adj[i+length(nextgly1)].vr);
              inc(i,length(nextgly1));
              if (f2=0) then dec(i); // excpetion explicitly stated in specs
            end;
          end;
        end;
      end;
    end;
    inc(i);
  end;
end;




procedure TMarkBasePosFormat1.adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs);
var i,j,c,k,j2,lobd : longint; 
begin
writeln(classname,'.adjust');
  b := min(b,length(s));
  if tabs.use_long_distance_matching 
  then lobd := 1
  else lobd := a; 
  for i := a to b do begin
    if tabs.dont_ignore_one 
    then j :=   cpos(s[i],MarkCoverage)-1
    else j := covpos(s[i],MarkCoverage,tabs)-1;
    if (j>high(MarkArray)) then j := -1;
    if j>=0 then begin
      j2 := -1;
      k := 0;
      for c := i-1 downto lobd do begin
        j2 := covpos(s[c],BaseCoverage,tabs)-1;
        if (j2>=0) then begin
          k := c; break; // out of c-looü
        end;        
      end;
      if (j2>=0)and(j2<=high(BaseArray)) then begin
        c := MarkArray[j].cls;
        if (c<=high(BaseArray[j2])) then begin
writeln(classname,' adjusted ',i);
          adj[i].aself := MarkArray[j].a; 
          adj[i].abase := BaseArray[j2][c];
          adj[i].base  := k;
        end;
      end;
    end;
  end;
end;





procedure TMarkMarkPosFormat1.adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs);
var i,j,c,k,j2,lobd : longint; 
begin
writeln(classname,'.adjust');
  b := min(b,length(s));
  if tabs.use_long_distance_matching 
  then lobd := 1
  else lobd := a; 
  for i := a to b do begin
    if tabs.dont_ignore_one 
    then j :=   cpos(s[i],MarkCoverage)-1
    else j := covpos(s[i],MarkCoverage,tabs)-1;
    if (j>high(MarkArray)) then j := -1;
    if j>=0 then begin
      j2 := -1;
      k := i;
      for c := i-1 downto lobd do begin
        if not(tabs.isIgnGly(s[c])) then begin
          k := c; 
          break;
          j2 := covpos(s[k],BaseCoverage,tabs)-1;
          if (j2>=0) then break; // out of c-looü
          j2 := cpos(s[k],MarkCoverage)-1;
          if (j2<0) then begin
//writeln('found non-mark');
//            break; // out of c-looü
          end;
        end;        
      end;
      j2 := covpos(s[k],BaseCoverage,tabs)-1;
      if (j2>=0)and(j2<=high(BaseArray))and(k<i) then begin
        c := MarkArray[j].cls;
        if (c<=high(BaseArray[j2])) then begin
writeln(classname,' adjusted ',i);
          adj[i].aself := MarkArray[j].a; 
          adj[i].abase := BaseArray[j2][c];
          adj[i].base  := k;
        end;
      end;
    end;
  end;
end;


procedure TMarkLigPosFormat1.adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs);
var i,j,c,k,j2,lobd,cc : longint; 
begin
writeln(classname,'.adjust');
  b := min(b,length(s));
  if tabs.use_long_distance_matching 
  then lobd := 1
  else lobd := a; 
  for i := a to b do begin
    if tabs.dont_ignore_one 
    then j :=   cpos(s[i],MarkCoverage)-1
    else j := covpos(s[i],MarkCoverage,tabs)-1;
    if (j>high(MarkArray)) then j := -1;
    if j>=0 then begin
      j2 := -1;
      cc := 0;
      k := 0;
      for c := i-1 downto lobd do begin
        if tabs.is_deleted_ligpart(s[c]) then inc(cc);
        j2 := covpos(s[c],LigatureCoverage,tabs)-1;
        if (j2>=0) then begin
          k := c; break; // out of c-loop
        end;        
      end;
      if (j2>=0)and(j2<=high(LigArray)) then begin
        if (cc<=high(LigArray[j2])) then begin
          c := MarkArray[j].cls;
          if (c<=high(LigArray[j2][cc])) then begin
writeln(classname,' adjusted ',i);
            adj[i].aself := MarkArray[j].a; 
            adj[i].abase := LigArray[j2][cc][c];
            adj[i].base  := k;
          end;
        end;
      end;
    end;
  end;
end;





{ do the pos lookups.
  sa : the rule array
  i : the current glyph position in s
  n : number of nonignored "input"/context glyphs for the operation
  d : number of glyphs in  "input"/context icluding ignored ones
  maxi : limit for 
}
procedure do_poslookups(const sa : Tsubstarr; i,n,d,maxi : longint; s : Tglystr; var adj : Tglyphplacement; tabs : Tlookuptabs);
var j,i2,i3,k,r : longint; 
begin
  maxi := min(maxi,length(s));
  for j := 0 to high(sa) do begin
    r := sa[j].SequenceIndex;
    { The glystr might contain "ignored" glyphs
      so we cannot simply index }
    i3 := 0;
    i2 := i;
    for k := 1 to r do begin
      i3 := k;
      repeat
        inc(i2);
      until (i2>=maxi) or not(tabs.isIgnGly(s[i2]));
      if (i2>=maxi) then break;
    end;
    { noe i2 is the postion where to apply the lookup }
    if (i3=r) and not(tabs.isIgnGly(s[i2])) then begin
 
      i3 := i2;
      if not(tabs.use_one_lookup) then begin
        i3 := i+d-1;
      end;
      adjust_lookup(s,adj,i2,i3, sa[j].LookupListIndex,tabs);

    end;
  end;
end;



procedure TChainContextPosFormat1.adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); 
var i,j,k,k2 : longint; x,z,dy : Tglystr;
begin
writeln(classname,'.adjust');
  b := min(b,length(s));
  i := 1;
  while (i<=length(s)) do begin
    dy := s[i];
    if tabs.dont_ignore_one 
    then j :=   cpos(s[i],Coverage)-1
    else j := covpos(s[i],Coverage,tabs)-1;
    if (j>=0)and(j<=high(csrs)) then begin
      k2 := high(csrs[j]);
      for k := 0 to k2 do with csrs[j][k] do begin
        x := ignocopy(btrk,1  ,+1,length(btrk),tabs);
        z := ignocopy(s   ,i-1,-1,length(   x),tabs);
        if (x=z) then begin
          x := ignocopy(inpt,1  ,+1,length(inpt),tabs);
          z := ignocopy(s   ,i+1,+1,length(   x),tabs);
          if (x=z) then begin
            z := ignacopy(s   ,i+1,+1,length(   x),tabs);
            x := ignocopy(lahd,1  ,+1,length(lahd),tabs);
            z := ignocopy(s,i+1+length(z),+1,length(x),tabs);
            if (x=z) then begin

              x  := ignocopy(inpt,1  ,+1,length(inpt),tabs);
              dy := ignacopy(s   ,i  ,+1,length(x)+1,tabs);
              x := dy;
              do_poslookups(sa,i,length(x)+1,length(dy),b,s,adj,tabs);
writeln(classname,' adjusted ',i);
              inc(i,length(x)-1);
              break; // break out of k-loop
            end;
          end;
        end;
      end; 
    end;
    inc(i);
  end;
end;





procedure TChainContextPosFormat2.adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); 
var d,i,j,k,k2 : longint; dy : Tglystr;
begin
writeln(classname,'.adjust');
  b := min(b,length(s));
  i := 1;
  while (i<=length(s)) do begin
    dy := copy(s,i,1);
    if tabs.dont_ignore_one 
    then j :=   cpos(s[i],Coverage)-1
    else j := covpos(s[i],Coverage,tabs)-1;
    if j>=0 then begin
      j := get_glyphclass(dy[1],ipcd);
      if (j>=0)and(j<=high(cscs)) then begin
        k2 := high(cscs[j]);
        for k := 0 to k2 do with cscs[j][k] do begin

          if  check_clsmatch(s,i-1,-1,bt,btcd,tabs)
          and check_clsmatch(s,i+1, 1,ip,ipcd,tabs)
          then begin
            d := length( ignacopy(s,i,+1,length(ip)+1,tabs) );
            if check_clsmatch(s,i+d, 1,la,lacd,tabs) 
            then begin
              do_poslookups(sa,i,length(ip)+1,d,b,s,adj,tabs);
writeln(classname,' adjusted ',i);
              inc(i,d-1);
              break; // out of k-loop
            end;
          end;

        end;
      end;
    end;
    inc(i);
  end;
end;




procedure TChainContextPosFormat3.adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); 
var c,d,i : longint; dy : Tglystr;
begin
writeln(classname,'.adjust');
  b := min(b,length(s));
  c := high(cin)+1;
  i := a;
  while (i<=b) do begin
    dy := copy(s,i,1);
    if covpos(dy[1],cin[0],tabs)>0 then begin
      if  check_covarrmatch(s,i-1,-1,high(cbt)+1,cbt,tabs)
      and check_covarrmatch(s,i  , 1,high(cin)+1,cin,tabs)
      then begin
        d := length( ignacopy(s,i,+1,c,tabs) );
        if check_covarrmatch(s,i+d, 1,high(cla)+1,cla,tabs) 
        then begin
          do_poslookups(sa,i,high(cin)+1,d,b,s,adj,tabs);
writeln(classname,' adjusted ',i);
          inc(i,d-1);
        end;
      end;
    end;
    inc(i);
  end;
end;






















constructor TOTgposfont.create();
begin
  inherited create();
  setlength(gposScripts,0);
  setlength(gposFeaturelist,0);
  gposLookUps := Tlookuptabs.create();
  gposLookUps.font := self;
end;

destructor TOTgposfont.destroy();
begin
  gposLookUps.destroy();
  inherited destroy();
end;


function create_lookup(typ,fmt : longint) : TOTlookup;
begin
  create_lookup := nil;
  case typ of
    1 : begin
          case fmt of
            1 : create_lookup := TSinglePosFormat1.create();
            2 : create_lookup := TSinglePosFormat2.create();
          end;
        end;
    2 : begin
          case fmt of
            1 : create_lookup := TPairPosFormat1.create();
            2 : create_lookup := TPairPosFormat2.create();
          end;
        end;
    3 : begin
          case fmt of
            1 : create_lookup := TCursivePosFormat1.create();
          end;
        end;
    4 : begin
          case fmt of
            1 : create_lookup := TMarkBasePosFormat1.create();
          end;
        end;
    5 : begin
          case fmt of
            1 : create_lookup := TMarkLigPosFormat1.create();
          end;
        end;
    6 : begin
          case fmt of
            1 : create_lookup := TMarkMarkPosFormat1.create();
          end;
        end;
    7 : begin
          case fmt of
            1 : create_lookup := TContextPosFormat1.create();
            2 : create_lookup := TContextPosFormat2.create();
            3 : create_lookup := TContextPosFormat3.create();
          end;
        end;
    8 : begin
          case fmt of
            1 : create_lookup := TChainContextPosFormat1.create();
            2 : create_lookup := TChainContextPosFormat2.create();
            3 : create_lookup := TChainContextPosFormat3.create();
          end;
        end;
    9 : begin
          case fmt of
            1 : create_lookup := TExtensionPosFormat1.create();
          end;
        end;
    else begin
    end;
  end;
end;





procedure TOTgposfont.load_gpos();
var i,versmin,versmaj : longint; 
  s : TMemoryStream;
  ScriptList, FeatureList, LookupList, FeatureVariations : dword;
begin
writeln('load gpos');
  i := seek_tab('GPOS');
  if (i>0) then begin
writeln('found gpos:',i);
    s := TMemoryStream.create();
    if (i=s.copyFrom(src,i)) then begin
writeln('copied ',i,' bytes to s');
      s.seek(0,soFromBeginning);
      versmaj := read_word(s);
writeln('versmaj=',versmaj);
      if (versmaj=1) then begin // version major
        versmin := read_word(s);
writeln('versmin:',versmin);
        ScriptList  := read_word(s);
        FeatureList := read_word(s);
        LookupList  := read_word(s);
writeln('sl:',ScriptList);
writeln('fl:',FeatureList);
writeln('ll:',LookupList);
        if (versmin>0) 
        then FeatureVariations := read_dword(s)
        else FeatureVariations := 0;

        if (ScriptList<>0)and(ScriptList<i) then begin
          s.seek(ScriptList,soFromBeginning);
          load_scriptlist(gposScripts,s);
        end;

        if (FeatureList<>0)and(FeatureList<i) then begin
          s.seek(FeatureList,soFromBeginning);
          load_FeatureList(gposFeaturelist,s);
        end;

        if (LookupList<>0)and(LookupList<i) then begin
          s.seek(LookupList,soFromBeginning);
          load_lookups(gposLookUps,@create_lookup,s);
        end;

        if (FeatureVariations<>0)and(FeatureVariations<i) then begin
          s.seek(FeatureVariations,soFromBeginning);
//          load_FeatureVariations(s);
        end;

      end;
      s.destroy();
    end else s.destroy();
  end;
end;


procedure init_vr(var vr : TposValueRecord);
begin
  fillbyte(vr,sizeof(vr),0);
end;

procedure init_anchor(var a : Tanchor);
begin
  fillbyte(a,sizeof(a),0);
  a.AnchorPoint := 65535;
end;

procedure init_gp(var r : Tglyphplace);
begin
  with r do begin
    init_vr(vr);
    init_anchor(selfentry);
    init_anchor(selfexit);
    init_anchor(aself);
    init_anchor(abase);
    base := 0;
  end;
end;

procedure init_adj(s : Tglystr; var adj : Tglyphplacement);
var i : longint;
begin
  setlength(adj,length(s)+1);
  for i := 0 to high(adj) do init_gp(adj[i]);
end;

procedure TOTgposfont.adjustLookup(s : Tglystr; var adj : Tglyphplacement; i : longint);
begin
  adjust_lookup(s,adj,1,length(s),i,gposlookups);
end;

procedure TOTgposfont.adjustFeature(s : Tglystr; var adj : Tglyphplacement; i : longint);
var j : longint;
begin
  if (i>=0)and(i<=high(gposFeaturelist)) then with gposFeaturelist[i] do begin
    writeln('Processing GPOS feature ',i,' : ',FeatureTag);
    for j := 0 to high(lu) do begin
      adjustLookup(s,adj,lu[j]);
    end;
  end;
end;

procedure TOTgposfont.adjustScript(s : Tglystr; var adj : Tglyphplacement; i : longint);
var j : longint;
begin
  if (i>=0)and(i<=high(gposScripts)) then with gposScripts[i] do begin
    writeln('Processing GPOS script ',i,' : ',ScriptTag);
    with DefaulTOTlangsys do begin
      for j := 0 to high(Feature) do begin
        adjustFeature(s,adj,Feature[j]);
      end;
    end;
  end;
end;

function TOTgposfont.findgposScript(nam : string) : longint;
begin
  findgposScript := findScript(nam,gposScripts);
end;





begin
end.

