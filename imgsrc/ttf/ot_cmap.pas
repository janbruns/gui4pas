unit ot_cmap;
{$mode objfpc}
{$COPERATORS ON}

{ 

}


interface
uses classes,sysutils,fontfile;

type
TOTcmapfont = class(TTfont)

  constructor create();
  function UnicodeToGlyphIndex(unicodeval : dword) : longint;

  protected

  procedure compact_unimap();

  private
  
  function load_unimap12(ofs : longint) : boolean;
  function load_unimap10(ofs : longint) : boolean;
  function load_unimap6(ofs : longint) : boolean;
  function load_unimap4(ofs : longint) : boolean;
  procedure load_cmap();


  public
  probably_not_unicode : boolean;
  cmap_loaded,
  cmap_error  : boolean;

  private
  unicodetab : array of Tunicodemapentry;
end;




implementation


constructor TOTcmapfont.create();
begin
  inherited create();
  setlength(unicodetab,0);
  cmap_loaded := false;
  cmap_error  := false;
  probably_not_unicode := false;
end;


function TOTcmapfont.UnicodeToGlyphIndex(unicodeval : dword) : longint;
var i,j : longint;
begin
  if not(cmap_loaded) then load_cmap();
  j := 0;
  for i := 0 to high(unicodetab) do with unicodetab[i] do begin
    if (startCharCode > unicodeval) then break;
    if (startCharCode <= unicodeval) then begin
      if (endCharCode >= unicodeval) then begin
        j :=  (unicodeval -startCharCode)*delta +startGlyphID;
        break;
      end;
    end;
  end;
  UnicodeToGlyphIndex := j;
end;

function TOTcmapfont.load_unimap12(ofs : longint) : boolean;
var i,n,d : longint;
begin
  load_unimap12 := false;
  src.seek(ofs,soFromBeginning);
  d := 1;
  i := read_word(src); // format
  if (i=13) then d := 0 else if not(i=12) then exit;  
  i := read_word(src); // reserved
  i := read_dword(src); // byte-len
  i := read_dword(src); // lang
  n := read_dword(src); // num groups
  setlength(unicodetab,n);
  dec(n);
  for i := 0 to n do begin
    unicodetab[i].startCharCode := read_dword(src);
    unicodetab[i].endCharCode   := read_dword(src);
    unicodetab[i].startGlyphID  := read_dword(src);
    unicodetab[i].delta := d;
  end;
  compact_unimap();
  load_unimap12 := true;
end;

function TOTcmapfont.load_unimap10(ofs : longint) : boolean;
var i,n,l,a : longint;
begin
  load_unimap10 := false;
  src.seek(ofs,soFromBeginning);
  i := read_word(src); // format
  if not(i=10) then exit;
  i := read_word(src); // reserved
  l := read_dword(src); // byte-len
  i := read_dword(src); // lang
  a := read_dword(src); // start-char
  n := read_dword(src); // num groups
  if (l<0)or(n<0)or(n>1024*1024) then exit;
  if (2*n<l) then exit;
  setlength(unicodetab,n);
  dec(n);
  for i := 0 to n do begin
    unicodetab[i].startCharCode := a+i;
    unicodetab[i].endCharCode   := a+i;
    unicodetab[i].startGlyphID  := read_word(src);
    unicodetab[i].delta := 1;
  end; 
  compact_unimap();
  load_unimap10 := true;
end;

function TOTcmapfont.load_unimap6(ofs : longint) : boolean;
var i,n,l,a : longint;
begin
  load_unimap6 := false;
  src.seek(ofs,soFromBeginning);
  i := read_word(src); // format
  if not(i=6) then exit;
  l := read_word(src); // byte-len
  i := read_word(src); // lang
  a := read_word(src); // start-char
  n := read_word(src); // num groups
  if (2*n<l) then exit;
  setlength(unicodetab,n);
  dec(n);
  for i := 0 to n do with unicodetab[i] do begin
    startCharCode := a+i;
    endCharCode   := a+i;
    startGlyphID  := read_word(src);
    delta := 1;
  end; 
  compact_unimap();
  load_unimap6 := true;
end;


function TOTcmapfont.load_unimap4(ofs : longint) : boolean;
var i,j,n,segcnt,blen,xlen : longint;  w : word;
idRangeOff,idDelta,startCode,endCode,gidx : array of word;
begin
  load_unimap4 := false;
  src.seek(ofs,soFromBeginning);
  i := read_word(src); // format
  if not(i=4) then exit;
  blen := read_word(src); // byte-len
  read_word(src); // lang
  segcnt := read_word(src) DIV 2;
  read_word(src); // searchRange
  read_word(src); // entrySelector
  read_word(src); // rangeShift
  xlen := (blen -16 -2*segcnt);
  if xlen<0 then exit;
  xlen := xlen div 2;
  setlength(startcode,segcnt);
  setlength(endcode,segcnt);
  setlength(idDelta,segcnt);
  setlength(idRangeOff,segcnt);
  setlength(idRangeOff,segcnt);
  setlength(gidx,xlen);
  for i := 0 to segcnt-1 do endcode[i] := read_word(src);
  read_word(src); // 
  for i := 0 to segcnt-1 do startCode[i] := read_word(src);
  for i := 0 to segcnt-1 do idDelta[i] := read_word(src);
  for i := 0 to segcnt-1 do idRangeOff[i] := read_word(src);

  for i := 0 to xlen-1 do gidx[i] := read_word(src);

  n := 0;
  for i := 0 to segcnt-1 do begin
    n := n + (endcode[i]-startCode[i])+1;
  end;
  setlength(unicodetab,n);
  n := 0;

  for i := 0 to segcnt-1 do begin
//    writeln(i,' : ',startCode[i],'..',endcode[i],' d:',idDelta[i],' o:',idRangeOff[i]);
    for j := 0 to (endcode[i]-startCode[i]) do begin
      if idRangeOff[i]=0 then begin
        w := j+startCode[i] +idDelta[i];
      end else begin
        w := (idRangeOff[i] DIV 2) +i -segcnt +j;
        if (w<xlen) then begin
          if not(w=0) then w := gidx[w] +idDelta[i];
        end;
      end;
      if w>=numGlyphs then w := 0;
      with unicodetab[n] do begin
        startCharCode := j+startCode[i];
        endCharCode   := j+startCode[i];
        startGlyphID  := w;
        delta := 1;
      end;
      inc(n);
    end;
  end;
  setlength(unicodetab,n);
  compact_unimap();
  load_unimap4 := true;
end;




procedure TOTcmapfont.compact_unimap();
var i,j,nxt : longint; b : boolean;
begin
  j := 0;
  for i := 1 to high(unicodetab) do begin
    with unicodetab[j] do begin
      { if block j has only one elem, it's delta can be adjusted }
      if (startCharCode=endCharCode) then begin
        delta := unicodetab[i].startGlyphID -startGlyphID;
      end;
      nxt := (endCharCode -startCharCode +1)*delta;
    end;
    with unicodetab[i] do begin
      { blocks i and j might get combined, if j's delta 
        leads to i's startglyph }
      b := (nxt = startGlyphID );
      if not(startCharCode=endCharCode) then begin
        { the block starting at i has more than one elem,
          so it needs the same delta as the j-block to be comined  }
        if not(delta=unicodetab[j].delta) then b := false;
      end;
      { but we don't want to define any new charcodes...}
      if not(startCharCode=unicodetab[j].endCharCode+1) then b := false;
      if b then begin
        unicodetab[j].endCharCode := endCharCode;
      end else begin
        inc(j);
        unicodetab[j] := unicodetab[i];
      end;
    end;
  end;
  setlength(unicodetab,j+1);
end;



type
Ttab = packed record
  platform,enc : smallint;
  ofs : dword;
  fmt : smallint;
end;

procedure TOTcmapfont.load_cmap();
var i : longint; {vers,}tabs,p0 : dword; arr : array of Ttab;
begin
  cmap_loaded := true;
  cmap_error  := false;
  if ( seek_tab('cmap') <0 ) then throwerr('cannot find cmap-table in file');
  p0 := src.position;
  {vers :=} read_word(src);
  tabs := 0;
  tabs := read_word(src);
  if tabs<1000 then begin
    setlength(arr,tabs);
    for i := 0 to tabs-1 do with arr[i] do begin
      platform := read_word(src);
      enc := read_word(src);
      ofs := read_dword(src);
    end;
    for i := 0 to tabs-1 do with arr[i] do begin
      src.seek(p0+ofs,soFromBeginning);
      fmt := read_word(src);
    end;
    probably_not_unicode := false;
    { first check for unicode-platform id 0
      and assume all charcodes are unicodes there  }
    for i := 0 to tabs-1 do with arr[i] do begin
      if (platform=0) then begin
        if ((fmt=12)or(fmt=13)) then begin
          if load_unimap12(p0+ofs) then exit;
        end;
      end;
    end;
    { Well, maybe try MS platform also }
    for i := 0 to tabs-1 do with arr[i] do begin
      if (platform=3) then begin
        if ((enc=1)or(enc=10)) then begin
          if ((fmt=12)or(fmt=13)) then begin
            if load_unimap12(p0+ofs) then exit;
          end;
        end;
      end;
    end;
    { unicode, format 10?}
    for i := 0 to tabs-1 do with arr[i] do begin
      if (platform=0)or((platform=3)and((enc=1)or(enc=10))) then begin
        if (fmt=10) then begin
          if load_unimap10(p0+ofs) then exit;
        end;
      end;
    end;
    { unicode, format 6?}
    for i := 0 to tabs-1 do with arr[i] do begin
      if (platform=0)or((platform=3)and((enc=1)or(enc=10))) then begin
        if (fmt=6) then begin
          if load_unimap10(p0+ofs) then exit;
        end;
      end;
    end;
    probably_not_unicode := true;
    for i := 0 to tabs-1 do with arr[i] do begin
      if ((fmt=12)or(fmt=13)) then begin
        if load_unimap12(p0+ofs) then exit;
      end;
    end;
    for i := 0 to tabs-1 do with arr[i] do begin
      if (fmt=10) then begin
        if load_unimap10(p0+ofs) then exit;
      end;
    end;
    for i := 0 to tabs-1 do with arr[i] do begin
      if (fmt=6) then begin
        if load_unimap6(p0+ofs) then exit;
      end;
    end;
    for i := 0 to tabs-1 do with arr[i] do begin
      if (fmt=4) then begin
        if load_unimap4(p0+ofs) then exit;
      end;
    end;
  end;
  cmap_error := true;
end;


begin
end.

