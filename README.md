# gui4pas


Status: Very work-in-progress

An attempt to get interoperability with many windowing-systems or bare display drivers plus basic gui like windows into a single unit.

So that

a) a compiled program could have a run on X11, and the neext time it could be using wayland.

b) keep control over most aspects of windowing so that switching to a "just a canvas" is possible, but normally keep basic hw-acceleration by using the system's windowing driver

c) allow for "combined" drivers in a way, that GPU processing can be used in combi with a winsys driver

d) allow for pure 3D-mode backends (GUIs for fullscreen games)

e) do no hardlinks to any external lib (except libc), everything in pascal, except for the dynlinked basic "get me some drawing area" functionality

More detailed current status:

There is a test app "testdrive" (compiled using "fpc -B testrive", -B is required except for first compile, because of the directoy search paths) which displays some testing content on ms-windows (32+64 bit), linux+wayland, linux+x11.

Currently working on: RichText Renderer.


The media subdir contains the font "DejaVu", because it is known to work exceptionally well with the ttf renderer's gridfit "VM".

