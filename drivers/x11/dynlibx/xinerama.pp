{$unitpath .}
unit xinerama;
interface
uses
 ctypes, dynxlib;
{ Converted from X11/Xinerama.h }

{$PACKRECORDS C}

const
libXinerama='libXinerama.so';

function libXineramaAvailable() : boolean;
function libXineramaLibHandle() : TLibHandle;

type

   PXineramaScreenInfo = ^TXineramaScreenInfo;
   TXineramaScreenInfo = record
        screen_number : cint;
        x_org         : cshort;
        y_org         : cshort;
        width         : cshort;
        height        : cshort;
     end;

T_XineramaQueryExtension=function(dpy:PDisplay; event_base:Pcint; error_base:Pcint):TBoolResult;cdecl;
T_XineramaQueryVersion=function(dpy:PDisplay; major:Pcint; minor:Pcint):TStatus;cdecl;
T_XineramaIsActive=function(dpy:PDisplay):TBoolResult;cdecl;
T_XineramaQueryScreens=function(dpy:PDisplay; number:Pcint):PXineramaScreenInfo;cdecl;

var
XineramaQueryExtension : T_XineramaQueryExtension;
XineramaQueryVersion : T_XineramaQueryVersion;
XineramaIsActive : T_XineramaIsActive;
XineramaQueryScreens : T_XineramaQueryScreens;



implementation

procedure init_procvars();
begin
  pointer(XineramaQueryExtension) := nil;
  pointer(XineramaQueryVersion) := nil;
  pointer(XineramaIsActive) := nil;
  pointer(XineramaQueryScreens) := nil;
end;

procedure load_procvars(libh : TLibHandle);
begin
  pointer(XineramaQueryExtension) := GetProcedureAddress(libh,'XineramaQueryExtension');
  pointer(XineramaQueryVersion) := GetProcedureAddress(libh,'XineramaQueryVersion');
  pointer(XineramaIsActive) := GetProcedureAddress(libh,'XineramaIsActive');
  pointer(XineramaQueryScreens) := GetProcedureAddress(libh,'XineramaQueryScreens');
end;


var 
lh : TLibHandle;

function libXineramaAvailable() : boolean;
begin
  libXineramaAvailable := (lh<>NilHandle);
end;

function libXineramaLibHandle() : TLibHandle;
begin
  libXineramaLibHandle := lh;
end;

procedure do_init();
begin
  init_procvars();
  if XlibAvailable() and (lh<>NilHandle) then begin
    load_procvars(lh);
  end;
end;


initialization
begin
  lh := LoadLibrary(libXinerama);
  do_init();
end;

finalization
begin
  if lh<>NilHandle then UnloadLibrary(lh);
end;


end.

