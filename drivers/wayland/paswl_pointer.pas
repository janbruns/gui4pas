{$mode objfpc}
{$unitpath .}
unit paswl_pointer;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_bindable;


type

Tpaswl_pointer=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  procedure set_cursor(serial : dword; surface : Tpaswl_bindable; hotspot_x, hotspot_y : longint);


  protected
  procedure cb_pointer_enter(serial : dword; surface : Tpaswl_bindable; surface_x, surface_y : Twlfixed); virtual;
  procedure cb_pointer_leave(serial : dword; surface : Tpaswl_bindable); virtual;
  procedure cb_pointer_motion(time : dword; surface_x, surface_y : Twlfixed); virtual;
  procedure cb_pointer_button(serial, time, button, state : dword); virtual;
  procedure cb_pointer_axis(time, axis : dword; value : Twlfixed); virtual;
  procedure cb_pointer_frame(); virtual;
  procedure cb_pointer_axis_source(axis_source : dword); virtual;
  procedure cb_pointer_axis_stop(time, axis : dword); virtual;
  procedure cb_pointer_axis_discrete(axis : dword; discrete : longint); virtual;
  procedure cb_pointer_axis_value120(axis : dword; value120 : longint); virtual;
  procedure cb_pointer_axis_relative_direction(axis, direction : dword); virtual;

  public
  seat : Tpaswl_bindable;
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;

Tpaswl_pointer_cls = class of Tpaswl_pointer;







implementation
uses strings,math;




procedure pointer_enter(dat:pointer; o:wl_pointer; serial : dword; surface : wl_surface; surface_x, surface_y : Twlfixed); cdecl;
var d : Tpaswl_pointer;
begin
  if dat<>nil then begin
    d := Tpaswl_pointer(dat);
    if d.wlo=o then d.cb_pointer_enter(serial, Tpaswl_bindable(wl_proxy_get_user_data(surface)), surface_x, surface_y)
    else ;//
  end;
end;

procedure pointer_leave(dat:pointer; o:wl_pointer; serial : dword; surface : wl_surface); cdecl;
var d : Tpaswl_pointer;
begin
  if dat<>nil then begin
    d := Tpaswl_pointer(dat);
    if d.wlo=o then d.cb_pointer_leave(serial, Tpaswl_bindable(wl_proxy_get_user_data(surface)))
    else ;//
  end;
end;

procedure pointer_motion(dat:pointer; o:wl_pointer; time : dword; surface_x, surface_y : Twlfixed); cdecl;
var d : Tpaswl_pointer;
begin
  if dat<>nil then begin
    d := Tpaswl_pointer(dat);
    if d.wlo=o then d.cb_pointer_motion(time, surface_x, surface_y)
    else ;//
  end;
end;

procedure pointer_button(dat:pointer; o:wl_pointer; serial, time, button, state : dword); cdecl;
var d : Tpaswl_pointer;
begin
  if dat<>nil then begin
    d := Tpaswl_pointer(dat);
    if d.wlo=o then d.cb_pointer_button(serial, time, button, state)
    else ;//
  end;
end;

procedure pointer_axis(dat:pointer; o:wl_pointer; time, axis : dword; value : Twlfixed); cdecl;
var d : Tpaswl_pointer;
begin
  if dat<>nil then begin
    d := Tpaswl_pointer(dat);
    if d.wlo=o then d.cb_pointer_axis(time, axis, value)
    else ;//
  end;
end;

procedure pointer_frame(dat:pointer; o:wl_pointer); cdecl;
var d : Tpaswl_pointer;
begin
  if dat<>nil then begin
    d := Tpaswl_pointer(dat);
    if d.wlo=o then d.cb_pointer_frame()
    else ;//
  end;
end;

procedure pointer_axis_source(dat:pointer; o:wl_pointer; axis_source : dword); cdecl;
var d : Tpaswl_pointer;
begin
  if dat<>nil then begin
    d := Tpaswl_pointer(dat);
    if d.wlo=o then d.cb_pointer_axis_source(axis_source)
    else ;//
  end;
end;

procedure pointer_axis_stop(dat:pointer; o:wl_pointer; time, axis : dword); cdecl;
var d : Tpaswl_pointer;
begin
  if dat<>nil then begin
    d := Tpaswl_pointer(dat);
    if d.wlo=o then d.cb_pointer_axis_stop(time, axis)
    else ;//
  end;
end;

procedure pointer_axis_discrete(dat:pointer; o:wl_pointer; axis : dword; discrete : longint); cdecl;
var d : Tpaswl_pointer;
begin
  if dat<>nil then begin
    d := Tpaswl_pointer(dat);
    if d.wlo=o then d.cb_pointer_axis_discrete(axis, discrete)
    else ;//
  end;
end;

procedure pointer_axis_value120(dat:pointer; o:wl_pointer; axis : dword; value120 : longint); cdecl;
var d : Tpaswl_pointer;
begin
  if dat<>nil then begin
    d := Tpaswl_pointer(dat);
    if d.wlo=o then d.cb_pointer_axis_value120(axis, value120)
    else ;//
  end;
end;

procedure pointer_axis_relative_direction(dat:pointer; o:wl_pointer; axis, direction : dword); cdecl;
var d : Tpaswl_pointer;
begin
  if dat<>nil then begin
    d := Tpaswl_pointer(dat);
    if d.wlo=o then d.cb_pointer_axis_relative_direction(axis, direction)
    else ;//
  end;
end;

var
imp_if_pointer_listener : wl_pointer_listener = (
  enter : @pointer_enter;
  leave : @pointer_leave;
  motion : @pointer_motion;
  button : @pointer_button;
  axis : @pointer_axis;
  frame : @pointer_frame;
  axis_source : @pointer_axis_source;
  axis_stop : @pointer_axis_stop;
  axis_discrete : @pointer_axis_discrete;
  axis_value120 : @pointer_axis_value120;
  axis_relative_direction : @pointer_axis_relative_direction;
);



procedure Tpaswl_pointer.cb_pointer_enter(serial : dword; surface : Tpaswl_bindable; surface_x, surface_y : Twlfixed);
begin
end;

procedure Tpaswl_pointer.cb_pointer_leave(serial : dword; surface : Tpaswl_bindable);
begin
end;

procedure Tpaswl_pointer.cb_pointer_motion(time : dword; surface_x, surface_y : Twlfixed);
begin
end;

procedure Tpaswl_pointer.cb_pointer_button(serial, time, button, state : dword);
begin
end;

procedure Tpaswl_pointer.cb_pointer_axis(time, axis : dword; value : Twlfixed);
begin
end;

procedure Tpaswl_pointer.cb_pointer_frame();
begin
end;

procedure Tpaswl_pointer.cb_pointer_axis_source(axis_source : dword);
begin
end;

procedure Tpaswl_pointer.cb_pointer_axis_stop(time, axis : dword);
begin
end;

procedure Tpaswl_pointer.cb_pointer_axis_discrete(axis : dword; discrete : longint);
begin
end;

procedure Tpaswl_pointer.cb_pointer_axis_value120(axis : dword; value120 : longint);
begin
end;

procedure Tpaswl_pointer.cb_pointer_axis_relative_direction(axis, direction : dword);
begin
end;





constructor Tpaswl_pointer.create();
begin
  inherited create();
end;

destructor Tpaswl_pointer.destroy();
begin
  wl_pointer_release(wl_pointer(wlo));
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_pointer.onBind();
begin
  inherited onBind();
  wl_pointer_add_listener(wl_pointer(wlo),imp_if_pointer_listener,self);
end;

procedure Tpaswl_pointer.onUnbind();
begin
  inherited onUnbind();
end;



procedure Tpaswl_pointer.set_cursor(serial : dword; surface : Tpaswl_bindable; hotspot_x, hotspot_y : longint);
begin
  wl_pointer_set_cursor(wl_pointer(wlo), serial, wl_surface(surface.wlo), hotspot_x, hotspot_y);
end;




class function Tpaswl_pointer.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_pointer_interface;
end;

class function Tpaswl_pointer.wl_ifver() : dword;
begin
  //writeln('Tpaswl_pointer.wl_ifver: ',wl_pointer_interface^.version);
  wl_ifver := 9;
end;


initialization
begin
end;

finalization
begin
end;

end.



