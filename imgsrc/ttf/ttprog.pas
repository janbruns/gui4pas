{$mode objfpc}
unit ttprog;

{ code to manage sub-function calls/definitions
  and the actual instruction code hierachy


  ep = entry point
  id = "name" of subfuntion
  

  This design allows glyph-progs to redefine
  functions in a way that would make font/cvt progs
  calling that name see the glyph-defined version.

  So it's almost compatible with a simple concat of
  font,cvt and glyph program. 

  Except for cases like

    fpgm executes
    cvt executes
    glyph executes
      glyph (re)defines func name X
      glyph calls some other fpgm/cvt-subroutine
        fpgm subroutine redfines func name X
        fpgm subroutine calls func name X // could still go to 
                                          // glyph-defined X. but 
                                          // func-defs simply go
                                          // to the calling env
  
  Allowing glyphs to override some font-subprog functionality for the 
  duration of glyph-code execution might be useful, just like virtual
  methods in object oriented programming.

  Note that per spec, func-defs are currently disallowed in glyph
  code, so that font-designers interested in using this feature
  for now should consider to add all the virtual method candidates
  into the font-code, and use some storage->call mechanism to work 
  around.

}

interface
uses sysutils;


type
Tsubfunc = packed record
  id,ep : dword;
end;

TTsubfunclist = class
  constructor create();

  protected
  function fl_lookup_function(id : dword; var ep : dword) : boolean;
  procedure fl_define_function(id : dword; ep : dword; ow : boolean);

  private
  sf : array of Tsubfunc;
end;


{ Function calls will often (for the glyphs) go to
  bodies defined in parent programs (like the Font-Program).
}

TTprogram = class(TTsubfunclist)
  constructor create(p : TTprogram);
  constructor create(p : TTprogram; instruct : array of byte);
  destructor destroy(); override;
 

  { instruction-defs, very similar to function defs }
  function lookup_idef(b : byte; var prg : TTprogram; var ep : dword) : boolean; 
  procedure definstr(b : byte; ep : dword);  

  function lookup_function(id : dword; var prg : TTprogram; var ep : dword) : boolean;
  procedure define_function(id : dword; ep : dword; ow : boolean);

  public
  parent : TTprogram;

  protected  
  idefs : TTsubfunclist; // normally nil

  public
  instructions : array of byte;
end;


Tttprogexception = Class(Exception);



implementation


{ worst case minimal TTsubfunclist implementation: 
  linear runtime for all operations  
}

function TTsubfunclist.fl_lookup_function(id : dword; var ep : dword) : boolean;
var i : longint;
begin
  for i := high(sf) downto 0 do begin
    if (sf[i].id=id) then begin
      ep := sf[i].ep;
      fl_lookup_function := true;
      exit;
    end;
  end;
  fl_lookup_function := false;
end;

procedure TTsubfunclist.fl_define_function(id : dword; ep : dword; ow : boolean);
var i : longint;
begin
  for i := high(sf) downto 0 do begin
    if (sf[i].id=id) then begin
      if ow then begin
        sf[i].ep := ep;
        exit;
      end else begin
        exit;
      end;
    end;
  end;
  setlength(sf,high(sf)+2);
  sf[high(sf)].ep := ep;
  sf[high(sf)].id := id;
end;

constructor TTsubfunclist.create();
begin
  setlength(sf,0);
end;














function TTprogram.lookup_idef(b : byte; var prg : TTprogram; var ep : dword) : boolean; 
begin
  if not(idefs=nil) then begin
    if idefs.fl_lookup_function(b,ep)  then begin
      prg := self;
      lookup_idef := true;
      exit;
    end;
  end;
  if not(parent=nil) then lookup_idef := parent.lookup_idef(b,prg,ep)
  else lookup_idef := false;
end;

procedure TTprogram.definstr(b : byte; ep : dword); 
begin
  if (idefs=nil) then idefs := TTsubfunclist.create();
  idefs.fl_define_function(b,ep,true);
end;

function TTprogram.lookup_function(id : dword; var prg : TTprogram; var ep : dword) : boolean;
begin
  if fl_lookup_function(id,ep) then begin
    prg := self;
    lookup_function := true;
  end else begin
    if not(parent=nil) then lookup_function := parent.lookup_function(id,prg,ep)
    else lookup_function := false;
  end;
end;

procedure TTprogram.define_function(id : dword; ep : dword; ow : boolean);
begin
  fl_define_function(id,ep,ow);
end;


constructor TTprogram.create(p : TTprogram);
begin
  inherited create();
  parent := p;
  idefs := nil;
  setlength(instructions,0);
end;

constructor TTprogram.create(p : TTprogram; instruct : array of byte);
var i : longint;
begin
  create(p);
  i := high(instruct)+1;
  setlength(instructions,i);
  move(instruct[0],instructions[0],i);
end;


destructor TTprogram.destroy();
begin
  if not(idefs=nil) then freeandnil(idefs);
  inherited destroy();
end;


begin
end.




