{$mode objfpc}
{$unitpath .}
unit paswl_output;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_bindable,paswl_collection;


type

Tpaswl_output=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  function onRegistryRemove() : boolean; override; // true: "destroyme"


  protected
  procedure cb_output_geometry(x, y, physical_width, physical_height, subpixel : longint; make, model : Pchar; transform : longint); virtual;
  procedure cb_output_mode(flags : dword; width, height, refresh : longint); virtual;
  procedure cb_output_done(); virtual;
  procedure cb_output_scale(factor : longint); virtual;
  procedure cb_output_name(name : Pchar); virtual;
  procedure cb_output_description(description : Pchar); virtual;
    
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;


Tpaswl_output_cls = class of Tpaswl_output;






implementation
uses strings,math;

procedure output_geometry(dat:pointer; o:wl_output; x, y, physical_width, physical_height, subpixel : longint; make, model : Pchar; transform : longint); cdecl;
var d : Tpaswl_output;
begin
  if dat<>nil then begin
    d := Tpaswl_output(dat);
    if d.wlo=o then d.cb_output_geometry(x,y,physical_width, physical_height, subpixel, make, model, transform)
    else ;//
  end;
end;

procedure output_mode(dat:pointer; o:wl_output; flags : dword; width, height, refresh : longint); cdecl;
var d : Tpaswl_output;
begin
  if dat<>nil then begin
    d := Tpaswl_output(dat);
    if d.wlo=o then d.cb_output_mode(flags, width, height, refresh)
    else ;//
  end;
end;

procedure output_done(dat:pointer; o:wl_output); cdecl;
var d : Tpaswl_output;
begin
  if dat<>nil then begin
    d := Tpaswl_output(dat);
    if d.wlo=o then d.cb_output_done()
    else ;//
  end;
end;


procedure output_scale(dat:pointer; o:wl_output; factor : longint); cdecl;
var d : Tpaswl_output;
begin
  if dat<>nil then begin
    d := Tpaswl_output(dat);
    if d.wlo=o then d.cb_output_scale(factor)
    else ;//
  end;
end;

procedure output_name(dat:pointer; o:wl_output; name : Pchar); cdecl;
var d : Tpaswl_output;
begin
  if dat<>nil then begin
    d := Tpaswl_output(dat);
    if d.wlo=o then d.cb_output_name(name)
    else ;//
  end;
end;

procedure output_description(dat:pointer; o:wl_output; description : Pchar); cdecl;
var d : Tpaswl_output;
begin
  if dat<>nil then begin
    d := Tpaswl_output(dat);
    if d.wlo=o then d.cb_output_description(description)
    else ;//
  end;
end;


var
imp_if_output_listener : wl_output_listener = (
  geometry : @output_geometry;
  mode : @output_mode;
  done : @output_done;
  scale : @output_scale;
  name : @output_name;
  description : @output_description;
);







procedure Tpaswl_output.cb_output_geometry(x, y, physical_width, physical_height, subpixel : longint; make, model : Pchar; transform : longint);
begin
end;

procedure Tpaswl_output.cb_output_mode(flags : dword; width, height, refresh : longint);
begin
end;

procedure Tpaswl_output.cb_output_done();
begin
end;

procedure Tpaswl_output.cb_output_scale(factor : longint);
begin
end;

procedure Tpaswl_output.cb_output_name(name : Pchar);
begin
end;

procedure Tpaswl_output.cb_output_description(description : Pchar);
begin
end;


function Tpaswl_output.onRegistryRemove() : boolean;
begin
  collection.onOutputRemove(self);
  onRegistryRemove := false;
end;


constructor Tpaswl_output.create();
begin
  inherited create();
end;

destructor Tpaswl_output.destroy();
begin
  wl_output_release(wlo);
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_output.onBind();
begin
  inherited;
  wl_output_add_listener(wl_output(wlo),imp_if_output_listener,self);
end;

procedure Tpaswl_output.onUnbind();
begin
  inherited;
end;

class function Tpaswl_output.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_output_interface;
end;

class function Tpaswl_output.wl_ifver() : dword;
begin
  wl_ifver := 4;
end;


initialization
begin
end;

finalization
begin
end;

end.



