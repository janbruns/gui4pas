{$mode objfpc}
{$COPERATORS ON}

uses ot_gsub,ot_comn;


const
teststr = 'Dieser Probetext dient zum Test verschiedenartigster Abwandlungs-Modifikatoren. Test. Triest. Dienst. D####ies Di####es Die####s D#i#e#s ent'; 


type
Tsomelutyp = class(Tlookuptabs)
  constructor create();
  function isIgnGly(g : Tglychar) : boolean; override;
  public
  ignchars : Tcoverage;
end;


constructor Tsomelutyp.create();
begin
  inherited create();
  ignchars := '';
end;

function Tsomelutyp.isIgnGly(g : Tglychar) : boolean;
begin
  isIgnGly := (pos(g,ignchars)>0);
end;


var
tabbis : Tsomelutyp;

begin
  tabbis := Tsomelutyp.create();
  tabbis.deleted_lig_part_marker := '|';
with tabbis do begin  
  setlength(tabs,5);

  setlength(tabs[0].sublu,1);
  tabs[0].sublu[0] := TSingleSubstFormat2.create();
  with TSingleSubstFormat2(tabs[0].sublu[0]) do begin
    coverage   := 'ed';
    Substitute := 'E_';
  end;

  writeln(
    apply_lookup(teststr,0,tabbis )
  );

  setlength(tabs[1].sublu,1);
  tabs[1].sublu[0] := TMultipleSubstFormat1.create();
  with TMultipleSubstFormat1(tabs[1].sublu[0]) do begin
    coverage   := 'nrx';
    setlength(Substitutes,3);
    Substitutes[0] := 'NNN';
    Substitutes[1] := 'RRR';
    Substitutes[2] := 'XXX';
  end;

  writeln(
    apply_lookup(teststr,1,tabbis )
  );


  setlength(tabs[2].sublu,1);
  tabs[2].sublu[0] := TLigatureSubstFormat1.create();
  with TLigatureSubstFormat1(tabs[2].sublu[0]) do begin
    coverage   := 'i';
    setlength(LigatureSets,1);
    setlength(LigatureSets[0],1);
    with LigatureSets[0][0] do begin
      LigGlyph := '_';
      Component := 'e';
    end;
  end;

  writeln(
    apply_lookup(teststr,2,tabbis )
  );


  setlength(tabs[3].sublu,2);
  tabs[3].sublu[0] := TChainContextSubstFormat3.create();
  with TChainContextSubstFormat3(tabs[3].sublu[0]) do begin
    setlength(cbt,1);
    setlength(cin,2);
    setlength(cla,1);
    cbt[0] := 'D';
    cin[0] := 'i';
    cin[1] := 'e';
    cla[0] := 's';
    setlength(sa,1);
    sa[0].SequenceIndex := 0;
    sa[0].LookupListIndex := 2;
  end;
  tabs[3].sublu[1] := TChainContextSubstFormat3.create();
  with TChainContextSubstFormat3(tabs[3].sublu[1]) do begin
    setlength(cbt,1);
    setlength(cin,1);
    setlength(cla,1);
    cbt[0] := 'e';
    cin[0] := 'n';
    cla[0] := 't';
    setlength(sa,1);
    sa[0].SequenceIndex := 0;
    sa[0].LookupListIndex := 1;
  end;

  tabbis.ignchars := '#';

  writeln(
    apply_lookup(teststr,3,tabbis )
  );

  setlength(tabs[4].sublu,1);
  tabs[4].sublu[0] := TChainContextSubstFormat1.create();
  with TChainContextSubstFormat1(tabs[4].sublu[0]) do begin
    coverage := 'i';
    setlength(csrs,1);
    setlength(csrs[0],1);
    with csrs[0][0] do begin
      setlength(sa,1);
      sa[0].SequenceIndex := 0;
      sa[0].LookupListIndex := 2;
      btrk := 'D'; 
      inpt := 'e'; 
      lahd := 's';
    end;
  end;

  writeln(
    apply_lookup(teststr,4,tabbis )
  );

end;
  tabbis.destroy();
end.