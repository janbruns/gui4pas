UNIT ttcalc;
{$mode objfpc}
{$COPERATORS ON}

{ About TTF instruction decoding.
}


interface
uses sysutils,ttstack,ttprog;


type
F26DOT6 = longint;
F2DOT14 = smallint; // smallint = int16
Trndstate = record
  premul, postmul : F26DOT6;
  peri,phase,thresh : longint;
end;
Tgraphicstate = record
  rnd : Trndstate;
  zp0,zp1,zp2 : longint;
  rp0,rp1,rp2 : longint;
  fvx,fvy, // fv
  pvx,pvy, // pv
  dvx,dvy : F2DOT14;
  loopcount : dword;
  single_width_value : smallint;
  delta_base,
  delta_shift : dword;
  single_width_cut_in : F26DOT6;
  control_value_cut_in : F26DOT6;
  minimum_distance : F26DOT6; 
  auto_flip : boolean;
  scanctrl : boolean;
  scantype : longint;

  { additional temps used internally }
  dotfp :  F26DOT6; // scalar product fv.pv
end;
Tloopcall = record
  prg  : TTprogram; // called program
  cprg : TTprogram; // calling program
  cnt : longint;
  ep  : dword; // call enty point 
  ip  : dword; // caller-side next instruction
end;

TTexecenv = class(TTexecStack)
  function do_exec(prog : TTprogram) : boolean; virtual;

  procedure set_max_ifnesting(n : longint);
  procedure set_max_loopcallnesting(n : longint);

  function maxdw(a,b : dword) : dword;
  function mindw(a,b : dword) : dword;

  function dorounding(x : longint) : longint;
  procedure do_call(p : TTprogram; ep : dword);
  procedure maybe_loopcall();
  procedure do_ret();
  function do_if_handling(b : byte) : boolean;
  procedure do_simple_setget(b : byte);
  procedure do_simple_setget2(b : byte);
  procedure do_stack_op(b : byte);
  procedure do_compare(b : byte);
  procedure do_simple_calc(b : byte);
  procedure do_push_from_code(b : byte);
  procedure do_rel_jump(b : byte);
  procedure do_other_instructions(b : byte);
  procedure set_super_rounding(b : byte); 
  procedure do_round_instr(b : byte);
  function do_instruction() : boolean;
  function decode_instruction_length() : boolean;
  function exec_instruction() : boolean;
  constructor create();

  procedure reset_gs();

  procedure onInstructionExec(); virtual;


  function apply_engine_compensation(x,ctyp : longint) : longint; virtual; abstract;
  function get_zonepoint_x(ptidx,zone : longint) : longint; virtual; abstract;
  function get_zonepoint_y(ptidx,zone : longint) : longint; virtual; abstract;
  procedure do_MDRP_instr(b : byte); virtual; abstract;
  procedure do_MIRP_instr(b : byte); virtual; abstract;
  procedure do_delta_instr(b : byte); virtual; abstract;
  procedure set_onoffcurve(b : byte); virtual; abstract;
  procedure do_readwrite_store(b : byte); virtual; abstract;

  procedure do_ALPIGNPTS(b : byte); virtual; abstract;
  procedure do_UTP(b : byte); virtual; abstract;
  procedure do_MDAP(b : byte); virtual; abstract;
  procedure do_IUP(b : byte); virtual; abstract;
  procedure do_SHP(b : byte); virtual; abstract;
  procedure do_SHC(b : byte); virtual; abstract;
  procedure do_SHZ(b : byte); virtual; abstract;
  procedure do_SHPIX(b : byte); virtual; abstract;
  procedure do_IP(b : byte); virtual; abstract;
  procedure do_MSIRP(b : byte); virtual; abstract;
  procedure do_ALIGNRP(b : byte); virtual; abstract;
  procedure do_MIAP(b : byte); virtual; abstract;
  procedure do_WCVTP(b : byte); virtual; abstract;
  procedure do_RCVT(b : byte); virtual; abstract;
  procedure do_GC(b : byte); virtual; abstract;
  procedure do_SCFS(b : byte); virtual; abstract;
  procedure do_MD(b : byte); virtual; abstract;
  procedure do_SCANCTRL(b : byte); virtual; abstract;
  procedure do_SDPVTL(b : byte); virtual; abstract;
  procedure do_GETINFO(b : byte); virtual; abstract;
  procedure do_SCANTYPE(b : byte); virtual; abstract;
  procedure do_MP_S_PPEM(b : byte); virtual; abstract;
  procedure do_INSTCTRL(b : byte); virtual; abstract;
  procedure do_ISECT(b : byte); virtual; abstract;


  protected
  ip   : dword; // instruction pointer
  ilen : dword; // length of current instruction (normally 1)
  prg  : TTprogram; // the program, the IP refers to
  oprg : TTprogram; // the program, the overall computation belongs to
  gs : Tgraphicstate;

  protected
  is_snormal_instruction : array[0..255] of boolean;
  nary_calc_instruction : array[0..255] of byte;
  instruction_length : array[0..255] of byte;
  loopcall : array of Tloopcall;
  loopcalls : longint;
  iflvl : array of byte;
  iflevel : longint;
  ifskip : boolean;
  defskip : longint; // true while inside function-definition
  reached_end_of_program : boolean;

  public
  use_signed_compare : boolean;


end;


function F2DOT14_MUL_F2DOT14_F2DOT14(a,b : F2DOT14) : F2DOT14;
function MUL_F2DOT14_F2DOT14(a,b : F2DOT14) : F26DOT6;
function MUL_F26DOT6_F2DOT14(a : F26DOT6; b : F2DOT14) : F26DOT6;
function MULF26DOT6(a,b : F26DOT6) : F26DOT6;
function DIVF26DOT6(a,b : F26DOT6) : F26DOT6;
function DIVF26DOT6_F2DOT14(a : F26DOT6; b : F2DOT14) : F26DOT6;
function doround(a : F26DOT6; const rs : Trndstate) : F26DOT6;

procedure F2DOT14_normalize_F26DOT6(var px,py : longint);


implementation

type
Troundmode = (rs_RTG, rs_RTHG, rs_RTDG, rs_ROFF, rs_RUTG, rs_RDTG);



function F2DOT14_MUL_F2DOT14_F2DOT14(a,b : F2DOT14) : F2DOT14;
begin
  F2DOT14_MUL_F2DOT14_F2DOT14 := sarint64(int64(a)*int64(b),14);;
end;

function MUL_F2DOT14_F2DOT14(a,b : F2DOT14) : F26DOT6;
begin
  MUL_F2DOT14_F2DOT14 := sarint64(int64(a)*int64(b),14+14-6);;
end;

function MUL_F26DOT6_F2DOT14(a : F26DOT6; b : F2DOT14) : F26DOT6;
begin
  MUL_F26DOT6_F2DOT14 := sarint64(int64(a)*int64(b),14);;
end;

function MULF26DOT6(a,b : F26DOT6) : F26DOT6;
begin
  MULF26DOT6 := sarint64(int64(a)*int64(b),6);
end;

function DIVF26DOT6(a,b : F26DOT6) : F26DOT6;
begin
  if (b<>0) then DIVF26DOT6 := (int64(a) shl 6) div b
  else begin
    if (a<0) then a := -$7fffffff
             else a :=  $7fffffff;
    DIVF26DOT6 := a;  
  end;
end;

function DIVF26DOT6_F2DOT14(a : F26DOT6; b : F2DOT14) : F26DOT6;
begin
  if (b<>0) then DIVF26DOT6_F2DOT14 := (int64(a) shl 14) div b
  else begin
    if (a<0) then a := -$7fffffff
             else a :=  $7fffffff;
    DIVF26DOT6_F2DOT14 := a;  
  end;
end;


procedure F2DOT14_normalize_F26DOT6(var px,py : longint);
var l : extended;
begin
  l := sqrt(px*px+py*py);
  if (l>0) then begin
    px := round(16384*px/l);
    py := round(16384*py/l);
  end;
end;





procedure set_rounding(var rs : Trndstate; rm : Troundmode);
begin
  rs.premul := 64;
  rs.postmul := 64;
  case rm of
    rs_RTG : begin rs.peri := 64; rs.phase := 0; rs.thresh := 32;
             end;
    rs_RTHG: begin rs.peri := 64; rs.phase :=32; rs.thresh := 32;
             end;
    rs_RTDG: begin rs.peri := 32; rs.phase := 0; rs.thresh := 16;
             end;
    rs_RUTG: begin rs.peri := 64; rs.phase := 0; rs.thresh := 63;
             end;
    rs_RDTG: begin rs.peri := 64; rs.phase := 0; rs.thresh :=  0;
             end;
    rs_ROFF: begin rs.peri :=  1; rs.phase := 0; rs.thresh :=  0;
             end;
  end;
end;

function doround(a : F26DOT6; const rs : Trndstate) : F26DOT6;
var b : F26DOT6;
begin
  b := a;
  b := MULF26DOT6(a,rs.premul);
  b := b-rs.phase;
  b := b+rs.thresh;
  b := b xor (b and(rs.peri-1));
  b := b+rs.phase;
  { don't accept sign-flips here, the specs say }
  if ((a xor b) and $80000000)>0 then begin
    if not(b=0) then begin // not sure: is -1 => 0 already a sign-flip?
      b := rs.phase;
      if (a<0) then  b := b-rs.peri;
    end;
  end;
  b := MULF26DOT6(b,rs.postmul);
  doround := b;
end;

function boolify(n : dword) : dword;
begin
  if (n=0) then boolify := 0 else boolify := 1;
end;

function nboolify(n : dword) : dword;
begin
  if (n=0) then nboolify := 1 else nboolify := 0;
end;

function TTexecenv.maxdw(a,b : dword) : dword;
begin
  if use_signed_compare then begin
    if (longint(a)>longint(b)) then maxdw := a else maxdw := b;
  end else begin
    if ((a)>(b)) then maxdw := a else maxdw := b;
  end;
end;

function TTexecenv.mindw(a,b : dword) : dword;
begin
  if use_signed_compare then begin
    if (longint(a)<longint(b)) then mindw := a else mindw := b;
  end else begin
    if ((a)<(b)) then mindw := a else mindw := b;
  end;
end;

function boolval(b : boolean) : dword;
begin
  if b then boolval := 1 else boolval := 0;
end;

procedure TTexecenv.reset_gs();
begin
  with gs do begin
    auto_flip := true;
    scanctrl := false;
    scantype := 2;
    control_value_cut_in := 68; //17/16
    delta_base := 9;
    delta_shift := 3;
    fvx := 16384;
    fvy := 0;
    pvx := 16384;
    pvy := 0;
    dvx := 16384;
    dvy := 0;
    loopcount := 1;
    minimum_distance := 64;
    set_rounding(rnd,rs_RTG);
    rp0 := 0;
    rp1 := 0;
    rp2 := 0;
    single_width_cut_in := 0;
    single_width_value := 0;
    zp0 := 1;
    zp1 := 1;
    zp2 := 1;
  end;
end; 



procedure TTexecenv.set_super_rounding(b : byte); 
var x,peri,phase,thresh : longint;
begin
  if (b=$77) then begin
    gs.rnd.premul := 45;
    gs.rnd.postmul := 91;
  end else begin
    gs.rnd.premul := 64;
    gs.rnd.postmul := 64;
  end;
  x := pop_stack();
  peri := (x shr 6) and 3;
  if (peri=0) then begin
    peri := 32;
  end else if (peri=2) then begin
    peri := 128;
  end else peri := 64;
  phase := (x shr 4) and 3;
  phase := phase*peri DIV 4;
  thresh := 0;
  x := x and 15;
  case x of
    0  : thresh := peri-1;
    1  : thresh := -3*peri DIV 8;
    2  : thresh := -2*peri DIV 8;
    3  : thresh := -1*peri DIV 8;
    4  : thresh := 0;
    5  : thresh := 1*peri DIV 8;
    6  : thresh := 2*peri DIV 8;
    7  : thresh := 3*peri DIV 8;
    8  : thresh := 4*peri DIV 8;
    9  : thresh := 5*peri DIV 8;
    10 : thresh := 6*peri DIV 8;
    11 : thresh := 7*peri DIV 8;
    12 : thresh := 8*peri DIV 8;
    13 : thresh := 9*peri DIV 8;
    14 : thresh := 10*peri DIV 8;
    15 : thresh := 11*peri DIV 8;
  end;
  gs.rnd.peri   := peri;
  gs.rnd.phase  := phase;
  gs.rnd.thresh := thresh;
end;


procedure TTexecenv.do_call(p : TTprogram; ep : dword);
begin
  push_ret_addr(prg,ip+ilen);
  prg := p;
  ip := ep;
  ilen := 0;
end;

procedure TTexecenv.maybe_loopcall();
var lc : ^Tloopcall;
begin
  if (loopcalls>=0) then begin
    lc := @loopcall[loopcalls];
    if (lc^.ip=ip)and(lc^.cprg=prg) then begin
      dec(lc^.cnt);
      if (lc^.cnt>=0) then do_call(lc^.prg,lc^.ep)
      else dec(loopcalls);
    end;
  end;
end;

procedure TTexecenv.do_ret();
begin
  if pop_ret_addr(prg,ip) then begin
    ilen := 0;
    maybe_loopcall();
  end else raise Tttprogexception.create('callstack empty');
end;




function TTexecenv.do_if_handling(b : byte) : boolean;
var c : byte;
begin
  do_if_handling := true;
  case b of
    $58 : begin // if
            if ifskip then begin
              c := 2; // the whole if-else-endif section is skipped
            end else begin
              if (pop_stack()=0) then c := 1 // ignore if, take an elseif
              else c := 0; // take if, ignore elsieif
            end;
            inc(iflevel);
            if (iflevel>high(iflvl)) then raise Tttprogexception.create('max if-nesting exceeded');
            if (iflevel<0) then raise Tttprogexception.create('unexpected internal if-handling error');
            iflvl[iflevel] := c;
            ifskip := (c>0);
          end;
    $1B : begin // elseif
            { handles duplicate else like continued if-part }
            iflvl[iflevel] := iflvl[iflevel] xor 1;
            ifskip := ((iflvl[iflevel])>0);
          end;
    $59 : begin // endif
            dec(iflevel);
            if (iflevel<0) then begin
              ifskip := false;
              if (iflevel<-1) then raise Tttprogexception.create('unmatched endif');
            end else begin
              ifskip := (iflvl[iflevel] > 0);
            end;
          end;
    else begin
      do_if_handling := ifskip;
    end;
  end;
end;



procedure TTexecenv.do_simple_setget(b : byte);
var p1,p2,px,py : longint;
begin
  case b of
    $00 : begin  // set fv and pv to y-axis
            gs.fvx := 0;
            gs.pvx := 0;
            gs.fvy := $4000;
            gs.pvy := $4000;
            gs.dvx := gs.pvx;
            gs.dvy := gs.pvy;
          end;
    $01 : begin  // set fv and pv to x-axis
            gs.fvx := $4000;
            gs.pvx := $4000;
            gs.fvy := 0;
            gs.pvy := 0;
            gs.dvx := gs.pvx;
            gs.dvy := gs.pvy;
          end;
    $02 : begin  // set pv to y-axis
            gs.pvx := 0;
            gs.pvy := $4000;
            gs.dvx := gs.pvx;
            gs.dvy := gs.pvy;
          end;
    $03 : begin  // set pv to x-axis
            gs.pvx := $4000;
            gs.pvy := 0;
            gs.dvx := gs.pvx;
            gs.dvy := gs.pvy;
          end;
    $04 : begin  // set fv to y-axis
            gs.fvx := 0;
            gs.fvy := $4000;
          end;
    $05 : begin  // set fv to x-axis
            gs.fvx := $4000;
            gs.fvy := 0;
          end;
    6,7,
    8,9 : begin
            p1 := pop_stack();
            p2 := pop_stack();
            px := get_zonepoint_x(p2,gs.zp1) -get_zonepoint_x(p1,gs.zp2);
            py := get_zonepoint_y(p2,gs.zp1) -get_zonepoint_y(p1,gs.zp2);
            if (px=0)and(py=0) then begin
              px := gs.dvx;
              py := gs.dvy;
            end;
            F2DOT14_normalize_F26DOT6(px,py);
            if (b=6) then begin
              gs.pvx := px;
              gs.pvy := py;
              gs.dvx := gs.pvx;
              gs.dvy := gs.pvy;
            end else if (b=7) then begin
              gs.pvy := px;
              gs.pvx := -py;
              gs.dvx := gs.pvx;
              gs.dvy := gs.pvy;
            end else if (b=8) then begin
              gs.fvx := px;
              gs.fvy := py;
            end else begin
              gs.fvy := px;
              gs.fvx := -py;
            end;
          end;
    $0A : begin
            py := pop_stack();
            px := pop_stack();
            F2DOT14_normalize_F26DOT6(px,py);
            gs.pvx := px;
            gs.pvy := py;
            gs.dvx := gs.pvx;
            gs.dvy := gs.pvy;
          end;
    $0B : begin
            py := pop_stack();
            px := pop_stack();
            F2DOT14_normalize_F26DOT6(px,py);
            gs.fvy := py;
            gs.fvx := px;
          end;
    $0C : begin
            push_stack(gs.pvx);
            push_stack(gs.pvy);
          end;
    $0D : begin
            push_stack(gs.fvx);
            push_stack(gs.fvy);
          end;
    $0E : begin
            gs.fvx := gs.pvx;
            gs.fvy := gs.pvy;
          end;
  end;
end;

procedure TTexecenv.do_simple_setget2(b : byte);
begin
  case b of
    $10 : gs.rp0 := pop_stack();
    $11 : gs.rp1 := pop_stack();
    $12 : gs.rp2 := pop_stack();
    $13 : gs.zp0 := pop_stack();
    $14 : gs.zp1 := pop_stack();
    $15 : gs.zp2 := pop_stack();
    $16 : begin
            gs.zp0 := pop_stack();
            gs.zp1 := gs.zp0;
            gs.zp2 := gs.zp0;
          end;
    $17 : gs.loopcount := pop_stack();
    $18 : set_rounding(gs.rnd,rs_RTG);
    $19 : set_rounding(gs.rnd,rs_RTHG);
    $1A : gs.minimum_distance := pop_stack();
  end;
end;

procedure TTexecenv.do_stack_op(b : byte);
var x,y : dword;
begin
  case b of
    $20 : begin
            x := pop_stack();
            push_stack(x);
            push_stack(x);
          end;
    $21 : pop_stack();
    $22 : clear_stack();
    $23 : begin // swap
            x := pop_stack();
            y := pop_stack();
            push_stack(x);
            push_stack(y);
          end;
    $24 : push_stack( get_current_sp() );
    $25 : begin
            x := get_current_sp()-1;
            x := x-pop_stack();
            y := get_stack_element(x);
            push_stack(y);
          end;
    $26 : begin
            x := get_current_sp()-1;
            x := x-pop_stack();
            y := get_stack_element(x);
            remove_stack_element(x);
            push_stack(y);
          end;
  end;
end;


procedure TTexecenv.do_compare(b : byte);
var a1,b1 : dword; l,g,e : boolean;
begin
  b1 := pop_stack();
  a1 := pop_stack();
  if use_signed_compare then begin
    l := (longint(a1)<longint(b1));
    g := (longint(a1)>longint(b1));
    e := (longint(a1)=longint(b1));
  end else begin
    l := ((a1)<(b1));
    g := ((a1)>(b1));
    e := ((a1)=(b1));
  end;
  case b of
    $50 : push_stack( boolval(l) );
    $51 : push_stack( boolval(l or e) );
    $52 : push_stack( boolval(g) );
    $53 : push_stack( boolval(g or e) );
    $54 : push_stack( boolval(e) );
    $55 : push_stack( boolval(not(e)) );
  end;
end;

procedure TTexecenv.do_simple_calc(b : byte);
var x : dword;
begin
  if (nary_calc_instruction[b]=1) then begin
    case b of
      $56 : push_stack( boolify(doround(pop_stack(),gs.rnd) and 64)); // ODD
      $57 : push_stack(nboolify(doround(pop_stack(),gs.rnd) and 64)); // EVEN
      $5C : push_stack(nboolify(pop_stack())); // NOT
      $64 : push_stack( abs(longint(pop_stack())) ); // ABS
      $65 : push_stack( -longint(pop_stack()) ); // NEG
      $66 : push_stack( pop_stack() and $FFFFFFC0 ); // FLOOR
      $67 : begin // CEILING
              x := pop_stack();
              if ((x and $3F)>0) then x := x + 64;
              push_stack(x and $FFFFFFC0);              
            end;
    end;
  end else if (b<=$55) then begin
    do_compare(b);
  end else begin
    case b of
      $5A : push_stack( boolify(pop_stack()) and boolify(pop_stack()) );
      $5B : push_stack( boolify(pop_stack()) or  boolify(pop_stack()) );
      $8B : push_stack( maxdw(pop_stack(),pop_stack() ) );
      $8C : push_stack( mindw(pop_stack(),pop_stack() ) );
      $60 : push_stack( +pop_stack() +pop_stack() );
      $61 : begin
              x := pop_stack();
              push_stack( -x +pop_stack() );
            end;
      $62 : begin
              x := pop_stack();
              push_stack( DIVF26DOT6(pop_stack(), x) );
            end;
      $63 : push_stack( MULF26DOT6(pop_stack(),pop_stack()) );
    end;
  end;
end;

procedure TTexecenv.do_push_from_code(b : byte);
var n,j,i : longint; w : boolean; s : smallint;
begin
  if (b<$42) then begin
    n := prg.instructions[ip+1];
    w := (b=$41);
    j := ip+2;
  end else begin
    n := (b and 7)+1;
    w := (b>$B7);
    j := ip+1;
  end;
  for i := 0 to n-1 do begin
    s := 0;
    if (w) then begin
      s := prg.instructions[j];
      s := s*256;
      inc(j);
    end;
    s := s or prg.instructions[j];
    inc(j);
    push_stack(longint(s));
  end;
end;


procedure TTexecenv.do_rel_jump(b : byte);
var x : longint; doit : boolean;
begin
  doit := true;
  if (b=$78) then begin
    if (pop_stack()=0) then doit := false;
  end;
  if (b=$79) then begin
    if not(pop_stack()=0) then doit := false;
  end;
  x := pop_stack();
  if doit then begin
    ip := ip + x;
    ilen := 0;
  end;
end;

function TTexecenv.dorounding(x : longint) : longint;
begin
  dorounding := doround(x,gs.rnd);
end;


procedure TTexecenv.do_round_instr(b : byte);
var x,compensationtype : dword;
begin
  compensationtype := b and 3;
  x := pop_stack();
  x := apply_engine_compensation(x,compensationtype);
  if (b<$6c) then begin
    x := dorounding(x);
  end;
  push_stack(x);
end;


procedure TTexecenv.do_other_instructions(b : byte);
var x,y,z : dword;
begin
  if (b>=$68)and(b<=$6F) then do_round_instr(b)
  else if (b>=$C0)and(b<=$DF) then do_MDRP_instr(b)
  else if (b>=$E0) then do_MIRP_instr(b)
  else case b of
    $1C, $78, $79 : do_rel_jump(b);
    $4D : gs.auto_flip := true;
    $4E : gs.auto_flip := false;
    $71 .. $75, 
    $5D : do_delta_instr(b);
    $3D : set_rounding(gs.rnd,rs_RTDG);
    $7A : set_rounding(gs.rnd,rs_ROFF);
    $7C : set_rounding(gs.rnd,rs_RUTG);
    $7D : set_rounding(gs.rnd,rs_RDTG);
    $76 .. $77 : set_super_rounding(b);
    $1D : gs.control_value_cut_in := pop_stack();
    $1E : gs.single_width_cut_in := pop_stack();
    $1F : gs.single_width_value := pop_stack();
    $80 .. $82 : set_onoffcurve(b);
    $4F : pop_stack(); // DEBUG instruction
    $42 .. $43 : do_readwrite_store(b);
    $8A : begin // ROLL
            x := pop_stack();
            y := pop_stack();
            z := pop_stack();
            push_stack(y);
            push_stack(x);
            push_stack(z);
          end;
    $5E : gs.delta_base  := pop_stack();
    $5F : gs.delta_shift := pop_stack();
    $27 : do_ALPIGNPTS(b);
    $29 : do_UTP(b);
    $2E .. $2F : do_MDAP(b);
    $30 .. $31 : do_IUP(b);
    $32 .. $33 : do_SHP(b);
    $34 .. $35 : do_SHC(b);
    $36 .. $37 : do_SHZ(b);
    $38 : do_SHPIX(b);
    $39 : do_IP(b);
    $3A .. $3B : do_MSIRP(b);
    $3C : do_ALIGNRP(b);
    $3E .. $3F : do_MIAP(b);
    $44, $70 : do_WCVTP(b);
    $45 : do_RCVT(b);
    $46 .. $47 : do_GC(b);
    $48 : do_SCFS(b);
    $49 .. $4A : do_MD(b);
    $4B .. $4C : do_MP_S_PPEM(b);
    $7E : pop_stack(); // SANGW
    $7F : pop_stack(); // AA
    $85 : do_SCANCTRL(b);
    $86 .. $87 : do_SDPVTL(b);
    $88 : do_GETINFO(b);
    $8D : do_SCANTYPE(b);
    $8E : do_INSTCTRL(b);
  end;
end;

function TTexecenv.do_instruction() : boolean;
var p : TTprogram; ep,cnt : dword; b : byte;
begin
  b := prg.instructions[ip];
  if (defskip>0) then begin
    // if we're inside a func-def, overread erverything except func-defs
    case b of
      $2D : dec(defskip); // normal definition end
      $2C : inc(defskip); // raise Tttprogexception.create('nested function defs not allowed');
      $89 : inc(defskip); // raise Tttprogexception.create('nested instruction defs not allowed');
    end;
    do_instruction := true;
    exit;   
  end;
  if do_if_handling(b) then begin
    // inside deactivated IF-sections, do not allow func-defs to leak in
    do_instruction := true;
    exit;
  end;
  if ((b=$2C)or(b=$89)) then begin
    {  we're still neither in a func-def body,
       nor in a deactivated IF-section. 
       The func-def body about to follow won't be checked
       for IF-code, since we're currently not allowed to 
       execute the function body, which might modify booleans
       on the stack. Let's also skip ELSE and ENDIF, so that
       if-to-endif sections and func-bodies must always 
       either be completly or not all contained in each other. }
    cnt := pop_stack();
    ep := ip+ilen;
    if (b=$2C) then prg.define_function(cnt,ep,true)
               else prg.definstr(cnt,ep); 
//    if (b=$2C) then writeln('DEFFUNC',cnt)
//               else writeln('DEFINSTR',cnt);
    inc(defskip); 
    do_instruction := true;
    exit;
  end;
  if is_snormal_instruction[b] then begin
    do_instruction := true;
    if (b<=$0E) then do_simple_setget(b)
    else if (b=$0F) then do_ISECT(b)
    else if (b<=$1A) then do_simple_setget2(b)
    else if (nary_calc_instruction[b]>0) then do_simple_calc(b)
    else if (b>=$20)and(b<=$26) then do_stack_op(b)
    else if (b>=$B0)and(b<=$BF) then do_push_from_code(b)
    else if (b>=$40)and(b<=$41) then do_push_from_code(b)
    else do_other_instructions(b);
  end else begin
    { handle CALL, LOOPCALL, ENDF, 
      and user-defined instructions here
    }
    if (b=$2B) then begin // call
      ep := pop_stack();
      if oprg.lookup_function(ep,p,ep) then begin
        do_call(p,ep);
        do_instruction := true;       
      end else begin
        do_instruction := false;
        raise Tttprogexception.create('function not found ('+inttostr(ep)+')');
      end;
    end else if (b=$2A) then begin // loopcall
      ep :=  pop_stack();
      cnt := pop_stack();
      if oprg.lookup_function(ep,p,ep) then begin
        inc(loopcalls);
        if (loopcalls>high(loopcall)) then raise Tttprogexception.create('loopcall nesting too deep');
        ip := ip+ilen;
        ilen := 0;
        loopcall[loopcalls].prg  := p;
        loopcall[loopcalls].cprg := prg;
        loopcall[loopcalls].ep   := ep;
        loopcall[loopcalls].ip   := ip;
        loopcall[loopcalls].cnt  := cnt;
        maybe_loopcall();
        do_instruction := true;       
      end else do_instruction := false;      
    end else if (b=$2D) then begin // ret
      do_ret();
      do_instruction := true;
    end else if oprg.lookup_idef(b,p,ep) then begin
      { IDEF'ed instruction }
      do_call(p,ep);
      do_instruction := true;
    end else do_instruction := false;
  end;
end;

function TTexecenv.decode_instruction_length() : boolean;
var prgrem : longint; b,b2 : byte;
begin
  prgrem := high(prg.instructions) +1 -ip;
  if (prgrem>0) then begin
    b := prg.instructions[ip];
    ilen := instruction_length[b];
    if (prgrem>=ilen) then begin
      if ((b=$40)or(b=$41)) then begin
        b2 := prg.instructions[ip+1];
        inc(ilen,b2);
        if (b=$41) then inc(ilen,b2);
      end;
    end;
    decode_instruction_length := (prgrem>=ilen);
  end else decode_instruction_length := false;
end;




procedure TTexecenv.onInstructionExec();
begin
end;

function TTexecenv.exec_instruction() : boolean;
begin
  if (ip<=high(prg.instructions)) then begin
    if decode_instruction_length() then begin
      onInstructionExec();
      exec_instruction := do_instruction();
    end else exec_instruction := false;
    ip := ip + ilen;
  end else begin 
    reached_end_of_program := (prg=oprg)and(ip=high(prg.instructions)+1);
    exec_instruction := false;
  end;
end;


function TTexecenv.do_exec(prog : TTprogram) : boolean;
begin
  reached_end_of_program := false;
  loopcalls := -1;
  iflevel := -1;
  ifskip := false;
  defskip := 0;
  prg := prog;
  oprg := prog;
  ip := 0;
  while exec_instruction() do;
  do_exec := reached_end_of_program;
end;

procedure TTexecenv.set_max_ifnesting(n : longint);
begin
  setlength(iflvl,n);
  iflevel := -1;
  ifskip := false;
end;

procedure TTexecenv.set_max_loopcallnesting(n : longint);
begin
  setlength(loopcall,n);
  loopcalls := -1;
end;





constructor TTexecenv.create();
var i : longint;
begin
  for i := 0 to 255 do is_snormal_instruction[i] := true;
  is_snormal_instruction[$28] := false;  // undefined instr
  is_snormal_instruction[$2A] := false;  // LOOPCALL
  is_snormal_instruction[$2B] := false;  // CALL
  is_snormal_instruction[$2D] := false;  // ENDF
  is_snormal_instruction[$7B] := false;  // undefined instr
  is_snormal_instruction[$83] := false;  // undefined instr
  is_snormal_instruction[$84] := false;  // undefined instr
  for i := $8F to $AF do is_snormal_instruction[i] := false; // undefined instr

  for i := 0 to 255 do nary_calc_instruction[i] := 0;
  for i := $50 to $55 do nary_calc_instruction[i] := 2; // lt..gt
  nary_calc_instruction[$56] := 1; // ODD
  nary_calc_instruction[$57] := 1; // EVEN
  nary_calc_instruction[$5A] := 2; // AND
  nary_calc_instruction[$5B] := 2; // OR
  nary_calc_instruction[$5C] := 1; // NOT
  for i := $60 to $63 do nary_calc_instruction[i] := 2; // add,mul,...
  for i := $64 to $67 do nary_calc_instruction[i] := 1; // floor,...
  for i := $8B to $8C do nary_calc_instruction[i] := 2; // max,min


  for i := 0 to 255 do instruction_length[i] := 1;

  for i := 0 to 7 do begin
    instruction_length[i+$b0] := 1 + (i+1);   // PUSH BYTES
    instruction_length[i+$b8] := 1 + 2*(i+1); // PUSH WORDS
  end;
  instruction_length[$40] := 2; // NPUSH BYTES, >=2
  instruction_length[$41] := 2; // NPUSH WORDS, >=2

  set_max_ifnesting(1024);
  set_max_loopcallnesting(1024);
  use_signed_compare := true;
end;






begin
end.
