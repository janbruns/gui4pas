{$mode objfpc}
{$unitpath .}
UNIT ttflaten;



{ Take Bezier-segment contours of any order
  and flatten them to 1st order line contours.

}


interface
uses ttcontours;


type
TOT_float = double;

TOT_coord = packed record
  x,y : TOT_float;
end;

type
Tbezierstreamer = class(TcontourList)
  public
  procedure addBezierPoint(p : TOT_coord);
  procedure addBezierPoint(x,y : TOT_float);
  procedure done_curve();

  procedure done_contour();

  procedure clear();

  constructor create();
  destructor destroy(); override;


  protected
  function casteljau(i,j : longint; t : TOT_float) : TOT_coord;
  procedure divide_curve(t0,t1 : TOT_float);
  procedure do_curve();

  procedure curcontour_addPoint(p0 : TOT_coord);

  protected
  curv : array of TOT_coord;
  curvpts : longint;

  public small_err : TOT_float; // set to something like 0.1 pixel
  public scalevalx,scalevaly,xoffset,yoffset : TOT_float; 
end;




implementation



procedure Tbezierstreamer.addBezierPoint(p : TOT_coord);
begin
  if (curvpts>high(curv)) then setlength(curv,16+curvpts);
  curv[curvpts] := p;
  inc(curvpts);
end;

procedure Tbezierstreamer.addBezierPoint(x,y : TOT_float);
var p : TOT_coord;
begin
  if (curvpts>high(curv)) then setlength(curv,16+curvpts);
  curv[curvpts].x := x;
  p.x := x;
  p.y := y;
  addBezierPoint(p);
end;


procedure Tbezierstreamer.done_curve();
begin
  do_curve();
  curvpts := 0;
end;

procedure Tbezierstreamer.done_contour();
begin
  new_contour();
end;

procedure Tbezierstreamer.clear();
begin
  release_contours();
  curvpts := 0;
  new_contour();
end;

constructor Tbezierstreamer.create();
begin
  inherited create();
  scalevalx := 256/64;
  scalevaly := 256/64;
  xoffset := 32;
  yoffset := 32;
  small_err := 0.1;
  curvpts := 0;
  setlength(curv,16);
  new_contour();
end;

destructor Tbezierstreamer.destroy();
begin
  inherited destroy();
end;



function Tbezierstreamer.casteljau(i,j : longint; t : TOT_float) : TOT_coord;
var p1,p2 : TOT_coord;
begin
  if (j>0) then begin
    p1 := casteljau(i  ,j-1,t);
    p2 := casteljau(i+1,j-1,t);
    p1.x := p1.x*(1-t) +t*p2.x;
    p1.y := p1.y*(1-t) +t*p2.y;
    casteljau := p1;
  end else begin
    casteljau := curv[i];
  end;
end;

procedure Tbezierstreamer.divide_curve(t0,t1 : TOT_float);
var p0,p1,p : TOT_coord; t : TOT_float;
begin
  p0 := casteljau(0,curvpts-1,t0);
  p1 := casteljau(0,curvpts-1,t1);

  t := (t1+t0)*0.5;
  p  := casteljau(0,curvpts-1,t);

  p1.x := 0.5*(p1.x+p0.x);
  p1.y := 0.5*(p1.y+p0.y);

  p1.x := abs(p1.x-p.x)  +abs(p1.y-p.y);
  if (p1.x > small_err) then begin
    divide_curve(t0,t); // t0 will be put
    divide_curve(t,t1); // t  will be put
  end else begin
    curcontour_addPoint(p0);
  end;

end;

procedure Tbezierstreamer.do_curve();
var i : longint;
begin
  if (curvpts>2) then begin
    for i := 0 to curvpts-1 do begin
      divide_curve(i/curvpts,(i+1)/curvpts);
    end;
  end else if (curvpts=2) then begin
    curcontour_addPoint(curv[0]);
    curcontour_addPoint(curv[1]);
  end else if (curvpts=2) then begin
    curcontour_addPoint(curv[0]);
  end else begin
  end;
end;


procedure Tbezierstreamer.curcontour_addPoint(p0 : TOT_coord);
begin
  curcontour.addPointxy( round((p0.x+xoffset)*scalevalx), 
                         round((p0.y+yoffset)*scalevaly) );
end;




begin
end.

