{ WORK IN PROGRESS, here's why it takes some time to implement...

  basic algo:

  start new line
  repeat
    while newline_empty or next_word fits do add next word to newline;
    put newline
    start new line
  until all text done
  
  where "put newline" might include right or left algnment, or both.
  That should probably often be "auto":
  if leftmost char of newline is LTR, align left,
  if rightmost char of newline is RTL, align right,
  or blockset, if both conditions met.

  leftalign_forced,   leftalign_auto,   leftalign_disabled,
  rightalign_forced,  rightalign_auto,  rightalign_disabled,
  align_center
  
  
  One (at the time of wirting this comment) open question is
  the intended font-choosing mechanics:
  
  In many aspects, it appears straight forward to use a
  "just a single font-file active at a time/text position"
  approach. But this would be already be not so clear
  relative to the fact that some text properties like
  "italicness" would be implied with that static font choice,
  whereas others like font-size would already be something to 
  care about on a per-character level. And this association of
  details to care about to point of careabout would at least 
  as a tendency be a function of selected font-file (for
  example, a VAR-font implementation might allow both, some
  "derive a virtual complete font" way of dealing with that, 
  as well as -maybe less likely- allowing a per char way of 
  doing that).
  
  Furthermore, this design decision implies that a selected
  font might not be capable of rendering parts of the text,
  and this situation is very likely to be easily detected. By
  definition, this would add unnecessary burden on the user
  to ever explicitly redefine anything about font definition
  whereever the text changes language. So an approach that 
  allows for at least one alternative font definition with
  some reliance on auto-detection about where to use which one
  seems to make some sense.
  
  With a full blown any-lang set of font defs on the other
  hand, it's getting unlikely that auto-detection would always
  get what the user expects. The fonts might have a lang-
  support overlap, and the paramters used in auto-detection 
  might be bound to relatively simple aspects like "glyph 
  available" or "unicode script availabe", and it won't be able
  derive sub-script info from the text (where I'm very unsure
  about degree of importance... for western scripts, my personal
  office experience tells me that the exact setting doesn't
  make a notable difference at all, but what does that tell
  about other scripts?).
  
  Maybe I'll decide for something in between. Maybe a chain
  of well defined fallback settings, where the next level of
  fallback gets chosen only in case of "cannot render". Or
  maybe a little bit more of dependence on detected unicode
  script, that allows for one well defined active 
  setting(s-chain) per script?
  
  The problem here seems to be that this exact decision has
  a lot of influence on the structure of code to implement
  here, and whatever the decision, it'll leave the user with
  at least some unintended font-file-format specific options
  (combined with a often-very-low degree of significance) to
  explicitly define. And furthermore, the user should have
  the option to explicitly define anything, effectivly
  turning off any auto detection logic (besides maybe almost
  necessary fallbacks).
  
  fallback-handling-mode:
    never_fallback,
    
    only_on_no_glyidx,
    
    on_script_unavailable, // find script in fallback chain 
    // and on fail like only_on_no_glyidx
    
    on_chain_hasnt_lang
    // assume text is explicitly lang-markedup, choose the 
    // first font that explicitly supports lang, if none do 
    // on_script_unavailable style
    
    
  
  the following untested code was made before above text.
  Not sure what parts might finally make it.
}


Tcharfont=record
  fnt : TTfone;
  glyidx,flags : word;
end;

Tparagraphlayouter = class(Tabstractparagraphlayouter)
  procedure get_next_word(); virtual;
  function get_next_word_width() : longint; virtual;
  procedure add_next_word();  virtual;
  procedure add_break();  virtual;
  procedure add_all();  virtual;
  procedure BeginLine(); virtual; 

  procedure find_next_break_position();
  procedure remove_next_trailspace();
  procedure get_next_word_data();
  procedure next_word_to_glyidx();


  public
  pm : Tparagraphmanger;
  cur_trailspace, nxt_trailspace : longint;

  next_word : array of dword;
  next_word_glyidx : array of Tcharfont;
  
  nxt_shy_pos : longint;
end;




procedure Tparagraphlayouter.find_next_break_position();
var i,j : dword;
begin
  j := cur_position;
  for i := j to high(pm.basechars) do begin
    j := i;
    if pm.has_break_opportunity(i) then break;
  end;
  nxt_position := j+1;
end;

procedure Tparagraphlayouter.remove_next_trailspace();
var j : dword;
begin
  j := nxt_position -1;
  nxt_trailspace := 0;
  while (j>(cur_position+1)) and pm.is_CMless_spc(j) do begin
    nxt_trailspace += 1;
    dec(j);
  end;
end;


procedure Tparagraphlayouter.get_next_word_data();
var i,j,n,k : dword;
begin
  nxt_shy_pos := -1;
  n := 0;
  k := nxt_position -1 -nxt_trailspace;
  for i := k downto cur_position do begin
    n += 1+pm.combinercount[i];
  end;
  setlength(next_word,n);
  n := 0;
  for i := cur_position to k do begin
    for j := 0 to pm.combinercount[i] do begin
      next_word[n] := pm.getChar(pm.basechar[i]+j);
      next_dir[n] := pm.is_RTL(pm.basechar[i]+j);
      if next_word[n]=unicode_shy then begin
        next_word[n] := unicode_hyphen;
        nxt_shy_pos := n;
      end;
      inc(n);
    end;
  end;
end;

procedure Tparagraphlayouter.next_word_to_glyidx();
var i,j : dword;
begin
  setlength(next_word_glyidx,high(next_word)+1);
  if (high(next_word)<0) then exit;
  j := 0;
  for i := 0 to high(next_word) do begin
    next_word_glyidx[j] := fontchooser.process_char(next_word[i],next_dir[i]);
    if next_word_glyidx[j].glyidx<>0 then inc(j);
  end;
  setlength(next_word_glyidx,j-1);
end;

procedure Tparagraphlayouter.do_gsub();
var i,j : dword; s,s2 : unicodestring;
begin
  if (high(next_word)<0) then exit;
  s := chr(next_word_glyidx[0].glyidx);
  endi := high(next_word_glyidx);
  for i := 1 to endi do begin
    if (next_word_glyidx[i].font<>next_word_glyidx[i-1].font) 
    or (i=endi)
    then begin
      s2 := do_gsub_processing(next_word_glyidx[i-1].font, s);
      add_processed_block(next_word_glyidx[i-1], s2);
      s := '';
    end;
    s += chr(next_word_glyidx[i].glyidx);;
  end;
end;



function sum_glyidx_aw() : longint;
var sum,gidx : longint; i : dword; gly : TTglyph; gbmp : Pglyphbmp;
begin
  sum := 0;
  result := sum;
  if (high(next_word_glyidx)<0) then exit;
  for i := 0 to high(next_word_glyidx) do begin
    gidx := next_word_glyidx[i];
    if gidx>=0 then begin
      gly := current_font.getGlyph(gidx);
      if assigned(gly) then begin
        gbmp := get_glybmp(gly, current_fontsize, current_fontsize, current_gf);
        if assigned(gbmp) then sum += gbmp^.awx;
      end;
    end;
  end;
  result := sum;
end;

function get_next_word_width() : longint; override;
begin
end;


