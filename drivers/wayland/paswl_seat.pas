{$mode objfpc}
{$unitpath .}
unit paswl_seat;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_keyboard,paswl_pointer,paswl_touch,paswl_bindable;


type

Tpaswl_seat=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;
  
  function get_keyboard(cls : Tpaswl_keyboard_cls) : Tpaswl_keyboard;
  function get_pointer(cls : Tpaswl_pointer_cls) : Tpaswl_pointer;
  function get_touch(cls : Tpaswl_touch_cls) : Tpaswl_touch;


  protected
  procedure cb_seat_capabilities(c : dword); virtual;
  procedure cb_seat_name(n : Pchar); virtual;
  
  protected
  keyboard : Tpaswl_keyboard;
  mouseptr : Tpaswl_pointer;
  touchdev : Tpaswl_touch;
  caps : dword;
  caps_valid : boolean;
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;

Tpaswl_seat_cls = class of Tpaswl_seat;









implementation
uses strings,math;


procedure seat_capabilities(dat:pointer; o:wl_seat; capabilities : dword); cdecl;
var d : Tpaswl_seat;
begin
  if dat<>nil then begin
    d := Tpaswl_seat(dat);
    if d.wlo=o then d.cb_seat_capabilities(capabilities)
    else ;//
  end;
end;

procedure seat_name(dat:pointer; o:wl_seat; name : Pchar); cdecl;
var d : Tpaswl_seat;
begin
  if dat<>nil then begin
    d := Tpaswl_seat(dat);
    if d.wlo=o then d.cb_seat_name(name)
    else ;//
  end;
end;

var
imp_if_seat_listener : wl_seat_listener = (
  capabilities : @seat_capabilities;
  name : @seat_name;
);






procedure Tpaswl_seat.cb_seat_capabilities(c : dword); 
begin
  caps_valid := true;
  caps := c;
end;

procedure Tpaswl_seat.cb_seat_name(n : Pchar); 
begin
end;



constructor Tpaswl_seat.create();
begin
  inherited create();
  caps_valid := false;
  caps := 0;
  keyboard := nil;
  mouseptr := nil;
  touchdev := nil;
end;

destructor Tpaswl_seat.destroy();
begin
  if (keyboard<>nil) then keyboard.destroy();
  if (mouseptr<>nil) then mouseptr.destroy();
  if (touchdev<>nil) then touchdev.destroy();
  wl_seat_release(wl_seat(wlo));
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_seat.onBind();
begin
  inherited;
  wl_seat_add_listener(wl_seat(wlo),imp_if_seat_listener,self);
end;

procedure Tpaswl_seat.onUnbind();
begin
  inherited;
end;


function Tpaswl_seat.get_keyboard(cls : Tpaswl_keyboard_cls) : Tpaswl_keyboard;
var o : Tpaswl_keyboard; wo : wl_keyboard;
begin
  if keyboard=nil then begin
    if caps_valid and ((caps and wl_seat_capability_keyboard)>0) then begin
      wo := wl_seat_get_keyboard(wl_seat(wlo));
      o := Tpaswl_keyboard(make_child(cls,wo));
      if o<>nil then begin
        o.seat := self;
        keyboard := o;
      end;
    end;
  end;
  get_keyboard := keyboard;
end;

function Tpaswl_seat.get_pointer(cls : Tpaswl_pointer_cls) : Tpaswl_pointer;
var o : Tpaswl_pointer; wo : wl_pointer;
begin
  if mouseptr=nil then begin
    if caps_valid and ((caps and wl_seat_capability_pointer)>0) then begin
      wo := wl_seat_get_pointer(wl_seat(wlo));
      o := Tpaswl_pointer(make_child(cls,wo));
      if o<>nil then begin
        o.seat := self;
        mouseptr := o;
      end;
    end;
  end;
  get_pointer := mouseptr;
end;

function Tpaswl_seat.get_touch(cls : Tpaswl_touch_cls) : Tpaswl_touch;
var o : Tpaswl_touch; wo : wl_touch;
begin
  if touchdev=nil then begin
    if caps_valid and ((caps and wl_seat_capability_touch)>0) then begin
      wo := wl_seat_get_touch(wl_seat(wlo));
      o := Tpaswl_touch(make_child(cls,wo));
      if o<>nil then begin
        o.seat := self;
        touchdev := o;
      end;
    end;
  end;
  get_touch := touchdev;
end;


class function Tpaswl_seat.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_seat_interface;
end;

class function Tpaswl_seat.wl_ifver() : dword;
begin
  wl_ifver := 9;
end;


initialization
begin
end;

finalization
begin
end;

end.



