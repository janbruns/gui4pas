{$mode objfpc}

uses classes, sysutils,
     FPImage, FPCanvas, FPImgCanv,
     FPWritePNG, FPwriteBMP, ttcontours, ttflaten, ttrender, ot_cmap,
     fontfile,ttgridfit,math;



const
ptsiz = 12;

sizsq = ptsiz*2+5;
sizsq2 = sizsq div 2;


emumode = 0; // 0=with hinting, 3=no gridfit

const
cols = 20;
rows = cols;
xres = (cols+1)*sizsq;
yres = (rows+1)*sizsq;

type
Pglyphbmp = ^Tglyphbmp;
Tglyphbmp = record
  next : Pglyphbmp;
  dat  : pword;
  ppemx,xres,refx,lsb,awx : longint;
  ppemy,yres,refy,usb,awy : longint;
end;



function make_glybmp( g : TTglyph;
                      c : Tbezierstreamer; 
                      r : ttmultirenderer;
                      sm : longint          ) : Pglyphbmp;
var q : Pglyphbmp; p : Pword; x,y,x0,y0,x1,y1 : longint;
begin
  new(q);
  with q^ do begin
    next := nil;
    lsb :=  sarint64(g.slsb,6)-2;
    awx :=  round(g.saw/64);
    x0 := sarint64(g.sxmin,6)-2;
    x1 := sarint64(g.sxmax,6)+2;
    y0 := sarint64(g.symin,6)-2;
    y1 := sarint64(g.symax,6)+2;
    xres := abs(x1-x0)+2;
    yres := abs(y1-y0)+2;
    getmem(dat,xres*yres*sizeof(dat^));
    refx := x0;
    refy := y0;
    r.setzero(-x0,-y0);
    r.setClip(0,0,xres+1,yres+1);
    r.draw(sm,c);
    p := dat;
    if (sm=0) then begin
      for y := 1 to yres do begin
        for x := 1 to xres do begin
          p^ := r.getgrey(x,y) shr 14;
          inc(p);
        end;
      end;
    end else begin
      for y := 1 to yres do begin
        for x := 1 to xres do begin
          if (r.getbw(x,y)=0) then p^ := 0 else p^ := 65535;
          inc(p);
        end;
      end;
    end;
  end;
  make_glybmp := q;
end;

procedure destroy_glybmp(var p : Pglyphbmp);
var p2 : Pglyphbmp;
begin
  p2 := p^.next;
  freemem(p^.dat,p^.xres*p^.yres*2);
  dispose(p);
  p := p2;
end;


var
r : ttmultirenderer;
c,c2 : Tbezierstreamer;





procedure paint_glybmp(gp : Pglyphbmp; img : TFPCustomImage; ix,iy : longint);
var col : TFPColor; xr,yr,x,y,ax,ay : longint; p : Pword; t,cv : double;
begin
  xr := gp^.xres;
  yr := gp^.yres;
  ix += gp^.refx;
  iy += -gp^.refy +ptsiz;
  p := gp^.dat;
  for y := 1 to yr do for x := 1 to xr do begin
    ax := min(max(ix+x,0),xres-1); // for now, take global clip
    ay := min(max(iy-y,0),yres-1);
    col := img.Colors[ax, ay];
    t := 1-p^/65535;
    cv := col.red/65535;
    cv := (1-t)*cv + (t)*1;
    col.red := round( 65535*cv );
//    col.red := p^;
    inc(p);
    col.green := col.red;
    col.blue := col.red;

    img.Colors[ax, ay] := col;
  end;
end;

const
teststring : UTF8string = 'Dies ist ein Schriftprobentext, der vor Ort dazu genutzt werden könnte, Darstellungsdetails zu visualisieren. Er wurde also nicht dazu ersonnen, dem Leser einen Informationsgehalt darzubieten, so daß hier kaum irgendwelche Inhalte gelesen werden können. Aber immerhin: ';







var
p : Pglyphbmp;


var canvas : TFPCustomCanvas;
    image : TFPCustomImage;
    writer : TFPCustomImageWriter;
var
i,j : longint;
x,y : longint;
gfok,gfnok : longint;

var
//s : TFileStream;
s : TMemoryStream;
font : TOTcmapfont; //TTfont;
g : TTglyph;
gridfitter : Tgridfitter;

tstart : double;
scan_dropout,scan_dropout3 : boolean;
col : TFPColor;
sum : double;
gry : longint;

msgcnt : longint;
msg : UnicodeString;
ch,xpos : longint;


begin
  msg := UTF8Decode(UTF8Decode(teststring));
  msgcnt := 1;

  tstart := now();

  gridfitter := Tgridfitter.create();
  gridfitter.render_greyscale := false;

  c := Tbezierstreamer.create();

  r := ttmultirenderer.create();


//  r.setres(pointsize*3,pointsize*3);
  r.setres(xres,yres);
  r.dbgprint := false;

  r.setMultisample(1);


  x := 0;


  image := TFPMemoryImage.Create(xres,yres);
  Canvas := TFPImageCanvas.Create(image);
  Writer := TFPWriterBMP.Create;
//  Writer := TFPWriterPNG.Create;




  s := TMemoryStream.create();

//  s.LoadFromFile('Font3.ttf');
//  s.LoadFromFile('MgOpenCanonicaRegular.ttf');

//  s.LoadFromFile('./testfonts/verdana.ttf');
//  s.LoadFromFile('./testfonts/plantc.ttf');

  s.LoadFromFile('./times.ttf');
//  s.LoadFromFile('/usr/share/fonts/truetype/ttf-dejavu/DejaVuSerif.ttf');
//  s.LoadFromFile('/usr/share/fonts/truetype/ttf-dejavu/DejaVuSerif-Bold.ttf');

//  s.LoadFromFile('/usr/share/fonts/truetype/ttf-bitstream-vera/VeraSeBd.ttf');
//  s.LoadFromFile('/usr/share/fonts/truetype/ttf-bitstream-vera/VeraSe.ttf');

//  s.LoadFromFile('/usr/share/fonts/truetype/openoffice/opens___.ttf');

//  s.LoadFromFile('/usr/share/fonts/truetype/dejavu/DejaVuSerif-Bold.ttf');


  font := TOTcmapfont.create();
  font.load(s);


  gridfitter.set_font(font,emumode);
  gridfitter.set_ppem(ptsiz,ptsiz,ptsiz);



  gfok := 0;
  gfnok := 0;
  for y := 0 to rows-1 do begin
    xpos := 0;
    while xpos<xres do  begin

      if (msgcnt<=length(msg)) then ch := ord(msg[msgcnt]) else ch := ord(' ');
      inc(msgcnt);
      if msgcnt>length(msg) then msgcnt := 1;

writeln('n:',msgcnt,': Unicode: ',hexstr(ch,4),' => glphid:',font.UnicodeToGlyphIndex(ch),' ch:',UTF8Encode(chr(ch)) );
      ch := font.UnicodeToGlyphIndex(ch);

      g := font.getGlyph( ch );


      if not(g=nil) then begin
        g.scale(ptsiz,ptsiz,font.unitsPerEm);

 if (emumode<>3) then begin
        gridfitter.gridfit_glyph(g);
        if gridfitter.getFitStatus() then inc(gfok) else inc(gfnok);
 end;
        g.make_contours(c);

        gry := gridfitter.getScantype();
        gry := 0;

        p := make_glybmp(g,c,r,gry);
        paint_glybmp(p, image,xpos, y*sizsq+sizsq2);

        xpos := xpos + p^.awx;
        destroy_glybmp(p);

        c.clear();
      end;
    end;
  end;
  c.destroy();

  writeln(gfok, ' glyphs used gridfitted version');
  writeln(gfnok,' glyphs used scaled version');




  writeln((now()-tstart)*3600*24,' s: done rendering. save image to png-file...');

  image.SaveToFile('test_'+hexstr(emumode,1)+'.bmp', writer);

  r.destroy();

  gridfitter.destroy();
  font.destroy();
  s.destroy();

  Canvas.Free;
  image.Free;
  writer.Free;

  writeln((now()-tstart)*3600*24,' s: halt.');

writeln(length(teststring));
writeln(length(msg));


end.
