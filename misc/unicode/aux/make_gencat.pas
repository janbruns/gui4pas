{$mode objfpc}
{$unitpath ..}
uses unicode_gencat, unicode_utf8, unicode_tabinit;

function todot(var s : ansistring; const dot : shortstring) : ansistring;
var i : longint;
begin
  i := pos(dot,s);
  if (i>0) then begin
    result := copy(s,1,i-1);
    s := copy(s,i+length(dot));
  end else begin
    result := s;
    s := '';
  end;
end;


function encode(t,l : dword) : ansistring;
var s : ansistring; tc0,lc0 : longint;
begin
  s := '';
  if (t>=15) then tc0 := 15 else tc0 := t;
  if (l>=7)  then lc0 := 7  else lc0 := l;
  s += UTF8_codepoint_as_UTF8(tc0+lc0*16);
  if (t>=15) then s += UTF8_codepoint_as_UTF8(t);
  if (l>=7)  then s += UTF8_codepoint_as_UTF8(l);
  encode := s;
end;


var
dat : array[0..$EFFFF] of byte;

var
symbfreq : array[0..255] of longint;
arr2 : Tbarr;

procedure analyse_block();
var cnt,i,j : longint; cur : byte; s : ansistring;
arr : array of dword;
begin
  for i := 0 to 255 do symbfreq[i] := 0;
  j := 0;
  cur := dat[0];
  cnt := 0;
  for i := 1 to high(dat) do begin
    if (dat[i]<>cur) then begin
      cur := dat[i];
      cnt := 0;
      j += 1;
      inc(symbfreq[cur and 255]);
    end else cnt += 1;
  end;
  j +=1;
  inc(symbfreq[cur and 255]);

  setlength(arr,j*3);
  j := 0;
  cur := dat[0];
  arr[3*j+1] := 0;
  arr[3*j+0] := cur;
  cnt := 0;
  for i := 1 to high(dat) do begin
    if (dat[i]<>cur) then begin
      cur := dat[i];
      cnt := 0;
      arr[3*j+2] := i-1;
      j += 1;
      arr[3*j+1] := i;
      arr[3*j+0] := cur;
    end else cnt += 1;
  end;
  arr[3*j+2] := high(dat);
  s := '';
  i := 0;
  while i <high(arr) do begin
    s += encode(arr[i],arr[i+2]-arr[i+1]);
    i += 3;
  end;
  for i := 0 to 255 do begin
    //writeln(i,':',symbfreq[i]);
  end;
//  writeln('len=',length(s));
  setlength(arr2,0);
  unicode_initvaldecode(s,arr2);
  for i := 0 to high(dat) do begin
    if dat[i]<>arr2[i] then writeln('ERROR',i);
  end;
  writeln('sinc : array[0..',length(s)-1,'] of byte = (');
  write('  ');
  for i := 1 to length(s)-1 do begin
    write('$',hexstr(ord(s[i]),2),', ');
    if (((i-1) and 15)=15) then begin
      writeln();
      write('  ');
    end;
  end;
  writeln('$',hexstr(ord(s[i]),2),');');
end;



procedure doline(s : ansistring);
var s0,sc,sn,s1,s2,s3,s4,s5 : ansistring; w : word; cp,ce : dword; v : byte;
begin
  s0 := s;
  s := todot(s,'#');
  if s<>'' then begin
    sc := todot(s,';');
    sn := todot(s,';');
    s1 := todot(s,';');
    s2 := todot(s,';');
    s3 := todot(s,';');
    s4 := todot(s,';');
    s5 := todot(s,';');
    sc := '$'+sc;
    val(sc,cp,w);
    if (cp<high(dat)) then begin
      v := gencat_unknown;
      if s1='Lu' then v := gencat_Lu
      else if s1='Ll' then v := gencat_Ll
      else if s1='Lt' then v := gencat_Lt
      else if s1='Lm' then v := gencat_Lm
      else if s1='Lo' then v := gencat_Lo
      else if s1='Mn' then v := gencat_Mn
      else if s1='Mc' then v := gencat_Mc
      else if s1='Me' then v := gencat_Me
      else if s1='Nd' then v := gencat_Nd
      else if s1='Nl' then v := gencat_Nl
      else if s1='No' then v := gencat_No
      else if s1='Pc' then v := gencat_Pc
      else if s1='Pd' then v := gencat_Pd
      else if s1='Ps' then v := gencat_Ps
      else if s1='Pe' then v := gencat_Pe
      else if s1='Pi' then v := gencat_Pi
      else if s1='Pf' then v := gencat_Pf
      else if s1='Po' then v := gencat_Po
      else if s1='Sm' then v := gencat_Sm
      else if s1='Sc' then v := gencat_Sc
      else if s1='Sk' then v := gencat_Sk
      else if s1='So' then v := gencat_So
      else if s1='Zs' then v := gencat_Zs
      else if s1='Zl' then v := gencat_Zl
      else if s1='Zp' then v := gencat_Zp
      else if s1='Cc' then v := gencat_Cc
      else if s1='Cf' then v := gencat_Cf
      else if s1='Cs' then v := gencat_Cs
      else if s1='Co' then v := gencat_Co
      else if s1='Cn' then v := gencat_Cn
      else writeln('unknown gencat: ',s1);
      dat[cp] := v;
    end;
  end;
end;

var
f : text;
s : ansistring;

begin
  fillbyte(dat[0],high(dat)+1,0);
  assign(f,'UnicodeData.txt');
  reset(f);
  while not(eof(f)) do begin
    readln(f,s);
    doline(s);
  end;
  close(f);
  analyse_block();
end.
