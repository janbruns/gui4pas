{$mode objfpc}

UNIT ttcontours;


{ manage 1st order polylines
}


interface


type
TTcoord = longint;
TTvec = packed record
  x,y : TTcoord;
end;


Tcontour = class
  public
  procedure addPoint(c : TTvec);
  procedure addPointXY(x,y : TTcoord);
  function getPointX(i : longint) : TTcoord;
  function getPointY(i : longint) : TTcoord;
  function getPoint(i : longint) : TTvec;

  function pointCount() : longint;


  constructor create();
  destructor destroy(); override;


  public
  next : Tcontour;

  private
  coords : array of TTvec;
  pc : longint;
end;





TcontourList = class 
  public
  constructor create();
  destructor destroy(); override;

  procedure add_contour(c : Tcontour);
  function new_contour() : Tcontour;

  function first_contour() : Tcontour;
  function next_contour(c : Tcontour) : Tcontour;

  procedure release_contours();

  protected
  firstcontour,curcontour : Tcontour;
end;










implementation

{$COPERATORS ON}




function Tcontour.getPointX(i : longint) : TTcoord;
begin
  getPointX := coords[i].x;
end;

function Tcontour.getPointY(i : longint) : TTcoord;
begin
  getPointY := coords[i].y;
end;

function Tcontour.getPoint(i : longint) : TTvec;
begin
  getPoint := coords[i];
end;


procedure Tcontour.addPoint(c : TTvec);
begin
  if (pc>high(coords)) then begin
    setlength(coords,2*(pc));
  end;
  coords[pc] := c;
  inc(pc);
end;

procedure Tcontour.addPointXY(x,y : TTcoord);
var v : TTvec;
begin
  v.x := x; v.y := y; addPoint(v);
end;


function Tcontour.pointCount() : longint;
begin
  pointCount := pc;
end;

constructor Tcontour.create();
begin
  inherited create();
  pc := 0;
  setlength(coords,8);
  next := nil;
end;

destructor Tcontour.destroy();
begin
  inherited destroy();
end;






constructor TcontourList.create();
begin
  inherited create();
  firstcontour := nil;
  curcontour := nil;
end;

destructor TcontourList.destroy();
begin
  release_contours(); 
  inherited destroy();
end;

procedure TcontourList.add_contour(c : Tcontour);
begin
  if (firstcontour=nil) then firstcontour := c;
  if not(curcontour=nil) then begin
    curcontour.next := c;
  end;
  curcontour := c;
end;

function TcontourList.new_contour() : Tcontour;
var c : Tcontour;
begin
  c := Tcontour.create();
  add_contour(c);
  new_contour := c;
end;


procedure TcontourList.release_contours();
var c,c2 : Tcontour;
begin
  c := firstcontour;
  while not(c=nil) do begin
    c2 := c.next;
    c.destroy();
    c := c2;
  end;
  firstcontour := nil;
  curcontour := nil;
end;


function TcontourList.first_contour() : Tcontour;
begin
  first_contour := firstcontour;
end;

function TcontourList.next_contour(c : Tcontour) : Tcontour;
begin
  next_contour := c;
  if not(c=nil) then begin
    next_contour := c.next;
  end;
end;



begin
end.












