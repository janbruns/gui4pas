{$mode objfpc}
unit unicode_bidir_brackets;

interface

{ check wether v is an opening bracket,
  return 0 if it isn't, return paired-codepoint otherwise
}
function check_opening_bracket(v : dword) : dword;

{ check wether v is an closing bracket,
  return 0 if it isn't, return paired-codepoint otherwise
}
function check_closing_bracket(v : dword) : dword;

function canonical_bracketeq(v : dword) : dword;

implementation

type
Tbrackval = word;
Pbrackval = ^Tbrackval;

const 
num_bracks = 64;
openbracket : array[0..num_bracks-1] of Tbrackval = (
  $0028,$005B,$007B,$0F3A,$0F3C,
  $169B,
  $2045,$207D,$208D,
  $2308,$230A,$2329,
  $2768,$276A,$276C,$276E,$2770,$2772,$2774,$27C5,$27E6,$27E8,$27EA,$27EC,$27EE,
  $2983,$2985,$2987,$2989,$298B,$298D,$298F,$2991,$2993,$2995,$2997,$29D8,$29DA,$29FC,
  $2E22,$2E24,$2E26,$2E28,$2E55,$2E57,$2E59,$2E5B,
  $3008,$300A,$300C,$300E,$3010,$3014,$3016,$3018,$301A,
  $FE59,$FE5B,$FE5D,
  $FF08,$FF3B,$FF5B,$FF5F,$FF62
);
closebracket : array[0..num_bracks-1] of Tbrackval = (
  $0029,$005D,$007D,$0F3B,$0F3D,
  $169C,
  $2046,$207E,$208E,
  $2309,$230B,$232A,$2769,
  $276B,$276D,$276F,$2771,$2773,$2775,$27C6,$27E7,$27E9,$27EB,$27ED,$27EF,
  $2984,$2986,$2988,$298A,$298C,$298E,$2990,$2992,$2994,$2996,$2998,$29D9,$29DB,$29FD,
  $2E23,$2E25,$2E27,$2E29,$2E56,$2E58,$2E5A,$2E5C,
  $3009,$300B,$300D,$300F,$3011,$3015,$3017,$3019,$301B,
  $FE5A,$FE5C,$FE5E,
  $FF09,$FF3D,$FF5D,$FF60,$FF63
);



function find(v,n : dword; arr : Pbrackval) : dword;
var l,r,m : longint;
begin
  l := 0;
  r := n-1;
  while (l<=r) do begin
    m := (l+r) div 2;
    if arr[m]<v then begin
      l := m+1;
    end else if arr[m]>v then begin
      r := m-1;
    end else begin
      find := m;
      exit;
    end;
  end;
  find := n;
end;


function check_opening_bracket(v : dword) : dword;
begin
  v := find(v,num_bracks,@openbracket[0]);
  if v=num_bracks then begin
    result := 0;
  end else begin
    result := closebracket[v];
  end;
end;

function check_closing_bracket(v : dword) : dword;
begin
  v := find(v,num_bracks,@closebracket[0]);
  if v=num_bracks then begin
    result := 0;
  end else begin
    result := openbracket[v];
  end;
end;

function canonical_bracketeq(v : dword) : dword;
begin
  case v of
    $2329 : result := $3008;
    $232A : result := $3009;
    else result := v;
  end;
end;


begin
end.
