{$mode objfpc}
{$unitpath .}
{$unitpath ../../misc}
unit unicode_bidiralgo;

{ Reference: https://www.unicode.org/reports/tr9/tr9-46.html 
  
  about calculating the display-line order of unicode text streams
}

interface
uses 
  unicode_bidicls,
  unicode_bidir_brackets,
  unicode_utf8,
  sort,
  sysutils,math;


type
Tabstract_bidir_algo = class
  public
  constructor create(); virtual;
  destructor destroy(); override;
  procedure load_from_UTF8(s : ansistring); virtual;
  procedure load_from_codepoints(p : Pdword; cnt : dword); virtual;
  procedure onload(); virtual; abstract;
  procedure preprocess(); virtual; abstract;
  procedure calc_line_order(a,b : dword); virtual; abstract;
  { in case the algorithm decides to virtually delete symbols
    this could mean such symbold aren't in order
  }
  function charexists(i : dword) : boolean; virtual; abstract;
  function is_rtl(i : dword) : boolean; virtual; abstract;
  
  public
  chars : array of dword; // const copy of input

  { the thing that gets reordered instead of the chars }
  charidx : array of dword; // <-- result, but also used as input
end;
Tabstract_bidir_algo_cls = class of Tabstract_bidir_algo;



Tdirectionalisolatestatus = boolean;
Tdirectionaloverridestatus = (dovrr_neutral, dovrr_LTR, dovrr_RTL );
Tstackstatus = record
  emdlvl : byte;
  overridestatus : Tdirectionaloverridestatus;
  isolatestatus : Tdirectionalisolatestatus;
  isoinitpos : dword;
end;
Tlevelrun = record
  starti,endi : dword;
  nxt : longint;
  isostart,isopart : boolean;
end;
Tdbldword = record
  a,b : dword;
end;
Tisorunseq = record
  startlevelrun : longint; // the level-runs have next-"pointers"
  sos,eos : byte;
end;
Pdbldword=^Tdbldword;



Tunicode_bidir_algo = class(Tabstract_bidir_algo)
  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onload(); override;
  procedure preprocess(); override;
  procedure calc_line_order(a,b : dword); override;
  procedure printstate();
  function charexists(i : dword) : boolean; override;
  function is_rtl(i : dword) : boolean; override;

  { these must be public for tests }
  procedure do_L1(a,b : dword);
  procedure do_L2(a,b : dword);

  protected // relatively high level outer methods
  procedure do_x1(starti,endi : dword);
  procedure dox9();  
  procedure make_levelruns();
  procedure make_isoruns();
  procedure do_wni_each_isoseq();
  
  
  private
  { processing rtl/ltr embeds, see reference }
  procedure doX2345(i : dword; doX35,doX45 : boolean);
  procedure doX5abc(i : dword; doX5b,doX5c : boolean);
  procedure doX6(i : dword);
  procedure doX6a(i : dword);
  procedure doX7(i : dword);
  
  
  { to make implementation of the weak resolving rules
    easy, we'll put all char positions within
    isolating-run sequences into a plain array,
    so we don't need to handle levelrun-breaks and
    "deleted" chars there
  }
  procedure get_isorun_idxarr(startlr : longint);
  function calc_sos(i : dword) : byte;
  function calc_eos(i : dword) : byte;
  
  { weak resolving methods (see reference) }
  procedure do_w1();
  procedure do_w2();
  procedure do_w3();
  procedure do_w4();
  procedure do_w5();
  procedure do_w6();
  procedure do_w7();
  procedure do_n0();
  procedure do_n1();
  procedure do_n2();
  procedure do_i1();
  procedure do_i2();
  procedure do_w1_w7();
  procedure do_n0_n2();
  procedure do_i1_i2();
  procedure do_wni();
    
  function do_P23(i : dword; p23stop_on_unmatched_pdi : boolean) : byte;
  procedure apply_char_l_t(i : dword);
  function is_brack_rtl(t : byte) : boolean;
  procedure update_postbracket(i : dword);
  procedure do_swap(a,b : dword);
  procedure do_reverse_seq(starti,endi : dword);
  procedure do_reverse_level(lvl : byte; a,b : dword);
  procedure set_seq_t(starti,endi : dword; t : byte);
  procedure get_bracketlist();
  procedure sort_bracklist();

  
  public
  chari : array of dword; // not to confuse with charidx
  chart : array of byte;
  charl : array of byte;
  levelruns : array of Tlevelrun;
  isoruns : array of Tisorunseq;
  isorun_idxarr : array of dword;

  setting_embed : byte; // 0 : auto, 1 : ltr, 2 : rtl
  paragraph_embedding_level : byte;
  
  private
  stackdat : array[0..255] of Tstackstatus;
  stackpt, valid_isolate_count : byte;
  overflow_isolate_count, overflow_embedding_count : dword;
  endofparagraph,startofparagraph : dword;
  
  sos,eos : byte; // computed for isorun boundaries using calc_sos()/cal_eos()

  bracketlist : array of Tdbldword;
  bracklistlen : longint;
end;




implementation

const
max_depth = 125;
invalid_lvl = 255;
brackgrowconst = 16;


procedure Tunicode_bidir_algo.printstate();
var i : dword;
begin
  write('state chart: ');
  for i := 0 to high(chars) do begin
    write(chart[i]:3);
  end;
  writeln;
  write('state charl: ');
  for i := 0 to high(chars) do begin
    write(charl[i]:3);
  end;
  writeln;
end;

procedure Tunicode_bidir_algo.do_x1(starti,endi : dword);
var i : dword;
begin
  starti := min(high(chars),starti);
  endi := min(high(chars),endi);
  endofparagraph := endi;
  startofparagraph := starti;
  for i := starti to endi do chari[i]:=endi+1;
  stackpt := 0;
  valid_isolate_count := 0;
  overflow_isolate_count := 0;
  overflow_embedding_count := 0;
  if setting_embed=0 then paragraph_embedding_level := do_P23(0,false)
  else paragraph_embedding_level := setting_embed-1;
  with stackdat[stackpt] do begin
    emdlvl := paragraph_embedding_level;
    isolatestatus := false;
    overridestatus := dovrr_neutral;
    isoinitpos := endofparagraph+1;
  end;
  for i := starti to endi do begin
    case chart[i] of
      bidicls_Right_To_Left_Embedding : doX2345(i,false,false );
      bidicls_Left_To_Right_Embedding : doX2345(i,true, false );
      
      bidicls_Right_To_Left_Override  : doX2345(i,false,true  );
      bidicls_Left_To_Right_Override  : doX2345(i,true, true  );
      
      bidicls_Right_To_Left_Isolate   : doX5abc(i,false,false );
      bidicls_Left_To_Right_Isolate   : doX5abc(i,true, false );
      
      bidicls_First_Strong_Isolate    : doX5abc(i,false,true  );
      
      bidicls_Pop_Directional_Isolate : doX6a(i);
      bidicls_Pop_Directional_Format  : doX7(i);
      
      bidicls_Paragraph_Separator : charl[i] := paragraph_embedding_level;
      bidicls_Boundary_Neutral : ;
      
      else doX6(i);
    end;
  end;
end;



procedure Tunicode_bidir_algo.doX2345(i : dword; doX35,doX45 : boolean);
var cl,x : byte;
begin
  x := 0;
  if doX35 then x += 1;
  cl := stackdat[stackpt].emdlvl;
  cl += 1;
  if (cl and 1)=x then cl+= 1;
  if  (overflow_isolate_count=0)
  and (overflow_embedding_count=0)
  and (cl<=max_depth) then begin
    stackpt += 1;
    with stackdat[stackpt] do begin
      emdlvl :=cl;
      isolatestatus := false;
      isoinitpos := endofparagraph+1;
      if doX45 then begin
        if doX35 
        then overridestatus := dovrr_LTR // X5
        else overridestatus := dovrr_RTL // X4
      end else overridestatus := dovrr_neutral; // X2,X3
    end;
  end else begin
    if (overflow_isolate_count=0) then overflow_embedding_count+=1;
  end;
  // nothing done with the char's lvl
end;

procedure Tunicode_bidir_algo.doX5abc(i : dword; doX5b,doX5c : boolean);
var cl,x : byte;
begin
  if doX5c then begin
    if do_P23(i+1,true)=1 then doX5b := false else doX5b := true;
    //writeln('FSI_left:',dox5b);
  end;
  apply_char_l_t(i);
  x := 0;
  if doX5b then x += 1;
  cl := stackdat[stackpt].emdlvl;
  cl += 1;
  if (cl and 1)=x then cl+= 1;
  if  (overflow_isolate_count=0)
  and (overflow_embedding_count=0)
  and (cl<=max_depth) then begin
    valid_isolate_count += 1;
    stackpt += 1;
    with stackdat[stackpt] do begin
      emdlvl :=cl;
      isolatestatus := true;
      overridestatus := dovrr_neutral;
      isoinitpos := i;
    end;
  end else begin
    overflow_isolate_count+=1;
  end;
end;

procedure Tunicode_bidir_algo.apply_char_l_t(i : dword);
begin
  charl[i] := stackdat[stackpt].emdlvl;
  if stackdat[stackpt].overridestatus<>dovrr_neutral then begin
    if stackdat[stackpt].overridestatus=dovrr_LTR then begin
//      writeln('override char at ',i,' chartype ',chart[i],' => ',bidicls_Left_To_Right);
      chart[i] := bidicls_Left_To_Right;
    end else begin
//      writeln('override char at ',i,' chartype ',chart[i],' => ',bidicls_Right_To_Left);
      chart[i] := bidicls_Right_To_Left;
    end;
  end;
end;

procedure Tunicode_bidir_algo.doX6(i : dword);
begin
  apply_char_l_t(i);
end;

procedure Tunicode_bidir_algo.doX6a(i : dword);
begin
  if overflow_isolate_count>0 then begin
    overflow_isolate_count-=1
  end else begin
    if valid_isolate_count=0 then begin
    end else begin
      overflow_embedding_count := 0;
      while (stackpt>0)and(stackdat[stackpt].isolatestatus=false) do stackpt -=1;
      if stackdat[stackpt].isoinitpos <= endofparagraph then begin
        chari[stackdat[stackpt].isoinitpos] := i;
      end;
      if (stackpt>0) then stackpt -=1;
      valid_isolate_count -= 1;
    end;
  end;
  apply_char_l_t(i);
end;

procedure Tunicode_bidir_algo.doX7(i : dword);
begin
  if overflow_isolate_count>0 then begin
  end else begin
    if overflow_embedding_count>0 then begin
      overflow_embedding_count -= 1;
    end else begin
      if (stackpt>0)and(stackdat[stackpt].isolatestatus=false) then stackpt -=1;
    end;
  end;
  //apply_char_l_t(i);
end;

function is_ignore_type(v : byte) : boolean;
begin
  is_ignore_type := 
      (v=bidicls_Left_To_Right_Embedding)
    or(v=bidicls_Right_To_Left_Embedding)
    or(v=bidicls_Left_To_Right_Override)
    or(v=bidicls_Right_To_Left_Override)
    or(v=bidicls_Pop_Directional_Format)
    or(v=bidicls_Boundary_Neutral);
end;

function Tunicode_bidir_algo.charexists(i : dword) : boolean;
begin
  charexists := (chart[i]<>0);
end;

procedure Tunicode_bidir_algo.dox9();
var i : dword;
begin
  for i := startofparagraph to endofparagraph do begin
    if (get_unicode_bidi_class(chars[i])=bidicls_Pop_Directional_Format)
    and (chart[i]<>bidicls_Pop_Directional_Format) then begin
      writeln('PDF overwritten to: ',chart[i]); readln;
    end;
    if is_ignore_type(chart[i]) then begin
      chart[i] := 0;
    end;
  end;
  while (startofparagraph<=endofparagraph) do begin
    if not charexists(startofparagraph) 
    then startofparagraph+=1
    else break;
  end;
  while (startofparagraph<=endofparagraph) do begin
    if not charexists(endofparagraph) 
    then endofparagraph-=1
    else break;
  end;
end;


function Tunicode_bidir_algo.do_P23(i : dword; p23stop_on_unmatched_pdi : boolean) : byte;
var c : dword;
begin
  c := 0;
  while (i<=endofparagraph) do begin
    case chart[i] of
      bidicls_Left_To_Right : begin
        if (c=0) then begin
          do_P23 := 0;
          exit;
        end;
      end;
      bidicls_Right_To_Left, bidicls_Arabic_Letter : begin
        if (c=0) then begin
          do_P23 := 1;
          exit;
        end;
      end;
      bidicls_Left_To_Right_Isolate,
      bidicls_Right_To_Left_Isolate,
      bidicls_First_Strong_Isolate : begin
        c += 1;
      end;
      bidicls_Pop_Directional_Isolate : begin
        if c>0 then c-=1 else begin
          if p23stop_on_unmatched_pdi then begin
            do_P23 := 0;
            exit;
          end;
        end;
      end;
    end;
    i += 1;
  end;
  do_P23 := 0;
end;


procedure Tunicode_bidir_algo.make_levelruns();
var lastlvl : byte; c,i,lasti : dword;
begin
  if (startofparagraph>endofparagraph) then begin
    setlength(levelruns,0);
    exit;
  end;
  lastlvl := charl[startofparagraph];
  c := 1;
  for i := startofparagraph to endofparagraph do begin
    if charexists(i) then begin
      if charl[i]<>lastlvl then begin
        lastlvl := charl[i];
        inc(c);
      end;
    end;
  end;
  setlength(levelruns,c);
  c := 0;
  levelruns[c].starti := startofparagraph;
  lasti := startofparagraph;
  lastlvl := charl[lasti];
  for i := startofparagraph to endofparagraph do begin
    if charexists(i) then begin
      if charl[i]<>lastlvl then begin
        levelruns[c].endi := lasti;
        lastlvl := charl[i];
        inc(c);
        if (c>high(levelruns)) then raise Exception.create('prob in make_levelruns');
        levelruns[c].starti := i;
      end;
      lasti := i;
    end;
  end;
  levelruns[c].endi := endofparagraph;
end;


function is_isolate_initiator(c : dword) : boolean;
begin
  is_isolate_initiator := false;
  case c of
    bidicls_Left_To_Right_Isolate,
    bidicls_Right_To_Left_Isolate,
    bidicls_First_Strong_Isolate : is_isolate_initiator := true;
  end;
end;

procedure Tunicode_bidir_algo.make_isoruns();
var i,j,x : longint; k : dword; isocnt : longint;
begin
  isocnt := 0;
  for i := low(levelruns) to high(levelruns) do levelruns[i].nxt := -1;
  for i := low(levelruns) to high(levelruns) do levelruns[i].isostart := false;
  for i := low(levelruns) to high(levelruns) do levelruns[i].isopart := false;
  for i := low(levelruns) to high(levelruns) do begin
    if not levelruns[i].isopart then begin
      levelruns[i].isopart := true;
      levelruns[i].isostart := true;
      //writeln('new iso-seqence at levelrun ',i);
      isocnt += 1;
      j := i;
      while is_isolate_initiator(chart[levelruns[j].endi]) do begin
        x := j;
        k := chari[levelruns[j].endi];
        if (k<=endofparagraph) then begin
          { the isolate-initiator has a known matching PDI, which will be at
            the start of a levelrun 
          }
          while (levelruns[j].starti<k)and(j<high(levelruns)) do inc(j);
          if (levelruns[j].starti=k) then begin
            levelruns[x].nxt := j;
            levelruns[j].isopart := true;
            //writeln('glue levelrun ',x,' => ',j);
            x := j;
          end else raise exception.create('unexpected fail in finding isorun-glue');
        end else begin
          { the isolate-initiator might still have an unidentified matching PDI
            in case of overflow, but we'll simply break up the sequence in that case
          }
          break;
        end;
      end;
    end;
  end;
  setlength(isoruns,isocnt);
  isocnt := 0;
  for i := low(levelruns) to high(levelruns) do begin
    if levelruns[i].isostart then begin
      isoruns[isocnt].startlevelrun := i;
      isoruns[isocnt].sos := calc_sos(levelruns[i].starti);
      j := i;
      while (levelruns[j].nxt>=0) do j := levelruns[j].nxt;
      isoruns[isocnt].eos := calc_eos(levelruns[i].endi);
      isocnt += 1;
    end;
  end;
end;


procedure Tunicode_bidir_algo.get_isorun_idxarr(startlr : longint);
var i,cnt,endi : dword; lr : longint;
begin
  setlength(isorun_idxarr,0);
  cnt := 0;
  lr := startlr;
  while lr>=0 do begin
    endi := levelruns[lr].endi;
    for i := levelruns[lr].starti to endi do begin
      if charexists(i) then inc(cnt);
    end;
    lr := levelruns[lr].nxt;
  end;
  setlength(isorun_idxarr,cnt);
  cnt := 0;
  lr := startlr;
  while lr>=0 do begin
    endi := levelruns[lr].endi;
    for i := levelruns[lr].starti to endi do begin
      if charexists(i) then begin
        isorun_idxarr[cnt] := i;
        inc(cnt);
      end;
    end;
    lr := levelruns[lr].nxt;
  end;
end;



function Tunicode_bidir_algo.calc_sos(i : dword) : byte;
var t : byte;
begin
  t := charl[i];
  if (i>startofparagraph) then begin
    i -= 1;
    while (i>startofparagraph) and not( charexists(i) ) do dec(i);
    if (i>=startofparagraph) and charexists(i) then begin
      t := max(t, charl[i]);
    end else begin
      t := max(t, paragraph_embedding_level);
    end;
  end else begin
    t := max(t, paragraph_embedding_level);
  end;
  if (t and 1)=1 
  then t := bidicls_Right_To_Left 
  else t := bidicls_Left_To_Right;
  result := t;
end;


function Tunicode_bidir_algo.calc_eos(i : dword) : byte;
var t : byte;
begin
  t := charl[i];
  if (i<endofparagraph) 
  and not is_isolate_initiator(chart[i])
  then begin
    i += 1;
    while (i<endofparagraph) and not( charexists(i) ) do inc(i);
    if (i<=endofparagraph) and charexists(i) then begin
      t := max(t,charl[i]);
    end else begin
      t := max(t,paragraph_embedding_level);
    end;
  end else begin
    t := max(t,paragraph_embedding_level);
  end;
  if (t and 1)=1 
  then t := bidicls_Right_To_Left
  else t := bidicls_Left_To_Right;
  result := t;
end;




procedure Tunicode_bidir_algo.do_w1();
var i,c : dword; lastt : byte;
begin
  lastt := sos;
  for i := 0 to high(isorun_idxarr) do begin
    c := isorun_idxarr[i];
    if chart[c]=bidicls_Nonspacing_Mark then begin
      if is_isolate_initiator(lastt) 
      or (lastt=bidicls_Pop_Directional_Isolate) then begin
        chart[c] := bidicls_Other_Neutral;
      end else begin
        chart[c] := lastt;
      end;
    end;
    lastt := chart[c];
  end;
end;

procedure Tunicode_bidir_algo.do_w2();
var i,j,c,c2 : dword; t : byte;
begin
  for i := 0 to high(isorun_idxarr) do begin
    c := isorun_idxarr[i];
    if chart[c]=bidicls_European_Number then begin
      t := sos;
      if i>0 then for j := i-1 downto 0 do begin
        c2 := isorun_idxarr[j];
        case chart[c2] of
          bidicls_Left_To_Right,
          bidicls_Right_To_Left,
          bidicls_Arabic_Letter : begin
            t := chart[c2];
            break;
          end;
        end;
      end;
      if t=bidicls_Arabic_Letter then begin
        chart[c] := bidicls_Arabic_Number;
      end;
    end;
  end;
end;

procedure Tunicode_bidir_algo.do_w3();
var i,c : dword;
begin
  for i := 0 to high(isorun_idxarr) do begin
    c := isorun_idxarr[i];
    if chart[c]=bidicls_Arabic_Letter then begin
      chart[c] := bidicls_Right_To_Left;
    end;
  end;
end;

procedure Tunicode_bidir_algo.do_w4();
var i,j,c,cl,cr : dword;
begin
  j := high(isorun_idxarr);
  if j>1 then begin
    j := j-1;
    for i := 1 to j do begin
      c := isorun_idxarr[i];
      cl := isorun_idxarr[i-1];
      cr := isorun_idxarr[i+1];
      if (chart[cl]=chart[cr]) then begin
        case chart[c] of
          bidicls_European_Separator : begin
            if chart[cl]=bidicls_European_Number then begin
              chart[c] := chart[cl];
            end;
          end;
          bidicls_Common_Separator : begin
            case chart[cl] of
              bidicls_European_Number,
              bidicls_Arabic_Number : begin
                chart[c] := chart[cl];
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure Tunicode_bidir_algo.do_w5();
var i,c : dword; t : byte;
begin
  t := sos;
  for i := 0 to high(isorun_idxarr) do begin
    c := isorun_idxarr[i];
    if t=bidicls_European_Number then begin
      if chart[c]=bidicls_European_Terminator then begin
        chart[c]:=t;
      end;
    end;
    t := chart[c];
  end;
  t := eos;
  for i := high(isorun_idxarr) downto 0 do begin
    c := isorun_idxarr[i];
    if t=bidicls_European_Number then begin
      if chart[c]=bidicls_European_Terminator then begin
        chart[c]:=t;
      end;
    end;
    t := chart[c];
  end;
end;

procedure Tunicode_bidir_algo.do_w6();
var i,c : dword;
begin
  for i := 0 to high(isorun_idxarr) do begin
    c := isorun_idxarr[i];
    case chart[c] of
      bidicls_Common_Separator,
      bidicls_European_Terminator,
      bidicls_European_Separator : begin
        chart[c] := bidicls_Other_Neutral;
      end;
    end;
  end;
end;

procedure Tunicode_bidir_algo.do_w7();
var i,j,c,c2 : dword; t : byte;
begin
  for i := 0 to high(isorun_idxarr) do begin
    c := isorun_idxarr[i];
    if chart[c]=bidicls_European_Number then begin
      t := sos;
      for j := i downto 0 do begin
        c2 := isorun_idxarr[j];
        case chart[c2] of
          bidicls_Left_To_Right,
          bidicls_Right_To_Left : begin
            t := chart[c2];
            break;
          end;
        end;
      end;
      if t=bidicls_Left_To_Right then begin
        chart[c] := t;
      end;
    end;
  end;
end;


function check_opening_bracket_bd14(c : dword; t : byte) : dword;
begin
  check_opening_bracket_bd14 := 0;
  if t=bidicls_Other_Neutral
  then check_opening_bracket_bd14 := check_opening_bracket(canonical_bracketeq(c));
end;

function check_closing_bracket_bd15(c : dword; t : byte) : dword;
begin
  check_closing_bracket_bd15 := 0;
  if t=bidicls_Other_Neutral
  then check_closing_bracket_bd15 := check_closing_bracket(canonical_bracketeq(c));
end;


procedure Tunicode_bidir_algo.get_bracketlist();
var brackstackp,j : longint; i,v,c : dword;
var brackstack : array[0..62] of Tdbldword;
begin
//writeln('get bracket list');
  bracklistlen := 0;
  brackstackp := 0;
  for i := 0 to high(isorun_idxarr) do begin
    c := isorun_idxarr[i];
    //writeln('at idx ',c);
    v := check_opening_bracket_bd14(chars[c],chart[c]);
    if v<>0 then begin
      if brackstackp<=high(brackstack) then begin
//writeln('push brackstackat at idx ',c,' v=',v);
        brackstack[brackstackp].a := i;
        brackstack[brackstackp].b := canonical_bracketeq(v);
        brackstackp += 1;
      end else begin
        bracklistlen := 0;
        exit;
      end;
    end else begin
      v := check_closing_bracket_bd15(chars[c],chart[c]);
      if v<>0 then begin
        //writeln('check bracket stack for match at pos ',c);
        for j := brackstackp-1 downto 0 do begin
          //writeln('cmp stack element ',j,' for match');
          if (brackstack[j].b=canonical_bracketeq(chars[c])) then begin
            //writeln('Match found');
            if bracklistlen>high(bracketlist) then begin
              setlength(bracketlist,bracklistlen+brackgrowconst);
            end;
            bracketlist[bracklistlen].a := brackstack[j].a;
            bracketlist[bracklistlen].b := i;
            bracklistlen += 1;
            brackstackp := j;
            break;
          end;
        end;
      end else begin
      end;
    end;
  end;
  sort_bracklist();
end;


function cmp_bracklist(pl : pointer; a,b : Tcmpindex; ud : pointer) : boolean;
begin
  cmp_bracklist := (Pdbldword(pl)[b].a<Pdbldword(pl)[a].a);
end;

procedure swp_bracklist(pl : pointer; a,b : Tcmpindex; ud : pointer);
var tmp : Tdbldword;
begin
  tmp := Pdbldword(pl)[a];
  Pdbldword(pl)[a] := Pdbldword(pl)[b];
  Pdbldword(pl)[b] := tmp;
end;

procedure Tunicode_bidir_algo.sort_bracklist();
var i : longint;
begin
  if bracklistlen>1 
  then qsort(@bracketlist[0], 0,bracklistlen-1, @cmp_bracklist, @swp_bracklist,nil);
  for i := 1 to bracklistlen-1 do begin
    if bracketlist[i].a<bracketlist[i-1].a then raise exception.create('SORTERR');
  end;
end;

procedure Tunicode_bidir_algo.update_postbracket(i : dword);
var nt : byte; c : dword;
begin
  nt := chart[isorun_idxarr[i]];
//write('update_postbracket ',nt);
  i += 1;
  while i<=high(isorun_idxarr) do begin
    //write('.');
    c := isorun_idxarr[i];
    if get_unicode_bidi_class(chars[c])=bidicls_Nonspacing_Mark then begin
      chart[c] := nt;
    end else break;
    i += 1;
  end;
  //writeln;
end;

function Tunicode_bidir_algo.is_brack_rtl(t : byte) : boolean;
begin
  result := false;
  case t of
    bidicls_Right_To_Left,
    bidicls_Arabic_Letter,
    bidicls_European_Number,
    bidicls_Arabic_Number : result := true;
  end;
end;

procedure Tunicode_bidir_algo.do_n0();
var i,j,endi,c : dword; t : byte; b : longint; foundo : boolean;
begin
//writeln('n0 here');
//printstate();
//writeln('char-indices to process:');
//for i := low(isorun_idxarr) to high(isorun_idxarr) do write(isorun_idxarr[i],' ');
//writeln;
  get_bracketlist();
//writeln('found ',bracklistlen,' bracket pairs:');
//for b := 0 to bracklistlen-1 do begin
//  writeln(isorun_idxarr[bracketlist[b].a],'..',isorun_idxarr[bracketlist[b].b]);
//end;
  
  for b := 0 to bracklistlen-1 do begin
    endi := bracketlist[b].b;
    if endi>0 then begin
      endi -= 1;
      t := 0;
      foundo := false;
      for i := bracketlist[b].a+1 to endi do begin
        c := isorun_idxarr[i];
        if (charl[c] and 1)=1 then begin
          if is_brack_rtl(chart[c]) then begin // =bidicls_Right_To_Left
            t := chart[c];
            break;
          end else if chart[c]=bidicls_Left_To_Right then begin
            foundo := true;
          end;
        end else begin
          if is_brack_rtl(chart[c]) then begin
            foundo := true;
          end else if chart[c]=bidicls_Left_To_Right then begin // =bidicls_Left_To_Right
            t := chart[c];
            break;
          end;
        end;
      end;
      if t<>0 then begin
        //writeln('strong e in bracket');
        if (chart[c]=bidicls_Left_To_Right) 
        then t := bidicls_Left_To_Right
        else t := bidicls_Right_To_Left;
        chart[isorun_idxarr[bracketlist[b].a]] := t;
        chart[isorun_idxarr[bracketlist[b].b]] := t;
        update_postbracket(bracketlist[b].b);
      end else if not foundo then begin
        // keep the brackets as they are, if no strong type inside
      end else begin
        //writeln('strong o in bracket');
        t := sos;
        for j := bracketlist[b].a downto 0 do begin
          c := isorun_idxarr[j];
          if (chart[c]=bidicls_Left_To_Right)
          or is_brack_rtl(chart[c])
          then begin
            //writeln('context also strong');
            if (chart[c]=bidicls_Left_To_Right) 
            then t := bidicls_Left_To_Right
            else t := bidicls_Right_To_Left;
            break;
          end;
        end;
        { the N0.c cases (1) and (2) }
        chart[isorun_idxarr[bracketlist[b].a]] := t;
        chart[isorun_idxarr[bracketlist[b].b]] := t;
        update_postbracket(bracketlist[b].b);
      end;
    end;
  end;
//writeln('n0 done');
//printstate();
end;

function is_NI(t : byte) : boolean;
begin
  result := false;
  case t of
    bidicls_Paragraph_Separator,
    bidicls_Segment_Separator,
    bidicls_White_Space,
    bidicls_Other_Neutral,
    bidicls_First_Strong_Isolate,
    bidicls_Right_To_Left_Isolate,
    bidicls_Left_To_Right_Isolate,
    bidicls_Pop_Directional_Isolate : result := true;
  end;
end;

function num_2_R(t : byte) : byte;
begin
  result := t;
  if (t=bidicls_European_Number)
  or (t=bidicls_Arabic_Number)
  then result := bidicls_Right_To_Left;
end;

function is_r_or_l(t : byte) : boolean;
begin
  result := false;
  if (t=bidicls_Right_To_Left)
  or (t=bidicls_Left_To_Right) then result := true;
end;

procedure Tunicode_bidir_algo.do_n1();
var lft,rgt : byte; i,starti,endi,c : dword; inNI : boolean;
begin
//writeln('n1 here');
//printstate();
  lft := sos;
  inNI := false;
  for i := 0 to high(isorun_idxarr) do begin
    c := isorun_idxarr[i];
    //writeln('check ',i,'=>',c, ' inNI:',inNI,' isNI():',is_NI(chart[c]),' lft:',lft);
    if is_NI(chart[c]) then begin
      if not inNI then begin
        starti := i;
        inNI := true;
      end;
    end else begin
      if inNI then begin
        endi := i-1;
        rgt := chart[c];
        //writeln('found NI seq ',starti,'..',endi,' lft=',lft,' rgt=',rgt);
        lft := num_2_R(lft);
        rgt := num_2_R(rgt);
        if (rgt=lft)and(is_r_or_l(lft)) then begin
          set_seq_t(starti,endi,lft);
        end;
      end else begin
      end;
      lft := chart[c];
      inNI := false;
    end;
  end;
  if inNI then begin
    rgt := eos;
    lft := num_2_R(lft);
    rgt := num_2_R(rgt);
    if (rgt=lft)and(is_r_or_l(lft)) then begin
      set_seq_t(starti,high(isorun_idxarr),lft);
    end;
  end;
//writeln('n1 done');
//printstate();
end;

procedure Tunicode_bidir_algo.set_seq_t(starti,endi : dword; t : byte);
var i,c : dword;
begin
  for i := starti to endi do begin
    c := isorun_idxarr[i];
    chart[c] := t;
  end;
end;


procedure Tunicode_bidir_algo.do_n2();
var c,i : dword; t : byte;
begin
  for i := 0 to high(isorun_idxarr) do begin
    c := isorun_idxarr[i];
    if is_NI(chart[c]) then begin
      if ((charl[c] and 1)=1) 
      then t := bidicls_Right_To_Left
      else t := bidicls_Left_To_Right;
      chart[c] := t;
    end;
  end;
end;

procedure Tunicode_bidir_algo.do_i1();
var c,i : dword;
begin
  c := isorun_idxarr[0];
  if ((charl[c] and 1)=0) then begin
    for i := 0 to high(isorun_idxarr) do begin
      c := isorun_idxarr[i];
      if chart[c]=bidicls_Right_To_Left then begin
        charl[c] += 1;
      end else begin
        if (chart[c]=bidicls_European_Number)
        or (chart[c]=bidicls_Arabic_Number) then begin
          charl[c] += 2;
        end;
      end;
    end;
  end else begin
    for i := 0 to high(isorun_idxarr) do begin
      c := isorun_idxarr[i];
      if chart[c]=bidicls_Left_To_Right then begin
        charl[c] += 1;
      end else begin
        if (chart[c]=bidicls_European_Number)
        or (chart[c]=bidicls_Arabic_Number) then begin
          charl[c] += 1;
        end;
      end;
    end;
  end;
end;

procedure Tunicode_bidir_algo.do_i2();
var c,i : dword;
begin
  c := isorun_idxarr[0];
  if ((charl[c] and 1)=1) then begin
    for i := 0 to high(isorun_idxarr) do begin
      c := isorun_idxarr[i];
      if chart[c]=bidicls_Left_To_Right then begin
        charl[c] += 1;
      end else begin
        if (chart[c]=bidicls_European_Number)
        or (chart[c]=bidicls_Arabic_Number) then begin
          charl[c] += 1;
        end;
      end;
    end;
  end;
end;

procedure Tunicode_bidir_algo.do_w1_w7();
begin
  do_w1();
  do_w2();
  do_w3();
  do_w4();
  do_w5();
  do_w6();
  do_w7();
end;

procedure Tunicode_bidir_algo.do_n0_n2();
begin
  do_n0();
  do_n1();
  do_n2();
end;

procedure Tunicode_bidir_algo.do_i1_i2();
begin
  do_i1();
  //do_i2();
end;

procedure Tunicode_bidir_algo.do_wni();
begin
  if high(isorun_idxarr)>=0 then begin
//    writeln('state befor w1-7');
//    printstate();
    do_w1_w7();
//    writeln('state befor n0-2');
//    printstate();
    do_n0_n2();
    
//    writeln('state befor i1-2');
//    printstate();
    do_i1_i2();
//    writeln('state after i1-2');
//    printstate();
  end;
end;

procedure Tunicode_bidir_algo.do_wni_each_isoseq();
var i : longint;
begin
  for i := 0 to high(isoruns) do begin
    get_isorun_idxarr(isoruns[i].startlevelrun);
    sos := isoruns[i].sos;
    eos := isoruns[i].eos;
    do_wni();
  end;
end;


procedure Tunicode_bidir_algo.do_L1(a,b : dword);
var i : dword; inseq : boolean;
begin
  inseq := true;
  for i := b downto a do begin
    if charexists(i) then begin
      case get_unicode_bidi_class(chars[i]) of
        bidicls_Paragraph_Separator,
        bidicls_Segment_Separator : begin
          charl[i] := paragraph_embedding_level;
          inseq := true;
        end;
        bidicls_White_Space,
        bidicls_Pop_Directional_Isolate,
        bidicls_First_Strong_Isolate,
        bidicls_Right_To_Left_Isolate,
        bidicls_Left_To_Right_Isolate : begin
          if inseq then begin
            charl[i] := paragraph_embedding_level;
          end;
        end;
        else inseq := false;
      end;
    end;
  end;
end;


procedure Tunicode_bidir_algo.do_swap(a,b : dword);
var tmp : dword;
begin
  { just swap the idx should do, shouldn't it?
    order-changes operate on sequences of
    same level chars at first, and the exact level
    drops influence later on.
  }
  tmp := charidx[a];
  charidx[a] := charidx[b];
  charidx[b] := tmp;
  
  { something's wrong with that theory, 
    for the test-usage at least 

  tmp := chars[a];
  chars[a] := chars[b];
  chars[b] := tmp;

  tmp := charl[a];
  charl[a] := charl[b];
  charl[b] := tmp;
  }  
  tmp := chart[a];
  chart[a] := chart[b];
  chart[b] := tmp;
end;

procedure Tunicode_bidir_algo.do_reverse_seq(starti,endi : dword);
begin
  while (endi>starti) do begin
    do_swap(starti,endi);
    starti += 1;
    if endi>0 then endi -= 1 else break;
  end;
end;

procedure Tunicode_bidir_algo.do_reverse_level(lvl : byte; a,b : dword);
var i,starti : dword; inseq : boolean;
begin
  //writeln('reverse lvl ',lvl,' and above');
  inseq := false;
  for i := a to b do begin
    if charexists(i) then begin
      if charl[i]>=lvl then begin
        if (inseq=false) then starti := i;
        inseq := true;
      end else begin
        if inseq then begin
          do_reverse_seq(starti,i-1);
        end;
        inseq := false;
      end;
    end;
  end;
  if inseq then do_reverse_seq(starti,b);
end;




procedure Tunicode_bidir_algo.do_L2(a,b : dword);
var i,maxl,minl : dword;
begin
  maxl := 0;
  minl := 1000;
  for i := a to b do begin
    if charexists(i) then begin
      maxl := max(maxl,charl[i]);
      if (charl[i] and 1)=1 then minl := min(minl,charl[i]);
      charidx[i] := i;
    end;
  end;
  for i := maxl downto minl do do_reverse_level(i,a,b);
end;


procedure Tunicode_bidir_algo.preprocess();
begin
  do_x1(0,high(chars));
  dox9();
  make_levelruns();
  make_isoruns();
  do_wni_each_isoseq();
end;

procedure Tunicode_bidir_algo.calc_line_order(a,b : dword);
begin
  do_L1(a,b);
  do_L2(a,b);
end;


procedure Tunicode_bidir_algo.onload();
var i : dword;
begin
  i := high(chars)+1;
  setlength(chart,i);
  setlength(charl,i);
  setlength(chari,i);
  setlength(charidx,i);
  for i := 0 to high(chars) do begin
    chart[i] := get_unicode_bidi_class(chars[i]);
    charl[i] := invalid_lvl;
    charidx[i] := i;
  end;
end;

function Tunicode_bidir_algo.is_rtl(i : dword) : boolean;
begin
  result := false;
  if i<=high(charl) then result := ((charl[i] and 1)>0);
end;

procedure Tabstract_bidir_algo.load_from_UTF8(s : ansistring);
var i,j : dword;
begin
  j := 0;
  i := 1;
  while (i<=length(s)) do begin
    i += UTF8_codelen(s[i]);
    j += 1;
  end;
  setlength(chars,j);
  j := 0;
  i := 1;
  while (i<=length(s)) do begin
    chars[j] := UTF8_getcodepoint_autoinc(s,i);
    j += 1;
  end;
  onload();
end;

procedure Tabstract_bidir_algo.load_from_codepoints(p : Pdword; cnt : dword);
begin
  setlength(chars,cnt);
  move(p^,chars[0],cnt*sizeof(dword));
  onload();
end;


constructor Tunicode_bidir_algo.create();
begin
end;
destructor Tunicode_bidir_algo.destroy();
begin
end;
constructor Tabstract_bidir_algo.create();
begin
end;
destructor Tabstract_bidir_algo.destroy();
begin
end;

begin
end.
