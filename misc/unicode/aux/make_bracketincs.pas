{$mode objfpc}

function todot(var s : ansistring; const dot : shortstring) : ansistring;
var i : longint;
begin
  i := pos(dot,s);
  if (i>0) then begin
    result := copy(s,1,i-1);
    s := copy(s,i+length(dot));
  end else begin
    result := s;
    s := '';
  end;
end;

function fromdoton(const s : ansistring; const dot : shortstring) : ansistring;
var i : longint;
begin
  i := pos(dot,s);
  if (i>0) then begin
    result := copy(s,i+length(dot));
  end else begin
    result := '';
  end;
end;

procedure removechar(var s : ansistring; c : char);
var i : longint;
begin
  i := 1;
  while (i<=length(s)) do begin
    if (s[i]=c) then delete(s,i,1) else i += 1;
  end;
end;


var
f : text;
s,s2,s3 : ansistring;

begin
  assign(f,'BidiBrackets.txt');
  reset(f);
  repeat 
    readln(f,s);
    s := todot(s,'#');
    if s<>'' then begin
      removechar(s,' ');
      s2 := todot(s,';');
      s3 := todot(s,';');
      if s=paramstr(1) then begin
        writeln('$',s2,',');
      end;
    end;
  until eof(f);
  close(f);
end.
