{$unitpath .}
unit xshm;
interface
uses
  ctypes,x,dynxlib,xge;


{
  Automatically converted by H2Pas 0.99.15 from xshm.h
  The following command line parameters were used:
    -p
    -T
    -S
    -d
    -c
    xshm.h
}

{$PACKRECORDS C}


const
   X_ShmQueryVersion = 0;
   X_ShmAttach = 1;
   X_ShmDetach = 2;
   X_ShmPutImage = 3;
   X_ShmGetImage = 4;
   X_ShmCreatePixmap = 5;
   ShmCompletion = 0;
   ShmNumberEvents = ShmCompletion + 1;
   BadShmSeg = 0;
   ShmNumberErrors = BadShmSeg + 1;
type

   PShmSeg = ^TShmSeg;
   TShmSeg = culong;

   PXShmCompletionEvent = ^TXShmCompletionEvent;
   TXShmCompletionEvent = record
        _type : cint;
        serial : culong;
        send_event : TBool;
        display : PDisplay;
        drawable : TDrawable;
        major_code : cint;
        minor_code : cint;
        shmseg : TShmSeg;
        offset : culong;
     end;

   PXShmSegmentInfo = ^TXShmSegmentInfo;
   TXShmSegmentInfo = record
        shmseg : TShmSeg;
        shmid : cint;
        shmaddr : Pchar;
        readOnly : TBool;
     end;

T_XShmQueryExtension=function(para1:PDisplay):TBoolResult;cdecl;
T_XShmGetEventBase=function(para1:PDisplay):cint;cdecl;
T_XShmQueryVersion=function(para1:PDisplay; para2:Pcint; para3:Pcint; para4:PBool):TBoolResult;cdecl;
T_XShmPixmapFormat=function(para1:PDisplay):cint;cdecl;
T_XShmAttach=function(para1:PDisplay; para2:PXShmSegmentInfo):TStatus;cdecl;
T_XShmDetach=function(para1:PDisplay; para2:PXShmSegmentInfo):TStatus;cdecl;
T_XShmPutImage=function(para1:PDisplay; para2:TDrawable; para3:TGC; para4:PXImage; para5:cint; para6:cint; para7:cint; para8:cint; para9:cuint; para10:cuint; para11:TBool):TStatus;cdecl;
T_XShmGetImage=function(para1:PDisplay; para2:TDrawable; para3:PXImage; para4:cint; para5:cint; para6:culong):TStatus;cdecl;
T_XShmCreateImage=function(para1:PDisplay; para2:PVisual; para3:cuint; para4:cint; para5:Pchar; para6:PXShmSegmentInfo; para7:cuint; para8:cuint):PXImage;cdecl;
T_XShmCreatePixmap=function(para1:PDisplay; para2:TDrawable; para3:Pchar; para4:PXShmSegmentInfo; para5:cuint; para6:cuint; para7:cuint):TPixmap;cdecl;

var
XShmQueryExtension : T_XShmQueryExtension;
XShmGetEventBase : T_XShmGetEventBase;
XShmQueryVersion : T_XShmQueryVersion;
XShmPixmapFormat : T_XShmPixmapFormat;
XShmAttach : T_XShmAttach;
XShmDetach : T_XShmDetach;
_XShmPutImage : T_XShmPutImage;
XShmGetImage : T_XShmGetImage;
XShmCreateImage : T_XShmCreateImage;
XShmCreatePixmap : T_XShmCreatePixmap;



{ overloaded functions to handle TBool parameters as actual booleans }
function XShmPutImage(para1:PDisplay; para2:TDrawable; para3:TGC; para4:PXImage; para5:cint; para6:cint; para7:cint; para8:cint; para9:cuint; para10:cuint; para11:Boolean):TStatus;

implementation







function XShmPutImage(para1:PDisplay; para2:TDrawable; para3:TGC; para4:PXImage; para5:cint;
           para6:cint; para7:cint; para8:cint; para9:cuint; para10:cuint;
           para11:Boolean):TStatus;
begin
  XShmPutImage := _XShmPutImage(para1,para2,para3,para4,para5,para6,para7,para8,para9,para10,Ord(para11));
end;


procedure init_procvars();
begin
  pointer(XShmQueryExtension) := nil;
  pointer(XShmGetEventBase) := nil;
  pointer(XShmQueryVersion) := nil;
  pointer(XShmPixmapFormat) := nil;
  pointer(XShmAttach) := nil;
  pointer(XShmDetach) := nil;
  pointer(_XShmPutImage) := nil;
  pointer(XShmGetImage) := nil;
  pointer(XShmCreateImage) := nil;
  pointer(XShmCreatePixmap) := nil;
end;

procedure load_procvars(libh : TLibHandle);
begin
writeln('load shm...');
writeln(GetLoadErrorStr());
  pointer(XShmQueryExtension) := GetProcedureAddress(libh,'XShmQueryExtension');
writeln(GetLoadErrorStr());
  pointer(XShmGetEventBase) := GetProcedureAddress(libh,'XShmGetEventBase');
  pointer(XShmQueryVersion) := GetProcedureAddress(libh,'XShmQueryVersion');
  pointer(XShmPixmapFormat) := GetProcedureAddress(libh,'XShmPixmapFormat');
  pointer(XShmAttach) := GetProcedureAddress(libh,'XShmAttach');
  pointer(XShmDetach) := GetProcedureAddress(libh,'XShmDetach');
  pointer(_XShmPutImage) := GetProcedureAddress(libh,'XShmPutImage');
  pointer(XShmGetImage) := GetProcedureAddress(libh,'XShmGetImage');
  pointer(XShmCreateImage) := GetProcedureAddress(libh,'XShmCreateImage');
  pointer(XShmCreatePixmap) := GetProcedureAddress(libh,'XShmCreatePixmap');
end;


procedure do_init();
begin
  init_procvars();
  if libXgeAvailable() then begin
    load_procvars(libXgeHandle());
  end;
end;


initialization
begin
  do_init();
end;

finalization
begin
end;


end.
