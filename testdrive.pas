{$mode objfpc}
{$unitpath drivers}
{$unitpath drivers/windows}
{$unitpath drivers/x11}
{$unitpath drivers/wayland}
{$unitpath imgsrc}
{$unitpath imgsrc/jpg}
{$unitpath imgsrc/ttf}
{$unitpath misc}

uses drvif, jpegloader,renderbox,
tttile,fontfile,ttgridfit,ot_cmap,classes;



function make_jpg(fn : ansistring) : TjpgimgBGdesciptor;
begin
  result := TjpgimgBGdesciptor.create();
  result.setFile(fn);
end;

function make_col(c : dword) : TcolorBGdesciptor;
begin
  result := TcolorBGdesciptor.create();
  result.col := c;
end;

var
theglyph,gnxt : TTglyph;
font : TOTcmapfont;

function make_renderbox(s : ansistring) : Trenderbox;
var rb : Trenderbox; s2 : unicodestring; i,x,ch,dv : longint;
begin
  rb := Trenderbox.create();
  rb.dx := 1700;
  rb.dy := 28;
  rb.bgcol := $ff808080;
  s2 := utf8decode(s);

  setlength(rb.arr,1+length(s2));

  rb.arr[0].img := make_jpg('./media/res2.jpg');
  rb.arr[0].x := 10;
  rb.arr[0].y := 10;
  rb.arr[0].flgs := 0;

  x := 50;
  for i := 1 to length(s2) do begin
    ch := font.UnicodeToGlyphIndex(ord(s2[i]));
    
    theglyph := font.getGlyph( ch );
    
    rb.arr[i].gly := get_glybmp(theglyph,16,16,true);
    
    //writeln(s2[i],'=>',ch,' awx:',rb.arr[i].gly^.awx);
    
    rb.arr[i].x := x +rb.arr[i].gly^.refx;
    rb.arr[i].y := 22 -rb.arr[i].gly^.refy;
    rb.arr[i].flgs := 1;
    rb.arr[i].fgc := $ffffffff;
    
    dv := 0;
    if (i<length(s2)) and (theglyph<>nil) then begin
      ch := font.UnicodeToGlyphIndex(ch);
      gnxt := font.getGlyph( ch );
      if gnxt<>nil then begin
        dv := font.lookup_kern(theglyph.idx,gnxt.idx,1);
      end;
    end;
    
    x += round((rb.arr[i].gly^.awx + dv*64*16/theglyph.font.unitsPerEm)/64);;
  end;
  
  
  make_renderbox := rb;
end;

function make_r(x,y,dx,dy : longint) : Twrect;
begin
  result.x := x;
  result.y := y;
  result.dx := dx;
  result.dy := dy;
end;


var
d : Twinsysdriverinterface;
w0,w1,w2,w3 : Tlayoutedwindow;
r : Twrect;

sl : array of string;
i : longint;

mstrm : TMemoryStream;

begin


  mstrm := TMemoryStream.create();
  mstrm.LoadFromFile('./media/fonts/dejavu-fonts-ttf-2.37/ttf/DejaVuSans.ttf');
  font := TOTcmapfont.create();
  font.load(mstrm);
  
  theglyph := font.getGlyph( font.UnicodeToGlyphIndex(ord('A')) );
  
  setlength(sl,0);
  get_useful_configs(sl);
  
  for i := low(sl) to high(sl) do begin
    writeln('try config idx: ',i,'/',high(sl));
    d := load_config(sl[i]);
    if assigned(d) then begin
      writeln('finally, using ',d.tellname,'@',sl[i]);
      if d.deeperup() then begin
        w0 := d.make_node(nil, make_col($ff00ffff), make_r(0,0,500,700));
        w1 := d.make_node(w0,  make_col($ff00ff00), make_r(25,25,350,350));
        w2 := d.make_node(w1,  make_jpg('./media/res2.jpg'), make_r(50,50,50,50));

        w3 := d.make_node(w0,  make_renderbox('Gar öffentliche Schrift mit voll das Gelaber mittendrinne.'), make_r(125,125,125,150));
        
        d.enterloop();
        d.shutdown();
        break;
      end;
    end;
  end;
  
  font.destroy();
  mstrm.destroy();
end.
