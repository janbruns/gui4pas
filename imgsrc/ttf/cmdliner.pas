unit cmdliner;
{$mode objfpc}
{$COPERATORS ON}


interface

type ToptAction = function(o,v : string) : boolean;

procedure addLongOption(s : string; needarg : Integer; a : ToptAction);
procedure addShortOption(s : char; needarg : Integer; a : ToptAction);

function checkCommandLine(var helpreq : boolean) : boolean;

{ string to int with error printing }
function getnum(s : string; var v : longint) : boolean;


implementation
uses getopts;

function getnum(s : string; var v : longint) : boolean;
var w : word;
begin
  val(s,v,w);
  if not(w=0) then writeln('Can''t translate to intger:',s);
  getnum := (w=0);
end;




type
Tstartcls = class
  procedure addLongOption(s : string; needarg : Integer; a : ToptAction);
  procedure addShortOption(s : char; needarg : Integer; a : ToptAction);
  procedure checkCmdLine();

  constructor create();

  private
  theopts : array of TOption;
  optaction : array of ToptAction;
  shortaction : array[0..255] of ToptAction;
  sshortaction : string;
  dontrun,
  shouldprinthelp,
  expshouldprinthelp,
  haveerr : boolean;
end;



procedure Tstartcls.addLongOption(s : string; needarg : Integer; a : ToptAction);
begin
  setlength(theopts,high(theopts)+2);
  with theopts[high(theopts)] do begin
    name    := s;
    has_arg := needarg;
    flag    := nil;
    value   := #0;
  end;
  setlength(optaction,high(optaction)+2);
  optaction[high(optaction)] := a;
end;

procedure Tstartcls.addShortOption(s : char; needarg : Integer; a : ToptAction);
begin
  sshortaction := sshortaction + s;
  while (needarg>0) do begin
    sshortaction := sshortaction + ':';
    dec(needarg);
  end;
  if (s='h') then begin
    if sshortaction[1]='h' then begin
      delete(sshortaction,1,1);
      while (length(sshortaction)>0) and (sshortaction[1]=':')
      do delete(sshortaction,1,1);
    end;
  end;
  shortaction[ord(s)] := a;
end;


constructor Tstartcls.create();
var i : longint;
begin
  inherited create();
  for i := 0 to 255 do shortaction[i] := nil;
  setlength(theopts,0);
  setlength(optaction,0);
  haveerr := false;
  shouldprinthelp := false;
  expshouldprinthelp := false;
  dontrun := false;
  sshortaction := '+h';
end;

procedure Tstartcls.checkCmdLine();
var c : char; optionindex,i : Longint;
begin
  for i := 1 to paramcount do begin
    if (paramstr(i)='-?')or(paramstr(i)='/?') then begin
      shouldprinthelp := true;
      expshouldprinthelp := true;
    end;
  end;
  c:=#0;
  optionindex := 0;
  if not expshouldprinthelp then repeat
    c:=getlongopts(sshortaction,@theopts[0],optionindex);
    case c of
      #0 : begin
             i :=optionindex-1; 
             if (i>=0)and(i<=high(optaction)) 
             then begin
               if not(optaction[i]=nil) then begin
                 if optaction[i](theopts[i].name,optarg) then begin
                 end else begin
                   haveerr := true;
                 end;
               end;
             end;
           end;
      '?' : begin
              if (optind>1) then writeln('error with option:',paramstr(optind-1));
              haveerr := true;
            end;
      endofoptions : ;
      else begin
        if not(shortaction[ord(c)]=nil) then begin
          if shortaction[ord(c)](c,optarg) then begin
          end else begin
            haveerr := true;
          end;
        end else if (c='h') then begin
          shouldprinthelp := true;
          expshouldprinthelp := true;
        end;
      end;
    end; 
  until haveerr or (c=endofoptions);
  if (optind<=paramcount) and not(shouldprinthelp or haveerr) then begin
    write('unrecognized option or unexpected option-param: ');
    writeln(paramstr(optind),' ');
    haveerr := true;
  end;
  if haveerr then shouldprinthelp := expshouldprinthelp;
  if shouldprinthelp then dontrun := true;
  if haveerr then dontrun := true;
end;



var
starter : Tstartcls;

procedure addLongOption(s : string; needarg : Integer; a : ToptAction);
begin
  if not(starter=nil) then starter.addLongOption(s,needarg,a);
end;

procedure addShortOption(s : char; needarg : Integer; a : ToptAction);
begin
  if not(starter=nil) then starter.addShortOption(s,needarg,a);
end;

function checkCommandLine(var helpreq : boolean) : boolean;
begin
  if  not(starter=nil) then begin
    starter.checkCmdLine();
    checkCommandLine := not(starter.dontrun);
    helpreq := starter.shouldprinthelp;
    starter.destroy(); starter := nil;
  end;
end;

begin
//  OptErr := false;
  starter := Tstartcls.create();
end.



