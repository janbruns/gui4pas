{$mode objfpc}
{$unitpath .}
unit paragraphmanager;

{ 
  about calculating the 2d arrangement of unicode text streams
  (the part that follows from the text content), linebreaks and
  reading direction.
}

interface
uses 
  unicode_gencat,
  unicode_bidiralgo,
  linebreakalgo,
  sysutils,math;

const
first_app_combiner = $0200000; // =first 5-byte UTF-8
last_app_combiner =  $3FFFFFF; // =last  5-byte UTF-8

type
TparagraphManager = class
  procedure make_bd(bd : Tabstract_bidir_algo_cls);
  procedure make_lb(lb : Tabstractlinebreaker_cls);
  procedure load_from_UTF8(s : ansistring); virtual;
  procedure load_from_codepoints(p : Pdword; cnt : dword); virtual;
  destructor destroy(); override;
  
  public
  linebreaker : Tabstractlinebreaker;
  bidirhandler : Tabstract_bidir_algo;
end;

TextparagraphManager = class(TparagraphManager)
  public
  function getChar(idx : dword) : dword;
  function length() : dword;
  
  { decide, wether to treat a char (codepoint cp) as base char }
  function is_basechar(cp : dword) : boolean; virtual;
  
  { do the bidir line rordering in basechar range a..b }
  procedure calc_order(a,b : dword);
  
  
  procedure load_from_UTF8(s : ansistring); override;
  procedure load_from_codepoints(p : Pdword; cnt : dword); override;
  
  protected
  procedure swap_basechar(a,b : dword);
  procedure make_basechartab();
  
  
  public
  basechars : array of dword; // indices
  combinercount : array of byte;
end;



implementation



procedure TparagraphManager.make_lb(lb : Tabstractlinebreaker_cls);
begin
  linebreaker := lb.create();
end;

procedure TparagraphManager.make_bd(bd : Tabstract_bidir_algo_cls);
begin
  bidirhandler := bd.create();
end;

procedure TparagraphManager.load_from_UTF8(s : ansistring);
begin
  bidirhandler.load_from_UTF8(s);
  {well, maybe not the prettiest construct ever}
  linebreaker.setCharDat(@bidirhandler.chars[0],high(bidirhandler.chars)+1);
end;

procedure TparagraphManager.load_from_codepoints(p : Pdword; cnt : dword);
begin
  bidirhandler.load_from_codepoints(p,cnt);
  linebreaker.setCharDat(@bidirhandler.chars[0],high(bidirhandler.chars)+1);
end;

destructor TparagraphManager.destroy();
begin
  if assigned(linebreaker) then freeandnil(linebreaker);
  if assigned(bidirhandler) then freeandnil(bidirhandler);
end;



function TextparagraphManager.is_basechar(cp : dword) : boolean;
var b : byte;
begin
  b := get_unicode_gencat(cp);
  result := true;
  case b of
    gencat_Mn,
    gencat_Mc,
    gencat_Cf, // includes codes like RLE,PDF,FSI,PDI
    gencat_Me : result := false;
  end;
  if (b=0)and(cp>=first_app_combiner) then begin
    if (cp<=last_app_combiner) 
    then result := false
    else result := true;
  end;
end;

function TextparagraphManager.getChar(idx : dword) : dword;
begin
  if idx<=high(bidirhandler.chars) then begin
    result := bidirhandler.chars[idx];
  end else result := 0;
end;

function TextparagraphManager.length() : dword;
begin
  result := high(bidirhandler.chars)+1;
end;

procedure TextparagraphManager.make_basechartab();
var i,endi,bc : dword;
begin
  endi := self.length();
  if endi>0 then begin
    endi -= 1;
    { count basechars, make the tab}
    bc := 0;
    for i := 0 to endi do begin
      if is_basechar(getChar(i)) then begin
        bc += 1;
      end;
    end;
    if bc>0 then begin
      setlength(basechars,bc);
      bc := 0;
      for i := 0 to endi do begin
        if is_basechar(getChar(i)) then begin
          basechars[bc] := i;
          bc += 1;
        end;
      end;
      {also make the combinercount}
      setlength(combinercount,bc);
      bc := high(combinercount);
      i := 0;
      while (i<bc) do begin
        combinercount[i] := basechars[i+1]-basechars[i];
        i += 1;
      end;
      combinercount[bc] := endi-basechars[bc];
    end else begin
      setlength(basechars,0);
      setlength(combinercount,0);
    end;
  end;
end;

const
invalid_charidx = $FFFFFFFF;

procedure TextparagraphManager.calc_order(a,b : dword);
var starti, endi, c,i,j : dword;
begin
  starti := basechars[a];
  endi := basechars[b]+combinercount[b];
  { the bidir algo might might have it's useless very own opinion about
    how to order combines, so first mark all indices as unintersting
  }
  for i := starti to endi do begin
    bidirhandler.charidx[i] := invalid_charidx;;
  end;
  { tell about interesting indices }
  for i := a to b do begin
    bidirhandler.charidx[basechars[i]] := i;
  end;
  bidirhandler.calc_line_order(starti,endi);
  { apply bidirhandler's recommendation }
  c := a;
  for i := starti to endi do begin
    j := bidirhandler.charidx[i];
    if (j<>invalid_charidx) then begin
      if (j<a)or(j>b)or(c<a)or(c>b) 
      then raise exception.create('out-of bounds bidir line order result');
      { basechar has move from j to c, but deny double-swap }
      if c<j then swap_basechar(c,j);
      inc(c);
    end;
  end;
end;

procedure TextparagraphManager.swap_basechar(a,b : dword);
var c : dword;
begin
  c := basechars[a];
  basechars[a] := basechars[b];
  basechars[b] := c;
  c := combinercount[a];
  combinercount[a] := combinercount[b];
  combinercount[b] := c;
end;


procedure TextparagraphManager.load_from_UTF8(s : ansistring);
begin
  inherited;
  make_basechartab();
end;

procedure TextparagraphManager.load_from_codepoints(p : Pdword; cnt : dword);
begin
  inherited;
  make_basechartab();
end;

end.
