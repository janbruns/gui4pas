{$mode objfpc}
{$unitpath .}
unit jpegloader;

interface
uses 
  jpeg_parser,
  jpeg_imagecomponent,
  jpeg_streamwriter,
  jpeg_headerdata,
  classes,
  sysutils,
  drvif,
  renderbox;


type
Tmyjpegparser = class(Tjpegparser)
  procedure handle_EOI(c : byte); override;
  destructor destroy(); override;
end;

Trgba8888img = record
  xres,yres,mem  : longint;
  dat : Pbyte;
end;

TjpgimgBGdesciptor = class(TimgBGdesciptor)
  constructor create(); override;
  destructor destroy(); override;
  
  function map_rgba8888(var bpp,bpl : longint) : pointer; override;
  procedure unmap(); override;
  
  procedure setFile(fn : ansistring);
  
  protected
  function getwidth() : longint; override;
  function getheight() : longint;  override;
  procedure releaseall();
  
  public
  jpg : Tmyjpegparser;
  carr : Tcompoarr;
  imgrgba8888 : Trgba8888img;
end;


implementation


procedure Tmyjpegparser.handle_EOI(c : byte);
begin
  done := true;
end;

destructor Tmyjpegparser.destroy();
begin
  inherited;
end;

procedure InitImg(var img : Trgba8888img);
begin
  img.mem := 0;
  img.xres := 0;
  img.yres := 0;
  img.dat := nil;
end;

procedure releaseImg(var img : Trgba8888img);
begin
  if (img.mem>0) then freemem(img.dat,img.mem);
  initImg(img);
end;

procedure allocImg(var img : Trgba8888img; xres,yres : longint);
begin
  img.xres := xres;
  img.yres := yres;
  img.mem  := xres*yres*4;
  getmem(img.dat,img.mem);
  fillbyte(img.dat^,img.mem,255);
end;

procedure paintcompo(c : Tjpeg_imagecomponent; const dstimg : Trgba8888img; col,xres,yres : longint);
var x,y,x1,y1,dx,dy,sx,sy,sx1,sy1,xr2,yr2 : longint; dst : Pbyte; q : single; w : byte;
begin
  sx := 1; while (sx*c.xres<xres) do inc(sx);
  sy := 1; while (sy*c.yres<yres) do inc(sy);
//writeln('compo ',col,' sx=',sx,' sy=',sy,' bw=$',hexstr(c.brightwhite,4));
  sx1 := sx-1;
  sy1 := sy-1;
  dst := @dstimg.dat[col];
  xr2 := c.xres-1;
  yr2 := c.yres-1;
  q := 255.0/c.brightwhite;
  for y := 0 to yr2 do begin
    for x := 0 to xr2 do begin
      w := round(q * c.get_line_pointer(x,y)^);
      for y1 := 0 to sy1 do begin
        for x1 := 0 to sx1 do begin
          dy := y*sy+y1;
          dx := x*sx+x1;
          if (dx<xres)and(dy<yres) then begin
            dst[(dy*xres+dx)*4] := w;
          end;
        end;
      end;
    end;
  end;
//writeln('donepaint');
end;

procedure JFIF_YCbCr(p : Pbyte);
var y,cb,cr : longint; r,g,b : longint;
begin
  y := p^;
  cb := p[1];
  cr := p[2];
  y := (y shl 8) or y;
  cb := (cb shl 8) or cb;
  cr := (cr shl 8) or cr;
  y := y * 16384;
  cr := cr -32768;
  cb := cb -32768;
  r := Y + 22970*Cr;
  g := Y - 5638*Cb -11700*Cr;
  b := Y + 29032*Cb;
  if (r<0) then r := 0;
  if (g<0) then g := 0;
  if (b<0) then b := 0;
  r := r shr 14;
  g := g shr 14;
  b := b shr 14;
  if (r>65535) then r := 65535;
  if (g>65535) then g := 65535;
  if (b>65535) then b := 65535;
  p[2] := r shr 8;
  p[1] := g shr 8;
  p[0] := b shr 8;
end;


constructor TjpgimgBGdesciptor.create();
begin
  inherited;
  InitImg(imgrgba8888);
  setlength(carr,0);
end;

destructor TjpgimgBGdesciptor.destroy();
begin
  releaseall();
end;

procedure TjpgimgBGdesciptor.releaseall();
begin
  releaseImg(imgrgba8888);
  freeandnil(jpg);
end;

procedure TjpgimgBGdesciptor.setFile(fn : ansistring);
var s : TMemoryStream; c : longint;
begin
  releaseall();
  s := TMemoryStream.create();
  s.LoadFromFile(fn);
  jpg := Tmyjpegparser.create();
  jpg.parse(s);
  jpg.get_all_components(carr);
  s.destroy();
  for c := 0 to 3 do begin
    if c<=high(carr) then begin
      carr[c].postprocess();
    end;
  end;
end;

function TjpgimgBGdesciptor.getwidth() : longint;
var w,h : longint;
begin
  jpg.getres(w,h);
  getwidth := w;
end;

function TjpgimgBGdesciptor.getheight() : longint;
var w,h : longint;
begin
  jpg.getres(w,h);
  getheight := h;
end;

function TjpgimgBGdesciptor.map_rgba8888(var bpp,bpl : longint) : pointer;
var w,h,c : longint; q1,q2 : byte;
begin
  jpg.getres(w,h);
  InitImg(imgrgba8888);
  allocImg(imgrgba8888,w,h);
  bpp := 4;
  bpl := w*4;
  for c := 0 to 3 do begin
    if c<=high(carr) then begin
      paintcompo(carr[c], imgrgba8888, c,w,h);
    end;
  end;
  if (jpg.sofh.is_DCT) and (high(carr)=2) then begin
    writeln('do JFIF_YCbCr');
    for c := w*h-1 downto 0 do begin
      JFIF_YCbCr(@imgrgba8888.dat[c*4]);
    end;
  end else begin
    writeln('no JFIF_YCbCr');
    if (high(carr)>=2) then begin
      for c := w*h-1 downto 0 do begin
        q1 := imgrgba8888.dat[c*4];
        q2 := imgrgba8888.dat[c*4+2];
        imgrgba8888.dat[c*4] := q2;
        imgrgba8888.dat[c*4+2] := q1;
      end;
    end;
  end;
  map_rgba8888 := imgrgba8888.dat;
end;

procedure TjpgimgBGdesciptor.unmap();
begin
  releaseImg(imgrgba8888);
end;





BEGIN
END.
