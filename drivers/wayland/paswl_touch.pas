{$mode objfpc}
{$unitpath .}
unit paswl_touch;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_bindable;


type

Tpaswl_touch=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  protected
  procedure cb_touch_down(serial, time : dword; surface : Tpaswl_bindable; id : longint; x, y : Twlfixed); virtual;
  procedure cb_touch_up(serial, time : dword; id : longint); virtual;
  procedure cb_touch_motion(time : dword; id : longint; x, y : Twlfixed); virtual;
  procedure cb_touch_frame(); virtual;
  procedure cb_touch_cancel(); virtual;
  procedure cb_touch_shape(id : longint; major, minor : Twlfixed); virtual;
  procedure cb_touch_orientation(id : longint; orientation : Twlfixed); virtual;

  public
  seat : Tpaswl_bindable;
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;

Tpaswl_touch_cls = class of Tpaswl_touch;



implementation
uses strings,math;





procedure touch_down(dat:pointer; o:wl_touch; serial, time : dword; surface : wl_surface; id : longint; x, y : Twlfixed); cdecl;
var d : Tpaswl_touch;
begin
  if dat<>nil then begin
    d := Tpaswl_touch(dat);
    if d.wlo=o then d.cb_touch_down(serial, time, Tpaswl_bindable(wl_proxy_get_user_data(surface)), id, x, y)
    else ;//
  end;
end;

procedure touch_up(dat:pointer; o:wl_touch; serial, time : dword; id : longint); cdecl;
var d : Tpaswl_touch;
begin
  if dat<>nil then begin
    d := Tpaswl_touch(dat);
    if d.wlo=o then d.cb_touch_up(serial, time, id)
    else ;//
  end;
end;

procedure touch_motion(dat:pointer; o:wl_touch; time : dword; id : longint; x, y : Twlfixed); cdecl;
var d : Tpaswl_touch;
begin
  if dat<>nil then begin
    d := Tpaswl_touch(dat);
    if d.wlo=o then d.cb_touch_motion(time, id, x, y)
    else ;//
  end;
end;

procedure touch_frame(dat:pointer; o:wl_touch); cdecl;
var d : Tpaswl_touch;
begin
  if dat<>nil then begin
    d := Tpaswl_touch(dat);
    if d.wlo=o then d.cb_touch_frame()
    else ;//
  end;
end;

procedure touch_cancel(dat:pointer; o:wl_touch); cdecl;
var d : Tpaswl_touch;
begin
  if dat<>nil then begin
    d := Tpaswl_touch(dat);
    if d.wlo=o then d.cb_touch_cancel()
    else ;//
  end;
end;

procedure touch_shape(dat:pointer; o:wl_touch; id : longint; major, minor : Twlfixed); cdecl;
var d : Tpaswl_touch;
begin
  if dat<>nil then begin
    d := Tpaswl_touch(dat);
    if d.wlo=o then d.cb_touch_shape(id, major, minor)
    else ;//
  end;
end;

procedure touch_orientation(dat:pointer; o:wl_touch; id : longint; orientation : Twlfixed); cdecl;
var d : Tpaswl_touch;
begin
  if dat<>nil then begin
    d := Tpaswl_touch(dat);
    if d.wlo=o then d.cb_touch_orientation(id, orientation)
    else ;//
  end;
end;

var
imp_if_touch_listener : wl_touch_listener = (
  down : @touch_down;
  up : @touch_up;
  motion : @touch_motion;
  frame : @touch_frame;
  cancel : @touch_cancel;
  shape : @touch_shape;
  orientation : @touch_orientation;
);



procedure Tpaswl_touch.cb_touch_down(serial, time : dword; surface : Tpaswl_bindable; id : longint; x, y : Twlfixed);
begin
end;

procedure Tpaswl_touch.cb_touch_up(serial, time : dword; id : longint);
begin
end;

procedure Tpaswl_touch.cb_touch_motion(time : dword; id : longint; x, y : Twlfixed);
begin
end;

procedure Tpaswl_touch.cb_touch_frame();
begin
end;

procedure Tpaswl_touch.cb_touch_cancel();
begin
end;

procedure Tpaswl_touch.cb_touch_shape(id : longint; major, minor : Twlfixed);
begin
end;

procedure Tpaswl_touch.cb_touch_orientation(id : longint; orientation : Twlfixed);
begin
end;



constructor Tpaswl_touch.create();
begin
  inherited create();
end;

destructor Tpaswl_touch.destroy();
begin
  wl_touch_release(wl_touch(wlo));
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_touch.onBind();
begin
  inherited;
  wl_touch_add_listener(wl_touch(wlo),imp_if_touch_listener,self);
end;

procedure Tpaswl_touch.onUnbind();
begin
  inherited;
end;






class function Tpaswl_touch.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_touch_interface;
end;

class function Tpaswl_touch.wl_ifver() : dword;
begin
  wl_ifver := 9;
end;


initialization
begin
end;

finalization
begin
end;

end.



