{$mode objfpc}
{$unitpath .}
unit paswl_xdg_ext1;

interface
uses 
  wl_basic_types,
  wl_core,
  wl_libwl,
  wl_xdgshell,
  paswl_xdg_toplevel;


type
TXDGstatereq=record
  width, height, bounds_width, bounds_height : longint;
  close : boolean;
  maximized,
  fullscreen,
  resize,
  activated,
  tiled_left,
  tiled_right,
  tile_top,
  tiled_bottom,
  suspended : boolean;
  wm_cap_window_menu,
  wm_cap_maximize,
  wm_cap_fullscreen,
  wm_cap_minimize : boolean;
end;

type
Txdgtoplevel_with_states = class(Tpaswl_xdg_toplevel)
  constructor create(); override;
  
  protected
  procedure cb_xdg_toplevel_configure(width, height : longint; states : wl_array); override;
  procedure cb_xdg_toplevel_close(); override;
  procedure cb_xdg_toplevel_configure_bounds(width, height : longint); override;
  procedure cb_xdg_toplevel_wm_capabilities(capabilities : wl_array); override;
  
  public
  reqstate : TXDGstatereq;
end;



procedure clear_XDGreq_config_states(var c : TXDGstatereq);
procedure clear_XDGreq_wmcap_states(var c : TXDGstatereq);
procedure init_XDGreq_states(var c : TXDGstatereq);



implementation

procedure clear_XDGreq_config_states(var c : TXDGstatereq);
begin
  with c do begin
    maximized := false;
    fullscreen := false;
    resize := false;
    activated := false;
    tiled_left := false;
    tiled_right := false;
    tile_top := false;
    tiled_bottom := false;
    suspended := false;
  end;
end;

procedure clear_XDGreq_wmcap_states(var c : TXDGstatereq);
begin
  with c do begin
    wm_cap_window_menu := false;
    wm_cap_maximize := false;
    wm_cap_fullscreen := false;
    wm_cap_minimize := false;
  end;
end;


procedure init_XDGreq_states(var c : TXDGstatereq);
begin
  clear_XDGreq_config_states(c);
  clear_XDGreq_wmcap_states(c);
  c.close := false;
  c.width := 0;
  c.height:= 0;
  c.bounds_width := 0;
  c.bounds_height:= 0;
end;

constructor Txdgtoplevel_with_states.create();
begin
  inherited;
  init_XDGreq_states(reqstate);
end;
  
procedure Txdgtoplevel_with_states.cb_xdg_toplevel_configure(width, height : longint; states : wl_array);
var p : Pdword; i,j : longint;
begin
  if width<>0 then reqstate.width := width;
  if height<>0 then reqstate.height := height;
  clear_XDGreq_config_states(reqstate);
  p := pointer(states^.data);
  j := states^.size DIV 4;
  for i := 0 to j-1 do begin
    case p[i] of
      xdg_toplevel_state_maximized : reqstate.maximized := true;
      xdg_toplevel_state_fullscreen : reqstate.fullscreen := true;
      xdg_toplevel_state_resizing : reqstate.resize := true;
      xdg_toplevel_state_activated : reqstate.activated := true;
      xdg_toplevel_state_tiled_left : reqstate.tiled_left := true;
      xdg_toplevel_state_tiled_right : reqstate.tiled_right := true;
      xdg_toplevel_state_tiled_top : reqstate.tile_top := true;
      xdg_toplevel_state_tiled_bottom : reqstate.tiled_bottom := true;
      xdg_toplevel_state_suspended : reqstate.suspended := true;
    end;
  end;
end;

procedure Txdgtoplevel_with_states.cb_xdg_toplevel_close();
begin
  reqstate.close := true;
end;

procedure Txdgtoplevel_with_states.cb_xdg_toplevel_configure_bounds(width, height : longint);
begin
  reqstate.bounds_width := width;
  reqstate.bounds_height := height;
end;

procedure Txdgtoplevel_with_states.cb_xdg_toplevel_wm_capabilities(capabilities : wl_array);
var p : Pdword; i,j : longint;
begin
  clear_XDGreq_wmcap_states(reqstate);
  p := pointer(capabilities^.data);
  j := capabilities^.size DIV 4;
  for i := 0 to j-1 do begin
    case p[i] of
      xdg_toplevel_wm_capabilities_window_menu : reqstate.wm_cap_window_menu := true;
      xdg_toplevel_wm_capabilities_maximize : reqstate.wm_cap_maximize := true;
      xdg_toplevel_wm_capabilities_fullscreen : reqstate.wm_cap_fullscreen := true;
      xdg_toplevel_wm_capabilities_minimize : reqstate.wm_cap_minimize := true;
    end;
  end;
end;




begin
end.
