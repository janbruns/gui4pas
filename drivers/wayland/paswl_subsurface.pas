{$mode objfpc}
{$unitpath .}
unit paswl_subsurface;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_surface,paswl_bindable;


type

Tpaswl_subsurface=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  procedure set_position(x, y : longint);
  procedure place_above(sibling : Tpaswl_surface);
  procedure place_below(sibling : Tpaswl_surface);
  procedure set_sync();
  procedure set_desync();


  public
  surface : Tpaswl_surface;
 
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;
Tpaswl_subsurface_cls = class of Tpaswl_subsurface;








implementation



constructor Tpaswl_subsurface.create();
begin
  inherited create();
end;

destructor Tpaswl_subsurface.destroy();
begin
  wl_subsurface_destroy(wl_subsurface(wlo));
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_subsurface.onBind();
begin
  inherited;
end;

procedure Tpaswl_subsurface.onUnbind();
begin
  inherited;
end;


procedure Tpaswl_subsurface.set_position(x, y : longint);
begin
  wl_subsurface_set_position(wl_subsurface(wlo), x, y);
end;
procedure Tpaswl_subsurface.place_above(sibling : Tpaswl_surface);
begin
  wl_subsurface_place_above(wl_subsurface(wlo),wl_surface(sibling.wlo));
end;
procedure Tpaswl_subsurface.place_below(sibling : Tpaswl_surface);
begin
  wl_subsurface_place_below(wl_subsurface(wlo),wl_surface(sibling.wlo));
end;
procedure Tpaswl_subsurface.set_sync();
begin
  wl_subsurface_set_sync(wl_subsurface(wlo));
end;
procedure Tpaswl_subsurface.set_desync();
begin
  wl_subsurface_set_desync(wl_subsurface(wlo));
end;



class function Tpaswl_subsurface.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_subsurface_interface;
end;

class function Tpaswl_subsurface.wl_ifver() : dword;
begin
  wl_ifver := 1;
end;


initialization
begin
end;

finalization
begin
end;

end.



