{$mode objfpc}
{$unitpath .}
unit paswl_display;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_basebindable,paswl_registry;


type

Tpaswl_display=class(Tpaswl_regbasebindable)

  public 
  constructor create(); override;
  destructor destroy(); override;

  procedure onBind(); override;
  procedure onUnbind(); override;

  function display_connect(name : Pchar) : boolean;
  procedure display_disconnect();
  function display_roundtrip() : Tcint;
  function display_dispatch() : Tcint;
  function display_get_fd() : Tcint;
  function display_sync() : wl_callback;
  function display_get_registry(c : Tpaswl_registry_cls) : Tpaswl_registry;
end;



implementation
uses strings,math,sysutils;


constructor Tpaswl_display.create();
begin
end;

destructor Tpaswl_display.destroy();
begin
writeln('display_disconnect();');
  display_disconnect();  
end;

function Tpaswl_display.display_connect(name : Pchar) : boolean;
begin
  display_connect := false;
  if libwayland_client_avail then begin
    wlo := wl_display_connect(name);
    if (wlo<>nil) then begin
      display_connect := true;
    end;
  end;
end;

procedure Tpaswl_display.display_disconnect();
begin
  if wlo<>nil then begin
    wl_display_disconnect(wlo);
  end;
  wlo := nil;
end;

function Tpaswl_display.display_roundtrip() : Tcint;
begin
  display_roundtrip := wl_display_roundtrip(wlo);
end;

function Tpaswl_display.display_dispatch() : Tcint;
begin
  display_dispatch := wl_display_dispatch(wlo);
end;

function Tpaswl_display.display_get_fd() : Tcint;
begin
  display_get_fd := wl_display_get_fd(wlo);
end;

function Tpaswl_display.display_sync() : wl_callback;
begin
  display_sync := wl_display_sync(wlo);
end;

function Tpaswl_display.display_get_registry(c : Tpaswl_registry_cls) : Tpaswl_registry;
var wo : wl_registry; o : Tpaswl_registry;
begin
  wo := wl_display_get_registry(wlo);
  o := c.create();
  //Tpaswl_registry( make_child(c,wo) ) as Tpaswl_registry;
  if o<>nil then begin
    o.display := self;
    o.wlo := wo;
    o.wl_regname := 0;
  end;
  display_get_registry := o;
end;



procedure Tpaswl_display.onBind();
begin
end;

procedure Tpaswl_display.onUnbind();
begin
end;



initialization
begin
end;

finalization
begin
end;

end.



