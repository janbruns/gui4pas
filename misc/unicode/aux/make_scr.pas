{$mode objfpc}
{$unitpath ..}
uses unicode_scripts, unicode_utf8, unicode_tabinit;

function todot(var s : ansistring; const dot : shortstring) : ansistring;
var i : longint;
begin
  i := pos(dot,s);
  if (i>0) then begin
    result := copy(s,1,i-1);
    s := copy(s,i+length(dot));
  end else begin
    result := s;
    s := '';
  end;
end;

procedure remove_lead_spc(var s : ansistring; spc : ansistring);
begin
  while (pos(spc,s)=1) do delete(s,1,length(spc));
end;

procedure remove_trail_spc(var s : ansistring; spc : ansistring);
begin
  while (length(s)>0)and(s[length(s)]=spc[1]) do delete(s,length(s),1);
end;


function encode(t,l : dword) : ansistring;
var s : ansistring; tc0,lc0 : longint;
begin
  s := '';
  if (t>=15) then tc0 := 15 else tc0 := t;
  if (l>=7)  then lc0 := 7  else lc0 := l;
  s += UTF8_codepoint_as_UTF8(tc0+lc0*16);
  if (t>=15) then s += UTF8_codepoint_as_UTF8(t);
  if (l>=7)  then s += UTF8_codepoint_as_UTF8(l);
  encode := s;
end;


var
dat : array[0..$EFFFF] of byte;

var
symbfreq : array[0..255] of longint;
arr2 : Tbarr;

procedure analyse_block();
var cnt,i,j : longint; cur : byte; s : ansistring;
arr : array of dword;
begin
  for i := 0 to 255 do symbfreq[i] := 0;
  j := 0;
  cur := dat[0];
  cnt := 0;
  for i := 1 to high(dat) do begin
    if (dat[i]<>cur) then begin
      cur := dat[i];
      cnt := 0;
      j += 1;
      inc(symbfreq[cur and 255]);
    end else cnt += 1;
  end;
  j +=1;
  inc(symbfreq[cur and 255]);

  setlength(arr,j*3);
  j := 0;
  cur := dat[0];
  arr[3*j+1] := 0;
  arr[3*j+0] := cur;
  cnt := 0;
  for i := 1 to high(dat) do begin
    if (dat[i]<>cur) then begin
      cur := dat[i];
      cnt := 0;
      arr[3*j+2] := i-1;
      j += 1;
      arr[3*j+1] := i;
      arr[3*j+0] := cur;
    end else cnt += 1;
  end;
  arr[3*j+2] := high(dat);
  s := '';
  i := 0;
  while i <high(arr) do begin
    s += encode(arr[i],arr[i+2]-arr[i+1]);
    i += 3;
  end;
  for i := 0 to 255 do begin
    //writeln(i,':',symbfreq[i]);
  end;
//  writeln('len=',length(s));
  setlength(arr2,0);
  unicode_initvaldecode(s,arr2);
  for i := 0 to high(dat) do begin
    if dat[i]<>arr2[i] then writeln('ERROR',i);
  end;
  writeln('sinc : array[0..',length(s)-1,'] of byte = (');
  write('  ');
  for i := 1 to length(s)-1 do begin
    write('$',hexstr(ord(s[i]),2),', ');
    if (((i-1) and 15)=15) then begin
      writeln();
      write('  ');
    end;
  end;
  writeln('$',hexstr(ord(s[i]),2),');');
end;



procedure doline(s : ansistring);
var s0,sc,sn,s1,s2,s3,s4,s5 : ansistring; w : word; cp1,cp2,ce,c : dword; v : byte;
begin
  s0 := s;
  s := todot(s,'#');
  if s<>'' then begin
    sc := todot(s,';');
    sn := todot(s,';');
    s1 := todot(s,';');
    s2 := todot(s,';');
    s3 := todot(s,';');
    s4 := todot(s,';');
    s5 := todot(s,';');
    s1 := todot(sc,'..');
    s2 := sc;

    remove_lead_spc(s1,' ');
    remove_trail_spc(s1,' ');
    remove_lead_spc(s2,' ');
    remove_trail_spc(s2,' ');
    if s2='' then s2 := s1;
    s1 := '$'+s1;
    s2 := '$'+s2;

    val(s1,cp1,w);
    val(s2,cp2,w);
        
    remove_lead_spc(sn,' ');
    remove_trail_spc(sn,' ');
    
    c := 0;
    if sn='Common' then c := unicode_scr_Common
    else if sn='Inherited' then c := unicode_scr_Inherited
    else if sn='Adlam' then c := unicode_scr_Adlam
    else if sn='Ahom' then c := unicode_scr_Ahom
    else if sn='Anatolian_Hieroglyphs' then c := unicode_scr_Anatolian_Hieroglyphs
    else if sn='Arabic' then c := unicode_scr_Arabic
    else if sn='Armenian' then c := unicode_scr_Armenian
    else if sn='Avestan' then c := unicode_scr_Avestan
    else if sn='Balinese' then c := unicode_scr_Balinese
    else if sn='Bamum' then c := unicode_scr_Bamum
    else if sn='Bassa_Vah' then c := unicode_scr_Bassa_Vah
    else if sn='Batak' then c := unicode_scr_Batak
    else if sn='Bengali' then c := unicode_scr_Bengali
    else if sn='Bhaiksuki' then c := unicode_scr_Bhaiksuki
    else if sn='Bopomofo' then c := unicode_scr_Bopomofo
    else if sn='Brahmi' then c := unicode_scr_Brahmi
    else if sn='Braille' then c := unicode_scr_Braille
    else if sn='Buginese' then c := unicode_scr_Buginese
    else if sn='Buhid' then c := unicode_scr_Buhid
    else if sn='Canadian_Aboriginal' then c := unicode_scr_Canadian_Aboriginal
    else if sn='Carian' then c := unicode_scr_Carian
    else if sn='Caucasian_Albanian' then c := unicode_scr_Caucasian_Albanian
    else if sn='Chakma' then c := unicode_scr_Chakma
    else if sn='Cham' then c := unicode_scr_Cham
    else if sn='Cherokee' then c := unicode_scr_Cherokee
    else if sn='Chorasmian' then c := unicode_scr_Chorasmian
    else if sn='Coptic' then c := unicode_scr_Coptic
    else if sn='Cuneiform' then c := unicode_scr_Cuneiform
    else if sn='Cypriot' then c := unicode_scr_Cypriot
    else if sn='Cypro_Minoan' then c := unicode_scr_Cypro_Minoan
    else if sn='Cyrillic' then c := unicode_scr_Cyrillic
    else if sn='Deseret' then c := unicode_scr_Deseret
    else if sn='Devanagari' then c := unicode_scr_Devanagari
    else if sn='Dives_Akuru' then c := unicode_scr_Dives_Akuru
    else if sn='Dogra' then c := unicode_scr_Dogra
    else if sn='Duployan' then c := unicode_scr_Duployan
    else if sn='Egyptian_Hieroglyphs' then c := unicode_scr_Egyptian_Hieroglyphs
    else if sn='Elbasan' then c := unicode_scr_Elbasan
    else if sn='Elymaic' then c := unicode_scr_Elymaic
    else if sn='Ethiopic' then c := unicode_scr_Ethiopic
    else if sn='Georgian' then c := unicode_scr_Georgian
    else if sn='Glagolitic' then c := unicode_scr_Glagolitic
    else if sn='Gothic' then c := unicode_scr_Gothic
    else if sn='Grantha' then c := unicode_scr_Grantha
    else if sn='Greek' then c := unicode_scr_Greek
    else if sn='Gujarati' then c := unicode_scr_Gujarati
    else if sn='Gunjala_Gondi' then c := unicode_scr_Gunjala_Gondi
    else if sn='Gurmukhi' then c := unicode_scr_Gurmukhi
    else if sn='Hangul' then c := unicode_scr_Hangul
    else if sn='Hanifi_Rohingya' then c := unicode_scr_Hanifi_Rohingya
    else if sn='Han' then c := unicode_scr_Han
    else if sn='Hanunoo' then c := unicode_scr_Hanunoo
    else if sn='Hatran' then c := unicode_scr_Hatran
    else if sn='Hebrew' then c := unicode_scr_Hebrew
    else if sn='Hiragana' then c := unicode_scr_Hiragana
    else if sn='Imperial_Aramaic' then c := unicode_scr_Imperial_Aramaic
    else if sn='Inscriptional_Pahlavi' then c := unicode_scr_Inscriptional_Pahlavi
    else if sn='Inscriptional_Parthian' then c := unicode_scr_Inscriptional_Parthian
    else if sn='Javanese' then c := unicode_scr_Javanese
    else if sn='Kaithi' then c := unicode_scr_Kaithi
    else if sn='Kannada' then c := unicode_scr_Kannada
    else if sn='Katakana' then c := unicode_scr_Katakana
    else if sn='Kawi' then c := unicode_scr_Kawi
    else if sn='Kayah_Li' then c := unicode_scr_Kayah_Li
    else if sn='Kharoshthi' then c := unicode_scr_Kharoshthi
    else if sn='Khitan_Small_Script' then c := unicode_scr_Khitan_Small_Script
    else if sn='Khmer' then c := unicode_scr_Khmer
    else if sn='Khojki' then c := unicode_scr_Khojki
    else if sn='Khudawadi' then c := unicode_scr_Khudawadi
    else if sn='Lao' then c := unicode_scr_Lao
    else if sn='Latin' then c := unicode_scr_Latin
    else if sn='Lepcha' then c := unicode_scr_Lepcha
    else if sn='Limbu' then c := unicode_scr_Limbu
    else if sn='Linear_A' then c := unicode_scr_Linear_A
    else if sn='Linear_B' then c := unicode_scr_Linear_B
    else if sn='Lisu' then c := unicode_scr_Lisu
    else if sn='Lycian' then c := unicode_scr_Lycian
    else if sn='Lydian' then c := unicode_scr_Lydian
    else if sn='Mahajani' then c := unicode_scr_Mahajani
    else if sn='Makasar' then c := unicode_scr_Makasar
    else if sn='Malayalam' then c := unicode_scr_Malayalam
    else if sn='Mandaic' then c := unicode_scr_Mandaic
    else if sn='Manichaean' then c := unicode_scr_Manichaean
    else if sn='Marchen' then c := unicode_scr_Marchen
    else if sn='Masaram_Gondi' then c := unicode_scr_Masaram_Gondi
    else if sn='Medefaidrin' then c := unicode_scr_Medefaidrin
    else if sn='Meetei_Mayek' then c := unicode_scr_Meetei_Mayek
    else if sn='Mende_Kikakui' then c := unicode_scr_Mende_Kikakui
    else if sn='Meroitic_Cursive' then c := unicode_scr_Meroitic_Cursive
    else if sn='Meroitic_Hieroglyphs' then c := unicode_scr_Meroitic_Hieroglyphs
    else if sn='Miao' then c := unicode_scr_Miao
    else if sn='Modi' then c := unicode_scr_Modi
    else if sn='Mongolian' then c := unicode_scr_Mongolian
    else if sn='Mro' then c := unicode_scr_Mro
    else if sn='Multani' then c := unicode_scr_Multani
    else if sn='Myanmar' then c := unicode_scr_Myanmar
    else if sn='Nabataean' then c := unicode_scr_Nabataean
    else if sn='Nag_Mundari' then c := unicode_scr_Nag_Mundari
    else if sn='Nandinagari' then c := unicode_scr_Nandinagari
    else if sn='Newa' then c := unicode_scr_Newa
    else if sn='New_Tai_Lue' then c := unicode_scr_New_Tai_Lue
    else if sn='Nko' then c := unicode_scr_Nko
    else if sn='Nushu' then c := unicode_scr_Nushu
    else if sn='Nyiakeng_Puachue_Hmong' then c := unicode_scr_Nyiakeng_Puachue_Hmong
    else if sn='Ogham' then c := unicode_scr_Ogham
    else if sn='Ol_Chiki' then c := unicode_scr_Ol_Chiki
    else if sn='Old_Hungarian' then c := unicode_scr_Old_Hungarian
    else if sn='Old_Italic' then c := unicode_scr_Old_Italic
    else if sn='Old_North_Arabian' then c := unicode_scr_Old_North_Arabian
    else if sn='Old_Permic' then c := unicode_scr_Old_Permic
    else if sn='Old_Persian' then c := unicode_scr_Old_Persian
    else if sn='Old_Sogdian' then c := unicode_scr_Old_Sogdian
    else if sn='Old_South_Arabian' then c := unicode_scr_Old_South_Arabian
    else if sn='Old_Turkic' then c := unicode_scr_Old_Turkic
    else if sn='Old_Uyghur' then c := unicode_scr_Old_Uyghur
    else if sn='Oriya' then c := unicode_scr_Oriya
    else if sn='Osage' then c := unicode_scr_Osage
    else if sn='Osmanya' then c := unicode_scr_Osmanya
    else if sn='Pahawh_Hmong' then c := unicode_scr_Pahawh_Hmong
    else if sn='Palmyrene' then c := unicode_scr_Palmyrene
    else if sn='Pau_Cin_Hau' then c := unicode_scr_Pau_Cin_Hau
    else if sn='Phags_Pa' then c := unicode_scr_Phags_Pa
    else if sn='Phoenician' then c := unicode_scr_Phoenician
    else if sn='Psalter_Pahlavi' then c := unicode_scr_Psalter_Pahlavi
    else if sn='Rejang' then c := unicode_scr_Rejang
    else if sn='Runic' then c := unicode_scr_Runic
    else if sn='Samaritan' then c := unicode_scr_Samaritan
    else if sn='Saurashtra' then c := unicode_scr_Saurashtra
    else if sn='Sharada' then c := unicode_scr_Sharada
    else if sn='Shavian' then c := unicode_scr_Shavian
    else if sn='Siddham' then c := unicode_scr_Siddham
    else if sn='SignWriting' then c := unicode_scr_SignWriting
    else if sn='Sinhala' then c := unicode_scr_Sinhala
    else if sn='Sogdian' then c := unicode_scr_Sogdian
    else if sn='Sora_Sompeng' then c := unicode_scr_Sora_Sompeng
    else if sn='Soyombo' then c := unicode_scr_Soyombo
    else if sn='Sundanese' then c := unicode_scr_Sundanese
    else if sn='Syloti_Nagri' then c := unicode_scr_Syloti_Nagri
    else if sn='Syriac' then c := unicode_scr_Syriac
    else if sn='Tagalog' then c := unicode_scr_Tagalog
    else if sn='Tagbanwa' then c := unicode_scr_Tagbanwa
    else if sn='Tai_Le' then c := unicode_scr_Tai_Le
    else if sn='Tai_Tham' then c := unicode_scr_Tai_Tham
    else if sn='Tai_Viet' then c := unicode_scr_Tai_Viet
    else if sn='Takri' then c := unicode_scr_Takri
    else if sn='Tamil' then c := unicode_scr_Tamil
    else if sn='Tangsa' then c := unicode_scr_Tangsa
    else if sn='Tangut' then c := unicode_scr_Tangut
    else if sn='Telugu' then c := unicode_scr_Telugu
    else if sn='Thaana' then c := unicode_scr_Thaana
    else if sn='Thai' then c := unicode_scr_Thai
    else if sn='Tibetan' then c := unicode_scr_Tibetan
    else if sn='Tifinagh' then c := unicode_scr_Tifinagh
    else if sn='Tirhuta' then c := unicode_scr_Tirhuta
    else if sn='Toto' then c := unicode_scr_Toto
    else if sn='Ugaritic' then c := unicode_scr_Ugaritic
    else if sn='Vai' then c := unicode_scr_Vai
    else if sn='Vithkuqi' then c := unicode_scr_Vithkuqi
    else if sn='Wancho' then c := unicode_scr_Wancho
    else if sn='Warang_Citi' then c := unicode_scr_Warang_Citi
    else if sn='Yezidi' then c := unicode_scr_Yezidi
    else if sn='Yi' then c := unicode_scr_Yi
    else if sn='Zanabazar_Square' then c := unicode_scr_Zanabazar_Square;

    for ce := cp1 to cp2 do dat[ce] := c;
  end;
end;

var
f : text;
s : ansistring;

begin
  fillbyte(dat[0],high(dat)+1,unicode_scr_UNKNOWN);
  assign(f,'Scripts.txt');
  reset(f);
  while not(eof(f)) do begin
    readln(f,s);
    doline(s);
  end;
  close(f);
  analyse_block();
end.
