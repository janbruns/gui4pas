{$mode objfpc}
{$unitpath .}
{$unitpath dynlibx}
unit x11_drv;

interface
uses dynxlib,x,xutil,
     sysutils,baseunix,math,classes,drvif,renderbox;



type
Tx11windowID=Twindow;





type
TonMainwinResize = procedure(dx,dy : longint) of object;
TonMotion = procedure(dx,dy : longint; buttons : dword; t : TTime) of object;
TonButton = procedure(dx,dy : longint; buttons : dword; t : TTime) of object;

Tx11connection = class
  function connect(displayname : Pchar; d : longint) : boolean;
  function createAppwin(name : ansistring) : Tx11windowID;
  function createChildwin(par : Tx11windowID; x,y,w,h : longint) : Twindow;

  function create_ximage(w,h : longint) : PXImage;
  procedure destroy_ximage(img : PXImage);
  procedure fill_ximage(img : PXImage);
  function getRoot(win : TWindow) : Twindow;
  procedure send_winMgr_Message(var sev : TXEvent; win : Twindow);
  procedure toggle_fullscreen(win : TWindow);
  procedure toggle_maximized(win : TWindow);
  procedure lookup_atoms();


  procedure msgloop();

  constructor create();
  destructor destroy(); override;


  public
  win,w2,w3 : Tx11windowID;
  atom_atom,
  atom_string,
  atom_WM_DELETE_WINDOW : Tatom;
  display : PDisplay;
  vi : PXvisualinfo; 
  atom_clp : Tatom;
  
    atom_utf8_string,
    atom_utf8_string2,
    atom_text,
    atom_text_html,
    atom_NET_WM_STATE, 
  atom_NET_WM_STATE_FULLSCREEN,
  atom_NET_WM_STATE_MAXIMIZED_HORZ,
  atom_NET_WM_STATE_MAXIMIZED_VERT : Tatom;

  temp_wa : TXWindowAttributes;
  
  mainwin : Twindow;
  gc : Tgc;
  onMainwinResize : TonMainwinResize;
  onMotion : TonMotion;
  onButton : TonButton;
end;


TlayoutedXwindow = class;

Tbarex11driverinterface=class(Twinsysdriverinterface)
  function deeperup() : boolean; override;
  function startup() : boolean; override;
  function make_node(parent : Tlayoutedwindow; img : TsomeBGdesciptor; r : Twrect) : Tlayoutedwindow; override;
  function enterloop() : longint; override;
  procedure shutdown(); override;
  function tellname() : ansistring; override;
  procedure cleanup(); override;
  
  procedure onMainwinResize(dx,dy : longint); virtual;
  procedure onMotion(dx,dy : longint; buttons : dword; t : Ttime); virtual;
  procedure onButton(dx,dy : longint; buttons : dword; t : Ttime); virtual;
  
  destructor destroy(); override;
  
  public
  x11con : Tx11connection;
  tree : TlayoutedXwindow;
  
  
end;


TlayoutedXwindow=class(Tlayoutedwindow)
  constructor create(); override;
  destructor destroy(); override;
  procedure load(); override;
  
  public
  x11win : Twindow;
  pm : TPixmap;
  drv : Tbarex11driverinterface;  
end;



implementation





PROCEDURE setWindowName(display : Pdisplay; w : Tx11windowID; n : ansistring);
VAR namprop : TXTextProperty; pc : Pchar;
BEGIN
  pc := @n[1];
  XStringListToTextProperty(@pc,1,@namprop);
  XSetWMName(display,w,@namprop);
  Xfree(namprop.value);
END;

PROCEDURE setWindowProtocolls(display : Pdisplay; w : Tx11windowID; prot : Array of TAtom);
BEGIN
  XSetWMProtocols(display,w,@prot[low(prot)],1+high(prot)-low(prot));
END;


constructor Tx11connection.create();
begin
  inherited;
end;

destructor Tx11connection.destroy();
begin
  xFree(gc);
  xFree(vi);
  if display<>nil then XCloseDisplay(display);
end;

function Tx11connection.create_ximage(w,h : longint) : PXImage;
var img : PXImage;
begin
  new(img);
  with img^ do begin
    width  := w;
    height := h;
    xoffset:= 0;
    format := ZPixmap; // XYBitmap, XYPixmap, ZPixmap
    depth  := vi^.depth;
    bits_per_pixel := 32;
    red_mask   := $00FF0000;
    green_mask := $0000FF00;
    blue_mask  := $000000FF;
    byte_order := LSBFirst;//	LSBFirst, MSBFirst
    bitmap_unit:= 32; //quant. of scanline 8, 16, 32
    bitmap_bit_order := LSBFirst; // LSBFirst, MSBFirst 
    bitmap_pad := ZPixmap; // 8, 16, 32 either XY or ZPixmap 
    bytes_per_line := 4*w;
    getmem(data,int64(h)*bytes_per_line);
  end;
  XInitImage(img);
  create_ximage := img;
end;

procedure Tx11connection.destroy_ximage(img : PXImage);
begin
  with img^ do begin
    freemem(data,int64(height)*bytes_per_line);
  end;
  dispose(img);
end;

procedure Tx11connection.fill_ximage(img : PXImage);
var y,x : longint; p : Pbyte;
begin
  with img^ do begin
    for y := 0 to height-1 do begin
      p := pointer(data);
      p := @p[y*bytes_per_line];
      for x := 0 to width-1 do begin
        p[4*x+0] := 255;
        p[4*x+1] := 255; //random(256);
        p[4*x+2] := 255;
        p[4*x+3] := 255;
      end;
    end;
  end;
end;

function Tx11connection.getRoot(win : TWindow) : Twindow;
begin
  XGetWindowAttributes(display,win,@temp_wa);
  getRoot := temp_wa.root;
end;

procedure Tx11connection.send_winMgr_Message(var sev : TXEvent; win : Twindow);
begin
  sev.xclient._type := ClientMessage;
  sev.xclient.window := win;
  XSendEvent(display, getRoot(win) , false, SubstructureRedirectMask or SubstructureNotifyMask, @sev );
end;

procedure Tx11connection.toggle_fullscreen(win : TWindow);
var sev : TXEvent;
begin
  fillbyte(sev,sizeof(sev),0);
  with sev.xclient do begin
    message_type := atom_NET_WM_STATE;
    format := 32;
    data.l[0] := 2; // _NET_WM_STATE_TOGGLE
    data.l[1] := atom_NET_WM_STATE_FULLSCREEN;
    data.l[2] := none;
    data.l[3] := 1;
  end;
  send_winMgr_Message(sev,win);
end;

procedure Tx11connection.toggle_maximized(win : TWindow);
var sev : TXEvent;
begin
  fillbyte(sev,sizeof(sev),0);
  with sev.xclient do begin
    message_type := atom_NET_WM_STATE;
    format := 32;
    data.l[0] := 2; // _NET_WM_STATE_TOGGLE
    data.l[1] := atom_NET_WM_STATE_MAXIMIZED_VERT;
    data.l[2] := atom_NET_WM_STATE_MAXIMIZED_HORZ;
    data.l[3] := 1;
  end;
  send_winMgr_Message(sev,win);
end;

procedure Tx11connection.lookup_atoms();
begin
  atom_atom := XInternAtom(display,'ATOM',false);
  atom_string := XInternAtom(display,'STRING',false);

  atom_NET_WM_STATE := XInternAtom(display,'_NET_WM_STATE',false);
  atom_NET_WM_STATE_MAXIMIZED_HORZ := XInternAtom(display,'_NET_WM_STATE_MAXIMIZED_HORZ',false);
  atom_NET_WM_STATE_MAXIMIZED_VERT := XInternAtom(display,'_NET_WM_STATE_MAXIMIZED_VERT',false);
  atom_NET_WM_STATE_FULLSCREEN := XInternAtom(display,'_NET_WM_STATE_FULLSCREEN',false);
  atom_WM_DELETE_WINDOW := XInternAtom(display,'WM_DELETE_WINDOW',false);

  atom_clp := XInternAtom(display,'CLIPBOARD',false);
  atom_utf8_string := XInternAtom(display,'UTF8_STRING',false);
  atom_utf8_string2 := XInternAtom(display,'text/plain;charset=utf-8',false);
  atom_text := XInternAtom(display,'TEXT',false);
  atom_text_html := XInternAtom(display,'text/html',false);;
end;







function Tx11connection.connect(displayname : Pchar; d : longint) : boolean;
var v0 : TXvisualinfo; cnt : longint;
begin
  gc := nil;
  connect := false;
  display := XOpenDisplay(displayname);
  if not(display=nil) then begin
    lookup_atoms();
    connect := true;
    cnt := 1;
    with v0 do begin
      depth:=d;
      _class := TrueColor;
      red_mask   := $00FF0000;
      green_mask := $0000FF00;
      blue_mask  := $000000FF;
      screen := XDefaultScreen(display);
    end;
    vi := XGetVisualInfo(
            display,
            VisualClassMask
            or VisualScreenMask
            or VisualDepthMask 
            or VisualRedMaskMask 
            or VisualGreenMaskMask 
            or VisualBlueMaskMask,
            @v0,
            @cnt
         );
    writeln('vis-info cnt: ',cnt);
  end;
end;

function Tx11connection.createChildwin(par : Twindow; x,y,w,h : longint) : Twindow;
var win2 : Tx11windowID;
begin
  win2 := XCreateWindow( 
           display, 
           par, 
           x, y, w,h, 
           0,
           vi^.depth,InputOutput,vi^.visual,
           0,nil
         );
  //XSelectInput(display,win2, ExposureMask  );
  XMapRaised(display,win2);
  createChildwin := win2;
end;

function Tx11connection.createAppwin(name : ansistring) : Tx11windowID;
var  a : TXSetWindowAttributes; 
begin
  a.colormap := XCreateColormap(display, XRootWindow(display,XDefaultScreen(display)), vi^.visual, AllocNone);
  a.border_pixel := $FF808080;
  a.background_pixel := $FF808080;
  a.backing_store := NotUseful;//Always;
  
  writeln('vi^.depth:',vi^.depth);
  win := XCreateWindow( 
           display, 
           XRootWindow(display,XDefaultScreen(display)), 
           0, 0, 500,300, 
           3,
           vi^.depth,InputOutput,vi^.visual,
           CWColormap or CWBackPixel or CWBorderPixel or CWBackingStore
           ,@a
         );
  
  setWindowProtocolls(display,win, [atom_WM_DELETE_WINDOW]  );
  setWindowName(display,win,name);

  XSelectInput(display,win,
    ExposureMask 
    or ButtonPressMask
    or ButtonReleaseMask
    or EnterWindowMask
    or LeaveWindowMask
    or PointerMotionMask
    or KeyPressMask  
    or KeyReleaseMask
    or FocusChangeMask
    or PropertyChangeMask
    or StructureNotifyMask
    //or SubstructureNotifyMask
    //or ResizeRedirectMask
  );
  
  
  gc := XCreateGC(display, win, 0, nil);
  
  XMapRaised(display,win);
  mainwin := win;
  createAppwin := win;
end;










procedure Tx11connection.msgloop();
var ev : TXEvent; done : boolean; v,buttons : dword;
begin
  done := false;
  buttons := 0;
  repeat
    XNextEvent(display,@ev);
    case ev.xany._type of
      GraphicsExpose : begin
      end;
      Expose : begin
        writeln('expose: win=',ev.xexpose.window,' x/y=',ev.xexpose.x,'/',ev.xexpose.y,' w/h=',ev.xexpose.width,'/',ev.xexpose.height);
      end;
      ClientMessage : begin
          writeln('client Message');
          if ev.xclient.format=32 then begin
            if (Tatom(ev.xclient.data.l[0])=atom_WM_DELETE_WINDOW) then begin
              XDestroyWindow(display,ev.xclient.window);
              done := true;
            end;
          end;
        end;
      MotionNotify : begin
        if (ev.xmotion.window=mainwin)and assigned(onMotion) 
        then onMotion(ev.xmotion.x,ev.xmotion.y,buttons,ev.xmotion.time);
      end;
      ButtonPress : begin
        v := 0;
        case ev.xbutton.button of
          1 : v := 1;
          2 : v := $10;
          3 : v := 2;
        end;
        if v>0 then begin
          buttons := buttons or v;
          v := buttons;
          if (ev.xbutton.window<>mainwin) then v := v or $80000000;
          if assigned(onButton) then onButton(ev.xbutton.x,ev.xbutton.y,v,ev.xbutton.time);
        end;
      end;
      ButtonRelease : begin
        v := 0;
        case ev.xbutton.button of
          1 : v := 1;
          2 : v := $10;
          3 : v := 2;
        end;
        if v>0 then begin
          buttons := buttons xor (v and buttons);
          v := buttons;
          if (ev.xbutton.window<>mainwin) then v := v or $80000000;
          if assigned(onButton) then onButton(ev.xbutton.x,ev.xbutton.y,v,ev.xbutton.time);
        end;
      end;
      LeaveNotify,
      EnterNotify : begin
//                      with ev.xcrossing do writeln('cross',_type,' win=',window,' sub=',subwindow,' focus:',focus,' detail:',detail,' ',detail=NotifyInferior);
//                      besttarg := None;
//                      bestwin :=  ev.xcrossing.window;
//                      clp.enum_clp_targets(@print_target, ev.xcrossing.window);
//                      writeln('set_clpdat:',clp.set_clp_data(atom_clp, @convert_clp,nil, clp_targets,w,ev.xcrossing.time));
                    end;
      UnmapNotify : ;
      ResizeRequest : begin
//        writeln('ResizeRequest ',(ev.xresizerequest.window=mainwin),' x:',ev.xresizerequest.width);
//        if ev.xresizerequest.window=mainwin then begin
//          XResizeWindow(display, mainwin, ev.xresizerequest.width, ev.xresizerequest.height);
//        end;
      end;
      ConfigureNotify : begin
        
        if ev.xconfigure.window=mainwin then begin
          if assigned(onMainwinResize) then begin
            onMainwinResize(ev.xconfigure.width, ev.xconfigure.height);
          end;
        end;
      end;
      else begin
      end; 
    end;
  until done;
end;



constructor TlayoutedXwindow.create();
begin
  inherited;
end;

destructor TlayoutedXwindow.destroy();
begin
  inherited ;
  if pm<>0 then XFreePixmap(drv.x11con.display,pm);
  //XDestroyWindow(drv.x11con.display,x11win);
end;

procedure TlayoutedXwindow.load();
var imgd : TimgBGdesciptor; x11con : Tx11connection; bpp,bpl : longint; img : PXImage; p,p2 : pointer;
begin
  if dirty then begin 
    x11con := drv.x11con;
    if BG is TcolorBGdesciptor then begin
      XSetWindowBackground(x11con.display, x11win, (BG as TcolorBGdesciptor).col)
    end else if BG is TimgBGdesciptor then begin
      imgd := BG as TimgBGdesciptor;
      r.dx := imgd.width;
      r.dy := imgd.height;
      pm := XCreatePixmap(x11con.display, x11win, r.dx, r.dy, x11con.vi^.depth);
      img := x11con.create_ximage(r.dx,r.dy);
      fillbyte(img^.data^,img^.bytes_per_line*int64(img^.height),0);
      
      p2 := imgd.map_rgba8888(bpp,bpl);
      p := img^.data;
      if (bpl=img^.bytes_per_line) then begin
        writeln('full-block copy');
        move(p2^,p^,bpl*int64(r.dy));
      end;
      imgd.unmap();
      XPutImage(x11con.display, pm, x11con.gc, img, 0, 0, 0, 0, r.dx, r.dy);
      x11con.destroy_ximage(img);
      XSetWindowBackgroundPixmap(x11con.display, x11win, pm);
      XResizeWindow(x11con.display, x11win, r.dx, r.dy);
      XClearArea(x11con.display, x11win, 0, 0, r.dx, r.dy, true);
    end;
    dirty := false;
  end;
end;






function Tbarex11driverinterface.deeperup() : boolean;
var an : ansistring;
begin
  an := 'Testapp';
  an := reverseif.get_config_str('appname',an);
  x11con.createAppwin(an);
  deeperup := true;
end;

function Tbarex11driverinterface.startup() : boolean;
var displayname : Pchar; dn : ansistring;
begin
  dn := reverseif.get_config_str('displayname','');
  if length(dn)=0 
  then displayname := nil
  else displayname := @dn[1];
  startup := false;
  x11con := Tx11connection.create();
  x11con.onButton := @onButton;
  x11con.onMotion := @onMotion;
  x11con.onMainwinResize := @onMainwinResize;  
  if x11con.connect(displayname, reverseif.get_config_int('depth',32) ) then begin
    startup := true;
  end else writeln('couldn''t open display ',dn);
end;

function Tbarex11driverinterface.make_node(parent : Tlayoutedwindow; img : TsomeBGdesciptor; r : Twrect) : Tlayoutedwindow;
var w : TlayoutedXwindow;
begin
  w := TlayoutedXwindow.create();
  if assigned(parent) then begin
    w.x11win := x11con.createChildwin((parent as TlayoutedXwindow).x11win,r.x,r.y,r.dx,r.dy);
  end else begin
    w.x11win := x11con.createChildwin(x11con.mainwin,r.x,r.y,r.dx,r.dy);
    tree := w;
  end;
  w.drv := self;
  w.BG := img;
  w.dirty := true;
  w.load();
  if parent<>nil then parent.insertChild(w);
  make_node := w;
end;


function Tbarex11driverinterface.enterloop() : longint;
begin
  enterloop := 0;
  if assigned(x11con) then x11con.msgloop();
end;

procedure Tbarex11driverinterface.shutdown();
begin
  freeandnil(tree);
  if assigned(x11con) then begin
    //if x11con.mainwin<>0 then XDestroyWindow(x11con.display,x11con.mainwin);
    
  end;
  freeandnil(x11con);
end;

function Tbarex11driverinterface.tellname() : ansistring;
begin
  result := 'bareX11';
end;

procedure Tbarex11driverinterface.cleanup();
begin
end;

procedure Tbarex11driverinterface.onMainwinResize(dx,dy : longint); 
begin
  if assigned(tree) then begin
    XResizeWindow(x11con.display, tree.x11win, dx, dy);
    tree.r.dx := dx;
    tree.r.dy := dy;
  end;
end;

procedure Tbarex11driverinterface.onMotion(dx,dy : longint; buttons : dword; t : TTime);
begin
  reverseif.onMouseEvent(dx,dy,{_$40000000 or} buttons,0.001*t);
end;

procedure Tbarex11driverinterface.onButton(dx,dy : longint; buttons : dword; t : Ttime);
begin
  reverseif.onMouseEvent(dx,dy,buttons,0.001*t);
end;


destructor Tbarex11driverinterface.destroy();
begin
  freeandnil(tree);
  freeandnil(x11con);
  inherited;
end;

var
d : Tbarex11driverinterface;


initialization
begin 
  d := Tbarex11driverinterface.create();
  register_driver(d);
end;


finalization
begin
  freeandnil(d);
end;


end.
