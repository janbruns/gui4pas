{$mode objfpc}
unit unicode_utf8;

interface

function UTF8_codelen(c : char) : byte;
function UTF8_getcodepoint_autoinc(const s : ansistring; var idx : dword) : dword;
function UTF8_getcodepoint(const s : ansistring; idx : dword) : dword;
function UTF8_codepoint_as_UTF8(c : dword) : ansistring;


implementation
uses sysutils;


function UTF8_codelen(c : char) : byte;
var q,b : byte;
begin
  q := ord(c);
  if (q and $80)=0 then begin
    UTF8_codelen := 1
  end else begin
    b := 2;
    q := (q*4);
    while (q and $80)>0 do begin
      q *= 2;
      b += 1;
    end;
    UTF8_codelen := b;
  end;
end;

const
maskarr : array[0..7] of byte = (0,$7f,$3f,$1f,$f,$7,$3,$1);

function UTF8_getcodepoint_autoinc(const s : ansistring; var idx : dword) : dword;
var q,c : byte; d : dword;
begin
  result := $FFFD;
  c := ord(s[idx]);
  d := 0;
  q := UTF8_codelen(chr(c)) and 7;
  d := maskarr[q] and c;
  dec(q);
  while q>0 do begin
    inc(idx);
    if idx>length(s) then exit; //raise Exception.create('utf-8 charlen exceeds string');
    c := ord(s[idx]);
    if (c and $c0)<>$80 then exit; // raise Exception.create('utf-8 char malformed'+hexstr(c,2));
    d := (d shl 6) or (c and $3f);
    dec(q);
  end;
  inc(idx);
  result := d;
end;

function UTF8_getcodepoint(const s : ansistring; idx : dword) : dword;
begin
  result := UTF8_getcodepoint_autoinc(s,idx);
end;

var tab2 : array[1..8] of byte = (0,%11000000,%11100000,%11110000,%11111000,%11111100,%11111110,%11111111);

function UTF8_codepoint_as_UTF8(c : dword) : ansistring;
var s : ansistring; n,i : longint;
begin
  n := 1;
  if c>$007F then begin // 7 bit
    inc(n);
    if c>$07FF then begin // 11 = 5 + 6 bit
      inc(n);
      if c>$FFFF then begin // 16 = 4 + 2*6 bit
        inc(n);
        if (c>$1FFFFF) then begin // 21 = 3 + 3*6 bit
          inc(n);
          if (c>$3FFFFFF) then begin // 26 = 2 + 4*6 bit
            inc(n);
            if (c>$7FFFFFFF) then begin // 31 = 1 + 5*6 bit
              inc(c);
            end;
          end;
        end;
      end;
    end;
  end;
  setlength(s,n);
  for i := n downto 2 do begin
    s[i] := chr((c and $3f)or($80));
    c := c shr 6;
  end;
  s[1] := chr(c or tab2[n]);
  UTF8_codepoint_as_UTF8 := s;
end;

begin
end.
