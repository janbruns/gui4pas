{$mode objfpc}
unit x11_error;

interface
uses xlib,x,baseunix;

procedure begin_no_xerr(d : Pdisplay);
function end_no_xerr(d : Pdisplay) : PXErrorEvent;


implementation

type
Tx11errorhandler = function(d : Pdisplay; r : PXErrorEvent) : cint; cdecl;

var
oldrh : Tx11errorhandler;
xerr : TXErrorEvent;
noxerr,waserr : culong;

procedure begin_no_xerr(d : Pdisplay);
begin
  noxerr := XNextRequest(d);
end;

function end_no_xerr(d : Pdisplay) : PXErrorEvent;
begin
  XSync(d,false);
  if (noxerr=waserr) then end_no_xerr := @xerr else end_no_xerr := nil;
end;

function errhandler(d : Pdisplay; r : PXErrorEvent) : cint; cdecl;
begin
  writeln('error handler here');
  if r^.serial=noxerr then begin
    move(r^,xerr,sizeof(xerr));
    waserr := noxerr;
    errhandler := 0;
  end else errhandler := oldrh(d,r);
end;



begin
  noxerr := -1;
  waserr := -2;
  oldrh := XSetErrorHandler(@errhandler);
end.

