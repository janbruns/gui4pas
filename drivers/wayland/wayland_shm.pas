{$mode objfpc}
{$unitpath .}
unit wayland_shm;


interface
uses minifont,paswl_collection, paswl_collection2, wl_libwl, wl_core,wl_basic_types,
dbllinkedlist,
paswl_shm_pool,
paswl_buffer,
sysutils,
paswl_bindable,
paswl_seat,
paswl_pointer,
paswl_surface,
paswl_subsurface,
paswl_xdg_wm_base,
paswl_xdg_surface,
paswl_xdg_toplevel,
paswl_xdg_ext1,
paswl_shell,
paswl_viewport,
paswl_shell_surface,
paswl_decor_border,
paswl_decor_knobs,
paswl_decor,
math,
drvif
;


implementation
uses renderbox;

type

Txdgdecor=class;
Twlshelldecor=class;

Tdecorxdgmainsurf = class(Tpaswl_xdg_surface)
  protected procedure cb_xdg_surface_configure(serial : dword); override;
  public decor : Txdgdecor;
end;
Txdgdecor=class(Tdecor)
  public
  procedure initial_settings(); override;
  procedure init(); override;
  procedure check_states();
  procedure onConfigure(c : Tpaswl_xdg_surface; serial : dword);
  procedure setgeo(); override;
  procedure doack(var q : qword); override;
  destructor destroy(); override;
  procedure onMaximizeButton(k : Tpaswl_decor_knob); override; 
  procedure addStatereq(serial : dword; var st : TXDGstatereq);
  procedure start_resize(seat : Tpaswl_seat; serial,where : dword); override;
  procedure start_move(seat : Tpaswl_seat; serial : dword); override;
  procedure onFrame(d : dword); override;

  public
  maintoplevel : Txdgtoplevel_with_states;
  mainxdg : Tdecorxdgmainsurf;
  reqstate : TXDGstatereq;
end;

Twlshellmainsurf = class(Tpaswl_shell_surface)
  protected procedure cb_shell_surface_configure(edges : dword; width, height : longint); override;
  public decor : Twlshelldecor;
end;
Twlshelldecor=class(Tdecor)
  public
  procedure initial_settings(); override;
  procedure init(); override;
  procedure setgeo(); override;
  procedure doack(var q : qword); override;
  destructor destroy(); override;

  procedure onMaximizeButton(k : Tpaswl_decor_knob); override; 
  procedure onConfigure(edges : dword; width, height : longint);
  procedure start_resize(seat : Tpaswl_seat; serial,where : dword); override;
  procedure start_move(seat : Tpaswl_seat; serial : dword); override;
  procedure onFrame(d : dword); override;
  procedure onEnter(serial : dword; surface : Tpaswl_bindable; surface_x, surface_y : Twlfixed); override;

  public
  mainshellsurf : Twlshellmainsurf;
  use_wlshell_resizeworkaround : boolean;
end;




procedure Txdgdecor.check_states();
var didresize : boolean;
begin
  didresize := false;
  
  if reqstate.close then clc.alldone := true;
  if reqstate.resize <>state_resizeing then begin
    state_resizeing := not state_resizeing;
    redrawwhat := redrawwhat or redraw_titlebar_bar;
    if not state_resizeing then redrawwhat := redrawwhat or redraw_doneresize;
  end;
  if reqstate.activated<>state_activated then begin
    redrawwhat := redrawwhat or redraw_titlebar_bar;
    state_activated := not state_activated;
  end;
  if reqstate.maximized<>state_maximized then begin
    state_maximized := not state_maximized;
    redrawwhat := redrawwhat or redraw_resize or redraw_doneresize;;
    if not state_maximized then begin
      reqstate.width := stored_width;
      reqstate.height := stored_height;
    end else begin
      stored_width := planwidth;
      stored_height := planheight;
    end;
  end else;

  if reqstate.width<>0 then begin
    if planwidth<>reqstate.width then begin
      redrawwhat := redrawwhat or redraw_resize or repaint_on_resize;
      planwidth := min(maxwidth,max(minwidth,reqstate.width));
      reqstate.width := 0;
      didresize := true;
    end;
  end;
  if reqstate.height<>0 then begin
    if planheight<>reqstate.height then begin
      redrawwhat := redrawwhat or redraw_resize or repaint_on_resize;
      planheight := min(maxheight,max(minheight,reqstate.height));
      reqstate.height := 0;
      didresize := true;
    end;
  end;
  if didresize then prepare_new_size();
end;

procedure Txdgdecor.addStatereq(serial : dword; var st : TXDGstatereq);
begin
  reqserial := serial;
  reqstate.close := reqstate.close or st.close;
  reqstate.maximized := st.maximized;
  reqstate.fullscreen:= st.fullscreen;
  reqstate.resize    := st.resize;
  reqstate.activated := st.activated;
  reqstate.suspended := st.suspended;
  if st.width<>0  then reqstate.width  := st.width;
  if st.height<>0 then reqstate.height := st.height;
  st.width := 0;
  st.height := 0;
end;



procedure Txdgdecor.onConfigure(c : Tpaswl_xdg_surface; serial : dword);
begin
  if c=mainxdg then begin
  //writeln('onconfigure',serial);
    addStatereq(serial, maintoplevel.reqstate);
    request_draw_frame();
  end;
end;

procedure Txdgdecor.onMaximizeButton(k : Tpaswl_decor_knob);
begin
  writeln('Txdgdecor.onMaximizeButton');
  if state_maximized 
  then maintoplevel.unset_maximized()
  else maintoplevel.set_maximized();
end;

procedure Txdgdecor.onFrame(d : dword);
begin
  if reqserial<>invalid_serial then begin
    mainxdg.ack_configure(reqserial);
    reqserial := invalid_serial;
    check_states();
  end;
  redraw();
  redrawwhat := 0;
  init_XDGreq_states(reqstate);
  //request_draw_frame();
end;


procedure Txdgdecor.initial_settings();
begin
  inherited;
  repaint_on_resize := redraw_doneresize;
  name := 'Nothing as emptyness';
  appid := 'app.example.gui.wlxdg';
end;









procedure Txdgdecor.init();
begin
//  freeandnil(clc.subcompositor);
//  freeandnil(clc.viewporter);
  initial_settings();

  mainsurf := clc.compositor.create_surface(Tpaswl_surface) as Tpaswl_surface;
  mainxdg := clc.xdg_wm_base.get_xdg_surface(mainsurf,Tdecorxdgmainsurf) as Tdecorxdgmainsurf;
  maintoplevel := mainxdg.get_toplevel(Txdgtoplevel_with_states) as Txdgtoplevel_with_states;
  mainsurf.commit();
  while not mainxdg.seen_configure do clc.display.display_roundtrip();
  

  if assigned(clc.subcompositor) then begin
    titlesurf := clc.compositor.create_surface(Tpaswl_surface) as Tpaswl_surface;
    titlesub  := clc.subcompositor.get_subsurface(titlesurf,mainsurf,Tpaswl_subsurface);
  end;
  pool := clc.shm.create_managed_pool(1024);
  make_fallbackcursor();
  mouse := clc.seat.get_pointer(Tdecormouse) as Tdecormouse;
  if assigned(mouse) then mouse.decor := self;

  make_mainbuf();
  mainsurf.attach(cbuf);
  
  draw_titletext();
  if assigned(titlesurf) then titlesurf.commit();
  make_border();
  makeknobs();

  while not mainxdg.seen_configure do clc.display.display_roundtrip();
  mainxdg.set_window_geometry(0,0,planwidth,planheight);
  maintoplevel.set_min_size(minwidth,minheight);
  maintoplevel.set_max_size(maxwidth,maxheight);
  maintoplevel.set_title(@name[1]);
  maintoplevel.set_app_id(@appid[1]);
  mainxdg.ack_configure(mainxdg.consume_configure());
  request_draw_frame();
  mainsurf.commit();
  mainxdg.decor := self;
  redrawwhat := redrawwhat or redraw_titlebar_but or redraw_resize or redraw_doneresize;
end;



procedure Txdgdecor.setgeo();
begin
  mainxdg.set_window_geometry(0,0,planwidth,planheight);
end;

procedure Txdgdecor.doack(var q : qword);
begin
  if (q<>invalid_serial) then mainxdg.ack_configure(q);
end;



procedure Txdgdecor.start_resize(seat : Tpaswl_seat; serial,where : dword);
begin
  maintoplevel.resize(seat, serial, where);
end;

procedure Txdgdecor.start_move(seat : Tpaswl_seat; serial : dword);
begin
  maintoplevel.move(seat, serial);
end;



destructor Txdgdecor.destroy();
begin
  inherited;
  freeandnil(maintoplevel);
  freeandnil(mainxdg);
end;



procedure Tdecorxdgmainsurf.cb_xdg_surface_configure(serial : dword);
begin
  if assigned(decor) then decor.onConfigure(self,serial) else inherited;
end;








procedure Twlshelldecor.initial_settings();
begin
  inherited;
  name := 'Nothing as emptyness';
  appid := 'app.example.gui.wlshell';
  repaint_on_resize := redraw_doneresize;
  use_wlshell_resizeworkaround := true; {weston}
end;

procedure Twlshelldecor.init();
begin
//  freeandnil(clc.subcompositor);
//  freeandnil(clc.viewporter);
  initial_settings();
  mainsurf := clc.compositor.create_surface(Tpaswl_surface) as Tpaswl_surface;
  mainshellsurf := clc.coreshell.get_shell_surface(mainsurf,Twlshellmainsurf) as Twlshellmainsurf;
  mainshellsurf.decor := self;
  writeln('mainshellsurf=',hexstr(ptruint(mainshellsurf),16));
  writeln('mainshellsurf.wlo=',hexstr(ptruint(mainshellsurf.wlo),16));

  mainsurf.commit();
  clc.display.display_roundtrip();  

  if assigned(clc.subcompositor) then begin
    titlesurf := clc.compositor.create_surface(Tpaswl_surface) as Tpaswl_surface;
    titlesub  := clc.subcompositor.get_subsurface(titlesurf,mainsurf,Tpaswl_subsurface);
  end;
  pool := clc.shm.create_managed_pool(1024);
  make_fallbackcursor();
  mouse := clc.seat.get_pointer(Tdecormouse) as Tdecormouse;
  if assigned(mouse) then mouse.decor := self;

  make_mainbuf();
  mainsurf.attach(cbuf);
  
  draw_titletext();
  if assigned(titlesurf) then titlesurf.commit();
  make_border();
  makeknobs();

  clc.display.display_roundtrip();
  mainshellsurf.set_title(@name[1]);
  mainshellsurf.set_class(@appid[1]);
  mainshellsurf.set_toplevel();
  
  request_draw_frame();
  redrawwhat := redrawwhat or redraw_titlebar_but or redraw_resize or redraw_doneresize;
  mainsurf.commit();
end;

procedure Twlshelldecor.setgeo();
begin
end;

procedure Twlshelldecor.doack(var q : qword);
begin
end;

destructor Twlshelldecor.destroy();
begin
  freeandnil(mainshellsurf);
  inherited;
end;

procedure Twlshelldecor.onMaximizeButton(k : Tpaswl_decor_knob);
begin
  writeln('Twlshelldecor.onMaximizeButton');
  if state_maximized then begin
    mainshellsurf.set_toplevel();
    planwidth := stored_width;
    planheight := stored_height;
    state_maximized := false;
    prepare_new_size();
    redrawwhat := redrawwhat or redraw_resize or redraw_doneresize;
    request_draw_frame();
    mainsurf.commit();
  end else begin
    stored_width := planwidth;
    stored_height := planheight;
    mainshellsurf.set_maximized(nil);
    state_maximized := true;
    redrawwhat := redrawwhat or redraw_resize or redraw_doneresize;
  end;
end;

procedure Twlshelldecor.start_resize(seat : Tpaswl_seat; serial,where : dword);
begin
  state_resizeing := true;
  writeln('Start resize');
  mainshellsurf.resize(seat, serial, where);
end;

procedure Twlshelldecor.start_move(seat : Tpaswl_seat; serial : dword);
begin
  mainshellsurf.move(seat, serial);
end;

procedure Twlshelldecor.onFrame(d : dword);
begin
  redraw();
  redrawwhat := 0;
end;

procedure Twlshelldecor.onEnter(serial : dword; surface : Tpaswl_bindable; surface_x, surface_y : Twlfixed);
begin
  inherited;
  if state_resizeing and use_wlshell_resizeworkaround then begin
    state_resizeing := false;
    redrawwhat := redrawwhat or redraw_resize or redraw_doneresize;
    request_draw_frame();
    mainsurf.commit();
    writeln('Done resize');
  end;
end;

procedure Twlshelldecor.onConfigure(edges : dword; width, height : longint);
var sc : boolean;
begin
  sc := false;
  if (width>0)and(width<>planwidth) then begin
    planwidth := min(maxwidth,max(minwidth,width));
    sc := true;
  end;
  if (height>0)and(height<>planheight) then begin
    planheight := min(maxheight,max(minheight,height));
    sc := true;
  end;
  if sc or state_resizeing then begin
    prepare_new_size();
    redrawwhat := redrawwhat or redraw_resize or repaint_on_resize;
    if (edges=0)and(not(use_wlshell_resizeworkaround)) then begin
      state_resizeing := false;
      redrawwhat := redrawwhat or redraw_doneresize;
      writeln('Done resize');
    end;
    request_draw_frame();
    mainsurf.commit();
  end;
end;

procedure Twlshellmainsurf.cb_shell_surface_configure(edges : dword; width, height : longint);
begin
  if assigned(decor) then decor.onConfigure(edges, width, height) else inherited;
end;




type
Tlayoutedwlwindow = class;
Tdrivertreesurface = class(Tpaswl_surface)
  public
  layoutwin : Tlayoutedwlwindow;
end;
Tlayoutedwlwindow=class(Tlayoutedwindow)
  destructor destroy(); override;
  procedure paint(gx,gy :longint);
  
  procedure preparemode_container();
  procedure preparemode_bgcolor_using_vp();
  procedure preparemode_bgcolor_using_vb();
  procedure preparemode_bgcolor_image_vb();
  procedure load(); override;
  
  public
  surf : Tdrivertreesurface;
  sub : Tpaswl_subsurface;
  vp : Tpaswl_viewport;
  buf : T_paswl_buffer;
  vbuf : Tpaswl_virtualbuffer;
  pool : Tpaswl_shm_managedpool;
  mode : longint;
end;


destructor Tlayoutedwlwindow.destroy();
begin
  inherited;
  freeandnil(buf);
  freeandnil(vbuf);
  freeandnil(vp);
  freeandnil(sub);
  freeandnil(surf);
end;

procedure Tlayoutedwlwindow.preparemode_container();
begin
  freeandnil(buf);
  buf := pool.create_buffer(1,1,4,0,Tpaswl_buffer) as Tpaswl_buffer;
  buf.setPixel(0,0,$00000000);
  mode := 1;
end;

procedure Tlayoutedwlwindow.preparemode_bgcolor_using_vp();
begin
  freeandnil(buf);
  buf := pool.create_buffer(1,1,4,0,Tpaswl_buffer) as Tpaswl_buffer;
  buf.setPixel(0,0,(BG as TcolorBGdesciptor).col);
  mode := 2;
end;

procedure Tlayoutedwlwindow.preparemode_bgcolor_using_vb();
var col : dword;
begin
  col := (BG as TcolorBGdesciptor).col;
  freeandnil(buf);
  freeandnil(vbuf);
  vbuf := pool.create_buffer(r.dx,r.dy,4*r.dx,0,Tpaswl_virtualbuffer) as Tpaswl_virtualbuffer;
  vbuf.fillcolor(col,col,col,col);
  mode := 3;
end;

procedure Tlayoutedwlwindow.preparemode_bgcolor_image_vb();
var img : TimgBGdesciptor; x,y,dx,dy,bpl,bpp : longint; p0,psrc : Pbyte;
begin
  freeandnil(buf);
  freeandnil(vbuf);
  img := (BG as TimgBGdesciptor);
  dx := img.width;
  dy := img.height;
  psrc := img.map_rgba8888(bpp,bpl);
  vbuf := pool.create_buffer(dx,dy,4*dx,0,Tpaswl_virtualbuffer) as Tpaswl_virtualbuffer;
  p0 := vbuf.getDataPtr();
  if (bpp=4) then begin
    if bpl=dx*bpp then begin
      move(psrc^,p0^,4*dx*int64(dy));
    end else begin
      for y := 0 to dy-1 do begin
        move(psrc[int64(y)*bpl],p0[4*dx*int64(y)],4*dx);
      end;
    end;
  end else begin
    for y := 0 to dy-1 do begin
      for x := 0 to dx-1 do begin
        Pdword(@p0[4*dx*int64(y) + 4*x])^ := Pdword(@psrc[int64(y)*bpl +x*bpp])^;
      end;
    end;  
  end;
  img.unmap();
  r.dx := dx;
  r.dy := dy;
  mode := 4;
end;

procedure Tlayoutedwlwindow.load();
begin
  if assigned(BG) then begin
    if (BG is TimgBGdesciptor) then begin
      preparemode_bgcolor_image_vb();
    end else if (BG is TcolorBGdesciptor) then begin
      if assigned(vp) 
      then preparemode_bgcolor_using_vp()
      else preparemode_bgcolor_using_vb();
    end else preparemode_container();
  end else preparemode_container();
  dirty := false;
end;

procedure Tlayoutedwlwindow.paint(gx,gy :longint);
var p : Pdbllinkedlist; c : Tlayoutedwlwindow;
begin
  if dirty then begin
    load();
    surf.damage(0,0,r.dx,r.dy);
  end;
writeln('paint' ,r.x,'/',r.y,' ',r.dx,'/',r.dy);
  calc_vr();
  if (vr.dx>0) and (vr.dy>0) then begin
    if (mode=3) or (mode=4) then begin
      if assigned(buf) then begin
        buf := (buf as Tpaswl_recropablebuffer).recrop(vr.x,vr.y,vr.dx,vr.dy);
      end else begin
        buf := vbuf.get_recropable(vr.x,vr.y,vr.dx,vr.dy);
      end;
    end;
    surf.attach(buf);
    if assigned(vp) then begin
      case mode of
        1 : vp.set_source(0,0,256,256);
        2 : vp.set_source(0,0,256,256);
        3 : vp.set_source(vr.x*256,vr.y*256,256*buf.width,256*buf.height);
        4 : vp.set_source(vr.x*256,vr.y*256,256*buf.width,256*buf.height);
      end;
      vp.set_destination(vr.dx,vr.dy);
    end;    
    sub.set_position(r.x+vr.x,r.y+vr.y);
    writeln('paint.setpos: ',r.x+vr.x,'/',r.y+vr.y,' s=',vr.dx,'/',vr.dy);
    p := dblLinkedList_getFirst(childlist);
    //p := nil;
    while p<>nil do begin
      c := Tlayoutedwlwindow(p^.dat);
      c.paint(gx,gy);
      p := dblLinkedList_getNext(p);
    end;
    
    surf.commit();
  end else begin
    surf.attach(nil);
    surf.commit();
  end;
end;




type
Twldriverinterface=class(Twinsysdriverinterface)
  function startup() : boolean; override;
  function deeperup() : boolean; override;
  function make_node(parent : Tlayoutedwindow; img : TsomeBGdesciptor; r : Twrect) : Tlayoutedwindow; override;
  function enterloop() : longint; override;
  procedure shutdown(); override;
  function tellname() : ansistring; override;
  procedure cleanup(); override;
  
  function Render(b : T_paswl_buffer; x,y,dx,dy,reason : longint) : boolean;
  procedure MouseMove(time : dword; surface_x, surface_y : Twlfixed);
  procedure MouseEnter(serial : dword; surface : Tpaswl_bindable; surface_x, surface_y : Twlfixed);
  procedure MouseLeave(serial : dword; surface : Tpaswl_bindable);
  procedure MouseButton(serial, time, button, state : dword);
  
  
  destructor destroy(); override;
  
  public
  o : Tpaswl_collection2;
  deco : Tdecor;
  pool : Tpaswl_shm_managedpool;
  tree : Tlayoutedwlwindow;
  lastenteredsurf : Tpaswl_bindable;
  lastMotion, buttons : dword;
  lastwinglobX,lastwinglobY : longint;
end;


function Twldriverinterface.Render(b : T_paswl_buffer; x,y,dx,dy,reason : longint) : boolean;
begin
  if tree<>nil then begin
    tree.r.x := x;
    tree.r.y := y;
    tree.r.dx:= dx;
    tree.r.dy:= dy;
    tree.paint(0,0);
  end;
  Render := true;
end;

procedure Twldriverinterface.MouseMove(time : dword; surface_x, surface_y : Twlfixed);
var x,y : longint;
begin
  lastMotion := time;
  if assigned(lastenteredsurf) then begin
    x := longint(surface_x) div 256;
    y := longint(surface_y) div 256;
    reverseif.onMouseEvent(x+lastwinglobX,y+lastwinglobX,$40000000,0.001*time);
  end;
end;

procedure Twldriverinterface.MouseEnter(serial : dword; surface : Tpaswl_bindable; surface_x, surface_y : Twlfixed);
var w : Tlayoutedwlwindow; 
begin
  if (surface<>nil)and(surface is Tdrivertreesurface) then begin
    lastenteredsurf := surface;
    lastwinglobX := 0;
    lastwinglobY := 0;
    w := (surface as Tdrivertreesurface).layoutwin;
    while (w.parent<>nil) do begin
      lastwinglobX += w.r.x;
      lastwinglobY += w.r.y;
      w := w.parent as Tlayoutedwlwindow;
    end;
  end;
  mouseMove(lastMotion,surface_x, surface_y);
end;

procedure Twldriverinterface.MouseLeave(serial : dword; surface : Tpaswl_bindable);
begin
  { according to wayland.xml, leave is sent before enter }
  lastenteredsurf := nil;
end;

procedure Twldriverinterface.MouseButton(serial, time, button, state : dword);
var v : dword;
begin
  v := 0;
  case button of
    $110 : v := 1;
    $111 : v := 2;
    $112 : v := $10;
    $113 : v := $20;
    $114 : v := $40;
  end;
  if state=0 then begin
    buttons := buttons xor (buttons and v);
  end else begin
    buttons := buttons or v;
  end;
  reverseif.onMouseEvent(0,0,buttons or $80000000,0.001*time);
end;



function Twldriverinterface.deeperup() : boolean;
begin
    if (o<>nil) then begin
      o.display.display_roundtrip();
      if o.testAutobinds() then begin
        deco := nil;
        if assigned(o.xdg_wm_base) then begin
          writeln('using xdg-shell');
          deco := Txdgdecor.create()
        end else if assigned(o.coreshell) then begin
          writeln('using wl-shell');
          deco := Twlshelldecor.create();
        end;
        if assigned(deco) then begin
          deco.clc := o;
          deco.init();
          pool := o.shm.create_managed_pool(512);
          o.display.display_roundtrip();
          deco.HonRender := @Render;
          deco.HonEnter := @MouseEnter;
          deco.HonLeave := @MouseLeave;
          deco.HonButton:= @MouseButton;
          deco.HonMotion:= @MouseMove;
          deeperup := true;
          writeln('ok!');
          exit;
        end else writeln('Sorry, no supported shell (currently: core wl_shell + xdg-shell) found.');
      end else begin
        writeln('Insufficient set of available wayland interfaces, sorry.');
      end;
      o.destroy();
    end else writeln('no connected Tpaswl');
end;

function Twldriverinterface.startup() : boolean;
var dn : ansistring; dnp  : Pchar;
begin
  startup := false;
  if import_libwlc() then begin
    dn := reverseif.get_config_str('displayname','');
    if dn='' then dnp := nil else dnp := @dn[1];
    o := startup_display(dnp, Tpaswl_collection2) as Tpaswl_collection2;
    startup := assigned(o);
  end;
end;

function Twldriverinterface.make_node(parent : Tlayoutedwindow; img : TsomeBGdesciptor; r : Twrect) : Tlayoutedwindow;
var w : Tlayoutedwlwindow;
begin
  w := Tlayoutedwlwindow.create();
  w.BG := img;
  w.pool := pool;
  w.surf := deco.clc.compositor.create_surface(Tdrivertreesurface) as Tdrivertreesurface;
  w.surf.layoutwin := w;
  w.r := r;
  if parent<>nil
  then w.sub := deco.clc.subcompositor.get_subsurface(w.surf,Tlayoutedwlwindow(parent).surf)
  else w.sub := deco.clc.subcompositor.get_subsurface(w.surf,deco.mainsurf);
  if parent<>nil then parent.insertChild(w);
  w.vp := deco.clc.viewporter.get_viewport(w.surf);
  
  if parent=nil then tree := w;
  make_node := w;
end;

function Twldriverinterface.enterloop() : longint;
begin
  if assigned(o) then o.msgloop();
  enterloop := 0;
end;

procedure Twldriverinterface.shutdown();
begin
  freeandnil(tree);
  freeandnil(pool);
  freeandnil(deco);
  freeandnil(o);
end;

function Twldriverinterface.tellname() : ansistring;
begin
  tellname := 'wayland_shm';
end;

procedure Twldriverinterface.cleanup();
begin
end;

destructor Twldriverinterface.destroy();
begin
  freeandnil(reverseif);
end;

var maindrv : Twldriverinterface;

initialization
begin 
  maindrv := Twldriverinterface.create();
  register_driver(maindrv);
end;

finalization
begin
  freeandnil(maindrv);
end;


end.
