unit ot_comn;
{$mode objfpc}
{$COPERATORS ON}

{ Some helper procedures commonly used by 
  GDEF,GSUB,GPOS etc.

}



interface
uses classes,sysutils,fontfile,ot_cmap;


type
Tglychar = unicodechar;
Tglystr = unicodestring;

TOTcomnfont = class(TOTcmapfont)
  function gdef_isBaseGlyph(g : Tglychar) : boolean; virtual; abstract;
  function gdef_isLigGlyph(g : Tglychar) : boolean; virtual; abstract;
  function gdef_isMarkGlyph(g : Tglychar) : boolean; virtual; abstract;
  function gdef_isinMarkFilteringSet(g : Tglychar; mfs : longint) : boolean; virtual; abstract;
  function gdef_isofMarkAttachmentType(g : Tglychar; mat : longint) : boolean; virtual; abstract;
end;




Tcoverage = Tglystr;
Tcoveragearr = array of Tcoverage;

Tglycls = array of word;


TOTlookup = class
  procedure load(s : Tstream); virtual;

  public LookupFlag,MarkFilteringSet : longint;
end;

TOText32lookup = class(TOTlookup)
end;


Tlookuptab = record
  LookupType,LookupFlag,MarkFilteringSet : longint;
  sublu : array of TOTlookup;
end;



{ a containter for the lookups,
  two instance of this class, one for gsub
  and one for gpos lookups.
}


Tlookuptabs = class
  constructor create();
  destructor destroy(); override;

  { the "lookups" can ignore glyphs, so they
    ask us here about wether or not to ignore
  }
  function isIgnGly(g : Tglychar) : boolean; virtual;

  { The decision about ignoring Glyphs mainly
    depends on flags associated with "lookups",
    so something has to tell us about which lookup
    is currently active and might be asking isIgnGly?
  }
  function setActiveLookup(i : longint) : longint; virtual;

  public
  { the list of contained lookups }
  tabs : array of Tlookuptab;

  { There is a GPOS lookup called 

    "MarkToLigature Attachment Positioning Subtable",

    which according to the specs, requires us 

    "To correctly access the subtables, the client must keep 
     track of the component associated with the mark."
    
    Given generic text processing done in GSUB, this is
    an impossible task, so we have to live with a usable
    workaround. If the font includes such a GPOS Ligature 
    accent lookup, we'll make GSUB ligature lookups
    replace the deleted ligature parts by a special
    invalid glyph marker ($FFFF), instead of really
    deleting the deleted ligature parts, so that
    marks can count the number of these invalid glyphs 
    markers to find out where out to which ligature 
    component they belong, assuming text processing 
    didn't mess up too much (as I said, it's already
    an impossible task to keep track about this).

    Default is to not use this mechanism, so
    glyph id $ffff can be freely used, and deleted
    ligature parts really becoming deleted.
    It becomes enabled, reserving gly $ffff when
    the font includes gpos.MarkLigPosFormat1 lookups,
    ignoring the feature activation status.

    Update: Since the GPOS stuff is so full of open 
    questions, it seems to be overkill to care about
    that default-behaviour. ENABLED permanently, until
    somone finds the time.
  }
  deleted_lig_part_marker : Tglystr;


  { GPOS, only:
    Especially for gpos, it is unclear if
    operations initiated by contxt lookups 
    should only operate on a single item.

    If so, a further question is about 
    limits in search range for the innermost
    lookups. For example, does the rerverse
    search for base-glyphs inititated on
    mark position have to be within the
    context "input"?
    
    With use_long_distance_matching=true,
    such limits will be the entire string,
    otherwise it will be the "input" part
    of a context (minus the part masked out
    by the ).

    With use_one_lookup the search range
    passed to a callee lookup will be
    a single glyph.
    
  }
  use_long_distance_matching : boolean;
  use_one_lookup : boolean;
  dont_ignore_one : boolean;

  public
  function is_deleted_ligpart(g : Tglychar) : boolean;

  public
  font : TOTcomnfont;

  private
  curlu : longint;
end;







TOTgsublookup = class(TOTlookup)
{ We probably need recursive processing with 
  somewhat global meaning of callee specification.

  So these little text processing classes might need
  some reference to the tab that organizes them. 
  To be sure we really reference what we expect on
  invoking the text processing from the outside,
  let's use a call parameter instead of storing
  a static reference in the processing class.  
}
  function apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr; virtual;
end;



{ some GPOS related types needed here for TOTgposlookup.adjust }

{ The font class should be capable of working with
  multiple ppem settings, so we'll need the ppem
  specific values here. }

TposValueRecord = packed record
  XPlacement, YPlacement, XAdvance,   YAdvance   : smallint;
  XPlaDevice, YPlaDevice, XAdvDevice, YAdvDevice : word;
end;

Tanchor = record
  fmt : word;
  XCoordinate,
  YCoordinate : smallint;
  AnchorPoint : word;
  XDeviceTable,
  YDeviceTable : word;
end;

Tglyphplace = record
  vr : TposValueRecord;
  selfentry : Tanchor;
  selfexit  : Tanchor;
  aself, abase : Tanchor;
  base : longint; // index of refernce-glyph
end;
Tglyphplacement = array of Tglyphplace;


TOTgposlookup = class(TOTlookup)
  procedure adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs); virtual;
end;








{ used during text processing to manage recursive "lookup" calls 
  in context/chainContext lookups.
}
function apply_lookup(s0 : Tglystr; i : longint; const tabs : Tlookuptabs) : Tglystr;
procedure adjust_lookup(s : Tglystr; var adj : Tglyphplacement; a,b : longint; i : longint; const tabs : Tlookuptabs);









{ to share the common GSUB/GPOS lookup-loading part,
  both define a construction func like:
}

type
Tmakelookup = function(typ,fmt : longint) : TOTlookup;


TOTlangsys = record
  LangSysTag : TTTFtag;
  ReqFeatureIndex : word;
  Feature : array of word;
  valid : boolean;
end;
TOTscript = record
  ScriptTag : TTTFtag;
  DefaulTOTlangsys : TOTlangsys;
  LangSys : array of TOTlangsys;
end;
TOTscriptList = array of TOTscript;
TOTFeature = record
  FeatureTag : TTTFtag;
  lu : array of word;
end;
TOTfeaturelist = array of TOTFeature;




Tsubst = record
  SequenceIndex, LookupListIndex : word;
end;
Tsubstarr = array of Tsubst;



// global functions

function read_glychar(s : Tstream) : Tglychar;
function read_glystr(n : longint; s : Tstream) : Tglystr;


{ loading }
procedure load_coverage(var c : Tcoverage; s : Tstream);
procedure load_FeatureList(var features : TOTfeaturelist; s : Tstream);
procedure load_lookups(var lus : Tlookuptabs; mlu : Tmakelookup; s : Tstream);
procedure load_scriptlist(var scripts : TOTscriptList; s : Tstream);
procedure load_covarr(n,i0 : longint; var ca : Tcoveragearr; s : Tstream);





{ copy n non-ignored chars from source string s
  from position i on in direction di (-1 = backwards),
  truncated if the source string doesn't have enough 
  non-ignored chars.  
}

function ignocopy(s : Tglystr; i,di,n : longint; const tabs : Tlookuptabs) : Tglystr;

{ same as ignocopy, but also copy the ignored chars to the returned str. 
}
function ignacopy(s : Tglystr; i,di,n : longint; const tabs : Tlookuptabs) : Tglystr;




{ coverage-test }
function cpos(c : Tglychar; cov : Tcoverage) : longint;
function covpos(c : Tglychar; cov : Tcoverage; const tabs : Tlookuptabs) : longint;






procedure load_glyphclassdef(var gclsdef : Tglycls; s : Tstream);
function get_glyphclass(g : Tglychar; const gclsdef : Tglycls) : longint;

function check_covarrmatch(s : Tglystr; i,di,n : longint; ca : Tcoveragearr; const tabs : Tlookuptabs) : boolean;
function check_clsmatch(s : Tglystr; i,di : longint; ca : Tglystr; cd : Tglycls; const tabs : Tlookuptabs) : boolean;

procedure load_substarr(n : longint; var sa : Tsubstarr; s : Tstream);

function findScript(nam : string; sl : TOTscriptList) : longint;



implementation
uses math;




function apply_lookup(s0 : Tglystr; i : longint; const tabs : Tlookuptabs) : Tglystr;
var s : Tglystr; j,oldi : longint;
begin
  s := s0;
  if (i>=0)and(i<=high(tabs.tabs)) then with tabs.tabs[i] do begin
    oldi := tabs.setActiveLookup(i);
    for j := 0 to high(sublu) do begin
      if not(sublu[j]=nil) 
      then s := TOTgsublookup(sublu[j]).apply(s,tabs);
    end;
    tabs.setActiveLookup(oldi);
  end;
  apply_lookup := s;
end;


procedure adjust_lookup(s : Tglystr; var adj : Tglyphplacement; a,b : longint; i : longint; const tabs : Tlookuptabs);
var oldi,j : longint;
begin
  if (i>=0)and(i<=high(tabs.tabs)) then with tabs.tabs[i] do begin
    oldi := tabs.setActiveLookup(i);
    for j := 0 to high(sublu) do begin
      if not(sublu[j]=nil) 
      then TOTgposlookup(sublu[j]).adjust(s,adj,a,b,tabs);
    end;
    tabs.setActiveLookup(oldi);
  end;
end;













procedure TOTlookup.load(s : Tstream);
begin
  writeln('Loading of ',classname,' currently unsupported.');
end;

function TOTgsublookup.apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr;
begin
  apply := s;
end;

procedure TOTgposlookup.adjust(s : Tglystr; var adj : Tglyphplacement; a,b : longint; tabs : Tlookuptabs);
begin
end;



function read_glychar(s : Tstream) : Tglychar;
var w : word;
begin
  w := read_word(s);
  read_glychar := Tglychar(w);
end;

function read_glystr(n : longint; s : Tstream) : Tglystr;
var i : longint; y : Tglystr;
begin
  if (n>0) then begin
    setlength(y,n);
    for i := 1 to n do y[i] := read_glychar(s);
    read_glystr := y;
  end else read_glystr := '';
end;


















procedure load_substarr(n : longint; var sa : Tsubstarr; s : Tstream);
var i : longint;
begin
  setlength(sa,n);
  for i := 0 to n-1 do begin
    sa[i].SequenceIndex   := read_word(s);
    sa[i].LookupListIndex := read_word(s);
  end;
end;





procedure load_coverage1(var c : Tcoverage; s : Tstream);
var gc,i : longint;
begin
  gc := read_word(s);
  setlength(c,gc);
  for i := 1 to gc do begin  // string is 1-based
    c[i] := read_glychar(s);
  end;
end;

procedure load_coverage2(var c : Tcoverage; s : Tstream);
var rc,i,j,q,gstart,gend : longint;
begin
  rc := read_word(s);
  q := s.position;
  j := 0;
  for i := 0 to rc-1 do begin
    gstart := read_word(s);
    gend   := read_word(s);
    if (gstart<=gend) then begin
      j += (gend-gstart)+1;
    end;
    read_word(s);
  end;
  s.seek(q,soFromBeginning);
  setlength(c,j);
  j := 1;
  for i := 0 to rc-1 do begin
    gstart := read_word(s);
    gend   := read_word(s);
    for q := gstart to gend do begin
      c[j] := Tglychar(q);
      inc(j);
    end;
    read_word(s);
  end;
end;

procedure load_coverage(var c : Tcoverage; s : Tstream);
var CoverageFormat : longint;
begin
  setlength(c,0);
  CoverageFormat := read_word(s);
  case CoverageFormat of
    1 : load_coverage1(c,s);
    2 : load_coverage2(c,s);
  end;
end;






// common part of GPOS/GSUB lookup loading




procedure load_lookup(var lu : Tlookuptab; mlu : Tmakelookup; s : Tstream);
var n,i,i0,j,t,t2 : longint; os : array of word;
begin
  i0 := s.position;
  lu.LookupType := read_word(s);
  lu.LookupFlag := read_word(s);
  n := read_word(s);
  setlength(os,n);
  setlength(lu.sublu,n);
  for i := 0 to n-1 do begin
    os[i] := read_word(s);
  end;
  if (lu.LookupFlag and $10)>0 
  then lu.MarkFilteringSet := read_word(s)
  else lu.MarkFilteringSet := 0;
  for i := 0 to n-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    j := read_word(s);
    s.seek(i0+os[i],soFromBeginning);
    lu.sublu[i] := mlu(lu.LookupType,j);
    if not(lu.sublu[i]=nil) then begin

      { also handle weird 32-bit extension meachnism in OpenType specification  }

      if lu.sublu[i] is TOText32lookup then begin
        lu.sublu[i].destroy();
        lu.sublu[i] := nil;
        read_word(s);       // SubstFormat, already processed
        t := read_word(s);  // ExtensionLookupType
        j := read_dword(s); // ExtensionOffset (to subtab-start)
        j := j + i0+os[i];
        s.seek(j,soFromBeginning);
        t2 := read_word(s);
        s.seek(j,soFromBeginning);
        lu.sublu[i] := mlu(t,t2);
        { don't let a font leave us with unspecific extension
          classes we want to typecast later}
        if lu.sublu[i] is TOText32lookup then begin
          lu.sublu[i].destroy();
          lu.sublu[i] := nil;
        end;
        if not(lu.sublu[i]=nil) then begin
          lu.sublu[i].LookupFlag := lu.LookupFlag;
          lu.sublu[i].MarkFilteringSet := lu.MarkFilteringSet;
          lu.sublu[i].load(s);
        end;
      end else begin
        lu.sublu[i].LookupFlag := lu.LookupFlag;
        lu.sublu[i].MarkFilteringSet := lu.MarkFilteringSet;
        lu.sublu[i].load(s);
      end;

    end;
  end;  
end;


procedure load_lookups(var lus : Tlookuptabs; mlu : Tmakelookup; s : Tstream);
var lc,i,i0 : longint; os : array of word;
begin
  i0 := s.position;
  lc := read_word(s);
  setlength(lus.tabs,lc);
  setlength(os,lc);
  for i := 0 to lc-1 do begin
    os[i] := read_word(s);
  end;
  for i := 0 to lc-1 do begin
    s.seek(os[i]+i0,soFromBeginning);
    load_lookup(lus.tabs[i],mlu,s);
  end;
end;








// common part of GPOS/GSUB feature loading



procedure load_feature(var f : TOTFeature; s : Tstream);
var lc,i : longint;
begin
  read_word(s); // reserved
  lc := read_word(s);
  setlength(f.lu, lc);
  for i := 0 to lc-1 do begin
    f.lu[i] := read_word(s);
  end;
  writeln('  feature ',f.FeatureTag,' has ',lc,' lookups');
end;


procedure load_FeatureList(var features : TOTfeaturelist; s : Tstream);
var i,n,i0 : longint; os : array of word;
begin
  writeln('featurelist');
  i0 := s.position;
  n := read_word(s);
  setlength(features,n);
  setlength(os,n);
  for i := 0 to n-1 do begin
    features[i].FeatureTag := read_tag(s);
    os[i] := read_word(s);
  end;
  for i := 0 to n-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    load_feature(features[i],s);
  end;
end;



// common part of GPOS/GSUB script/langsys loading


procedure read_langsys(var ls : TOTlangsys; s : Tstream);
var FeatureCount : word; i : longint;
begin
  writeln('  reading langsys :',ls.LangSysTag,'#');
  if (read_word(s)=0) then begin  // overred reserved "LookupOrder"
    ls.ReqFeatureIndex := read_word(s);
    FeatureCount := read_word(s);
    setlength(ls.Feature,FeatureCount);
writeln('    featurecnt=',FeatureCount);
    for i := 0 to FeatureCount-1 do begin
      ls.Feature[i] := read_word(s);
    end;
    ls.valid := true;
  end else begin
    ls.ReqFeatureIndex := 0;
    ls.valid := false;
    setlength(ls.Feature,0);
  end;
end;

procedure load_script(var scr : TOTscript; s : Tstream);
var DefaulTOTlangsys,LangSysCount : word; i,i0 : longint; os : array of word;
begin
  writeln('loading script :',scr.ScriptTag);
  i0 := s.position;
  DefaulTOTlangsys := read_word(s);
  LangSysCount   := read_word(s);
  setlength(scr.LangSys, LangSysCount);
  setlength(os, LangSysCount);
  for i := 0 to high(os) do begin
    scr.LangSys[i].LangSysTag := read_tag(s);
    os[i] := read_word(s);
  end;
  for i := 0 to high(os) do begin
    s.seek(i0+os[i],soFromBeginning);
    read_langsys(scr.LangSys[i],s);
  end;
  if (DefaulTOTlangsys>0) then begin
    scr.DefaulTOTlangsys.LangSysTag := ' ** ';
    s.seek(i0+DefaulTOTlangsys,soFromBeginning);
    read_langsys(scr.DefaulTOTlangsys,s);
  end else begin
    scr.DefaulTOTlangsys.LangSysTag := 'none';
    scr.DefaulTOTlangsys.valid := false;
  end;
end;

procedure load_scriptlist(var scripts : TOTscriptList; s : Tstream);
var w,i,i0 : longint; os : array of word;
begin
  i0 := s.position;
  w := read_word(s);
  setlength(scripts,w);
  setlength(os,w);
  for i := 0 to w-1 do begin
    scripts[i].ScriptTag := read_tag(s);
    os[i] := read_word(s);
  end;
  for i := 0 to w-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    load_script(scripts[i],s);
  end;
end;



function findScript(nam : string; sl : TOTscriptList) : longint;
var i,d : longint;
begin
  d := 0;
  while length(nam)<4 do nam += ' ';
  nam := copy(nam,1,4);
  for i := 0 to high(sl) do begin
    if sl[i].ScriptTag=nam then begin
      findScript := i; exit;
    end;
    if sl[i].ScriptTag='DFLT' then d := i;
  end;
  findScript := d;
end;



procedure load_covarr(n,i0 : longint; var ca : Tcoveragearr; s : Tstream);
var i1,i : longint; os : array of longint;
begin
  setlength(ca,n);
  setlength(os,n);
  for i := 0 to n-1 do os[i] := read_word(s);
  i1 := s.position;
  for i := 0 to n-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    load_coverage(ca[i],s);
  end;
  s.seek(i1,soFromBeginning);
end;









constructor Tlookuptabs.create();
begin
  inherited create();
  deleted_lig_part_marker := #$ffff;
  use_long_distance_matching := true;
  use_one_lookup := true;
  dont_ignore_one := true;
  setlength(tabs,0);
end;

destructor Tlookuptabs.destroy();
var i,j : longint;
begin
  for i := 0 to high(tabs) do begin
    for j := 0 to high(tabs[i].sublu) do begin
      if not(nil=tabs[i].sublu[j]) then tabs[i].sublu[j].destroy();
    end;
    setlength(tabs[i].sublu,0);
  end;
  setlength(tabs,0);
  inherited destroy();
end;


function Tlookuptabs.isIgnGly(g : Tglychar) : boolean;
var mat : longint;
begin
  if is_deleted_ligpart(g) 
  then isIgnGly := true
  else begin
    isIgnGly := false;
    if (curlu>=0)and(curlu<=high(tabs)) then with tabs[curlu] do begin
      if not(font=nil) then begin
        if (LookupFlag and $02)>0 then begin
          if font.gdef_isBaseGlyph(g) then begin
            isIgnGly := true;
            exit;
          end;
        end;
        if (LookupFlag and $04)>0 then begin
          if font.gdef_isLigGlyph(g) then begin
            isIgnGly := true;
            exit;
          end;
        end;
        if (LookupFlag and $08)>0 then begin
          if font.gdef_isMarkGlyph(g) then begin
            isIgnGly := true;
            exit;
          end;
        end;
        if (LookupFlag and $10)>0 then begin
          isIgnGly := not(font.gdef_isinMarkFilteringSet(g,MarkFilteringSet));
          exit;
        end;
        mat := (LookupFlag shr 8) and 255;
        if not(mat=0) then begin
          if font.gdef_isMarkGlyph(g) then begin
            isIgnGly := not(font.gdef_isofMarkAttachmentType(g,mat));
            exit;
          end else begin
            isIgnGly := false;
          end;
        end;
      end;
    end;
  end;
end;

function Tlookuptabs.setActiveLookup(i : longint) : longint;
begin
  setActiveLookup := curlu;
  curlu := i;
end;

function Tlookuptabs.is_deleted_ligpart(g : Tglychar) : boolean;
begin
  is_deleted_ligpart := (pos(g,deleted_lig_part_marker)>0);
end;





{ copy n non-ignored chars from source string s
  from position i on in direction di (-1 = backwards),
  truncated if the source string doesn't have enough 
  non-ignored chars.  
}

function ignocopy(s : Tglystr; i,di,n : longint; const tabs : Tlookuptabs) : Tglystr;
var j : longint; y : Tglystr;
begin
  y := '';
  for j := 1 to n do begin
    while (i>0)and(i<=length(s)) do begin
      if not(tabs.isIgnGly(s[i])) then begin
        y := y + s[i];
        break; // out of while-loop
      end else inc(i,di);
    end;
    inc(i,di);
  end;
  ignocopy := y;
end;

{ same as ignocopy, but also copy the ignored chars to the returned str
  in a IGNore Aware way
}
function ignacopy(s : Tglystr; i,di,n : longint; const tabs : Tlookuptabs) : Tglystr;
var j : longint; y : Tglystr;
begin
  y := '';
  for j := 1 to n do begin
    while (i>0)and(i<=length(s)) do begin
      if not(tabs.isIgnGly(s[i])) then begin
        y := y + s[i];
        break; // out of while-loop
      end else begin
        y := y + s[i];
        inc(i,di);
      end;
    end;
    inc(i,di);
  end;
  ignacopy := y;
end;


{ well, probably not the prettiest binsrch ever seen
}
function _binpos(c : Tglychar; s : Tcoverage; a,b : longint) : longint;
var x : longint;
begin
  x := (a+b) shr 1;
  if (word(c)<word(s[a])) then _binpos := 0
  else if (c=s[a]) then _binpos := a
  else if (b=a) then _binpos := 0
  else if (word(c)<word(s[x])) then begin
    if (x=a) then _binpos := 0
    else _binpos := _binpos(c,s,a,x-1);
  end else begin
    if (x=a) then _binpos := _binpos(c,s,x+1,b)
    else _binpos := _binpos(c,s,x,b);
  end;
end;


function cpos(c : Tglychar; cov : Tcoverage) : longint;
begin
  if length(cov)>24 then begin
    cpos := _binpos(c,cov,1,length(cov));
  end else begin
    cpos := pos(c,cov);
  end;
end;

function covpos(c : Tglychar; cov : Tcoverage; const tabs : Tlookuptabs) : longint;
begin
  if tabs.isIgnGly(c) then covpos := 0
  else begin
    covpos := cpos(c,cov); 
  end;
end;




procedure load_gcls1(var glycls : Tglycls; s : Tstream);
var startg,endg,i : longint;
begin
  startg := read_word(s);
  endg   := read_word(s);
  endg := endg +startg;
  setlength(glycls,endg);
  for i := 0 to endg-1 do glycls[i] := 0;
  for i := startg to endg-1 do begin
    if (i>=0)and(i<=high(glycls)) then glycls[i] := read_word(s);
  end;
end;

procedure load_gcls2(var glycls : Tglycls; s : Tstream);
var ClassRangeCount,i,j,c : longint; startg,endg : dword;
begin
  ClassRangeCount := smallint(read_word(s));
  c := s.position;
  j := 0;
  for i := 1 to ClassRangeCount do begin
    startg := read_word(s);
    endg   := read_word(s);
    read_word(s);
    j := max(j,endg);
  end;
  s.seek(c,soFromBeginning);
  setlength(glycls,j+1);
  for i := 0 to j do glycls[i] := 0;
  for i := 1 to ClassRangeCount do begin
    startg := read_word(s);
    endg   := read_word(s);
    c      := smallint(read_word(s));
    for j := startg to endg do begin
      if (j>=0)and(j<=high(glycls)) then glycls[j] := c;
    end;
  end;
end;

procedure load_glyphclassdef(var gclsdef : Tglycls; s : Tstream);
var clsfmt : longint;
begin
  clsfmt := read_word(s);
  setlength(gclsdef,0);
  case clsfmt of
    1 : load_gcls1(gclsdef,s);
    2 : load_gcls2(gclsdef,s);
  end;
end;

function get_glyphclass(g : Tglychar; const gclsdef : Tglycls) : longint;
begin
  if word(g)>high(gclsdef) then get_glyphclass := 0
  else get_glyphclass := gclsdef[word(g)];
end;


function check_covarrmatch(s : Tglystr; i,di,n : longint; ca : Tcoveragearr; const tabs : Tlookuptabs) : boolean;
var q : longint;
begin
  check_covarrmatch := false;
  for q := 0 to n-1 do begin
    while (i>0)and(i<=length(s)) do begin
      if tabs.isIgnGly(s[i]) then inc(i,di)
      else break;
    end;
    if (i<1)or(i>length(s)) then exit;
    if covpos(s[i],ca[q],tabs)<1 then exit;
    i := i + di;
  end;
  check_covarrmatch := true;
end;


function check_clsmatch(s : Tglystr; i,di : longint; ca : Tglystr; cd : Tglycls; const tabs : Tlookuptabs) : boolean;
var q : longint;
begin
  check_clsmatch := false;
  for q := 1 to length(ca) do begin
    while (i>0)and(i<=length(s)) do begin
      if tabs.isIgnGly(s[i]) then inc(i,di)
      else break;
    end;
    if (i<1)or(i>length(s)) then exit;
    if get_glyphclass(s[i],cd)<>word(ca[q]) then exit;
    i := i + di;
  end;
  check_clsmatch := true;
end;




begin
end.

