{$mode objfpc}
{$unitpath ..}
uses unicode_bidir_brackets;

function todot(var s : ansistring; const dot : shortstring) : ansistring;
var i : longint;
begin
  i := pos(dot,s);
  if (i>0) then begin
    result := copy(s,1,i-1);
    s := copy(s,i+length(dot));
  end else begin
    result := s;
    s := '';
  end;
end;


procedure doline(s : ansistring);
var s0,sc,sn,s1,s2,s3,s4,s5 : ansistring; w : word; cp,ce : dword;
begin
  s0 := s;
  s := todot(s,'#');
  if s<>'' then begin
    sc := todot(s,';');
    sn := todot(s,';');
    s1 := todot(s,';');
    s2 := todot(s,';');
    s3 := todot(s,';');
    s4 := todot(s,';');
    s5 := todot(s,';');
    sc := '$'+sc;
    val(sc,cp,w);
    if s4<>'' then begin
      if pos(' ',s4)<1 then begin
        if pos('<',s4)<1 then begin
          s4 := '$'+s4;
          val(s4,ce,w);
          if check_opening_bracket(ce)<>0 then begin
            writeln('    $',hexstr(cp,6),' : result := $',hexstr(ce,6),';');
          end;
          if check_closing_bracket(ce)<>0 then begin
            writeln('    $',hexstr(cp,6),' : result := $',hexstr(ce,6),';');
          end;
        end;
      end;
    end;
  end;
end;

var
f : text;
s : ansistring;

begin
  assign(f,'UnicodeData.txt');
  reset(f);
  while not(eof(f)) do begin
    readln(f,s);
    doline(s);
  end;
  close(f);
end.
