{$mode objfpc}
unit settingsreader;

interface
uses DOM, XMLRead,sysutils;

const
app_useconfig : ansistring = 'app.useconf';

type
Tconfiglist = array of string;
Tsettingsreader=class
  procedure setXML(fn : ansistring);
  
  function get_setting(s : ansistring) : ansistring;
  procedure get_useful_configs(var cl : Tconfiglist);
  function get_node(s : ansistring) : TDOMNode;
  
  destructor destroy(); override;
  
  public
  doc: TXMLDocument;
end;




implementation


function todot(var s : ansistring; const dot : shortstring) : ansistring;
var i : longint;
begin
  i := pos(dot,s);
  if (i>0) then begin
    result := copy(s,1,i-1);
    s := copy(s,i+length(dot));
  end else begin
    result := s;
    s := '';
  end;
end;





function checkname(n : TDOMNode; const s : ansistring) : boolean;
begin
  checkname := false;
  if s<>'' then begin
    if n.NodeName=UTF8ToString(s) then begin
      checkname := true;
    end else if (n is TDOMElement) then begin
      if (n as TDOMElement).GetAttribute('name')=UTF8ToString(s) then begin
        checkname := true;
      end;        
    end;
  end;
end;

function trycmdline(s : ansistring) : ansistring;
var i : longint; var s2 : ansistring;
begin
  trycmdline := '';
  s := '--'+s;
  for i := 1 to paramcount() do begin
    //writeln('check: ',copy(paramstr(i),1,length(s)),' = ',s);
    if copy(paramstr(i),1,length(s))=s then begin
      s2 := paramstr(i);
      delete(s2,1,length(s));
      if copy(s2,1,1)='=' then begin
        trycmdline := copy(s2,2);
        exit;
      end;
    end;
  end;
end;

function Tsettingsreader.get_setting(s : ansistring) : ansistring;
var  n : TDOMNode; s2 : ansistring;
begin
writeln('get_setting: ',s);
  s2 := trycmdline(s);
  get_setting := s2;
  if (s2<>'')or(doc=nil) then exit;
  n := doc.firstchild;
  n := n.firstChild;
  s2 := todot(s,'.');
  while n<>nil do begin
    if checkname(n,s2) then begin
      s2 := todot(s,'.');
      if (s2='') then begin
        if (n is TDOMElement) then begin
          s := UTF8Encode((n as TDOMElement).GetAttribute('value'));
          get_setting := s;
          s2 := UTF8Encode((n as TDOMElement).GetAttribute('override'));
          if s2<>'' then begin
            s2 := get_setting(s2);
            if (s2<>'') then s := s2;
            get_setting := s;
          end;
          exit;
        end;
      end else n := n.firstChild;
    end else n := n.nextSibling;
  end;
end;


procedure Tsettingsreader.setXML(fn : ansistring);
begin
  ReadXMLFile(Doc, fn);
end;

function Tsettingsreader.get_node(s : ansistring) : TDOMNode;
var  n : TDOMNode; s2 : ansistring;
begin
  get_node := nil;
  if (doc=nil) then exit;
  n := doc.firstchild;
  n := n.firstChild;
  s2 := todot(s,'.');
  while n<>nil do begin
    if checkname(n,s2) then begin
      s2 := todot(s,'.');
      if (s2='') then begin
        get_node := n;
      end else n := n.firstChild;
    end else n := n.nextSibling;
  end;
end;



procedure Tsettingsreader.get_useful_configs(var cl : Tconfiglist);
var s,s2 : ansistring; n : TDOMNode;
begin
  setlength(cl,0);
  if (doc=nil) then exit;
  n := doc.firstchild;
  n := n.firstChild;
  s := app_useconfig;
  s2 := todot(s,'.');
  while n<>nil do begin
    if checkname(n,s2) then begin
      s2 := todot(s,'.');
      if (s2='') then begin
        if (n is TDOMElement) then begin
          s2 := UTF8Encode((n as TDOMElement).GetAttribute('cond'));
          s := UTF8Encode((n as TDOMElement).GetAttribute('default'));

          if s2<>'' then s2 := GetEnvironmentVariable(s2);

          if s2='' then s2 := s;
          n := n.firstChild;
          while n<>nil do begin
            if (n is TDOMElement) then begin
              s := UTF8Encode((n as TDOMElement).GetAttribute('cond'));
              if s2=s then begin
                setlength(cl,high(cl)+2);
                cl[high(cl)] := UTF8Encode((n as TDOMElement).GetAttribute('value'));
              end;
              n := n.nextSibling;
            end else n := n.nextSibling;
          end;
        end;
      end else n := n.firstChild;
    end else n := n.nextSibling;
  end;
end;



destructor Tsettingsreader.destroy();
begin
  freeandnil(doc);
end;


begin
end.
