{$mode objfpc}
{$unitpath wayland}
{$unitpath x11}
{$unitpath .}
{$unitpath ../imgsrc}
unit drvif;

interface
uses 
  dbllinkedlist,
  settingsreader,
  renderbox;

const
Loopres_functional = 1; // msgloop exited on request
Loopres_userrequested_appshutdown = 2; // msgloop exited on explicit user request
Loopres_defunct = 4; // cannot continue with this driver, maybe try another one
Loopres_restart = 8; // this driver could contine after a fresh startup()



type
Tlayoutedwindow=class
  constructor create(); virtual;
  destructor destroy(); override;
  
  function insertChild(c : Tlayoutedwindow; cref : Tlayoutedwindow=nil) : Tlayoutedwindow;
  procedure removeChild(c : Tlayoutedwindow);

  procedure load(); virtual; abstract;
  
  public
  parent : Tlayoutedwindow;
  
  { position relative to parent, size as planned by layout }
  r : Twrect;

  { a list of child layouted window }
  childlist : Tdbllinkedlist;
  siblings : Pdbllinkedlist;
  
  {finally, some descripton of what to draw (as background, if childlist nonempty) }
  BG : TsomeBGdesciptor;
  dirty : boolean;
  
  public
  { calc the crop rect }
  procedure calc_vr();
  procedure calc_vrs(); // recursive

  procedure print(lvl : string; gx,gy : longint);

  public
  { computed parent-cropped visual part of this window
    in this window's coord space
  }
  vr : Twrect; 
end;

Tdriverinterface=class;

Tdrvdrvinterface=class
  function get_config_int(settingsname : ansistring; dflt : int64) : int64; virtual;
  function get_config_flt(settingsname : ansistring; dflt : double) : double; virtual;
  function get_config_bin(settingsname : ansistring; dflt : boolean) : boolean; virtual;
  function get_config_str(settingsname,dflt : ansistring) : ansistring; virtual;

  { tree owner is driver, excpet during events }
  
  { the driver identified a resize-like situation, 
    we probably need to re-layout the gui-node-tree }
  function do_layout(width, height : longint) : boolean; virtual;
  //procedure on_event_later(); virtual;
  
  public
  drv : Tdriverinterface;
end;

Tdriverinterface=class
  function startup() : boolean; virtual; abstract;
  procedure shutdown(); virtual; abstract;
  function tellname() : ansistring; virtual; abstract;
  procedure cleanup(); virtual; abstract;
  
  destructor destroy(); override;
  public
  reverseif : Tdrvdrvinterface;
  next,next2 : Tdriverinterface;
  defunct : boolean;
end;

Twinsysdriverinterface=class(Tdriverinterface)
  function deeperup() : boolean; virtual; abstract;
  function make_node(parent : Tlayoutedwindow; img : TsomeBGdesciptor; r : Twrect) : Tlayoutedwindow; virtual; abstract;
  function enterloop() : longint; virtual; abstract;
end;



procedure register_driver(d : Tdriverinterface);


var known_drivers : Tdriverinterface = nil;



procedure get_useful_configs(var cl : Tconfiglist);
function load_config(n : ansistring) : Twinsysdriverinterface;


implementation
uses math,DOM,

wayland_shm,
X11_drv,

sysutils
;


var 
setread : Tsettingsreader;
currentconfigname : ansistring;

procedure register_driver(d : Tdriverinterface);
begin
writeln('registering driver: ',d.tellname());
  d.next := known_drivers;
  known_drivers := d;
  d.reverseif := Tdrvdrvinterface.create();
  d.reverseif.drv := d;
end;





constructor Tlayoutedwindow.create();
begin
  dblLinkedList_init(childlist);
  dirty := true;
end;

destructor Tlayoutedwindow.destroy();
var p : Pdbllinkedlist;
begin
writeln('Tlayoutedwindow.destroy();');
  release_BGdescr(BG);
  p := dblLinkedList_getFirst(childlist);
  while p<>nil do begin
    Tlayoutedwindow(p^.dat).destroy();
    p := dblLinkedList_getFirst(childlist);
  end;
  if parent<>nil then parent.removeChild(self);
end;

function Tlayoutedwindow.insertChild(c : Tlayoutedwindow; cref : Tlayoutedwindow=nil) : Tlayoutedwindow;
begin
  if cref<>nil
  then c.siblings := dblLinkedList_insertAfter(childlist,c.siblings,c)
  else c.siblings := dblLinkedList_insertBefore(childlist,nil,c);
  c.parent := self;
  insertChild := c;
end;

procedure Tlayoutedwindow.removeChild(c : Tlayoutedwindow);
begin
  dblLinkedList_remove(c.siblings);
  c.siblings := nil;
  c.parent := nil;
end;


procedure Tlayoutedwindow.calc_vr();
begin
  if parent=nil then begin
    vr.x := 0;
    vr.y := 0;
    vr.dx := r.dx;
    vr.dy := r.dy;
  end else begin
    vr.x := max(0,parent.vr.x-r.x);
    vr.y := max(0,parent.vr.y-r.y);
    vr.dx := parent.vr.x +parent.vr.dx; // max right coord of parent
    vr.dy := parent.vr.y +parent.vr.dy;
    vr.dx -= r.x; // local coord max right coord of parent
    vr.dy -= r.y;
    vr.dx := min(vr.dx,r.dx);
    vr.dy := min(vr.dy,r.dy);
    vr.dx -= vr.x;
    vr.dy -= vr.y;
    vr.dx := max(0,vr.dx);
    vr.dy := max(0,vr.dy);
  end;  
end;

procedure Tlayoutedwindow.calc_vrs();
var p : Pdbllinkedlist;
begin
  calc_vr();
  p := dblLinkedList_getFirst(childlist);
  while p<>nil do begin
    Tlayoutedwindow(p^.dat).calc_vrs();
    p := dblLinkedList_getNext(p);
  end;
end;


procedure Tlayoutedwindow.print(lvl : string; gx,gy : longint);
var p : Pdbllinkedlist; c : Tlayoutedwindow;
begin
  writeln(lvl,'+ ',gx,'/',gy,' .. ',gx+r.dx,'/',gy+r.dy,' [',vr.x,'/',vr.y,' ',vr.dx,'/',vr.dy,']');
  p := dblLinkedList_getFirst(childlist);
  while p<>nil do begin
    c := Tlayoutedwindow(p^.dat);
    c.print(lvl+'|',c.r.x+gx,c.r.y+gy);
    p := dblLinkedList_getNext(p);
  end;
end;




function Tdrvdrvinterface.get_config_int(settingsname : ansistring; dflt : int64) : int64;
var s : ansistring; w : word;
begin
  result := dflt;
  s := '';
  str(dflt,s);
  s := get_config_str(settingsname,s);
  val(s,result,w);
  if w<>0 then begin
    writeln('Couldnt interpret setting value ',s,' for ',settingsname,' @',drv.tellname());
  end;
end;

function Tdrvdrvinterface.get_config_flt(settingsname : ansistring; dflt : double) : double;
var s : ansistring; w : word;
begin
  result := dflt;
  s := '';
  str(dflt,s);
  s := get_config_str(settingsname,s);
  val(s,result,w);
  if w<>0 then begin
    writeln('Couldnt interpret setting value ',s,' for ',settingsname,' @',drv.tellname());
  end;
end;

function Tdrvdrvinterface.get_config_bin(settingsname : ansistring; dflt : boolean) : boolean;
var s : ansistring; w : word; b : byte;
begin
  result := dflt;
  s := '';
  str(dflt,s);
  s := get_config_str(settingsname,s);
  val(s,b,w);
  if (w<>0)or(b>1) then begin
    writeln('Couldnt interpret setting value ',s,' for ',settingsname,' @',drv.tellname());
  end else begin
    if b=0 then result := false else result := true;
  end;
end;

function Tdrvdrvinterface.get_config_str(settingsname,dflt : ansistring) : ansistring;
var s : ansistring;
begin
  get_config_str := dflt;
  s := setread.get_setting('drivers.'+drv.tellname()+'.'+settingsname);
  if s<>'' then get_config_str := s;
  s := setread.get_setting('config.'+currentconfigname+'.drivers.'+drv.tellname()+'.'+settingsname);
  if s<>'' then get_config_str := s;
  writeln('=',result);
end;



function Tdrvdrvinterface.do_layout(width, height : longint) : boolean;
begin
  writeln('LAYOUT REQ(',width,', ',height,')');
  do_layout := true;
end;




procedure get_useful_configs(var cl : Tconfiglist);
begin
  setread.get_useful_configs(cl);
end;

function lookup_driver(s : ansistring) : Tdriverinterface;
var drv : Tdriverinterface;
begin
  drv := known_drivers;
  while assigned(drv) do begin
    if (s=drv.tellname()) then begin
      lookup_driver := drv;
      exit;
    end;
    drv := drv.next;
  end;
end;

var
cfg_drivers : Tdriverinterface;

procedure unload_config();
begin
  while assigned(cfg_drivers) do begin
    cfg_drivers.shutdown();
    cfg_drivers := cfg_drivers.next2;
  end;
end;

function load_config(n : ansistring) : Twinsysdriverinterface;
var q : TDOMNode; drv : Tdriverinterface; s : ansistring;
begin
  load_config := nil;
  cfg_drivers := nil;
  currentconfigname := n;
  writeln('try loading config ',n);
  q := setread.get_node('config.'+n+'.drivers');
  if assigned(q) then begin
    q := q.firstChild;
    while q<>nil do begin
      if (q is TDOMElement)and(q.nodename='driver') then begin
        s := UTF8Encode((q as TDOMElement).getAttribute('name'));
        writeln('  required driver:',s,'...');
        drv := lookup_driver(s);
        if assigned(drv) and drv.startup() then begin
          writeln('ok, driver ',s,' started up.');
          drv.next2 := cfg_drivers;
          cfg_drivers := drv;
          if (drv is Twinsysdriverinterface) then begin
            load_config := (drv as Twinsysdriverinterface);
          end;
        end else begin
          writeln('fail');
          unload_config();
          load_config := nil;
          exit;
        end;
      end;
      q := q.nextSibling;
    end;
  end else load_config := nil;
end;


destructor Tdriverinterface.destroy();
begin
  reverseif.destroy();
end;

initialization
begin
  setread := Tsettingsreader.create();
  setread.setXML('./drivers/test.xml');
writeln(GetAppConfigDir(false));
currentconfigname := 'config01';
end;

finalization
begin
  setread.destroy();
end;

end.
