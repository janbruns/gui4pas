{$mode objfpc}
unit x11_clp;

interface
uses xlib,x,xutil,xshm,xinput2,xi2,ipc,sysutils,baseunix,x11_clp_snd;

{
   https://www.x.org/releases/X11R7.6/doc/xorg-docs/specs/ICCCM/icccm.html
   https://tronche.com/gui/x/icccm/
   https://tronche.com/gui/x/xlib/
   https://www.freedesktop.org/wiki/Specifications/XDND/
   https://www.freedesktop.org/wiki/Specifications/XDNDRevision/

}

const
LIMIT_READ_CHUNKSIZE  = 16384; // in dw -> 64 KByte
LIMIT_WRITE_CHUNKSIZE = LIMIT_READ_CHUNKSIZE;
REQ_DISCARD_TIME = 120/(3600*24); // 120 sec of one day

type
Tmimearr = array of Pchar;
Tatomarr = array of Tatom;

Tx11getdatacb = procedure(ud : pointer; const dat : ansistring; final_type : Tatom) of object;


Tbusyproplistentry = class
  next : Tbusyproplistentry;
  atom,sel,targ : Tatom;
  time : Ttime;
  win : Twindow; // a local eequestor window, the prop is curently used on
  mode_incr, selnotify_seen, transfer_seen, transfer_complete : boolean;
  age : double;
end;





Tx11clipboard = class(Tx11clp_snd)
  public

  procedure setdisplay(d : Pdisplay); override;
  procedure onPropertyNotify(const ev : TXPropertyEvent); override;
  procedure onSelectionNotify(const ev : TXSelectionEvent); virtual;
  { Request clipboard data to be received.
    The function return immediately.
    When the data arrives, cb(ud, data, datatype) gets called.
    win : should be an app-created window linstened for PropertyChangeMask
    sel : the clipboard to ask (typically atom_clipboard)  
    targ : an atom specifying the requested data type (use atom_targets 
           to get a list of data types, the current clipboard content supports)
    tim : should be the X timestamp of the X event, that triggered the request
    reject_sametime : wether to ignore get_clp_data(), if there a pending 
                      requests with the same timestamp
  }
  procedure get_clp_data( cb : Tx11getdatacb; ud : pointer; 
                          win : Twindow; 
                          sel,targ : Tatom; 
                          tim : Ttime; 
                          reject_sametime : boolean
                         );

  constructor create();
  destructor destroy(); override;

  private

  procedure discard_old_requests();
  function has_req_timestamp(ts : Ttime) : boolean;
  function generate_dest_property_atom() : Tbusyproplistentry;
  procedure onCompletion(bp : Tbusyproplistentry);
  function get_property_atom() : Tbusyproplistentry;

  procedure push_read_data(p : Pchar; n : dword);
  procedure rcv_and_del(bp : Tbusyproplistentry);
  procedure just_del(bp : Tbusyproplistentry);
  function bp_ev_match(bp : Tbusyproplistentry; const ev : TXSelectionEvent) : boolean;
  function bp_ev_propmatch(bp : Tbusyproplistentry; const ev : TXPropertyEvent) : boolean;
  procedure onPropertyNotify_check_busy(const ev : TXPropertyEvent);

  procedure link_bp(bp : Tbusyproplistentry; cb : Tx11getdatacb; ud : pointer );

  protected

  mostrecent_type : Tatom;
  mostrecent_data : ansistring;
  mostrecent_datalen : int64;
  mostrecent_format : cint;
  mostrecent_cb : Tx11getdatacb;
  mostrecent_ptr : pointer;

  private
  availprops, busyprops : Tbusyproplistentry;
  reservedprpcnt : longint;
end;



implementation

function Tx11clipboard.generate_dest_property_atom() : Tbusyproplistentry;
var atomname : ansistring; bp : Tbusyproplistentry;
begin
  atomname := 'CLPDAT_' + hexstr(reservedprpcnt,8);
writeln('generating new clipboard-property name: ',atomname);
  inc(reservedprpcnt);
  bp := Tbusyproplistentry.create();
  bp.atom := XInternAtom(display,@atomname[1],false);
  bp.next := nil;
  generate_dest_property_atom := bp;
end;

function Tx11clipboard.get_property_atom() : Tbusyproplistentry;
var bp : Tbusyproplistentry;
begin
  bp := availprops;
  if bp<>nil then begin
    availprops := bp.next;
  end else bp := generate_dest_property_atom();
  bp.next := nil;
  bp.sel := None;
  bp.targ := None;
  bp.win := None;
  bp.mode_incr := false;
  bp.selnotify_seen := false;
  bp.transfer_seen := false;
  bp.transfer_complete := false;
  get_property_atom := bp;
end;






procedure Tx11clipboard.setdisplay(d : Pdisplay);
var mrs : dword;
begin
  inherited setdisplay(d);
  mrs := XMaxRequestSize(d);
  max_read := (mrs -32) div 2;
  if max_read >LIMIT_READ_CHUNKSIZE  then max_read := LIMIT_READ_CHUNKSIZE;
end;









{ this code is only called in case bp=busyprops}
procedure Tx11clipboard.push_read_data(p : Pchar; n : dword);
var oldlen,newlen : int64;
begin
  if mostrecent_type<>atom_INCR then begin
    oldlen := mostrecent_datalen;
    newlen := oldlen+n;
    if length(mostrecent_data)<newlen then setlength(mostrecent_data,newlen*2);
    move(p^,mostrecent_data[1+oldlen],n);
    mostrecent_datalen := newlen;
  end else begin
    writeln('INCR requested!');
    busyprops.mode_incr := true;
  end;
end;

{ this code is only called in case bp=busyprops}
procedure Tx11clipboard.rcv_and_del(bp : Tbusyproplistentry);
var 
  total_items : qword;
  bytes_after_return : culong;
  prop_return : pchar; 
  nitems_return : culong; 
  total_dwords, bytesread : qword;
begin
  total_dwords := 0;
  total_items := 0;
  repeat
    bytes_after_return := 0;
    XGetWindowProperty( display,bp.win,bp.atom,
                        total_dwords, 
                        max_read, 
                        true, 
                        AnyPropertyType, 
                        @mostrecent_type, 
                        @mostrecent_format, 
                        @nitems_return, 
                        @bytes_after_return, 
                        @prop_return 
                      );

    total_items += nitems_return;
    { ugly Xlib-related hack }
    case mostrecent_format of
       8 : bytesread := nitems_return;
      16 : bytesread := nitems_return*sizeof(cshort);
      32 : bytesread := nitems_return*sizeof(clong);
    end;
    push_read_data(prop_return, bytesread);
    xfree(prop_return);
    total_dwords += (bytesread div 4);
  until (bytes_after_return=0);
  bp.transfer_seen := true;
  if bp.mode_incr then begin
    if (total_items=0) then bp.transfer_complete := true;
  end else begin
    { the sender might actually be appending more data,
      as long as we didn't see SelectionNotify
    }
    bp.transfer_complete := true;
  end;
end;


procedure Tx11clipboard.just_del(bp : Tbusyproplistentry);
var 
  bytes_after_return : culong;
  rtype : Tatom;
  rfmt : cint;
  nitems_return : culong; 
  prop_return : pchar;
begin
  bytes_after_return := 0;
  XGetWindowProperty( display,bp.win,bp.atom,
                      0, 
                      1, 
                      false, 
                      AnyPropertyType, 
                      @rtype, 
                      @rfmt, 
                      @nitems_return, 
                      @bytes_after_return, 
                      @prop_return 
                    );
  xfree(prop_return);
  bp.transfer_seen := true;
  if rtype=atom_INCR then bp.mode_incr := true;
  if bp.mode_incr then begin
    if (nitems_return=0) then bp.transfer_complete := true;
  end else begin
    { the sender might actually be appending more data,
      as long as we didn't see SelectionNotify
    }
    bp.transfer_complete := true;
  end;
  XDeleteProperty(display,bp.win,bp.atom);
end;


function Tx11clipboard.bp_ev_propmatch(bp : Tbusyproplistentry; const ev : TXPropertyEvent) : boolean;
begin
  bp_ev_propmatch := (bp.win=ev.window) and (bp.atom=ev.atom);
end;

procedure Tx11clipboard.onPropertyNotify_check_busy(const ev : TXPropertyEvent);
var bp : Tbusyproplistentry; n : longint;
begin
  n := 0;
  writeln('Tx11clipboard.onPropertyNotify: onPropertyNotify_check_busy');
  bp := busyprops;
  while bp<>nil do begin
    if bp_ev_propmatch(bp,ev) then begin
      if (bp=busyprops)
      then rcv_and_del(bp) 
      else just_del(bp);
      if (bp.transfer_complete and bp.selnotify_seen) 
      then onCompletion(bp);
      exit;
    end;
inc(n);
    bp := bp.next;
  end;
  writeln('Tx11clipboard.onPropertyNotify: onPropertyNotify_check_busy << ',n);

end;


procedure Tx11clipboard.onPropertyNotify(const ev : TXPropertyEvent);
begin
  writeln('Tx11clipboard.onPropertyNotify');
  case ev.state of
    PropertyDelete : inherited onPropertyNotify(ev);
    PropertyNewValue : onPropertyNotify_check_busy(ev);
  end;
end;


function Tx11clipboard.bp_ev_match(bp : Tbusyproplistentry; const ev : TXSelectionEvent) : boolean;
begin
  if (bp.win=ev.requestor)
  and (bp.time=ev.time)
  and not(bp.selnotify_seen)
  and (bp.sel=ev.selection)
  and (bp.targ=ev.target)
  then begin
    if (ev._property=None) 
    then bp_ev_match := true
    else bp_ev_match := (bp.atom=ev._property);
  end else bp_ev_match := false;
end;


procedure Tx11clipboard.onSelectionNotify(const ev : TXSelectionEvent);
var bp,bp2 : Tbusyproplistentry; n : longint;
begin
  write('Tx11clipboard.onSelectionNotify... ');
  { find the oldest matching request that hasn't seen notification}
  bp2 := nil;
  bp := busyprops;
n := 0;
  while bp<>nil do begin
    if bp_ev_match(bp,ev) then bp2 := bp;
    bp := bp.next;
inc(n);
  end;
writeln(n,' requests pending.');
  if bp2=nil then begin
    writeln('didn''t find a matching request!');
  end else begin
    bp2.selnotify_seen := true;
    { (ev._property=None) indicates rejected requests,
      so no data is to be expected }
    if (ev._property=None) then bp2.transfer_complete := true;
    if bp2.transfer_complete then begin
      onCompletion(bp2);
      exit;
    end;
  end;
end;

procedure Tx11clipboard.onCompletion(bp : Tbusyproplistentry);
var bp2 : Tbusyproplistentry;
begin
writeln('Tx11clipboard.onCompletion',bp=busyprops,mostrecent_cb<>nil);
  { if the transaction is the most recent one, it isn't aborted,
    and data received was collected and is valid to be passed to cb.
  }
  if bp=busyprops then begin
    busyprops := busyprops.next;
    if (mostrecent_cb<>nil) then begin
      setlength(mostrecent_data,mostrecent_datalen);
      mostrecent_cb(mostrecent_ptr, mostrecent_data, mostrecent_type);
    end;
    mostrecent_data := '';
    mostrecent_datalen := 0;
    mostrecent_type := none;
    mostrecent_ptr := nil;
    mostrecent_cb := nil;
    { recycle }
    bp.next := availprops;
    availprops := bp;
  end else begin
    { it otherwise doesn't have valid data or cb anymore, just recycle }
    bp2 := busyprops;
    while (bp2<>nil) do begin
      if (bp2.next=bp) then begin
        bp2.next := bp.next;
        bp.next := availprops;
        availprops := bp;
      end else bp2 := bp2.next;
    end;
  end;
end;


{ if there are irresponsive requests pending
  for a while, discard them, so that
  a) the list of pending requests stays moderate sized
  b) the property names used stay reserved for safety
     This would ideally be done per-window, 
     but isn't to keep code simple.
}

procedure Tx11clipboard.discard_old_requests();
var bp,bp2 : Tbusyproplistentry; t : double;
begin
  t := now();
  { repeatedly destroy the first busyprop, if that is aged already }
  while (busyprops<>nil) do begin
    if (t-busyprops.age)>REQ_DISCARD_TIME then begin
      bp2 := busyprops.next;
      busyprops.destroy();
      busyprops := bp2;
      mostrecent_data := '';
      mostrecent_datalen := 0;
      mostrecent_type := none;
      mostrecent_ptr := nil;
      mostrecent_cb := nil;
    end else break;
  end;
  { now the rest, if any }
  bp := busyprops;
  if (bp=nil) then exit;
  while bp.next<>nil do begin
    if (t-bp.next.age)>REQ_DISCARD_TIME then begin
      bp2 := bp.next.next;
      bp.next.destroy();
      bp.next := bp2;
    end else bp := bp.next;
  end;
end;

function Tx11clipboard.has_req_timestamp(ts : Ttime) : boolean;
var bp : Tbusyproplistentry;
begin
  bp := busyprops;
  has_req_timestamp := false;
  while (bp<>nil) do begin
    if (bp.time=ts) then begin
      has_req_timestamp := true;
      exit;
    end;
    bp := bp.next;
  end;
end;

procedure Tx11clipboard.link_bp(bp : Tbusyproplistentry; cb : Tx11getdatacb; ud : pointer );
begin
  bp.next := busyprops;
  busyprops := bp;
  mostrecent_data := '';
  mostrecent_datalen := 0;
  mostrecent_type := none;
  mostrecent_ptr := ud;
  mostrecent_cb := cb;
end;

procedure Tx11clipboard.get_clp_data( cb : Tx11getdatacb; ud : pointer; win : Twindow; sel,targ : Tatom; tim : Ttime; reject_sametime : boolean );
var bp : Tbusyproplistentry; idat : ansistring; itype : Tatom;
begin
  writeln('get_clp_data');
  bp := get_property_atom();
  bp.win := win;
  bp.sel := sel;
  bp.targ := targ;
  bp.time :=  tim;
  bp.age := now();
  if owns_clipboard(sel) then begin
    writeln(' self-owned-get_clp_data');
    idat := '';
    itype := None;
    if appinternal_get_clipboard(sel,targ,idat,itype) then begin
      link_bp(bp,cb,ud);
      mostrecent_data := idat;
      mostrecent_datalen := length(mostrecent_data);
      mostrecent_type := itype;
      onCompletion(bp);
    end else begin
      // do not link bp, allowing mostrecent transfer to still be fullfilled 
      bp.next := availprops;
      availprops := bp;
    end;
  end else begin
    writeln(' extenral get_clp_data');
    reject_sametime := reject_sametime and has_req_timestamp(tim);
    if reject_sametime then begin
      writeln('  rejected sametime get_clp_data');
      bp.next := availprops;
      availprops := bp;
    end else begin
      writeln('  doing get_clp_data');
      link_bp(bp,cb,ud);
      XConvertSelection(display,sel,targ,bp.atom,win,tim);
    end;
  end;
end;


constructor Tx11clipboard.create();
begin 
  inherited create();
  reservedprpcnt := 0;
end;

destructor Tx11clipboard.destroy();
var bp : Tbusyproplistentry;
begin
  while busyprops<>nil do begin
    bp := busyprops.next;
    busyprops.destroy();
    busyprops := bp;
  end;
  while availprops<>nil do begin
    bp := availprops.next;
    availprops.destroy();
    availprops := bp;
  end;
  inherited destroy();
end;



begin
end.

