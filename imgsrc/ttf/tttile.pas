{$mode objfpc}
{$unitpath .}
{$COPERATORS ON}
UNIT tttile;

interface
uses fontfile;

type
Pglyphbmp = ^Tglyphbmp;
Tglyphbmp = record
  next : Pglyphbmp;
  dat  : ^byte;
  ppemx,xres,refx,lsb,awx : longint;
  ppemy,yres,refy,grf,awy : longint;
end;


function get_glybmp(g : TTglyph; ppemx,ppemy : longint; gf : boolean) : Pglyphbmp;


implementation
uses ttptinst,ttstack,ttprog,ttcalc,ttdata,ttgridfit,ttrender,ttflaten;


function find_tile_in_gly(g : TTglyph; ppx,ppy : longint; gf : boolean) : Pglyphbmp;
var bmp : Pglyphbmp; gft : longint;
begin
  find_tile_in_gly := nil;
  gft := 0;
  if (gf) then gft := 1;
  bmp := g.tiles;
  while bmp<>nil do begin
    if (bmp^.ppemx=ppx)and(bmp^.ppemy=ppy)and(bmp^.grf=gft) then begin
      find_tile_in_gly := bmp;
      bmp := nil;
      exit;
    end else begin
      bmp := bmp^.next;
    end;
  end;
end;

procedure destroy_glybmp(var p : Pglyphbmp);
var p2 : Pglyphbmp;
begin
  p2 := p^.next;
  freemem(p^.dat,p^.xres*p^.yres*sizeof(p^.dat^));
  dispose(p);
  p := p2;
end;

procedure destroy_glybmps(g : TTglyph);
begin
  while g.tiles<>nil do destroy_glybmp(g.tiles);
end;

procedure destroy_gridfitter(f : TTfont);
begin
  if f.gridfitter<>nil then Tgridfitter(f.gridfitter).destroy();
end;




procedure add_tile_to_gly(g : TTglyph; bmp : Pglyphbmp);
begin
  g.tilesdestroy := @destroy_glybmps;
  bmp^.next := g.tiles;
  g.tiles := bmp;
end;


function make_glybmp( g : TTglyph;
                      c : Tbezierstreamer; 
                      sm : longint          ) : Pglyphbmp;
var q : Pglyphbmp; p : Pbyte; x,y,x0,y0,x1,y1 : longint; r : ttmultirenderer;
begin
  r := g.font.renderer;
  new(q);
  with q^ do begin
    next := nil;
    lsb := round(g.slsb/64)-2;
    awx := g.saw;
    x0 := round(g.sxmin/64)-2;
    x1 := round(g.sxmax/64)+2;
    y0 := round(g.symin/64)-2;
    y1 := round(g.symax/64)+2;
    xres := abs(x1-x0)+2;
    yres := abs(y1-y0)+2;
    getmem(dat,xres*yres*sizeof(dat^));
    refx := x0;
    refy := y0;
    r.check_size(xres+3,yres+3);
    r.setzero(-x0,-y0);
    r.setClip(0,0,xres+1,yres+1);
    r.draw(sm,c);
    p := dat;
    for y := 1 to yres do begin
      for x := 1 to xres do begin
        p^ := (r.getgrey(x,y) shr (14+9)) and $7f;
        if (r.getbw(x,y)<>0) then p^ := p^ or $80;
        inc(p);
      end;
    end;
  end;
  add_tile_to_gly(g,q);
  make_glybmp := q;
end;


function get_glybmp(g : TTglyph; ppemx,ppemy : longint; gf : boolean) : Pglyphbmp;
var bmp : Pglyphbmp; c : Tbezierstreamer; gridfitter : Tgridfitter;
begin
  bmp := find_tile_in_gly(g,ppemx,ppemy,gf);
  if bmp=nil then begin
    g.scale(ppemx,ppemy,g.font.unitsPerEm);
    c := Tbezierstreamer.create();
    if gf then begin
      gridfitter := Tgridfitter(g.font.gridfitter);
      if gridfitter=nil then begin
        gridfitter := Tgridfitter.create();
        g.font.gridfitter := gridfitter;
        gridfitter.set_font(g.font,0);
        g.font.gfdestroy := @destroy_gridfitter;
      end;
      { In case the font's gridfit program asks, 
        tell it to gridfit the outline for monochrome rendering,
        so mono can be drawn as overlay to grey.
      }
      gridfitter.render_greyscale := false;
      gridfitter.set_ppem(ppemx,ppemy,ppemy);
      gridfitter.gridfit_glyph(g);
      g.make_contours(c);
      bmp := make_glybmp(g,c,gridfitter.getScantype());
      bmp^.grf := 1;
    end else begin
      g.make_contours(c);
      bmp := make_glybmp(g,c,3);
      bmp^.grf := 0;
    end;
    c.destroy();
    bmp^.ppemx := ppemx;
    bmp^.ppemy := ppemy;
  end;
  get_glybmp := bmp;
end;



begin
end.


