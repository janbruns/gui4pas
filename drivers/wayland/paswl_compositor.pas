{$mode objfpc}
{$unitpath .}
unit paswl_compositor;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_display,paswl_surface,paswl_subsurface,paswl_bindable;


type

Tpaswl_compositor=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  function create_surface(c : Tpaswl_surface_cls) : Tpaswl_surface;
 
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;

Tpaswl_compositorcls = class of Tpaswl_compositor;







implementation



constructor Tpaswl_compositor.create();
begin
  inherited create();
end;

destructor Tpaswl_compositor.destroy();
begin
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure Tpaswl_compositor.onBind();
begin
  inherited;
end;

procedure Tpaswl_compositor.onUnbind();
begin
  inherited;
end;

function Tpaswl_compositor.create_surface(c : Tpaswl_surface_cls) : Tpaswl_surface;
var o : Tpaswl_surface; wo : wl_surface; 
begin
  wo := wl_compositor_create_surface(wl_compositor(wlo));
  o := make_child(c,wo) as Tpaswl_surface;
  create_surface := o;
end;


class function Tpaswl_compositor.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_compositor_interface;
end;

class function Tpaswl_compositor.wl_ifver() : dword;
begin
  wl_ifver := 6;
end;


initialization
begin
end;

finalization
begin
end;

end.



