unit ot_gsub;
{$mode objfpc}
{$COPERATORS ON}

{ The GSUB table in OpenType fonts describes
  Text processing that can be apllied to improve
  visual appearance based on the font design.

  For example, consider a font that for optical
  reasons has a very high captital "T" glyph,
  and wants to define that Text sequences like
  "Ta" and "Tc" (but probably not "Tb") should 
  be replaced by special glyphs.

  This text processing works on Glyph indices,
  wich are limited to 16 bit. So let's make use
  of fpc's unicodestring, even if we aren't 
  talking about unicodes.  

}



interface
uses classes,sysutils,fontfile,ot_cmap,ot_gdef,ot_comn;


type



TSingleSubstFormat1 = class(TOTgsublookup)
  procedure load(s : Tstream); override;
  function apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr; override;

  public
  Coverage : Tcoverage;
  DeltaGlyphID : Tglychar;
end;

TSingleSubstFormat2 = class(TOTgsublookup)
  procedure load(s : Tstream); override;
  function apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr; override;

  public
  Coverage : Tcoverage;
  Substitute : Tglystr;
end;

TMultipleSubstFormat1 = class(TOTgsublookup)
  procedure load(s : Tstream); override;
  function apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr; override;

  public
  Coverage : Tcoverage;
  Substitutes : array of Tglystr;
end;

TAlternateSubstFormat1 = class(TOTgsublookup) // not required
end;

Tligature = record
  LigGlyph : Tglychar;
  Component : Tglystr;
end;
TligatureSet = array of Tligature;
TligatureSetList = array of TligatureSet;

TLigatureSubstFormat1 = class(TOTgsublookup)
  procedure load(s : Tstream); override;
  function apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr; override;

  public
  Coverage : Tcoverage;
  LigatureSets : TligatureSetList;
end;





TChainSubRule = record
  btrk,inpt,lahd : Tglystr;
  sa : Tsubstarr;
end;
TchainsubruleArr = array of TChainSubRule;

TChainContextSubstFormat1 = class(TOTgsublookup)
  procedure load(s : Tstream); override;
  function apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr; override;

  public
  Coverage : Tcoverage;
  csrs : array of TchainsubruleArr;
end;


TChainSubClassRule = record
  bt,ip,la : Tglystr;
  sa : Tsubstarr;
end;
TChainSubClassSet = array of TChainSubClassRule;


TChainContextSubstFormat2 = class(TOTgsublookup)
  procedure load(s : Tstream); override;
  function apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr; override;

  public
  cscs : array of TChainSubClassSet;
  coverage : Tcoverage;
  btcd,ipcd,lacd : Tglycls;
end;

TChainContextSubstFormat3 = class(TOTgsublookup)
  procedure load(s : Tstream); override;
  function apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr; override;

  public
  cbt,cin,cla : Tcoveragearr;
  sa : Tsubstarr;
end;

TContextSubstFormat1 = class(TChainContextSubstFormat1)
  procedure load(s : Tstream); override;
end;

TContextSubstFormat2 = class(TChainContextSubstFormat2)
  procedure load(s : Tstream); override;
end;

TContextSubstFormat3 = class(TChainContextSubstFormat3)
  procedure load(s : Tstream); override;
  // using inherited apply+fields 
end;




TExtensionSubstFormat1 = class(TOText32lookup)
end;

TReverseChainSingleSubstFormat1 = class(TOTgsublookup)
  procedure load(s : Tstream); override;
  function apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr; override;
  public
  cbt,cla : Tcoveragearr;
  coverage : Tcoverage;
  Substitute : Tglystr;
end;

{ note: even though this one is called reverse chaining, and the
  standard defines reverse processing for this type, 
  with inp.length=1 (the only option with format 8.1) the processing 
  order shouldn't matter, I think.

  The specs talk about ReverseChainSingleSubstFormat1 was introduced
  late. To me it seems like the authors mixed up what "chaining" 
  means: There's a chain of context parts, namely backtrack, input 
  and lookahead. This is diffrent from the nonchaining context rules
  where the complete context is within "input" sequence.
  It DOESN'T mean that replacements should recursively become part of 
  the pattern search (see section 6.1 Chaining Context Substitution Format 1):

  "If there is a match, then the client finds the target glyph positions for 
   substitutions and completes the substitutions. Please note that (just like 
   in the ContextSubstFormat1 subtable) these lookups are required to operate 
   within the range of text from the covered glyph to the end of the input 
   sequence. No substitutions can be defined for the backtracking sequence or 
   the lookahead sequence.

   Once the substitutions are complete, the client should move to the glyph 
   position immediately following the matched input sequence and resume the 
   lookup process from there."
  
  So maybe the real reason for introducing "reversed chain" was to clarify
  there are mismatched implementations concerning the recursiveness of the
  chaining rules substitutes: With an input length of 1, meaning that 
  single chars get replaced by whatever (single chars), the processing order
  doesn't matter if you always continue with the thing that follows the 
  matched input sequence.

  So Font designers probably should'nt rely on specific behaviour concerning 
  wether or not replacements become reprocessed within that same rule.

  For example, if there is a Ligature rule 
    replace( 'AB' by 'C' AND 'CD' by 'E' )
  applying that rule to "ABD" probably could result in "E", even though
  the result should be "CD" (because of the specs cited above).

  There could also be implementations that let rules modify the string
  currently proceessed, instead of building up the result independently.
  This would mainly affect what the "backtrack" part of chained rules
  get to see (this could either be the string to be processed, like
  with this implementation, or a string that already has been partially 
  modified). The specs are vague about this.

}




/////////////////////////////////////////////////////////////////
// the TTfont descendant
/////////////////////////////////////////////////////////////////


TOTgsubfont = class(TOTgdeffont)
  constructor create();
  destructor destroy(); override;

  procedure load_gsub();

  function applyScript(s : Tglystr; n : longint) : Tglystr;
  function applyFeature(s : Tglystr; n : longint) : Tglystr;


  function applyLookup(s : Tglystr; n : longint) : Tglystr;

  function findgsubScript(nam : string) : longint;



  private
  procedure load_FeatureVariations(s : Tstream); virtual;

  public
  gsubScripts : TOTscriptList;
  gsubFeaturelist : TOTfeaturelist;
  gsubLookUps : Tlookuptabs;
end;








// finally some exports to be reused in gpos:

procedure read_csr(var csr : TChainSubRule; s : Tstream);
procedure load_csrs(var csrs : TchainsubruleArr; s : Tstream);
procedure read_ChainSubClassRule(var c : TChainSubClassRule; s : Tstream);
procedure read_ChainSubClassSet(var cscs : TChainSubClassSet; s : Tstream);
procedure read_SubClassRule(var c : TChainSubClassRule; s : Tstream);
procedure read_SubClassSet(var cscs : TChainSubClassSet; s : Tstream);
procedure read_sr(var csr : TchainSubRule; s : Tstream);
procedure load_srs(var csrs : TchainsubruleArr; s : Tstream);




implementation
uses math;





/////////////// replace one char by one char

procedure TSingleSubstFormat1.load(s : Tstream);
var i0,co : longint;
begin
  i0 := s.position;
  read_word(s); // version, already processed
  co := read_word(s);
  DeltaGlyphID := read_glychar(s);
  s.seek(i0+co,soFromBeginning);
  load_coverage(coverage,s);
  writeln(classname, ' loaded ',length(coverage));
end;

procedure TSingleSubstFormat2.load(s : Tstream);
var i0,co,gc,i : longint;
begin
  i0 := s.position;
  read_word(s); // version, already processed
  co := read_word(s);
  gc := read_word(s);
  Substitute := read_glystr(gc,s);
  s.seek(i0+co,soFromBeginning);
  load_coverage(coverage,s);
  i := length(coverage);
  if not(i=gc) then begin
    i := min(i,gc);
    setlength(coverage,i);
    setlength(Substitute,i);
  end;  
//  for i := 1 to gc do writeln('  map "',hexstr(word(coverage[i]),4),'" to "',hexstr(word(Substitute[i]),4),'"');
  writeln(classname, ' loaded ',i);
end;


function TSingleSubstFormat1.apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr; 
var y : Tglystr; i,j,len : longint;
begin
  y := s; // is copy on write, I think
  len := length(s);
  for i := 1 to len do begin
    j := covpos(y[i],coverage,tabs);
    if (j>0) then begin
      y[i] := Tglychar(word(DeltaGlyphID)+word(y[i]));
    end;
  end;
  apply := y;
end;

function TSingleSubstFormat2.apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr; 
var y : Tglystr; i,j,len : longint;
begin
  y := s; // is copy on write, I think
  len := length(s);
  for i := 1 to len do begin
    j := covpos(y[i],coverage,tabs);
    if (j>0) then begin
writeln('singlesubst: replace ',word(y[i]),' by ',word(Substitute[j]));
      y[i] := Substitute[j]; // Substitute also glystr
    end;
  end;
  apply := y;
end;









/////////////// replace one char by strings


procedure TMultipleSubstFormat1.load(s : Tstream);
var i0,co,sc,i,n : longint; os : array of longint;
begin
  i0 := s.position;
  read_word(s); // version, already processed
  co := read_word(s);
  sc := read_word(s);
  setlength(os,sc);
  for i := 0 to sc-1 do os[i] := read_word(s);

  s.seek(i0+co,soFromBeginning);
  load_coverage(coverage,s);

  sc := min(sc,length(coverage));
  setlength(substitutes,sc);
  for i := 0 to sc-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    n := read_word(s);
    substitutes[i] := read_glystr(n,s);
  end;

  writeln(classname, ' loaded ',sc);
end;


function TMultipleSubstFormat1.apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr; 
var y : Tglystr; i,j,len : longint;
begin
  y := ''; 
  len := length(s);
  for i := 1 to len do begin
    j := covpos(s[i],coverage,tabs);
    if (j>0) then begin
      y := y + substitutes[j-1];  // substitues is a dynarr of Tglyst
    end else y := y + s[i];
  end;
  apply := y;
end;



/////////////// replace one char by an alternative is optinoall, left out here




/////////////// replace string by a char



procedure load_lig(var lig : Tligature; s : Tstream);
var n : longint;
begin
  lig.LigGlyph := read_glychar(s);
  n := read_word(s);
  lig.Component := read_glystr(n,s) ;
end;

procedure load_ligset(var ls : TligatureSet; s : Tstream);
var n,i,i0 : longint; os : array of longint;
begin
  i0 := s.position;
  n := read_word(s);
  setlength(os,n);
  setlength(ls,n);
  for i := 0 to n-1 do os[i] := read_word(s);
  for i := 0 to n-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    load_lig(ls[i],s);
  end;
end;

procedure load_ligset(n,i0 : longint; var lsl : TligatureSetList; s : Tstream);
var i : longint; os : array of longint;
begin
  setlength(os ,n);
  setlength(lsl,n);
  for i := 0 to n-1 do os[i] := read_word(s);
  for i := 0 to n-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    load_ligset(lsl[i],s);
  end;
end;

procedure TLigatureSubstFormat1.load(s : Tstream);
var i0,n,co : longint;
begin
  i0 := s.position;
  read_word(s); // version, already processed
  co := read_word(s);
  n := read_word(s);
  load_ligset(n,i0,LigatureSets,s);

  s.seek(i0+co,soFromBeginning);
  load_coverage(coverage,s);
  n := min(length(coverage),n);

  setlength(coverage,n);
  setlength(LigatureSets,n);
  writeln(classname, ' loaded ',n);
end;





function TLigatureSubstFormat1.apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr;
var y,cmp : Tglystr; rs : Tglystr; i,j,k,k2,len,q : longint;
begin
  y := ''; 
  len := length(s);
  i := 1;
  while (i<=len) do begin
    rs := s[i];
    j := covpos(s[i],coverage,tabs);
    if (j>0) then begin
      k2 := high(LigatureSets[j-1]);

      for k := 0 to k2 do with LigatureSets[j-1][k] do begin
        cmp := ignocopy(component,1,+1,length(component),tabs);
        if ( cmp=ignocopy(s,i+1,+1,length(cmp),tabs) ) then begin
          cmp := ignacopy(s,i+1,+1,length(cmp),tabs);
          rs := LigGlyph;
          for q := 1 to length(cmp) do begin
            if tabs.isIgnGly(cmp[q]) 
            then rs := rs + cmp[q]
            else rs := rs + tabs.deleted_lig_part_marker;
          end;
          inc(i, length(cmp) );
          break; // break out of k-loop
        end;
      end;

    end;
    y := y + rs;
    inc(i);
  end;
  apply := y;
end;


/////////////// TContextSubstFormat1
/////////////// replace a string by further rules applied to mid





procedure read_sr(var csr : TchainSubRule; s : Tstream);
var n,n2 : longint;
begin
  n := read_word(s);
  n2 := read_word(s);
  csr.btrk := '';
  csr.lahd := '';
  csr.inpt := read_glystr(n-1,s);
  load_substarr(n2,csr.sa,s);
end;

procedure load_srs(var csrs : TchainsubruleArr; s : Tstream);
var i,i0,n : longint; os : array of longint;
begin
  i0 := s.position;
  n := read_word(s);
  setlength(os,n);
  for i := 0 to n-1 do os[i] := read_word(s);
  setlength(csrs,n);
  for i := 0 to n-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    read_sr(csrs[i],s);
  end;
end;

procedure TContextSubstFormat1.load(s : Tstream);
var i,i0,n,co : longint; os : array of longint;
begin
  i0 := s.position;
  read_word(s); // version, already processed
  co := read_word(s);
  n := read_word(s);
  setlength(os,n);
  for i := 0 to n-1 do os[i] := read_word(s);
  s.seek(i0+co,soFromBeginning);
  load_coverage(coverage,s);
  n := min(n,length(coverage));
  setlength(coverage,n);
  setlength(csrs,n);
  for i := 0 to n-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    load_srs(csrs[i],s);
  end; 
end;



/////////////// TContextSubstFormat2
/////////////// replace a string by further rules applied to mid


procedure read_SubClassRule(var c : TChainSubClassRule; s : Tstream);
var n,n2 : longint;
begin
  c.bt := '';
  c.la := '';
  n := read_word(s); 
  n2 := read_word(s); 
  c.ip := read_glystr(n-1,s);
  load_substarr(n2,c.sa,s);
end;


procedure read_SubClassSet(var cscs : TChainSubClassSet; s : Tstream);
var n,i0,i : longint; os : array of longint;
begin
  i0 := s.position;
  n := read_word(s);
  setlength(cscs,n);
  setlength(os,n);
  for i := 0 to n-1 do os[i] := read_word(s);
  for i := 0 to n-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    read_SubClassRule(cscs[i],s);
  end;
end;

procedure TContextSubstFormat2.load(s : Tstream);
var ipo,co,cs,i,i0 : longint; os : array of longint;
begin
  i0 := s.position;
  read_word(s); // SubstFormat=2, already processed
  co  := read_word(s);
  ipo := read_word(s);
  cs  := read_word(s);
  setlength(os,cs);
  for i := 0 to cs-1 do os[i] := read_word(s);
  setlength(btcd,0);
  setlength(lacd,0);
  s.seek(i0+co ,soFromBeginning);  load_coverage(coverage,s);
  s.seek(i0+ipo,soFromBeginning);  load_glyphclassdef(ipcd,s);
  setlength(cscs,cs);
  for i := 0 to cs-1 do begin
    if not(os[i]=0) then begin
      s.seek(i0+os[i],soFromBeginning);
      read_SubClassSet(cscs[i],s);
    end else setlength(cscs[i],0)
  end;
  writeln(classname, ' loaded ');
end;





/////////////// TContextSubstFormat3
/////////////// replace a string by further rules applied to mid








procedure TContextSubstFormat3.load(s : Tstream);
var n,n2,i0 : longint;
begin
  setlength(cbt,0);
  setlength(cla,0);
  i0 := s.position;
  read_word(s); // SubstFormat
  n := read_word(s);
  n2 := read_word(s);
  load_covarr(n,i0,cin,s);
  load_substarr(n2,sa,s);
  writeln(classname, ' loaded ',high(cbt)+1,'[',high(cin)+1,']',high(cla)+1);
end;



/////////////// TChainContextSubstFormat1
/////////////// replace mid of a string by further rules applied to mid


procedure read_csr(var csr : TChainSubRule; s : Tstream);
var n : longint;
begin
  n := read_word(s);
  csr.btrk := read_glystr(n,s);
  n := read_word(s);
  csr.inpt := read_glystr(n-1,s);
  n := read_word(s);
  csr.lahd := read_glystr(n,s);
  n := read_word(s);
  load_substarr(n,csr.sa,s);
end;

procedure load_csrs(var csrs : TchainsubruleArr; s : Tstream);
var i,i0,n : longint; os : array of longint;
begin
  i0 := s.position;
  n := read_word(s);
  setlength(os,n);
  for i := 0 to n-1 do os[i] := read_word(s);
  setlength(csrs,n);
  for i := 0 to n-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    read_csr(csrs[i],s);
  end;
end;

procedure TChainContextSubstFormat1.load(s : Tstream);
var i,i0,n,co : longint; os : array of longint;
begin
  i0 := s.position;
  read_word(s); // version, already processed
  co := read_word(s);
  n := read_word(s);
  setlength(os,n);
  for i := 0 to n-1 do os[i] := read_word(s);
  s.seek(i0+co,soFromBeginning);
  load_coverage(coverage,s);
  n := min(n,length(coverage));
  setlength(coverage,n);
  setlength(csrs,n);
  for i := 0 to n-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    load_csrs(csrs[i],s);
  end; 
  writeln(classname, ' loaded ');
end;

function TChainContextSubstFormat1.apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr;
var i,j,k,k2,a : longint; y,x,z,dy,dy0,dy1 : Tglystr;
begin
  i := 1;
  y := '';
  while (i<=length(s)) do begin
    dy := s[i];
    j := covpos(s[i],coverage,tabs)-1;
    if (j>=0)and(j<=high(csrs)) then begin
      k2 := high(csrs[j]);
      for k := 0 to k2 do with csrs[j][k] do begin
        x := ignocopy(btrk,1  ,+1,length(btrk),tabs);
        z := ignocopy(s   ,i-1,-1,length(   x),tabs);
        if (x=z) then begin
          x := ignocopy(inpt,1  ,+1,length(inpt),tabs);
          z := ignocopy(s   ,i+1,+1,length(   x),tabs);
          if (x=z) then begin
            z := ignacopy(s   ,i+1,+1,length(   x),tabs);
            x := ignocopy(lahd,1  ,+1,length(lahd),tabs);
            z := ignocopy(s,i+1+length(z),+1,length(x),tabs);
            if (x=z) then begin
              x  := ignocopy(inpt,1  ,+1,length(inpt),tabs);
              dy := ignacopy(s   ,i  ,+1,length(x)+1,tabs);
              x := dy;
              for j := 0 to high(sa) do begin
                a := sa[j].SequenceIndex;
                dy0 := ignacopy(dy,1,+1,a,tabs);
                dy1 := ignacopy(dy,length(dy0)+1,+1,length(dy),tabs);
                dy1 := apply_lookup(dy1,sa[j].LookupListIndex,tabs);
                dy := dy0+dy1;
              end;
              inc(i,length(x)-1);
              break; // break out of k-loop
            end;
          end;
        end;
      end; 
    end;
    y := y + dy;
    inc(i);
  end;
  apply := y;
end;


/////////////// TChainContextSubstFormat2 using class-based rules
/////////////// replace mid of a string by further rules applied to mid



procedure read_ChainSubClassRule(var c : TChainSubClassRule; s : Tstream);
var n : longint;
begin
  n := read_word(s); c.bt := read_glystr(n,s);
  n := read_word(s); c.ip := read_glystr(n-1,s);
  n := read_word(s); c.la := read_glystr(n,s);
  n := read_word(s); load_substarr(n,c.sa,s);
end;


procedure read_ChainSubClassSet(var cscs : TChainSubClassSet; s : Tstream);
var n,i0,i : longint; os : array of longint;
begin
  i0 := s.position;
  n := read_word(s);
  setlength(cscs,n);
  setlength(os,n);
  for i := 0 to n-1 do os[i] := read_word(s);
  for i := 0 to n-1 do begin
    s.seek(i0+os[i],soFromBeginning);
    read_ChainSubClassRule(cscs[i],s);
  end;
end;

procedure TChainContextSubstFormat2.load(s : Tstream);
var bto,ipo,lao,co,cs,i,i0 : longint; os : array of longint;
begin
  i0 := s.position;
  read_word(s); // SubstFormat=2, already processed
  co  := read_word(s);
  bto := read_word(s);
  ipo := read_word(s);
  lao := read_word(s);
  cs  := read_word(s);
  setlength(os,cs);
  for i := 0 to cs-1 do os[i] := read_word(s);
  s.seek(i0+co ,soFromBeginning);  load_coverage(coverage,s);
  s.seek(i0+ipo,soFromBeginning);  load_glyphclassdef(ipcd,s);
  if (bto=0) then setlength(btcd,0) else begin
    s.seek(i0+bto,soFromBeginning);  load_glyphclassdef(btcd,s);
  end;
  if (lao=0) then setlength(lacd,0) else begin
    s.seek(i0+lao,soFromBeginning);  load_glyphclassdef(lacd,s);
  end;
  setlength(cscs,cs);
  for i := 0 to cs-1 do begin
    if not(os[i]=0) then begin
      s.seek(i0+os[i],soFromBeginning);
      read_ChainSubClassSet(cscs[i],s);
    end else setlength(cscs[i],0)
  end;

  writeln(classname, ' loaded ',cs);
end;


function TChainContextSubstFormat2.apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr;
var d,i,j,k,k2,a : longint; y,dy,dy0,dy1 : Tglystr;
begin
  y := '';
  apply := y;
  i := 1;
  while (i<=length(s)) do begin
    dy := copy(s,i,1);
    if covpos(dy[1],coverage,tabs)>0 then begin
      j := get_glyphclass(dy[1],ipcd);
      if (j>=0)and(j<=high(cscs)) then begin
        k2 := high(cscs[j]);
        for k := 0 to k2 do with cscs[j][k] do begin

          if  check_clsmatch(s,i-1,-1,bt,btcd,tabs)
          and check_clsmatch(s,i+1, 1,ip,ipcd,tabs)
          then begin
            d := length( ignacopy(s,i,+1,length(ip)+1,tabs) );
            if check_clsmatch(s,i+d, 1,la,lacd,tabs) 
            then begin
              dy := ignacopy(s,i,+1,length(ip)+1,tabs);
              for j := 0 to high(sa) do begin
                a := sa[j].SequenceIndex;
                dy0 := ignacopy(dy,1,+1,a,tabs);
                dy1 := ignacopy(dy,length(dy0)+1,+1,length(dy),tabs);
                dy1 := apply_lookup(dy1,sa[j].LookupListIndex,tabs);
                dy := dy0+dy1;
              end;
              inc(i,d-1);
              break; // out of k-loop
            end;
          end;

        end;
      end;
    end;
    y := y + dy;
    inc(i);
  end;
  apply := y;
end;





/////////////// TChainContextSubstFormat3
/////////////// replace mid of a string by further rules applied to mid



procedure TChainContextSubstFormat3.load(s : Tstream);
var n,i0 : longint;
begin
  i0 := s.position;
  read_word(s); // SubstFormat
  n := read_word(s);
  load_covarr(n,i0,cbt,s);
  n := read_word(s);
  load_covarr(n,i0,cin,s);
  n := read_word(s);
  load_covarr(n,i0,cla,s);
  n := read_word(s);
  load_substarr(n,sa,s);
  writeln(classname, ' loaded ',high(cbt)+1,'[',high(cin)+1,']',high(cla)+1);
end;



function TChainContextSubstFormat3.apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr;
var a,b,c,d,i,j : longint; y,dy,dy0,dy1 : Tglystr;
begin
  y := '';
  apply := y;
  a := high(cbt)+1;
  c := high(cin)+1;
  if (c<1) then exit;
  b := c+high(cla)+1;
  a += 1;
  b := 1+length(s)-b;
  y := copy(s,1,a-1);
  i := a;
  while (i<=b) do begin
    dy := copy(s,i,1);
    if covpos(dy[1],cin[0],tabs)>0 then begin
      if  check_covarrmatch(s,i-1,-1,high(cbt)+1,cbt,tabs)
      and check_covarrmatch(s,i  , 1,high(cin)+1,cin,tabs)
      then begin
        d := length( ignacopy(s,i,+1,c,tabs) );
        if check_covarrmatch(s,i+d, 1,high(cla)+1,cla,tabs) 
        then begin
          dy := ignacopy(s,i,+1,high(cin)+1,tabs);
          for j := 0 to high(sa) do begin
            a := sa[j].SequenceIndex;
            dy0 := ignacopy(dy,1,+1,a,tabs);
            dy1 := ignacopy(dy,length(dy0)+1,+1,length(dy),tabs);
            dy1 := apply_lookup(dy1,sa[j].LookupListIndex,tabs);
            dy := dy0+dy1;
          end;
          inc(i,d-1);
        end;
      end;
    end;
    y := y + dy;
    inc(i);
  end;
  y := y + copy(s,i,length(s));
  apply := y;
end;




/////////////// TReverseChainSingleSubstFormat1
/////////////// replace mid char of searched string by another char




procedure TReverseChainSingleSubstFormat1.load(s : Tstream);
var i0,n,co : longint;
begin
  i0 := s.position;
  read_word(s); // version, already processed
  co := read_word(s);
  n := read_word(s);
  load_covarr(n,i0,cbt,s);
  n := read_word(s);
  load_covarr(n,i0,cla,s);
  n := read_word(s);
  Substitute := read_glystr(n,s);
  s.seek(i0+co,soFromBeginning);
  load_coverage(coverage,s);
  n := min(length(coverage),length(Substitute));
  setlength(coverage,n);
  setlength(substitute,n);
  writeln(classname, ' loaded ',high(cbt)+1,'[1]',high(cla)+1);
end;

function TReverseChainSingleSubstFormat1.apply(s : Tglystr; const tabs : Tlookuptabs) : Tglystr;
var i,j : longint; y,dy : Tglystr; 
begin
  y := '';
  for i := 1 to length(s) do begin
    dy := s[i];
    j := covpos(dy[1],coverage,tabs);
    if (j>0) then begin
      if  check_covarrmatch(s,i-1,-1,high(cbt)+1,cbt,tabs)
      and check_covarrmatch(s,i+1, 1,high(cla)+1,cla,tabs)
      then dy := Substitute[j];
    end;
    y := y+dy;
  end;
  apply := y;
end;






//////////////////////////////////////////////////////////////////////








constructor TOTgsubfont.create();
begin
  inherited create();
  setlength(gsubScripts,0);
  setlength(gsubFeaturelist,0);
  //setlength(gsubLookUps,0);
  gsubLookUps := Tlookuptabs.create();
  gsubLookUps.font := self;
end;

destructor TOTgsubfont.destroy();
begin
  gsubLookUps.destroy();
  inherited destroy();
end;


function create_lookup(typ,fmt : longint) : TOTlookup;
begin
  create_lookup := nil;
  case typ of
    1 : begin
          case fmt of
            1 : create_lookup := TSingleSubstFormat1.create();
            2 : create_lookup := TSingleSubstFormat2.create();
          end;
        end;
    2 : begin
          case fmt of
            1 : create_lookup := TMultipleSubstFormat1.create();
          end;
        end;
    3 : begin
          case fmt of
            1 : create_lookup := TAlternateSubstFormat1.create();
          end;
        end;
    4 : begin
          case fmt of
            1 : create_lookup := TLigatureSubstFormat1.create();
          end;
        end;
    5 : begin
          case fmt of
            1 : create_lookup := TContextSubstFormat1.create();
            2 : create_lookup := TContextSubstFormat2.create();
            3 : create_lookup := TContextSubstFormat3.create();
          end;
        end;
    6 : begin
          case fmt of
            1 : create_lookup := TChainContextSubstFormat1.create();
            2 : create_lookup := TChainContextSubstFormat2.create();
            3 : create_lookup := TChainContextSubstFormat3.create();
          end;
        end;
    7 : begin
          case fmt of
            1 : create_lookup := TExtensionSubstFormat1.create();
          end;
        end;
    8 : begin
          case fmt of
            1 : create_lookup := TReverseChainSingleSubstFormat1.create();
          end;
        end;
    else begin
    end;
  end;
end;







procedure TOTgsubfont.load_FeatureVariations(s : Tstream);
begin
  writeln('  load gsub.ItemVarStore: currently unsupported. press enter'); readln;
end;





procedure TOTgsubfont.load_gsub();
var i,versmin,versmaj : longint; 
  s : TMemoryStream;
  ScriptList, FeatureList, LookupList, FeatureVariations : dword;
begin
writeln('load gsub');
  i := seek_tab('GSUB');
  if (i>0) then begin
writeln('found gsub:',i);
    s := TMemoryStream.create();
    if (i=s.copyFrom(src,i)) then begin
writeln('copied ',i,' bytes to s');
      s.seek(0,soFromBeginning);
      versmaj := read_word(s);
writeln('versmaj=',versmaj);
      if (versmaj=1) then begin // version major
        versmin := read_word(s);
writeln('versmin:',versmin);
        ScriptList  := read_word(s);
        FeatureList := read_word(s);
        LookupList  := read_word(s);
writeln('sl:',ScriptList);
writeln('fl:',FeatureList);
writeln('ll:',LookupList);
        if (versmin>0) 
        then FeatureVariations := read_dword(s)
        else FeatureVariations := 0;

        if (ScriptList<>0)and(ScriptList<i) then begin
          s.seek(ScriptList,soFromBeginning);
          load_scriptlist(gsubScripts,s);
        end;

        if (FeatureList<>0)and(FeatureList<i) then begin
          s.seek(FeatureList,soFromBeginning);
          load_FeatureList(gsubFeaturelist,s);
        end;

        if (LookupList<>0)and(LookupList<i) then begin
          s.seek(LookupList,soFromBeginning);
          load_lookups(gsubLookUps,@create_lookup,s);
        end;

        if (FeatureVariations<>0)and(FeatureVariations<i) then begin
          s.seek(FeatureVariations,soFromBeginning);
          load_FeatureVariations(s);
        end;

      end;
      s.destroy();
    end else s.destroy();
  end;
end;



function TOTgsubfont.applyLookup(s : Tglystr; n : longint) : Tglystr;
begin
  applyLookup := apply_lookup(s,n,gsubLookUps);
end;


function TOTgsubfont.applyFeature(s : Tglystr; n : longint) : Tglystr;
var j : longint;
begin
  if (n>=0)and(n<=high(gsubFeaturelist)) then with gsubFeaturelist[n] do begin
    writeln('Processing GSUB feature ',n,' : ',FeatureTag);
    for j := 0 to high(lu) do begin
      s := applyLookup(s,lu[j]);
    end;
  end;
  applyFeature := s;
end;

function TOTgsubfont.applyScript(s : Tglystr; n : longint) : Tglystr;
var j : longint;
begin
  if (n>=0)and(n<=high(gsubScripts)) then with gsubScripts[n] do begin
    writeln('Processing GSUB script ',n,' : ',ScriptTag);
    with DefaulTOTlangsys do begin
      for j := 0 to high(Feature) do begin
        s := applyFeature(s,Feature[j]);
      end;
    end;
  end;
  applyScript := s;
end;


function TOTgsubfont.findgsubScript(nam : string) : longint;
begin
  findgsubScript := findScript(nam,gsubScripts);
end;




begin
end.

