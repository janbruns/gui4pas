PROGRAM ttfdbgie;
{$mode objfpc}
{$COPERATORS ON}

uses classes, sysutils, cmdliner, 
     fontfile, ot_cmap, ttgridfit, ttdbggf;


var
fontname,svg_prefix : string;
fontsize,
emumode,
printstackelems,
glyphindex : longint;
gfvers : longint;
use_gfgrey,
printcode,
printmove,
printstate,
printstore,
skipfontprog,
skipprepprog,
makesvg,
glyphid_is_unicode : boolean;


procedure do_main_thing(s : Tstream);
var 
  font : TOTcmapfont;
  g : TTglyph;
  gridfitter : Tdebuggridfitter;
  i,v0,v1,v2 : longint;
begin
  writeln('Loading font...');
  s.seek(0,soFromBeginning);
  gridfitter := Tdebuggridfitter.create();
  gridfitter.render_greyscale := true;
  gridfitter.svg_prefix := svg_prefix;


  font := TOTcmapfont.create();
  font.load(s);

  i := printstackelems and 7;
  if (printcode) then i := i or 8;
  if (printmove) then i := i or 16;
  if (printstate) then i := i or 32;
  if (printstore) then i := i or 128;
  if (makesvg) then i := i or 64;
  if (skipfontprog) then v0 := 0 else v0 := i;
  if (skipprepprog) then v1 := 0 else v1 := i;
  v2 := i;

  writeln('tell gridfitter to report Rasterizer-version: ',gfvers);
  gridfitter.version_for_GETINFO := gfvers;

  writeln('tell gridfitter to use grey: ',use_gfgrey);
  gridfitter.render_greyscale := use_gfgrey;
  writeln('use emumode ',emumode);

  gridfitter.set_verbosity(v0);
  gridfitter.set_font(font,emumode ); // emumode

  writeln('using fontsize: ',fontsize);
  gridfitter.set_verbosity(v1);
  gridfitter.set_ppem(fontsize,fontsize,fontsize);

  gridfitter.set_verbosity(v2);

  if glyphid_is_unicode then begin
    i := glyphindex;
    writeln('Lookup unicode ',hexstr(i,8));
    glyphindex := font.UnicodeToGlyphIndex(i);
  end;
  writeln('get Glyph with index: ',glyphindex);
  g := font.getGlyph( glyphindex );
  if not(g=nil) then begin
    writeln('glyphidx:',g.idx);
//    g.print;
    writeln('countour points:',high(g.coordx)+1);
    gridfitter.gridfit_glyph(g);
  end;
  gridfitter.destroy();
  font.destroy();
end;




function createReadFileStream(const AFileName: string) : TFileStream;
begin
  try 
    createReadFileStream := TFileStream.create(AFileName,fmOpenRead);
  Except 
    on EFOpenError do createReadFileStream := nil; 
  end;
end;

procedure startup();
var s : TFileStream; m : TMemoryStream;
begin
  s := createReadFileStream(fontname);
  if not(s=nil) then begin
    m := TMemoryStream.create();
    m.copyFrom(s,0);
    FreeAndNil(s);
    try do_main_thing(m)
    finally 
      m.destroy();
    end;
  end else writeln('Problems opening File:',fontname);
end;


procedure print_hello();
begin
  writeln();
  writeln('TTFDBGIE - debug TrueType font instructions executed');
end;

procedure print_usage();
begin
  writeln('Prints lines like');
  writeln('@# 1-022E : 04C = 00000080 00000040 00000040 : ADD[ 60]');
  writeln('@ : instruction skipped because of an IF-ELSE section');
  writeln('# : instruction skipped because of function def');
  writeln('1 : current instruction is in font-prog (2:prep, 3:glyph)');
  writeln('022E  : current instruction is byte at hex 022E');
  writeln('04C   : the stack contains hex 04C elements');
  writeln('=     : the 3 topmost stack elems (right is topmost)');
  writeln('ADD   : the decoded instruction Name');
  writeln('[ 60] : the hex instruction byte(s)');
  writeln;
  writeln('Options:');
  writeln('--font-file=<filename> : the font to debug');
  writeln('-g<integer> : specify the glyph (default 0)');
  writeln('-s<integer> : specify the pointize (default 12)');
  writeln('--uni      : glyph id specified with -g is unicode');
  writeln('--nocode   : don''t print disassembly');
  writeln('--move     : print pointmoves (default off)');
  writeln('--state    : print state before Moves (default off)');
  writeln('--fontprog : also print the font program (default off)');
  writeln('--prepprog : also print the prep program (default off)');
  writeln('--gfgrey   : report the grey-flag on GETINFO');
  writeln('--make-svgs : make svg-images (requires --move)');
  writeln('--stack=<integer>   : print 0..7 steck elems (default 3)');
  writeln('--gfvers=<integer>  : report that version at GETINFO');
  writeln('--emumode=<integer> : emulation mode to use  (default 0)');
  writeln('--svg-prefix=<string> : prefix for svg-filenames to make');
  writeln;
  writeln('Note: int values can be hex like in -g$00d6, but just as with filenames, it depends on your shell what exactly to type in. For path-separators in filenames, you can probably use the unix-styled slash as well as your system default.');
  writeln;
  writeln('Example:');
  writeln('$ ttfdbgie --font-file=./times.ttf -g103 -s18');
end;

function enough_args() : boolean;
begin
  enough_args := (fontname<>'');
end;


function gotfontfile(s1,s2 : string) : boolean;
begin
  fontname := s2;
  gotfontfile := true;
end;

function got_g(s1,s2 : string) : boolean;
begin
  got_g := getnum(s2,glyphindex);
end;

function got_s(s1,s2 : string) : boolean;
begin
  got_s := getnum(s2,fontsize);
end;

function got_emumode(s1,s2 : string) : boolean;
begin
  got_emumode := getnum(s2,emumode);
end;

function got_stack(s1,s2 : string) : boolean;
begin
  got_stack := getnum(s2,printstackelems);
end;

function got_gfvers(s1,s2 : string) : boolean;
begin
  got_gfvers := getnum(s2,gfvers);
end;

function got_uni(s1,s2 : string) : boolean;
begin
  glyphid_is_unicode := true;
  got_uni := true;
end;

function got_code(s1,s2 : string) : boolean;
begin
  printcode := false;
  got_code := true;
end;

function got_move(s1,s2 : string) : boolean;
begin
  printmove := true;
  got_move := true;
end;

function got_state(s1,s2 : string) : boolean;
begin
  printstate := true;
  got_state := true;
end;

function got_store(s1,s2 : string) : boolean;
begin
  printstore := true;
  got_store := true;
end;


function got_fontprog(s1,s2 : string) : boolean;
begin
  skipfontprog := false;
  got_fontprog := true;
end;

function got_prepprog(s1,s2 : string) : boolean;
begin
  skipprepprog := false;
  got_prepprog := true;
end;

function got_gfgrey(s1,s2 : string) : boolean;
begin
  use_gfgrey := true;
  got_gfgrey := true;
end;

function got_svg(s1,s2 : string) : boolean;
begin
  makesvg := true;
  got_svg := true;
end;

function got_svgpref(s1,s2 : string) : boolean;
begin
  svg_prefix := s2;
  got_svgpref := true;
end;





var
printusage : boolean;


begin
  svg_prefix := 'ttfdebugie_svg_';
  fontname := '';
  fontsize := 12;
  emumode := 0;
  glyphindex := 0;
  printstackelems := 3;
  glyphid_is_unicode := false;
  use_gfgrey := false;
  printcode := true;
  printmove := false;
  printstate := false;
  printstore := false;
  skipfontprog := true;
  skipprepprog := true;
  makesvg := false;
  gfvers := 33;

  print_hello();
  addLongOption('font-file',1,@gotfontfile);
  addShortOption('g',1,@got_g);
  addShortOption('s',1,@got_s);
  addLongOption('uni',0,@got_uni);
  addLongOption('nocode',0,@got_code);
  addLongOption('move',0,@got_move);
  addLongOption('state',0,@got_state);
  addLongOption('store',0,@got_store);
  addLongOption('fontprog',0,@got_fontprog);
  addLongOption('prepprog',0,@got_prepprog);
  addLongOption('emumode',1,@got_emumode);
  addLongOption('stack',1,@got_stack);

  addLongOption('gfgrey',0,@got_gfgrey);
  addLongOption('gfvers',1,@got_gfvers);

  addLongOption('make-svgs',0,@got_svg);
  addLongOption('svg-prefix',1,@got_svgpref);




  if checkCommandLine(printusage) then begin
    if enough_args() then startup()
    else printusage := true;
  end;
  if printusage then print_usage();
end.


