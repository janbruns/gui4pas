{$mode objfpc}
{$unitpath .}
{ Not so much intention to literally follow unicode here,
  so unicode_ was left out of unit name.
  
  Some basic things appear to already work as expected,
  but I haven't tested each rule-implementation throughly.
  
  The unicode.org LineBreakTest.txt shows
  7654 tests done, ok:5083 fail:2571
  Besides it was made for specifics not implemented 
  here, I'd expect this result to indicate this code
  here is still full of bugs, but roughly gets what
  they're talking about.
  
  Update:
  7654 tests done, ok:7123 fail:531
}
unit linebreakalgo;

interface
uses unicode_bidiralgo, unicode_lbcls, unicode_utf8, unicode_gencat;

const
break_allowed = 1;
break_forced = 3;
break_disallowed = 4;



type
Tabstractlinebreaker = class
  public
  constructor create(); virtual;
  destructor destroy(); override;
  procedure setCharDat(p : Pdword; cnt : dword); virtual; abstract;
  procedure make_break(); virtual; abstract;
  
  public
  breakstatus : array of byte; { <-- result:
    linebreak allowed/forbidden AFTER charpos
  }
end;
Tabstractlinebreaker_cls = class of Tabstractlinebreaker;

Tlinebreaker = class(Tabstractlinebreaker)
  public
  constructor create(); override;
  destructor destroy(); override;
  procedure setCharDat(p : Pdword; cnt : dword); override;
  procedure make_break(); override;
  
  protected
  function lb30b(idx : dword) : boolean; virtual;
  function lb30a(idx : dword) : boolean; virtual;
  function lb30(idx : dword) : boolean; virtual;
  function lb29(idx : dword) : boolean; virtual;
  function lb28(idx : dword) : boolean; virtual;
  function lb27(idx : dword) : boolean; virtual;
  function lb26(idx : dword) : boolean; virtual;
  function lb25(idx : dword) : boolean; virtual;
  function lb24(idx : dword) : boolean; virtual;
  function lb23a(idx : dword) : boolean; virtual;
  function lb23(idx : dword) : boolean; virtual;
  function lb22(idx : dword) : boolean; virtual;
  function lb21b(idx : dword) : boolean; virtual;
  function lb21a(idx : dword) : boolean; virtual;
  function lb21(idx : dword) : boolean; virtual;
  function lb20(idx : dword) : boolean; virtual;
  function lb19(idx : dword) : boolean; virtual;
  function lb18(idx : dword) : boolean; virtual;
  function lb17(idx : dword) : boolean; virtual;
  function lb16(idx : dword) : boolean; virtual;
  function lb15(idx : dword) : boolean; virtual;
  function lb14(idx : dword) : boolean; virtual;
  function lb13(idx : dword) : boolean; virtual;
  function lb12a(idx : dword) : boolean; virtual;
  function lb12(idx : dword) : boolean; virtual;
  function lb11(idx : dword) : boolean; virtual;
  function lb10(idx : dword) : boolean; virtual;
  function lb9(var idx : dword) : boolean; virtual;
  function lb8(idx : dword) : boolean; virtual;
  function lb8a(idx : dword) : boolean; virtual;
  function lb7(idx : dword) : boolean; virtual;
  function lb6(idx : dword) : boolean; virtual;
  function lb5(idx : dword) : boolean; virtual;
  function lb4(idx : dword) : boolean; virtual;
  function lb3(idx : dword) : boolean; virtual;
  
  procedure decide_breakstatus(idx : dword); virtual;
  function cp_to_cls(d : dword) : byte; virtual;
  function char_cp(i : dword) : dword; virtual;

  private
  dat : Pdword;
  datlen : dword;

  private
  cur_lbc, nxt_lbc, prv_lbc : byte;
  nxt_idx : dword;
  lastrule : Pchar;
  
  public
  WJ_ignores_trailSP : boolean;
end;




implementation

function Tlinebreaker.lb30b(idx : dword) : boolean;
begin
  lastrule := 'lb30b';
  result := true;
  if ((cur_lbc=lbcls_EB))
  and((nxt_lbc=lbcls_EM))
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;
function Tlinebreaker.lb30a(idx : dword) : boolean;
begin
  lastrule := 'lb30a';
  result := true;
  {unimplemented}
end;

function Tlinebreaker.lb30(idx : dword) : boolean;
begin
  lastrule := 'lb30';
  result := true;
  result := true;
  if ((cur_lbc=lbcls_AL)or(cur_lbc=lbcls_HL)or(cur_lbc=lbcls_NU))
  and((nxt_lbc=lbcls_OP))
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;

function Tlinebreaker.lb29(idx : dword) : boolean;
begin
  lastrule := 'lb29';
  result := true;
  if ((cur_lbc=lbcls_IS))
  and((nxt_lbc=lbcls_AL)or(nxt_lbc=lbcls_HL))
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;


function Tlinebreaker.lb28(idx : dword) : boolean;
begin
  lastrule := 'lb28';
  result := true;
  if ((cur_lbc=lbcls_AL)or(cur_lbc=lbcls_HL))
  and((nxt_lbc=lbcls_AL)or(nxt_lbc=lbcls_HL))
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;

function Tlinebreaker.lb27(idx : dword) : boolean;
begin
  lastrule := 'lb27';
  result := true;
  if (cur_lbc=lbcls_PR)
  and((nxt_lbc=lbcls_JL)or(nxt_lbc=lbcls_JV)or(nxt_lbc=lbcls_JT)or(nxt_lbc=lbcls_H2)or(nxt_lbc=lbcls_H3))
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
  if (nxt_lbc=lbcls_PO)
  and((cur_lbc=lbcls_JL)or(cur_lbc=lbcls_JV)or(cur_lbc=lbcls_JT)or(cur_lbc=lbcls_H2)or(cur_lbc=lbcls_H3))
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;

function Tlinebreaker.lb26(idx : dword) : boolean;
begin
  lastrule := 'lb26';
  result := true;
  if (cur_lbc=lbcls_JL)
  and((nxt_lbc=lbcls_JL)or(nxt_lbc=lbcls_JV)or(nxt_lbc=lbcls_H2)or(nxt_lbc=lbcls_H3))
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
  if ((cur_lbc=lbcls_JV)or(cur_lbc=lbcls_H2))
  and((nxt_lbc=lbcls_JV)or(nxt_lbc=lbcls_JT))
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
  if ((cur_lbc=lbcls_JT)or(cur_lbc=lbcls_H3))
  and((nxt_lbc=lbcls_JT))
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;


function Tlinebreaker.lb25(idx : dword) : boolean;
begin
  lastrule := 'lb25';
  result := true;
  if ((cur_lbc=lbcls_CL)and(nxt_lbc=lbcls_PO))
  or ((cur_lbc=lbcls_CP)and(nxt_lbc=lbcls_PO))
  or ((cur_lbc=lbcls_CL)and(nxt_lbc=lbcls_PR))
  or ((cur_lbc=lbcls_CP)and(nxt_lbc=lbcls_PR))
  or ((cur_lbc=lbcls_NU)and(nxt_lbc=lbcls_PO))
  or ((cur_lbc=lbcls_NU)and(nxt_lbc=lbcls_PR))
  or ((cur_lbc=lbcls_PO)and(nxt_lbc=lbcls_OP))
  or ((cur_lbc=lbcls_PO)and(nxt_lbc=lbcls_NU))
  or ((cur_lbc=lbcls_PR)and(nxt_lbc=lbcls_OP))
  or ((cur_lbc=lbcls_PR)and(nxt_lbc=lbcls_NU))
  or ((cur_lbc=lbcls_HY)and(nxt_lbc=lbcls_NU))
  or ((cur_lbc=lbcls_IS)and(nxt_lbc=lbcls_NU))
  or ((cur_lbc=lbcls_NU)and(nxt_lbc=lbcls_NU))
  or ((cur_lbc=lbcls_SY)and(nxt_lbc=lbcls_NU))
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;

function Tlinebreaker.lb24(idx : dword) : boolean;
begin
  lastrule := 'lb24';
  result := true;
  if  ((cur_lbc=lbcls_AL)or(cur_lbc=lbcls_HL))
  and ((nxt_lbc=lbcls_PR)or(nxt_lbc=lbcls_PO))
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
  if  ((nxt_lbc=lbcls_AL)or(nxt_lbc=lbcls_HL))
  and ((cur_lbc=lbcls_PR)or(cur_lbc=lbcls_PO))
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;

function Tlinebreaker.lb23a(idx : dword) : boolean;
begin
  lastrule := 'lb23a';
  result := true;
  if ((nxt_lbc=lbcls_ID)or(nxt_lbc=lbcls_EB)or(nxt_lbc=lbcls_EM))
  and (cur_lbc=lbcls_PR)
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
  if ((cur_lbc=lbcls_ID)or(cur_lbc=lbcls_EB)or(cur_lbc=lbcls_EM))
  and (cur_lbc=lbcls_PO)
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;

function Tlinebreaker.lb23(idx : dword) : boolean;
begin
  lastrule := 'lb23';
  result := true;
  if ((cur_lbc=lbcls_AL)or(cur_lbc=lbcls_HL))
  and (nxt_lbc=lbcls_NU)
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
  if ((nxt_lbc=lbcls_AL)or(nxt_lbc=lbcls_HL))
  and (cur_lbc=lbcls_NU)
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;



function Tlinebreaker.lb22(idx : dword) : boolean;
begin
  lastrule := 'lb22';
  result := true;
  if (nxt_lbc=lbcls_IN)
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;


function Tlinebreaker.lb21b(idx : dword) : boolean;
begin
  lastrule := 'lb21b';
  result := true;
  if (cur_lbc=lbcls_SY)
  and (nxt_lbc=lbcls_HL)
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;

function Tlinebreaker.lb21a(idx : dword) : boolean;
begin
  lastrule := 'lb21a';
  result := true;
  if ((cur_lbc=lbcls_BA) or (cur_lbc=lbcls_HY))
  and (prv_lbc=lbcls_HL)
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;

function Tlinebreaker.lb21(idx : dword) : boolean;
begin
  lastrule := 'lb21';
  result := true;
  if (cur_lbc=lbcls_BB) 
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
  //writeln('rule21 nxt=',nxt_lbc);
  if (nxt_lbc=lbcls_BA) 
  or (nxt_lbc=lbcls_HY) 
  or (nxt_lbc=lbcls_NS) 
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;  
end;

function Tlinebreaker.lb20(idx : dword) : boolean;
begin
  lastrule := 'lb20';
  result := true;
  if (cur_lbc=lbcls_CB) 
  or (nxt_lbc=lbcls_CB)
  then begin
    breakstatus[idx] := break_allowed;
    result := false;
  end;
end;

function Tlinebreaker.lb19(idx : dword) : boolean;
begin
  lastrule := 'lb19';
  result := true;
  if (cur_lbc=lbcls_QU) 
  or (nxt_lbc=lbcls_QU)
  then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;

function Tlinebreaker.lb18(idx : dword) : boolean;
begin
  lastrule := 'lb18';
  result := true;
  if (cur_lbc=lbcls_SP) then begin
    breakstatus[idx] := break_allowed;
    result := false;
  end;
end;

function Tlinebreaker.lb17(idx : dword) : boolean;
var i,j : dword; lbc : byte;
begin
  lastrule := 'lb17';
  result := true;
  if (cur_lbc=lbcls_B2) then begin
    lbc := nxt_lbc;
    j := idx;
    i := nxt_idx;
    while (lbc=lbcls_SP)and(i<high(breakstatus)) do begin
      i := i+1;
      lbc := cp_to_cls(char_cp(i));
    end;
    if (lbc=lbcls_B2) then begin
      breakstatus[j] := break_disallowed;
      nxt_idx := i;
      result := false;
    end;
  end;
end;


function Tlinebreaker.lb16(idx : dword) : boolean;
var i,j : dword; lbc : byte;
begin
  lastrule := 'lb16';
  result := true;
  if (cur_lbc=lbcls_CL) 
  or (cur_lbc=lbcls_CP) 
  then begin
    lbc := nxt_lbc;
    j := idx;
    i := nxt_idx;
    while (lbc=lbcls_SP)and(i<high(breakstatus)) do begin
      i := i+1;
      lbc := cp_to_cls(char_cp(i));
    end;
    if (lbc=lbcls_NS) then begin
      breakstatus[j] := break_disallowed;
      nxt_idx := i;
      result := false;
    end;
  end;
end;


function Tlinebreaker.lb15(idx : dword) : boolean;
var i,j : dword; lbc : byte;
begin
  lastrule := 'lb15';
  result := true;
  if (cur_lbc=lbcls_QU) then begin
    lbc := nxt_lbc;
    j := idx;
    i := nxt_idx;
    while (lbc=lbcls_SP)and(i<high(breakstatus)) do begin
      i := i+1;
      lbc := cp_to_cls(char_cp(i));
    end;
    if (lbc=lbcls_OP) then begin
      breakstatus[j] := break_disallowed;
      nxt_idx := i;
      result := false;
    end;
  end;
end;


function Tlinebreaker.lb14(idx : dword) : boolean;
begin
  lastrule := 'lb14';
  result := true;
  if (cur_lbc=lbcls_OP) then begin
    while (nxt_lbc=lbcls_SP)and(nxt_idx<high(breakstatus)) do begin
      idx := nxt_idx;
      nxt_idx := idx+1;
      nxt_lbc := cp_to_cls(char_cp(nxt_idx));
    end;
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;

function Tlinebreaker.lb13(idx : dword) : boolean;
begin
  lastrule := 'lb13';
  result := true;
  case nxt_lbc of
    lbcls_CL,
    lbcls_CP,
    lbcls_EX,
    lbcls_IS,
    lbcls_SY : begin
      breakstatus[idx] := break_disallowed;
      result := false;
    end;
  end;
end;

function Tlinebreaker.lb12a(idx : dword) : boolean;
begin
  lastrule := 'lb12a';
  result := true;
  if nxt_lbc=lbcls_GL then begin
    if (cur_lbc<>lbcls_SP)
    and (cur_lbc<>lbcls_BA)
    and (cur_lbc<>lbcls_HY)
    then begin
      breakstatus[idx] := break_disallowed;
      result := false;
    end;
  end;
end;

function Tlinebreaker.lb12(idx : dword) : boolean;
begin
  lastrule := 'lb12';
  result := true;
  if cur_lbc=lbcls_GL then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;

function Tlinebreaker.lb11(idx : dword) : boolean;
begin
  lastrule := 'lb11';
  result := true;
  if (nxt_lbc=lbcls_WJ)or(cur_lbc=lbcls_WJ) then begin
    breakstatus[idx] := break_disallowed;
    result := false;
    if (cur_lbc=lbcls_WJ) and WJ_ignores_trailSP then begin
      while (nxt_lbc=lbcls_SP)and(nxt_idx<high(breakstatus)) do begin
        idx := nxt_idx;
        cur_lbc := nxt_lbc;
        nxt_idx := idx+1;
        nxt_lbc := cp_to_cls(char_cp(nxt_idx));
      end;
    end;
  end;
end;

function Tlinebreaker.lb10(idx : dword) : boolean;
begin
  lastrule := 'lb10';
  result := true;
  if cur_lbc=lbcls_ZWJ then begin
    cur_lbc:=lbcls_AL;
  end;
  if nxt_lbc=lbcls_ZWJ then begin
    nxt_lbc:=lbcls_AL;
  end;
  { this rule never "matches" }
end;

function is_cmzwj(t : byte) : boolean;
begin
  is_cmzwj := (t=lbcls_ZWJ) or (t=lbcls_CM);
end;

function isnt_bkcrlfnlspzw(t : byte) : boolean;
begin
  isnt_bkcrlfnlspzw := (t<>lbcls_BK)
    and(t<>lbcls_CR)
    and(t<>lbcls_LF)
    and(t<>lbcls_NL)
    and(t<>lbcls_SP)
    and(t<>lbcls_ZW);
end;


function Tlinebreaker.lb9(var idx : dword) : boolean;
var i,j : dword; i_lbc : byte;
begin
  lastrule := 'lb9';
  result := true;
  { while the next char is CM, advance idx to there, without changing cur_lbc}
  if is_cmzwj(nxt_lbc) and isnt_bkcrlfnlspzw(cur_lbc) then begin
    while is_cmzwj(nxt_lbc) and (nxt_idx<high(breakstatus)) do begin
      nxt_idx += 1;
      nxt_lbc := cp_to_cls(char_cp(nxt_idx));
    end;
    idx := nxt_idx-1;
  end;
  result := true;
end;

function Tlinebreaker.lb8a(idx : dword) : boolean;
begin
  lastrule := 'lb8a';
  result := true;
  if cur_lbc=lbcls_ZWJ then begin
    breakstatus[idx] := break_disallowed;
    result := false;
  end;
end;


function Tlinebreaker.lb8(idx : dword) : boolean;
begin
  lastrule := 'lb8';
  result := true;
  if cur_lbc=lbcls_ZW then begin
    { in case of following space, advance cur idx to the last spc }
    while (nxt_lbc=lbcls_SP)and(nxt_idx<high(breakstatus)) do begin
      idx := nxt_idx;
      nxt_idx := idx+1;
      nxt_lbc := cp_to_cls(char_cp(nxt_idx));
    end;
    breakstatus[idx] := break_allowed;
    result := false;
  end;
end;

function Tlinebreaker.lb7(idx : dword) : boolean;
begin
  lastrule := 'lb7';
  result := true;
  case nxt_lbc of
    lbcls_SP,
    lbcls_ZW : begin
      if cur_lbc<>lbcls_ZW then begin
        breakstatus[idx] := break_disallowed;
        result := false;
      end;
    end;
  end;
end;

function Tlinebreaker.lb6(idx : dword) : boolean;
begin
  lastrule := 'lb6';
  result := true;
  case nxt_lbc of
    lbcls_BK,
    lbcls_LF,
    lbcls_NL,
    lbcls_CR : begin
      breakstatus[idx] := break_disallowed;
      result := false;
    end;
  end;
end;

function Tlinebreaker.lb5(idx : dword) : boolean;
begin
  lastrule := 'lb5';
  result := true;
  if cur_lbc=lbcls_CR then begin
    if nxt_lbc=lbcls_LF 
    then breakstatus[idx] := break_disallowed
    else breakstatus[idx] := break_forced;
    result := false;
  end else if (cur_lbc=lbcls_LF) or (cur_lbc=lbcls_NL) then begin
    breakstatus[idx] := break_forced;
    result := false;
  end;  
end;

function Tlinebreaker.lb4(idx : dword) : boolean;
begin
  lastrule := 'lb4';
  result := true;
  if cur_lbc=lbcls_BK then begin
    breakstatus[idx] := break_forced;
    result := false;
  end;
end;

function Tlinebreaker.lb3(idx : dword) : boolean;
begin
  lastrule := 'lb3';
  result := true;
  if idx=high(breakstatus) then begin
    breakstatus[idx] := break_forced;
    result := false;
  end else begin
    nxt_lbc := cp_to_cls(char_cp(nxt_idx));
  end;
end;

procedure Tlinebreaker.decide_breakstatus(idx : dword);
begin  
  cur_lbc := cp_to_cls(char_cp(idx));
  if lb3(idx)
  and lb4(idx)
  and lb5(idx)
  and lb9(idx)
  and lb6(idx)
  and (not(WJ_ignores_trailSP) or lb11(idx))
  and lb7(idx)
  and lb8(idx)
  and lb8a(idx)
  and lb10(idx)
  and (WJ_ignores_trailSP or lb11(idx))
  and lb12(idx)
  and lb12a(idx)
  and lb13(idx)
  and lb14(idx)
  and lb15(idx)
  and lb16(idx)
  and lb17(idx)
  and lb18(idx)
  and lb19(idx)
  and lb20(idx)
  and lb21(idx)
  and lb21a(idx)
  and lb21b(idx)
  and lb22(idx)
  and lb23(idx)
  and lb23a(idx)
  and lb24(idx)
  and lb25(idx)
  and lb26(idx)
  and lb27(idx)
  and lb28(idx)
  and lb29(idx)
  and lb30(idx)
  and lb30a(idx)
  and lb30b(idx)
  then begin
lastrule := 'lb31';
    breakstatus[idx] := break_allowed;
  end;
end;


procedure Tlinebreaker.make_break();
var i : dword;
begin
  for i := 0 to high(breakstatus) do breakstatus[i] := 0;
  prv_lbc := 0;
  if high(breakstatus)>=0 then begin
    i := low(breakstatus);
    while i<=high(breakstatus) do begin
      nxt_idx := i+1;
      decide_breakstatus(i);
      //writeln(i:3,' ',hexstr(char_cp(i),6),' : ',binstr(breakstatus[i],3),' # ',UTF8_codepoint_as_UTF8(char_cp(i)),' @',lastrule );
      i := nxt_idx;
      prv_lbc := cur_lbc;
    end;
  end;
end;


function Tlinebreaker.cp_to_cls(d : dword) : byte;
var b,g : byte;
begin
  b := get_unicode_linebreak_class(d);
  case b of
    lbcls_SA : begin
      g := get_unicode_gencat(d);
      if (g=gencat_Mn)
      or (g=gencat_Mc) 
      then result := lbcls_CM
      else result := lbcls_AL;
    end;
    lbcls_AI,
    lbcls_SG,
    lbcls_XX : result := lbcls_AL;
    //lbcls_NS : result := lbcls_CJ;
    else result := b;
  end;
end;



function Tlinebreaker.char_cp(i : dword) : dword;
begin
  if (i<datlen) then char_cp := dat[i] else char_cp := 0;
end;

procedure Tlinebreaker.setCharDat(p : Pdword; cnt : dword);
begin
  dat := p;
  datlen := cnt;
  setlength(breakstatus,cnt);
end;


constructor Tlinebreaker.create();
begin
end;
destructor Tlinebreaker.destroy();
begin
end;
constructor Tabstractlinebreaker.create();
begin
end;
destructor Tabstractlinebreaker.destroy();
begin
end;



begin
end.
