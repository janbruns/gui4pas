{$unitpath .}
unit minifont;

interface
uses paswl_buffer;

const
fallbackcursorsize = 24;
fallbackcursor : array[0..fallbackcursorsize*fallbackcursorsize-1] of byte = (
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $AA, $55, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $44, $FF, $ED, $55, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $DC, $F8, $FC, $ED, 
$55, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $55, $FC, $F0, $F3, 
$FB, $ED, $66, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $DD, $F4, $F0, 
$F0, $F3, $FB, $ED, $55, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $55, $FC, $F0, 
$F0, $F0, $F0, $F3, $FB, $ED, 
$55, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $DD, $F4, 
$F0, $F0, $F0, $F0, $F0, $F3, 
$FB, $ED, $55, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $55, $FC, 
$F0, $F0, $F0, $F0, $F0, $F0, 
$F0, $F3, $FB, $ED, $55, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $ED, 
$F4, $F0, $F0, $F0, $F0, $F0, 
$F0, $F0, $F0, $F3, $FB, $ED, 
$55, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $55, 
$FC, $F0, $F0, $F0, $F0, $F0, 
$F0, $F0, $F0, $F0, $F0, $F3, 
$FC, $ED, $55, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$ED, $F4, $F0, $F0, $F0, $F0, 
$F0, $F0, $F0, $F0, $F0, $F0, 
$F0, $F4, $FC, $DD, $44, $00, 
$00, $00, $00, $00, $00, $00, 
$55, $FC, $F0, $F0, $F0, $F0, 
$F0, $F0, $F0, $F0, $F0, $F0, 
$F0, $F0, $F6, $EE, $22, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $ED, $F4, $F0, $F0, $F0, 
$F0, $F0, $F0, $F0, $F0, $F0, 
$F0, $F6, $EE, $22, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $55, $FB, $F0, $F0, $F0, 
$F0, $F0, $F0, $F0, $F0, $F0, 
$F6, $EE, $22, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $ED, $F3, $F0, $F0, 
$F0, $F0, $F0, $F0, $F0, $F6, 
$FE, $33, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $66, $FB, $F0, $F0, 
$F0, $F0, $F0, $F0, $F6, $FE, 
$FE, $FF, $DC, $88, $44, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $ED, $F3, $F0, 
$F0, $F0, $F0, $F5, $FE, $FD, 
$F1, $F0, $F4, $FE, $BA, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $66, $FB, $F0, 
$F0, $F0, $F5, $EE, $FF, $F1, 
$F0, $F0, $FB, $BA, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $ED, $F3, 
$F0, $F5, $EE, $22, $ED, $F3, 
$F0, $FB, $BA, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $66, $FB, 
$F5, $EE, $22, $00, $99, $F7, 
$FB, $BA, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $EE, 
$EE, $22, $00, $00, $44, $FF, 
$BA, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $55, 
$22, $00, $00, $00, $11, $AA, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00, 
$00, $00, $00, $00, $00, $00
); // $F0 : alpha, $0F : grey

const
tripatternA = #97;
tripatternB = #98;
tripatternC = #99;
unpatternA = #127;
unpatternB = #111;
chkrpattern1 = #96;
chkrpattern2 = #94;
arrpattern1 : array[0..3] of char = ( #100,#101,#102,#103 ); // up, down, right, left
arrpattern2 : array[0..3] of char = ( #92,#30,#104,#31 );
xpattern =#112;
ceckboxpattern =#113;
ceckboxcheckedpattern =#114;
radiopattern =#115;
radiocheckedpattern =#116;
cornerpattern : array[0..3] of char = (#124,#123,#122,#121);
pipeHpattern = #106;
pipeVpattern = #105;

function get_fontline(y,v : longint) : string;
function lookup_glyphindex(ch : widechar) : byte;
function get_glyphdata(ch : widechar) : string;
procedure paint_string_to_buffer(buf : T_paswl_buffer; ox,oy,sx,sy,rot : longint; s0 : ansistring; col : dword);
procedure paint_glyph_to_buffer(buf : T_paswl_buffer; ox,oy,sx,sy : longint; ch : byte; col : dword);


implementation

const
char128set_8x8_pixeldat : array[0..1023] of byte = (
60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,102,0,32,0,6,0,14,0,32,24,6,96,56,0,0,
0,110,28,96,60,6,60,24,58,96,0,0,96,24,102,92,60,110,38,124,96,62,102,62,
102,108,56,6,108,24,127,102,102,96,70,102,96,102,124,24,102,118,24,6,120,
24,127,102,102,98,70,102,96,102,96,24,62,102,24,6,108,24,107,102,102,60,
58,92,60,58,60,24,6,102,60,6,102,60,99,102,60,0,0,0,0,0,0,0,124,0,0,60,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,60,12,60,0,0,0,0,0,0,24,0,0,0,0,0,0,48,
18,12,24,16,92,58,92,60,124,38,66,67,102,70,124,48,48,12,60,48,118,102,
102,96,24,102,102,106,52,102,12,48,124,12,126,127,102,102,96,60,24,102,
102,42,24,102,24,48,48,12,24,127,124,62,96,6,24,102,60,62,60,62,48,48,98,
12,24,48,96,6,96,124,14,58,24,20,102,12,126,60,252,60,24,16,96,6,0,0,0,
0,0,0,0,120,0,0,0,0,24,0,0,24,102,102,24,98,56,6,12,48,0,0,0,0,0,0,0,24,
102,102,62,102,100,12,24,24,102,24,0,0,0,3,0,24,102,255,96,12,36,24,48,
12,60,24,0,0,0,6,0,24,0,102,60,24,24,0,48,12,255,126,0,126,0,12,0,0,0,255,
6,48,101,0,48,12,60,24,0,0,0,24,0,0,0,102,124,102,102,0,24,24,102,24,24,
0,24,48,0,24,0,102,24,70,63,0,12,48,0,0,24,0,24,96,0,0,0,0,0,0,0,0,0,0,
0,0,48,0,0,0,60,24,60,60,6,126,60,126,60,60,0,0,14,0,112,60,102,24,102,
102,14,96,102,102,102,102,0,0,24,0,24,102,110,56,6,6,30,124,96,12,102,102,
24,24,48,126,12,6,118,24,12,28,102,6,124,24,60,62,0,0,96,0,6,12,102,24,
48,6,127,6,102,24,102,6,0,0,48,126,12,24,102,24,96,102,6,102,102,24,102,
102,24,24,24,0,24,0,60,126,126,60,6,60,60,24,60,60,0,24,14,0,112,24,0,0,
0,0,0,0,0,0,0,0,0,48,0,0,0,0,0,24,124,60,120,126,126,60,102,60,30,102,96,
99,102,60,0,60,102,102,108,96,96,102,102,24,12,108,96,119,118,102,0,102,
102,96,102,96,96,96,102,24,12,120,96,127,126,102,255,126,124,96,102,120,
120,110,126,24,12,112,96,107,126,102,255,102,102,96,102,96,96,102,102,24,
12,120,96,99,110,102,0,102,102,102,108,96,96,102,102,24,108,108,96,99,102,
102,0,102,124,60,120,126,96,60,102,60,56,102,126,99,102,60,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,124,60,124,60,126,102,102,99,102,102,126,24,24,24,51,
0,102,102,102,102,24,102,102,99,102,102,6,24,24,24,51,0,102,102,102,96,
24,102,102,99,60,102,12,24,24,24,204,60,124,102,124,60,24,102,102,107,24,
60,24,255,24,24,204,60,96,102,120,6,24,102,102,127,60,24,48,255,126,24,
51,60,96,60,108,102,24,102,60,119,102,24,96,24,60,24,51,60,96,14,102,60,
24,60,24,99,102,24,126,24,24,24,204,0,0,0,0,0,0,0,0,0,0,0,0,24,0,24,204,
0,85,146,73,36,255,255,255,255,0,129,255,24,0,24,37,51,170,73,36,146,189,
231,159,249,8,129,0,24,0,24,66,153,85,36,146,73,153,231,199,227,12,129,
0,24,0,24,24,204,170,146,73,36,195,195,225,135,254,129,0,31,0,31,126,102,
85,73,36,146,195,195,225,135,254,129,0,31,15,31,102,51,170,36,146,73,231,
153,199,227,12,129,0,24,15,0,102,153,85,146,73,36,231,189,159,249,8,129,
0,24,15,0,102,204,170,73,36,146,255,255,255,255,0,129,255,24,15,0,60,102,
129,255,255,60,60,60,60,60,48,1,128,255,255,33,34,204,66,129,129,66,66,
66,66,66,120,1,128,1,128,66,68,153,36,129,153,129,165,165,165,165,104,1,
128,1,128,24,0,51,24,129,189,129,153,129,129,129,44,1,128,1,128,38,38,102,
24,129,189,129,153,165,153,189,36,1,128,1,128,34,102,204,36,129,153,129,
165,153,165,153,100,1,128,1,128,38,102,153,66,129,129,66,66,66,66,66,76,
1,128,1,128,26,58,51,129,255,255,60,60,60,60,60,64,255,255,1,128,0,0,102
);

var
tab : array[0..255] of byte;


function get_fontline(y,v : longint) : string;
var k : longint; s : string;
begin
  s := '';
  y := y*16 + (v div 16)*(16*8);
  v := char128set_8x8_pixeldat[y+(v and 15)];
  k := 128;
  while (k>0) do begin
    if (v and k)>0 then s += '+' else s += '-';
    k := k shr 1;
  end;
  get_fontline := s;
end;

function lookup_glyphindex(ch : widechar) : byte;
begin
  if (ch>#255) then lookup_glyphindex := 113
  else lookup_glyphindex := tab[ord(ch) and 255];
end;

function get_glyphdata(ch : widechar) : string;
var y : longint; s : string; c : byte;
begin
  s := '';
  c := lookup_glyphindex(ch);
  for y := 0 to 7 do begin
    s := s + get_fontline(y,c);
  end;
  get_glyphdata := s;
end;


procedure inittab();
var i : longint;
begin
  for i := 0 to 255 do tab[i] := 113;
  for i := 32 to 90 do tab[i] := i;
  tab[64] := 0;
  for i := 0 to 25 do tab[i+97] := i+1;
  tab[91] := 27;
  tab[93] := 29;
  tab[95] := 64;
  tab[124] := 93;
  tab[163] := 28;
  tab[223] := 120;
  tab[228] := 125;
  tab[252] := 126;
  tab[246] := 110;
end;


procedure paint_glyph_to_buffer(buf : T_paswl_buffer; ox,oy,sx,sy : longint; ch : byte; col : dword);
var s2 : shortstring; x,y,x2,y2,xp,yp : longint;
begin
  for y := 0 to 7 do begin
    s2 := get_fontline(y,ch);
    for x := 0 to 7 do begin
      if (s2[x+1]='+') then begin
        for y2 := 0 to sy-1 do begin
          for x2 := 0 to sx-1 do begin
            xp := x*sx +x2;
            yp := y*sy +y2;
            buf.setPixel( xp+ox,yp+oy,col);
          end;
        end;
      end;
    end;
  end;
end;

procedure paint_string_to_buffer(buf : T_paswl_buffer; ox,oy,sx,sy,rot : longint; s0 : ansistring; col : dword);
var s : unicodestring; s2 : shortstring; ch : byte; i,x,y,x2,y2,xp,yp : longint;
begin
  s := utf8decode(s0);
  for i := 1 to length(s) do begin
    ch := lookup_glyphindex(s[i]);
    for y := 0 to 7 do begin
      s2 := get_fontline(y,ch);
      for x := 0 to 7 do begin
        if (s2[x+1]='+') then begin
          for y2 := 0 to sy-1 do begin
            for x2 := 0 to sx-1 do begin
              xp := x*sx +x2;
              yp := y*sy +y2;
              if rot=0 then buf.setPixel( (i-1)*8*sx + xp+ox,yp+oy,col)
              else if rot=1 then buf.setPixel(8*sy-yp+ox, (i-1)*8*sx +xp+oy,col)
              else if rot=2 then buf.setPixel(yp+ox, -(i-1)*8*sx -xp+oy,col)
            end;
          end;
        end;
      end;
    end;
  end;
end;



initialization inittab();

finalization ;


end.




