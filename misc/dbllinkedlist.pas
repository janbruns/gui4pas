{$mode objfpc}
unit dbllinkedlist;

interface

type
Pdbllinkedlist = ^Tdbllinkedlist;
Tdbllinkedlist = record
  nxt,prv,owner : Pdbllinkedlist;
  dat : pointer;
end;

{ Instead of hardcoded new/dispose for inserts/removes,
  the owner/dummy node's dat field can hold the factory interface,
  so that the node type can easily be extended.
}
Pfactoryif = ^Tfactoryif;
Tfactoryif = record
  make : function() : Pdbllinkedlist;
  release : procedure(p : Pdbllinkedlist);
end;



procedure dblLinkedList_init(var owner : Tdbllinkedlist; d : Pfactoryif = nil);
function dblLinkedList_insertBefore(const owner : Tdbllinkedlist; ref : Pdbllinkedlist; d : pointer) : Pdbllinkedlist;
function dblLinkedList_insertAfter(const owner : Tdbllinkedlist; ref : Pdbllinkedlist; d : pointer) : Pdbllinkedlist;
procedure dblLinkedList_remove(ref : Pdbllinkedlist);
function dblLinkedList_getFirst(const owner : Tdbllinkedlist) : Pdbllinkedlist;
function dblLinkedList_getLast(const owner : Tdbllinkedlist) : Pdbllinkedlist;
function dblLinkedList_getNext(ref : Pdbllinkedlist) : Pdbllinkedlist;
function dblLinkedList_getPrev(ref : Pdbllinkedlist) : Pdbllinkedlist;








implementation


function default_make() : Pdbllinkedlist;
var p : Pdbllinkedlist;
begin
  new(p);
  default_make := p;
end;

procedure default_release(p : Pdbllinkedlist);
begin
  dispose(p);
end;

var
default_factoryif : Tfactoryif = ( make : @default_make; release : @default_release);

procedure dblLinkedList_init(var owner : Tdbllinkedlist; d : Pfactoryif = nil);
begin
  if (d=nil) 
  then owner.dat := @default_factoryif 
  else owner.dat := d;
  owner.owner := @owner;
  owner.nxt := @owner;
  owner.prv := @owner;
end;

function dblLinkedList_insertBefore(const owner : Tdbllinkedlist; ref : Pdbllinkedlist; d : pointer) : Pdbllinkedlist;
var n : Pdbllinkedlist;
begin
  if ref=nil then ref := @owner;
  n := Pfactoryif(owner.dat)^.make();
  n^.dat := d;
  n^.owner := @owner;
  n^.nxt := ref;
  n^.prv := ref^.prv;
  ref^.prv := n;
  n^.prv^.nxt := n;
  dblLinkedList_insertBefore := n;
end;



function dblLinkedList_insertAfter(const owner : Tdbllinkedlist; ref : Pdbllinkedlist; d : pointer) : Pdbllinkedlist;
var n : Pdbllinkedlist;
begin
  if ref=nil then ref := @owner;
  n := Pfactoryif(owner.dat)^.make();
  n^.dat := d;
  n^.owner := @owner;
  n^.prv := ref;
  n^.nxt := ref^.nxt;
  ref^.nxt := n;
  n^.nxt^.prv := n;
  dblLinkedList_insertAfter := n;
end;



procedure dblLinkedList_remove(ref : Pdbllinkedlist);
var p : Pfactoryif;
begin
  ref^.prv^.nxt := ref^.nxt;
  ref^.nxt^.prv := ref^.prv;
  p := Pfactoryif(ref^.owner^.dat);
  p^.release(ref);
end;


function dblLinkedList_getFirst(const owner : Tdbllinkedlist) : Pdbllinkedlist;
var n : Pdbllinkedlist;
begin
  n := owner.nxt;
  if (n=n^.owner) then n := nil;
  dblLinkedList_getFirst := n;
end;

function dblLinkedList_getLast(const owner : Tdbllinkedlist) : Pdbllinkedlist;
var n : Pdbllinkedlist;
begin
  n := owner.prv;
  if (n=n^.owner) then n := nil;
  dblLinkedList_getLast := n;
end;


function dblLinkedList_getNext(ref : Pdbllinkedlist) : Pdbllinkedlist;
var n : Pdbllinkedlist;
begin
  n := ref^.nxt;
  if (n=n^.owner) then n := nil;
  dblLinkedList_getNext := n;
end;

function dblLinkedList_getPrev(ref : Pdbllinkedlist) : Pdbllinkedlist;
var n : Pdbllinkedlist;
begin
  n := ref^.prv;
  if (n=n^.owner) then n := nil;
  dblLinkedList_getPrev := n;
end;



begin
end.

