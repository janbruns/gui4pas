{$mode objfpc}
{$unitpath ../wayland}
unit win_drv;


interface
uses windows, drvif,renderbox,dbllinkedlist;


type
Tsomewin = class;
Twindrv = class(Twinsysdriverinterface)
  procedure register_winclasses();
  procedure unregister_winclasses();

  destructor destroy(); override;

  function make_mainwin(dx,dy : longint) : Tsomewin;
  function make_childwin(x,y,dx,dy : longint; parent : Tsomewin) : Tsomewin;

  function deeperup() : boolean; override;
  function startup() : boolean; override;
  function make_node(parent : Tlayoutedwindow; img : TsomeBGdesciptor; r : Twrect) : Tlayoutedwindow; override;
  function enterloop() : longint; override;
  procedure shutdown(); override;
  function tellname() : ansistring; override;
  procedure cleanup(); override;


  public
  appwincls, chldwincls : PWNDCLASSA;
  appwinclsatom, chldwinclsatom : atom;
  defltcursor : Hcursor;
  mainwin : Tsomewin;
  alldone : boolean;
end;

Tsomewin = class(Tlayoutedwindow)
  function winproc(w : hwnd; msg : dword; wp : wparam; lp : lparam) : Lresult; stdcall;
  function paint_bg() : Lresult;
  procedure load(); override;
  procedure make_brush(dc : hdc);
  procedure make_bmp(dc : hdc);
  procedure make_bg(dc : hdc);
  
  procedure route_mousepos_down_to_root(x,y : longint; buttons : dword; is_bevent : boolean);

  constructor create(); override;
  destructor destroy(); override;

  public
  drv : Twindrv;
  win : hwnd;
  brush : Hbrush;
  bmp : Hbitmap;
  isbmp : boolean;
end;


implementation
uses sysutils;



constructor Tsomewin.create();
begin
  inherited;
end;

destructor Tsomewin.destroy();
begin
  inherited;
  if win<>0 then deleteObject(win);
  if bmp<>0 then deleteObject(bmp);
  if brush<>0 then deleteObject(brush);
end;

function winproc(w : hwnd; msg : dword; wp : wparam; lp : lparam) : Lresult; stdcall;
var s : Tsomewin;
begin
  s := Tsomewin(pointer(ptruint(GetWindowLongPtrA(w,0))));
  if assigned(s) and (s.win=w) then begin
    result := s.winproc(w,msg,wp,lp);
    exit;
  end else begin
    result := DefWindowProcA(w, msg,wp,lp);
  end;
end;

procedure Tsomewin.route_mousepos_down_to_root(x,y : longint; buttons : dword; is_bevent : boolean);
var w : Tsomewin; pcv,pcf : int64; q : double;
begin
  q := 0;
  if QueryPerformanceCounter(pcv) and QueryPerformanceFrequency(pcf) then begin
    if (pcf<>0) and (pcv<>0) then q := pcv/pcf;
  end;
  if is_bevent then begin
    x := 0;
    y := 0;
    buttons := buttons or $80000000;
  end else begin
    buttons := buttons or $40000000;
    w := self;
    while (w.parent<>nil) do begin
      x += w.r.x;
      y += w.r.y;
      w := w.parent as Tsomewin;
    end;
  end;
  drv.reverseif.onMouseEvent(x,y,buttons,q);
end;


function Tsomewin.winproc(w : hwnd; msg : dword; wp : wparam; lp : lparam) : Lresult; stdcall;
var q : ^Smallint;
begin
//  writeln('Tsomewinproc called: ',msg,'=',hexstr(msg,8));
  case msg of
    WM_MOUSEMOVE : begin
      q := pointer(@lp);
      //writeln('WM_MOUSEMOVE(',hexstr(w,16),'): ',q[0],'/',q[1],' ',hexstr(wp,8));
      route_mousepos_down_to_root(q[0],q[1],wp,false);
    end;
    WM_MBUTTONUP,WM_MBUTTONDOWN,
    WM_LBUTTONUP,WM_LBUTTONDOWN,
    WM_RBUTTONUP,WM_RBUTTONDOWN,
    WM_XBUTTONUP,WM_XBUTTONDOWN : begin
      q := pointer(@lp);
      route_mousepos_down_to_root(0,0,wp,true);
    end;
    WM_SIZE : begin 
      if parent=nil then begin
        drv.reverseif.do_layout(lp and $FFFF, (lp shr 16) and $FFFF);
        result := 0;
      end;
    end;
    WM_CLOSE : begin drv.alldone := true; result := DefWindowProcA(w, msg,wp,lp); end;
    WM_SHOWWINDOW : result := DefWindowProcA(w, msg,wp,lp);//ShowWindow(w,lp);
    WM_PAINT : begin result := paint_bg(); result := DefWindowProcA(w, msg,wp,lp); end;
    else winproc := DefWindowProcA(w, msg,wp,lp);
  end;
end;

procedure Tsomewin.load();
begin
  dirty := true;
end;

function Tsomewin.paint_bg() : Lresult;
var ps : LPPAINTSTRUCT; dc,dcsrc : hdc; wr : rect;
begin
  new(ps);
  dc := BeginPaint(win,ps);
  if dirty then make_bg(dc);
  if isbmp then begin
    dcsrc := CreateCompatibleDC(dc);
    selectObject(dcsrc,bmp);
    BitBlt( 
       dc,
       ps^.rcpaint.left,
       ps^.rcpaint.top,
       ps^.rcpaint.right-ps^.rcpaint.left,
       ps^.rcpaint.bottom-ps^.rcpaint.top,
       dcsrc,
       ps^.rcpaint.left,
       ps^.rcpaint.top,
       SRCCOPY
    );
    deleteObject(dcsrc);
  end else begin
    FillRect(dc, ps^.rcPaint, brush);
  end;
  EndPaint(win,ps);
  dispose(ps);
  paint_bg := 1;
  if dirty then begin
    dirty := false;
    wr.left := 0;
    wr.top := 0;
    wr.right := r.dx;
    wr.bottom := r.dy;
    MoveWindow(win,r.x,r.y,r.dx,r.dy,false);
    invalidateRect(win,wr,false);
  end;
end;

procedure Tsomewin.make_bg(dc : hdc);
begin
  if (BG is TimgBGdesciptor) then make_bmp(dc)
  else if (BG is TcolorBGdesciptor) then make_brush(dc)
  else writeln('BG OF UNKNOWN TYPE');
end;

procedure Tsomewin.make_brush(dc : hdc);
var imgd : TcolorBGdesciptor;
begin
  imgd := BG as TcolorBGdesciptor;
  if (brush<>0) then deleteObject(brush);
writeln('create brush for color: ',hexstr(imgd.col,8));
  brush := CreateSolidBrush(rgb(imgd.col and 255,(imgd.col shr 8) and 255,(imgd.col shr 16) and 255 ));
  isbmp := false;
end;

procedure Tsomewin.make_bmp(dc : hdc);
var imgd : TimgBGdesciptor; xr,yr,bpl,bpp : longint; p,psrc : pointer; bmi : BITMAPINFO; wr : rect;
begin
  imgd := BG as TimgBGdesciptor;
  xr := imgd.width;
  yr := imgd.height;
  r.dx := xr;
  r.dy := yr;
  psrc := imgd.map_rgba8888(bpp,bpl);
  if (bmp<>0) then deleteObject(bmp);
  bmp := 0;
  with bmi.bmiHeader do begin
    biSize := sizeof(bmi);
    biWidth := xr;
    biHeight := -yr;
    biPlanes := 1;
    biBitCount := 32;
    biCompression := BI_RGB;
    biSizeImage := xr*yr*4;
    biXPelsPerMeter := 0;
    biYPelsPerMeter := 0;
    biClrUsed := 0;
    biClrImportant := 0;
  end;
  bmp := CreateDIBSection(dc,bmi,DIB_RGB_COLORS,p,0,0);
  if (p<>nil)and(bmp<>0) then begin
    writeln('createDIB ok!');
    move(psrc^,p^,xr*yr*4);
  end else writeln('Porb with createDIBsect');
  imgd.unmap();
  isbmp := true;
end;


procedure Twindrv.register_winclasses();
begin

{ styles:
  CS_BYTEALIGNCLIENT,CS_BYTEALIGNWINDOW,CS_CLASSDC,CS_DBLCLKS,CS_DROPSHADOW,CS_GLOBALCLASS,
  CS_HREDRAW,CS_NOCLOSE,CS_OWNDC,CS_PARENTDC,CS_SAVEBITS,CS_VREDRAW 
}
  if not assigned(appwincls) then begin
    defltcursor := LoadCursor(0,IDC_ARROW);

    new(appwincls);
    appwincls^.style := CS_HREDRAW or CS_VREDRAW;
    appwincls^.lpfnWndProc := @winproc;
    appwincls^.cbClsExtra := 0;
    appwincls^.cbWndExtra := sizeof(pointer);
    appwincls^.hInstance := hinstance;
    appwincls^.hIcon := 0;
    appwincls^.hCursor := defltcursor ;
    appwincls^.hbrBackground := 0;
    appwincls^.lpszMenuName := nil;
    appwincls^.lpszClassName := 'appwincls';

    new(chldwincls);
    chldwincls^.style := 0;//CS_PARENTDC;
    chldwincls^.lpfnWndProc := @winproc;
    chldwincls^.cbClsExtra := 0;
    chldwincls^.cbWndExtra := sizeof(pointer);
    chldwincls^.hInstance := hinstance;
    chldwincls^.hIcon := 0;
    chldwincls^.hCursor := defltcursor ;
    chldwincls^.hbrBackground := 0;
    chldwincls^.lpszMenuName := nil;
    chldwincls^.lpszClassName := 'chldwincls';

    appwinclsatom := RegisterClassA(appwincls);
    chldwinclsatom := RegisterClassA(chldwincls);
  end;
end;

procedure Twindrv.unregister_winclasses();
begin
  if assigned(appwincls) then begin
    UnregisterClassA(chldwincls^.lpszClassName,hinstance);
    UnregisterClassA(appwincls^.lpszClassName,hinstance);
    dispose(chldwincls);
    dispose(appwincls);
    appwincls := nil;
    chldwincls := nil;
    deleteobject(defltcursor);
  end;
end;



function Twindrv.enterloop() : longint;
var m : PMSG; bret : longint;
begin
  showWindow(mainwin.win,SW_normal);
  new(m);
  repeat
    bret := longint( GetMessage(m,0,0,0) );
    if (bret=-1) then begin
    end else if (bret<>0) then begin
      TranslateMessage(m); 
      DispatchMessage(m); 
    end;
  until alldone or (bret=0);
  dispose(m);
writeln('mainloop ends');
  enterloop := 0;
end;


function Twindrv.deeperup() : boolean;
begin
  register_winclasses();
  deeperup := true;
end;

function Twindrv.startup() : boolean;
begin
  startup := true;
end;

function Twindrv.make_node(parent : Tlayoutedwindow; img : TsomeBGdesciptor; r : Twrect) : Tlayoutedwindow;
var w : Tsomewin;
begin
  if (img is TimgBGdesciptor) then begin
    r.dx := (img as TimgBGdesciptor).width;
    r.dy := (img as TimgBGdesciptor).height;
  end;
  if parent=nil 
  then w := make_mainwin(r.dx,r.dy)
  else w := make_childwin(r.x,r.y,r.dx,r.dy, parent as Tsomewin);
  w.BG := img;
  w.r := r;
  w.dirty := true;
  //if parent<>nil then parent.insertChild(w);
  result := w;
end;

procedure Twindrv.shutdown();
begin
  freeandnil(mainwin);
  unregister_winclasses();
end;

function Twindrv.tellname() : ansistring;
begin
  tellname := 'mswin';
end;

procedure Twindrv.cleanup();
begin
  shutdown();
end;


{
WS_EX_ACCEPTFILES, WS_EX_APPWINDOW, WS_EX_CLIENTEDGE, WS_EX_COMPOSITED, WS_EX_CONTEXTHELP
WS_EX_CONTROLPARENT, WS_EX_DLGMODALFRAME, WS_EX_LAYERED, WS_EX_LAYOUTRTL, WS_EX_LEFT, WS_EX_LEFTSCROLLBAR
WS_EX_LTRREADING, WS_EX_MDICHILD, WS_EX_NOACTIVATE, WS_EX_NOINHERITLAYOUT, WS_EX_NOPARENTNOTIFY
WS_EX_NOREDIRECTIONBITMAP, WS_EX_OVERLAPPEDWINDOW, WS_EX_PALETTEWINDOW, WS_EX_RIGHT, WS_EX_RIGHTSCROLLBAR
WS_EX_RTLREADING, WS_EX_STATICEDGE, WS_EX_TOOLWINDOW, WS_EX_TOPMOST, WS_EX_TRANSPARENT, WS_EX_WINDOWEDGE

WS_BORDER, WS_CAPTION, WS_CHILD, WS_CHILDWINDOW, WS_CLIPCHILDREN, WS_CLIPSIBLINGS
WS_DISABLED, WS_DLGFRAME, WS_GROUP, WS_HSCROLL, WS_ICONIC, WS_MAXIMIZE, WS_MAXIMIZEBOX
WS_MINIMIZE, WS_MINIMIZEBOX, WS_OVERLAPPED, WS_OVERLAPPEDWINDOW, WS_POPUP, WS_POPUPWINDOW
WS_SIZEBOX, WS_SYSMENU, WS_TABSTOP, WS_THICKFRAME, WS_TILED, WS_TILEDWINDOW, WS_VISIBLE, WS_VSCROLL
}

function Twindrv.make_mainwin(dx,dy : longint) : Tsomewin;
begin
writeln('make mainwin');
  mainwin := Tsomewin.create();
  mainwin.dirty := true;
  mainwin.drv := self;
  mainwin.win := CreateWindowExA(
    WS_EX_APPWINDOW,// dwExStyle
    appwincls^.lpszClassName,
    'testappname',
    WS_OVERLAPPEDWINDOW, 
    100,100, // x,y,
    dx,dy,
    0, // parent
    0, // menu
    hinstance,
    nil
  );
  SetWindowLongPtrA(mainwin.win,0,ptruint(mainwin));
  ShowWindow(mainwin.win,SW_normal);
  make_mainwin := mainwin;
end;

function Twindrv.make_childwin(x,y,dx,dy : longint; parent : Tsomewin) : Tsomewin;
var w : Tsomewin; sib : Tsomewin; p : Pdbllinkedlist;
begin
  w := Tsomewin.create();
  w.dirty := true;
  w.drv := self;
  w.win := CreateWindowExA(
    0,// dwExStyle
    chldwincls^.lpszClassName,
    nil,
    WS_CHILD, 
    x,y,
    dx,dy,
    parent.win, // parent
    0, // menu
    hinstance,
    nil
  );
  if parent<>nil then parent.insertChild(w);
  SetWindowLongPtrA(w.win,0,ptruint(w));
  ShowWindow(w.win,SW_normal);

  p := dblLinkedList_getPrev(w.siblings);
  { explicitly tell the new child is above siblings,
    -as is- doesn't seem to change a thing, also doesn't
    help with wrong mouse event targets I'm not so much
    interested in here (opposite stacking gives inverts
    not only optical order, but also the wrong event
    targets).
  }
  if p<>nil then begin
    sib := Tsomewin(p^.dat);
    SetWindowPos(w.win,sib.win,x,y,dx,dy,SWP_NOMOVE or SWP_NOSIZE);
  end;

  make_childwin := w;
end;


destructor Twindrv.destroy();
begin
  inherited;
  cleanup();
end;


var
d : Twindrv;


initialization
begin 
  d := Twindrv.create();
  register_driver(d);
end;


finalization
begin
  freeandnil(d);
end;


end.

