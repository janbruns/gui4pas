{$mode objfpc}
unit x11_clp_snd;

{ x11 clipboard sending code


  https://www.x.org/releases/X11R7.6/doc/xorg-docs/specs/ICCCM/icccm.html


  Note: This code calls XSelectInput on requestor windows.

  App-internal requests must either be filtered out or must come 
  from a separate display connection to avoid loss of events.

  Given that separate display connections would infer another nominal
  namespace for atoms, using another display connection often might
  not be very attrative. It'll typically be much easier to limit 
  clipbord read requests to a single window.  
  

}

interface
uses xlib,x,baseunix,x11_error;

const
LIMIT_READ_CHUNKSIZE  = 16384; // in dw -> 64 KByte
JOB_DISCARD_TIME = 120 / (3600*24); // 2 minutes expressed in days 


type
Tmimearr = array of Pchar;
Tatomarr = array of Tatom;

Tx11sndjob = class
  public
  { to be filled out by convert-cb }
  dat : ansistring;   
  final_type : Tatom;

  private
  { internally used by Tx11clp_snd }
  ev : TXSelectionRequestEvent;
  next : Tx11sndjob;
  sent : culong;
  age : double;
end;

{ To set clipboard contents, Tx11clp_snd.set_clp_data()
  is called with information about 
     selection : Tatom;   // the clipboard to use (example: Atom "CLIPBOARD" or "XdndSelection")
     cb : Tx11dataconvcb; // convert-callback function defined by user
     obj : pointer;       // obj-pointer to be passed to convert-callback 
     fmtlst : Tatomarr;   // a list of possible conversion targets, cb supports
     win : Twindow;       // the x window to use
     tim : TTime;         // the x server timestamp to use

  and reset by a call to Tx11clp_snd.forget_clp_data(selection : Tatom).
}
Tx11dataconvcb = function(dat : pointer; targ : Tatom; s : Tx11sndjob) : boolean of object;




{ internally used }
Tselowner = class
  selection : Tatom;
  win : Twindow;
  time : Ttime;
  converter : Tx11dataconvcb;
  obj : pointer;
  targets, internal_targets : Tatomarr;
end;





Tx11clp_snd = class
  public
  {user-calls}
  function set_clp_data(selection : Tatom; cb : Tx11dataconvcb; obj : pointer; fmtlst : Tatomarr; win : Twindow; tim : TTime) : boolean;
  procedure forget_clp_data(selection : Tatom);
  { call forget_clp_data() for all selection slots that
    have been claimed for converter cb, 
    just to make extra-sure cb can be destroyed safely.
  }
  procedure forget_clp_converter(cb : Tx11dataconvcb);

  {user calls for app-internal trransfers}
  function owns_clipboard(selection : Tatom) : boolean;
  function appinternal_get_clipboard(selection,target : Tatom; var dat : ansistring; typ : Tatom) : boolean;

  procedure addFilteredWindow(w : Twindow);
  procedure removeFilteredWindow(w : Twindow);
  function hasFilteredWindow(w : Twindow) : boolean;


  procedure setDisplay(d : Pdisplay); virtual;
  procedure register_clipboard(a : Tatom); virtual;
  constructor create();
  destructor destroy(); override;


  { to be called from msg-loop}
  procedure onSelectionClear(const ev : TXSelectionClearEvent); virtual;
  procedure onSelectionRequest(const ev : TXSelectionRequestEvent); virtual;
  procedure onPropertyNotify(const ev : TXPropertyEvent); virtual;
  procedure onDestroyNotify(const ev : TXDestroyWindowEvent); virtual;

  private
  function find_selection_slot(s : Tatom; var slot : Tselowner) : boolean;
  procedure onSelectionRequest_timestamp(const ev : TXSelectionRequestEvent; const slot : Tselowner);
  procedure onSelectionRequest_targets(const ev : TXSelectionRequestEvent; const slot : Tselowner);
  procedure onSelectionRequest_multiple(const ev0 : TXSelectionRequestEvent; const slot : Tselowner);
  function onSelectionRequest_convert(const ev : TXSelectionRequestEvent; send_notify : boolean; const slot : Tselowner) : boolean;
  procedure start_send(s : Tx11sndjob; send_notify : boolean);
  function continue_send(var s : Tx11sndjob) : boolean;
  procedure discontinue_send(var s : Tx11sndjob);
  procedure check_jobchain(w : Twindow; p : Tatom);
  function jobchain_find_entry(w : Twindow; p : Tatom) : Tx11sndjob;
  procedure discard_old_jobchain_entries();
  function jobmatch(s : Tx11sndjob; w : Twindow; p : Tatom) : boolean;
  procedure send_sel_notify(const ev : TXSelectionRequestEvent; prp : Tatom);


  public
  atom_INCR, atom_targets, atom_timestamp, atom_multiple, atom_integer, atom_atom : Tatom;

  protected

  display : Pdisplay;
  selslot : array of Tselowner;

  max_read, 
  max_write_bytes, // typical packet size for incr-transfers 
  send_incr_threshold : clong;
  propwatches : Tx11sndjob;

  public
  filterwindows : array of Twindow; // list of windows for which to supress XSelectInput calls
end;





implementation
uses math,sysutils;

{ return Tselowner class for specified selection
}
function Tx11clp_snd.find_selection_slot(s : Tatom; var slot : Tselowner) : boolean;
var i : longint;
begin
  slot := nil;
  find_selection_slot := false;
  for i := 0 to high(selslot) do begin
    if selslot[i].selection=s then begin
      slot := selslot[i];
      find_selection_slot := true;
      exit;
    end;
  end;
end;

function Tx11clp_snd.set_clp_data(selection : Tatom; cb : Tx11dataconvcb; obj : pointer; fmtlst : Tatomarr; win : Twindow; tim : TTime) : boolean;
var slot : Tselowner;
begin
  set_clp_data := false;
  if find_selection_slot(selection,slot) then begin
    { tell X we wanna have that selection-slot }
    XSetSelectionOwner(display,selection,win,tim);
    { if X rejected, keep track about we don't own the selection }
    if XGetSelectionOwner(display,selection)<>win then begin
      slot.converter := nil;
      slot.obj := nil;
      exit;
    end;
    set_clp_data := true;
    slot.win := win;
    slot.time := tim;
    slot.converter := cb;
    slot.obj := obj;
    slot.targets := copy(fmtlst,0,length(fmtlst));
    insert(slot.internal_targets, slot.targets, length(slot.targets) );
  end else;
end;

{ drop ownership of specified selection
}
procedure Tx11clp_snd.forget_clp_data(selection : Tatom);
var slot : Tselowner;
begin
  if find_selection_slot(selection,slot) then begin
    { if we believe to currently own the selection, tell X we wanna give it up}
    if slot.converter<>nil then XSetSelectionOwner(display,selection,None,slot.time);
    { make sure cached requests won't reference the data anymore }
    slot.win := None;
    slot.converter := nil;
    slot.obj := nil;
    setlength(slot.targets,0);
  end else;
end;

{ call forget_clp_data() for all selection slots that
  have been claimed for converter, to make extra-sure it can be destroyed safely.
}
procedure Tx11clp_snd.forget_clp_converter(cb : Tx11dataconvcb);
var i : longint;
begin
  for i := 0 to high(selslot) do begin
    if selslot[i].converter=cb then begin
      forget_clp_data(selslot[i].selection);
    end;
  end;
end;

procedure Tx11clp_snd.onSelectionClear(const ev : TXSelectionClearEvent);
var slot : Tselowner;
begin
  if find_selection_slot(ev.selection,slot) then begin
    slot.win := None;
    slot.converter := nil;
    slot.obj := nil;
    setlength(slot.targets,0);
  end else ;
end;

procedure Tx11clp_snd.onSelectionRequest(const ev : TXSelectionRequestEvent);
var slot : Tselowner; 
begin
  if find_selection_slot(ev.selection,slot) then begin
    if slot.converter<>nil then begin
      if ev.target=atom_targets then onSelectionRequest_targets(ev,slot)
      else if ev.target=atom_timestamp then onSelectionRequest_timestamp(ev,slot)
      else if ev.target=atom_multiple then onSelectionRequest_multiple(ev,slot)
      else onSelectionRequest_convert(ev,true,slot);
    end else begin
      { Maybe we have claimed selection ownership without having a valid 
        converter. Notify the reuestor about we won't be able to deliver anything.
      }
      send_sel_notify(ev,None);
    end;
  end else begin
    { if we even don't have a selection slot for the request, assume we
      also haven't been able to claim selection-ownership: 
      ignore the reques event.
    }
  end;
end;

procedure Tx11clp_snd.onSelectionRequest_timestamp(const ev : TXSelectionRequestEvent; const slot : Tselowner);
var dp : Tatom;
begin
  dp := ev._property;
  if dp=None then dp := ev.target;
  XChangeProperty(display, ev.requestor, dp, atom_integer, 32, PropModeReplace, @slot.time, 1);
  send_sel_notify(ev,dp);
end;

procedure Tx11clp_snd.onSelectionRequest_targets(const ev : TXSelectionRequestEvent; const slot : Tselowner);
var dp : Tatom;
begin
  dp := ev._property;
  if dp=None then dp := ev.target;
  XChangeProperty(display, ev.requestor, dp, atom_atom, 32, PropModeReplace, @slot.targets[0], length(slot.targets));
  send_sel_notify(ev,dp);
end;

{ this method wasn't yet tested/debugged!}
procedure Tx11clp_snd.onSelectionRequest_multiple(const ev0 : TXSelectionRequestEvent; const slot : Tselowner);
var 
  ev : TXSelectionRequestEvent; 
  prop_return : ^Tatom; 
  bytes_after_return, nitems_return : culong;
  m_read_type : Tatom;
  m_read_format : cint;
  i : longint;
begin
  if (ev0._property<>None) then begin
    ev := ev0;
    XGetWindowProperty( display,ev.requestor,ev0._property,
                        0, 
                        max_read, 
                        false, 
                        AnyPropertyType, 
                        @m_read_type, 
                        @m_read_format, 
                        @nitems_return, 
                        @bytes_after_return, 
                        @prop_return 
                      );
    if (m_read_format<>32)or((nitems_return and 1)=1) then begin
      { cannot interpret property as list of atom-pairs }
      send_sel_notify(ev0, None);
      exit;
    end;
    i := 0;
    while (i+1<nitems_return) do begin
      ev.target := prop_return[i];
      ev._property := prop_return[i+1];
      if onSelectionRequest_convert(ev,false,slot) then begin
        { convert succeeded, so the data is either already 
          complete on the requestor window's property, or
          at least an INCR-transfer is initialized.
          Nothing to do here.
        }
      end else begin
        { mark failed conversions, so the the requestor
          knows it shouldn't expect this data item
        }
        prop_return[i+1] := None;
      end;
      inc(i,2);
    end;
    XChangeProperty(display, ev0.requestor, ev0._property, m_read_type, m_read_format, PropModeReplace, pointer(prop_return), nitems_return);
    send_sel_notify(ev0, ev0._property);
    xfree(prop_return);
  end else send_sel_notify(ev0, None);
end;

function Tx11clp_snd.onSelectionRequest_convert(const ev : TXSelectionRequestEvent; send_notify : boolean; const slot : Tselowner) : boolean;
var s : Tx11sndjob; prp : Tatom;
begin
  onSelectionRequest_convert := false;
  if (slot.converter<>nil) then begin
    prp := ev._property;
    if prp=None then prp := ev.target;
    s := jobchain_find_entry(ev.requestor, prp);
    if s=nil then s := Tx11sndjob.create();
    s.ev := ev;
    s.ev._property := prp;
    if slot.converter(slot.obj,ev.target,s) then begin
      start_send(s,send_notify);
      onSelectionRequest_convert := true;
    end else begin
      if send_notify then begin
        s.ev._property := None;
        send_sel_notify(s.ev, None); // send fail notify
      end;
      s.destroy();
    end;
  end else ;
end;

procedure Tx11clp_snd.start_send(s : Tx11sndjob; send_notify : boolean);
var len : clong; addlist : boolean; w : Twindow;
begin
  begin_no_xerr(display);
  len := length(s.dat);
  if len<send_incr_threshold then begin
    XChangeProperty(display, s.ev.requestor, s.ev._property, s.final_type, 8, PropModeReplace, @s.dat[1], length(s.dat));
    addlist := false;
  end else begin
    w := s.ev.requestor;
    if not hasFilteredWindow(w) then XSelectInput(display,w, PropertyChangeMask  );
    XChangeProperty(display, w, s.ev._property, atom_INCR, 32, PropModeReplace, @len, 1);
    addlist := true;
  end;
  if end_no_xerr(display)<>nil then begin
    addlist := false;
    send_notify := false;
  end;

  if send_notify then begin
    send_sel_notify(s.ev, s.ev._property); // send success notify
  end;
  if addlist then begin
    s.next := propwatches.next;
    propwatches.next := s;
    s.age := now();
  end else begin
    s.destroy();
  end;
end;

function Tx11clp_snd.continue_send(var s : Tx11sndjob) : boolean;
var len : clong; w : Twindow;
begin
  continue_send := true;
  w := s.ev.requestor;
  len := min( length(s.dat)-s.sent, max_write_bytes );
  begin_no_xerr(display);
  XChangeProperty(display, w, s.ev._property, s.final_type, 8, PropModeAppend, @s.dat[1+s.sent], len);
  if end_no_xerr(display)<>nil then begin
    discontinue_send(s);
    continue_send := false;
    exit;
  end;
  s.sent += len;
  if len=0 then begin
    if not hasFilteredWindow(w) then XSelectInput(display,w, 0  );
    discontinue_send(s);
    continue_send := false;
  end else s.age := now();
end;

procedure Tx11clp_snd.discontinue_send(var s : Tx11sndjob);
var t : Tx11sndjob;
begin
  t := s;
  s := s.next;
  t.destroy();
end;

function oldjob(ct,t : double) : boolean; inline;
begin
  oldjob := (ct-t) > JOB_DISCARD_TIME;
end;

{ called on foreign window property deletes to indicate
  request-continue (as defined by X Consortium).
}
procedure Tx11clp_snd.check_jobchain(w : Twindow; p : Tatom);
var t : Tx11sndjob; 
begin
  t := propwatches;
  if t.next=nil then exit;
  while (t.next<>nil) do begin
    if jobmatch(t.next,w,p) then begin
      if continue_send(t.next) then t := t.next;
      exit;
    end else t := t.next;
  end;
  { if no match was found, but entries exist }
  discard_old_jobchain_entries();
end;


{ called on initial foreign requests to identify transfers.
}
function Tx11clp_snd.jobchain_find_entry(w : Twindow; p : Tatom) : Tx11sndjob;
var t : Tx11sndjob; 
begin
  jobchain_find_entry := nil;
  t := propwatches;
  { if there are transfers in use }
  while (t.next<>nil) do begin
    if jobmatch(t.next,w,p) then begin
      { a matching entry was found and is assumed to soon get reused }
      jobchain_find_entry := t.next;
      exit;
    end else t := t.next;
  end;
end;

{ discard irresponsive INCR-transfers after some timeout
}
procedure Tx11clp_snd.discard_old_jobchain_entries();
var t : Tx11sndjob; ct : double;
begin
  t := propwatches;
  ct := now();
  while (t.next<>nil) do begin
    if oldjob(ct,t.next.age) then begin
      discontinue_send(t.next);
    end else t := t.next;
  end;
end;


procedure Tx11clp_snd.onDestroyNotify(const ev : TXDestroyWindowEvent);
var t : Tx11sndjob;
begin
writeln('onDestroyNotify');
  t := propwatches;
  while (t.next<>nil) do begin
    if (ev.event=t.next.ev.requestor) then begin
      discontinue_send(t.next);
    end else t := t.next;
  end;
end;




function Tx11clp_snd.jobmatch(s : Tx11sndjob; w : Twindow; p : Tatom) : boolean;
begin
  jobmatch := (s.ev.requestor=w)and(s.ev._property=p); 
end;

procedure Tx11clp_snd.send_sel_notify(const ev : TXSelectionRequestEvent; prp : Tatom);
var ev2 : TXevent;
begin
  with ev2.xselection do begin
    _type := SelectionNotify;
    display := display;
    requestor := ev.requestor;
    selection := ev.selection;
    target := ev.target;
    _property := prp;
    time := ev.time;
  end;
  XSendEvent(display, ev.requestor, false, None, @ev2);
end;

procedure Tx11clp_snd.onPropertyNotify(const ev : TXPropertyEvent);
begin
  if ev.state=PropertyDelete then check_jobchain(ev.window, ev.atom);
end;


procedure Tx11clp_snd.setDisplay(d : Pdisplay);
var mrs : dword;
begin
  display := d;
  atom_INCR := XInternAtom(d,'INCR',false);
  atom_targets := XInternAtom(d,'TARGETS',false);
  atom_timestamp := XInternAtom(d,'TIMESTAMP',false);
  atom_multiple := XInternAtom(d,'MULTIPLE',false);
  atom_integer := XInternAtom(d,'INTEGER',false);
  atom_atom := XInternAtom(d,'ATOM',false);
  mrs := XMaxRequestSize(d);
  max_read := (mrs -32) div 2; 
  if max_read >LIMIT_READ_CHUNKSIZE  then max_read := LIMIT_READ_CHUNKSIZE;
  max_write_bytes := max_read;
  send_incr_threshold := 255;
end;

procedure Tx11clp_snd.register_clipboard(a : Tatom);
var s : Tselowner;
begin
  s := Tselowner.create();
  s.selection := a;
  setlength(selslot,length(selslot)+1);
  selslot[high(selslot)] := s;
  setlength(s.targets, 0);
  setlength(s.internal_targets,3);
  s.internal_targets[0] := atom_targets;
  s.internal_targets[1] := atom_multiple;
  s.internal_targets[2] := atom_timestamp;
end;

constructor Tx11clp_snd.create();
begin 
  inherited create();
  propwatches := Tx11sndjob.create();
  propwatches.next := nil;
  setlength(selslot,0);
end;

destructor Tx11clp_snd.destroy();
var t : Tx11sndjob; i : longint;
begin
  while propwatches.next <>nil do begin
    t := propwatches.next;
    propwatches.next := t.next;
    t.destroy();
  end;
  propwatches.destroy();
  for i := 0 to high(selslot) do selslot[i].destroy();
  inherited destroy();
end;





function Tx11clp_snd.owns_clipboard(selection : Tatom) : boolean;
var slot : Tselowner; 
begin
  if find_selection_slot(selection,slot) then begin
    owns_clipboard := (slot.converter <> nil);
  end else owns_clipboard := false;
end;



function Tx11clp_snd.appinternal_get_clipboard(selection,target : Tatom; var dat : ansistring; typ : Tatom) : boolean;
var slot : Tselowner; s : Tx11sndjob; 
begin
  if find_selection_slot(selection,slot) then begin
    if (slot.converter<>nil) then begin
      if (target=atom_targets) then begin
        appinternal_get_clipboard := true;
        setlength(dat,length(slot.targets)*sizeof(Tatom));
        move(slot.targets,dat[1],length(slot.targets)*sizeof(Tatom));
      end else if target=atom_timestamp then begin
        setlength(dat,sizeof(TTime));
        move(slot.time,dat[1],sizeof(TTime));
        appinternal_get_clipboard := true;
      end else begin
        s := Tx11sndjob.create();
        appinternal_get_clipboard := slot.converter(slot.obj,target,s);
        dat := s.dat;
        typ := s.final_type;
        s.destroy();
      end;
    end else appinternal_get_clipboard := false;
  end else appinternal_get_clipboard := false;
end;

function Tx11clp_snd.hasFilteredWindow(w : Twindow) : boolean;
var i : longint; 
begin
  hasFilteredWindow := false;
  for i := 0 to high(filterwindows) do if w=filterwindows[i] then begin
    hasFilteredWindow := true;
    exit;
  end;
end;

procedure Tx11clp_snd.addFilteredWindow(w : Twindow);
var i : longint; found : boolean;
begin
  found := false;
  for i := 0 to high(filterwindows) do if w=filterwindows[i] then found := true;
  if not found then begin
    setlength(filterwindows,length(filterwindows)+1);
    filterwindows[high(filterwindows)] := w;
  end;
end;

procedure Tx11clp_snd.removeFilteredWindow(w : Twindow);
var i,j : longint; 
begin
  j := 0;
  for i := 0 to high(filterwindows) do begin
    if filterwindows[i]<>w then begin
      filterwindows[j] := filterwindows[i];
      inc(j);
    end;
  end;
  setlength(filterwindows,j);
end;


begin
end.

