unit ot_gdef;
{$mode objfpc}
{$COPERATORS ON}

{ 

}


interface
uses classes,sysutils,fontfile,ot_cmap,ot_comn;

type
Tpointlist = array of smallint;
Tligcaret = packed record
  CaretValueFormat : smallint;
  coordorpoint : word;
  extra : dword;
end;
Tligcarets = array of Tligcaret;
Tglyphligcarets = record
  cl : Tligcarets;
  glyph : word;
end;


TOTgdeffont = class(TOTcomnfont)
  constructor create();

  function gdef_isBaseGlyph(g : Tglychar) : boolean; override;
  function gdef_isLigGlyph(g : Tglychar) : boolean; override;
  function gdef_isMarkGlyph(g : Tglychar) : boolean;  override;
  function gdef_isinMarkFilteringSet(g : Tglychar; mfs : longint) : boolean;  override;
  function gdef_isofMarkAttachmentType(g : Tglychar; mat : longint) : boolean;  override;


  procedure load_gdef();


  private
  procedure load_al(s : Tstream);
  procedure load_lcl(s : Tstream);
  procedure load_mgsd(s : Tstream);

  protected 
  procedure load_ivs(s : Tstream); virtual;

  protected
  lig : array of Tglyphligcarets;
  markglyphsets : array of Tcoverage;

  private
  glyphclass, markattachclass : Tglycls;
  atp_pl : array of Tpointlist;
  atp_cover : Tcoverage;

end;




implementation
uses math;






constructor TOTgdeffont.create();
begin
  inherited create();
  setlength(glyphclass,0);
  setlength(markattachclass,0);
  setlength(markglyphsets,0);
  setlength(lig,0);
  setlength(atp_cover,0);
  setlength(atp_pl,0);
end;





function TOTgdeffont.gdef_isBaseGlyph(g : Tglychar) : boolean;
begin
  gdef_isBaseGlyph := (1=get_glyphclass(g,glyphclass) );
end;

function TOTgdeffont.gdef_isLigGlyph(g : Tglychar) : boolean;
begin
  gdef_isLigGlyph := (2=get_glyphclass(g,glyphclass) );
end;

function TOTgdeffont.gdef_isMarkGlyph(g : Tglychar) : boolean;
begin
  gdef_isMarkGlyph := (3=get_glyphclass(g,glyphclass) );
end;

function TOTgdeffont.gdef_isinMarkFilteringSet(g : Tglychar; mfs : longint) : boolean;
begin
  if (mfs>=0)and(mfs<=high(markglyphsets)) then begin
    gdef_isinMarkFilteringSet := (cpos(g,markglyphsets[mfs])>0);
  end else  gdef_isinMarkFilteringSet := false;
end;

function TOTgdeffont.gdef_isofMarkAttachmentType(g : Tglychar; mat : longint) : boolean;
begin
  gdef_isofMarkAttachmentType := (mat=get_glyphclass(g,markattachclass) );
end;







procedure read_caret(var c : Tligcaret; s : Tstream);
begin
  with c do begin
    CaretValueFormat := smallint(read_word(s));
    extra := 0;
    case CaretValueFormat of
      1,2 : coordorpoint := read_word(s);
      3 : begin
            coordorpoint := read_word(s);
            extra := read_word(s);
          end;
    end;
  end;
end;

procedure read_caret_list(var cl : Tligcarets; s : Tstream);
var n,i : longint;
begin
  n := smallint(read_word(s));
  if (n>=0) then begin
    setlength(cl,n);
    for i := 0 to n-1 do begin 
      read_caret(cl[i],s);
    end;
  end else setlength(cl,0);
end;



procedure TOTgdeffont.load_lcl(s : Tstream);
var cov : Tcoverage; i,i0,oc,gc : longint; lgo : array of word;
begin
  writeln('  load LigCaretList');
  i0 := s.position;
  oc := read_word(s);
  gc := read_word(s);
writeln('  LigCaretList... num glyphs:',gc);
  setlength(lgo,gc);
  for i := 0 to gc-1 do begin
    lgo[i] := read_word(s);
  end;
  s.seek(i0+oc,soFromBeginning);
  load_coverage(cov,s);
  gc := min(gc,length(cov));
  setlength(lig,gc);
  for i := 0 to gc-1 do begin
    lig[i].glyph := word(cov[i+1]);
    s.seek(lgo[i]+i0,soFromBeginning);
    read_caret_list(lig[i].cl,s);
    writeln('  glyph ',lig[i].glyph,' has ',high(lig[i].cl)+1,' caret positions' );
  end;
  
end;




procedure TOTgdeffont.load_al(s : Tstream);
var i0,oc,i,j,gc,pc : longint; os : array of word;
begin
  i0 := s.position;
  oc := read_word(s);
  gc := read_word(s);
  if (gc>0) then begin
    setlength(os,gc);
    for i := 0 to gc-1 do begin
      os[i] := read_word(s);
    end;
    s.seek(i0+oc,soFromBeginning);
    load_coverage(atp_cover,s);
    gc := min(length(atp_cover), gc);
    setlength(atp_cover,gc);
    setlength(atp_pl,gc);
    for i := 0 to gc-1 do begin
      s.seek(i0+os[i],soFromBeginning);
      pc := read_word(s);
      setlength(atp_pl[i],pc);
      for j := 0 to pc-1 do atp_pl[i][j] := smallint(read_word(s));
    end;
  end;
end;



procedure TOTgdeffont.load_mgsd(s : Tstream);
var i,n,i0 : longint; os : array of longint;
begin
  i0 := s.position;
  if (read_word(s)=1) then begin
    setlength(markglyphsets,0);
    n := smallint(read_word(s));
    if (n>0) then begin
      setlength(os,n);
      setlength(markglyphsets,n);
      for i := 0 to n-1 do os[i] := read_dword(s);
      for i := 0 to n-1 do begin
        s.seek(i0+os[i],soFromBeginning);
        load_coverage(markglyphsets[i],s);
      end;
    end;
  end;
end;





procedure TOTgdeffont.load_ivs(s : Tstream);
begin
  writeln('  load gdef.ItemVarStore: currently unsupported. press enter'); readln;
end;









procedure TOTgdeffont.load_gdef();
var i,versmin,versmaj : longint; 
  GlyphClassDef,
  AttachList,
  LigCaretList,
  MarkAttachClassDef,
  MarkGlyphSetsDef,
  ItemVarStore : dword;
  s : TMemoryStream;
begin
writeln('load gdef');
  i := seek_tab('GDEF');
  if (i>0) then begin
writeln('found gdef:',i);
    s := TMemoryStream.create();
    if (i=s.copyFrom(src,i)) then begin
writeln('copied ',i,' bytes to s');
      s.seek(0,soFromBeginning);
      versmaj := read_word(s);
writeln('versmaj=',versmaj);
      if (versmaj=1) then begin // version major
        versmin := read_word(s);
writeln('versmin:',versmin);
        GlyphClassDef      := read_word(s);
        AttachList         := read_word(s);
        LigCaretList       := read_word(s);
        MarkAttachClassDef := read_word(s);
        MarkGlyphSetsDef   := 0;
        ItemVarStore       := 0;
        if (versmin>1)and(versmin<=3) then begin
          MarkGlyphSetsDef := read_word(s);
        end;
        if (versmin>2)and(versmin<=3) then begin
          ItemVarStore     := read_dword(s);
        end;

        if (GlyphClassDef<>0)and(GlyphClassDef<i) then begin
          s.seek(GlyphClassDef,soFromBeginning);
          load_glyphclassdef(glyphclass,s);
        end;

        if (AttachList<>0)and(AttachList<i) then begin
//          s.seek(AttachList,soFromBeginning);
//          load_al(s);
        end;

        if (LigCaretList<>0)and(LigCaretList<i) then begin
//          s.seek(LigCaretList,soFromBeginning);
//          load_lcl(s);
        end;

        if (MarkAttachClassDef<>0)and(MarkAttachClassDef<i) then begin
          s.seek(MarkAttachClassDef,soFromBeginning);
          load_glyphclassdef(markattachclass,s);
        end;

        if (MarkGlyphSetsDef<>0)and(MarkGlyphSetsDef<i) then begin
          s.seek(MarkGlyphSetsDef,soFromBeginning);
          load_mgsd(s);
        end;

        if (ItemVarStore<>0)and(ItemVarStore<i) then begin
//          s.seek(ItemVarStore,soFromBeginning);
//          load_ivs(s);
        end;

        s.destroy();
      end;
    end else s.destroy();
  end;
end;


















begin
end.

