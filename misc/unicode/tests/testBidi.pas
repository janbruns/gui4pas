{$mode objfpc}
{$unitpath ..}
uses 
  unicode_bidiralgo,
  unicode_utf8;


function todot(var s : ansistring; const dot : shortstring) : ansistring;
var i : longint;
begin
  i := pos(dot,s);
  if (i>0) then begin
    result := copy(s,1,i-1);
    s := copy(s,i+length(dot));
  end else begin
    result := s;
    s := '';
  end;
end;


var
lvlstr,ordstr,datstr : ansistring;
levelarr : array[0..1000] of longint;
orderarr : array[0..1000] of longint;
levelcnt : longint;
ordercnt : longint;
errcnt,goodcnt : longint;


procedure dotest(tst : ansistring; m : longint);
var a : Tunicode_bidir_algo; i,j : longint; err : boolean;
begin
  a := Tunicode_bidir_algo.create();
  a.setting_embed := m;
  a.load_from_UTF8(tst);
  a.preprocess();
  a.do_L1(0,high(a.chars));
  err := false;
  for i := 0 to high(a.chars) do begin
    if (levelarr[i]=-1) and (a.chart[i]<>0) then err := true;
    if (levelarr[i]>-1) and (a.charl[i]<>levelarr[i]) then err := true;
  end;
  if not err then begin
    a.do_L2(0,high(a.chars));
    j := 0;
    for i := 0 to high(a.chars) do begin
      if a.charexists(i) then begin
        if a.charidx[i]<>orderarr[j] then err := true;
        inc(j);
      end;
    end;
  end;
  if err then begin
    write('err(m=',m,'),got:');
    for i := 0 to high(a.chars) do begin
      if a.chart[i]=0 then write('x ') else write(a.charl[i],' ');
    end;
    writeln;
    writeln('expected:    ',lvlstr);
    writeln('dat: ',datstr);
    for i := 0 to high(a.chars) do begin
      write(a.chart[i],' ');
    end;
    writeln('paragraph_embedding_level=',a.paragraph_embedding_level);
    writeln;
    readln;
  end;
  if err then inc(errcnt) else inc(goodcnt);
  a.destroy();
end;




procedure do_levelsline(s : ansistring);
var w : word; s2 : ansistring; i : longint;
begin
  delete(s,1,length('@Levels:'));
  while copy(s,1,1)=' ' do delete(s,1,1);
  while copy(s,1,1)=#9 do delete(s,1,1);
  while copy(s,1,1)=' ' do delete(s,1,1);

  lvlstr := s;
  
  levelcnt := 0;
  while (s<>'') do begin
    s2 := todot(s,' ');
    if s2='x' 
    then levelarr[levelcnt] := -1
    else val(s2,levelarr[levelcnt],w);
    inc(levelcnt);
  end;

end;

procedure do_reorderline(s : ansistring);
var w : word; s2 : ansistring; i : longint;
begin
  ordstr := s;
  delete(s,1,length('@Reorder:'));
  while copy(s,1,1)=' ' do delete(s,1,1);
  while copy(s,1,1)=#9 do delete(s,1,1);
  while copy(s,1,1)=' ' do delete(s,1,1);
  
  ordercnt := 0;
  while (s<>'') do begin
    s2 := todot(s,' ');
    if s2='x' 
    then orderarr[ordercnt] := -1
    else val(s2,orderarr[ordercnt],w);
    inc(ordercnt);
  end;
end;

function makethechar(s3 : ansistring) : ansistring;
begin
  if s3='L' then result := UTF8_codepoint_as_UTF8($0061)
  else if s3='R' then result := UTF8_codepoint_as_UTF8($05EF)
  else if s3='EN' then result := UTF8_codepoint_as_UTF8($0030)
  else if s3='ES' then result := UTF8_codepoint_as_UTF8($002B)
  else if s3='ET' then result := UTF8_codepoint_as_UTF8($0023)
  else if s3='AN' then result := UTF8_codepoint_as_UTF8($0662)
  else if s3='CS' then result := UTF8_codepoint_as_UTF8($002C)
  else if s3='B' then result := UTF8_codepoint_as_UTF8($000A)
  else if s3='S' then result := UTF8_codepoint_as_UTF8($0009)
  else if s3='WS' then result := UTF8_codepoint_as_UTF8($0020)
  else if s3='ON' then result := UTF8_codepoint_as_UTF8($0021)
  else if s3='BN' then result := UTF8_codepoint_as_UTF8($0008)
  else if s3='NSM' then result := UTF8_codepoint_as_UTF8($0300)
  else if s3='AL' then result := UTF8_codepoint_as_UTF8($0620)
  else if s3='LRO' then result := UTF8_codepoint_as_UTF8($202D)
  else if s3='RLO' then result := UTF8_codepoint_as_UTF8($202E)
  else if s3='LRE' then result := UTF8_codepoint_as_UTF8($202A)
  else if s3='RLE' then result := UTF8_codepoint_as_UTF8($202B)
  else if s3='PDF' then result := UTF8_codepoint_as_UTF8($202C)
  else if s3='LRI' then result := UTF8_codepoint_as_UTF8($2066)
  else if s3='RLI' then result := UTF8_codepoint_as_UTF8($2067)
  else if s3='FSI' then result := UTF8_codepoint_as_UTF8($2068)
  else if s3='PDI' then result := UTF8_codepoint_as_UTF8($2069)
  else begin
    result := '';
    writeln('ERROR: ',s3);
    readln;
  end;
end;

procedure do_datline(s : ansistring);
var s2,s3,tst : ansistring; m,w : word;
begin
  datstr := s;
  tst := '';
  s2 := todot(s,';');
  while s2<>'' do begin
    s3 := todot(s2,' ');
    tst += makethechar(s3);
  end;
  while copy(s,1,1)=' ' do delete(s,1,1);
  
  val(s,m,w);
  for w := 0 to 2 do begin
    if ((1 shl w) and m)>0 then dotest(tst,w);
  end;
end;


procedure do_line(s : ansistring);
begin
  s := todot(s,'#');
  if s<>'' then begin
    if pos('@Levels:',s)=1 then do_levelsline(s)
    else if pos('@Reorder:',s)=1 then do_reorderline(s)
    else do_datline(s);
  end;
end;


var
f : text;
s : ansistring;

begin
  errcnt := 0; 
  goodcnt := 0;
  assign(f,'BidiTest.txt');
  reset(f);
  while not eof(f) do begin
    readln(f,s);
    do_line(s);
  end;
  close(f);
  writeln('errcnt: ',errcnt);
  writeln('goodcnt: ',goodcnt);
end.
