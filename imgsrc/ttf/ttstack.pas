{$mode objfpc}
{$unitpath .}
unit ttstack;

interface
uses ttprog,sysutils;


type
Tcallstackelement = record
  prog : TTprogram;
  reta : dword;
end;

TTexecCallStack = class
  constructor create();
  procedure set_calldepth_limit(n : longint);

  protected
  procedure push_ret_addr(prg : TTprogram; a : dword);
  function pop_ret_addr(var prg : TTprogram; var a : dword) : boolean;

  private
  callstack : array of Tcallstackelement;
  csp,maxcsp : longint;
end;

{ TrueType programs have unusual stack-access specs }

TTexecStack = class(TTexecCallStack)
  constructor create();
  procedure set_stacklimit(n : longint);
  function get_current_sp() : dword;

  protected
  procedure push_stack(d : dword);
  function pop_stack() : dword;
  procedure clear_stack();
  function get_stack_element(n : longint) : dword;
  procedure remove_stack_element(n : longint);

  private
  stack : array of dword;
  sp,maxsp : longint;
end;



implementation



procedure TTexecCallStack.push_ret_addr(prg : TTprogram; a : dword);
begin
  if (csp>=maxcsp) then begin
    raise Tttprogexception.create('max call depth exceeded');
  end;
  callstack[csp].prog := prg;
  callstack[csp].reta := a;
  csp := csp + 1;
end;

function TTexecCallStack.pop_ret_addr(var prg : TTprogram; var a : dword) : boolean;
begin
  if (csp>0) then begin
    csp := csp-1;
    prg := callstack[csp].prog;
    a   := callstack[csp].reta;
    pop_ret_addr := true;
  end else pop_ret_addr := false;
end;

procedure TTexecCallStack.set_calldepth_limit(n : longint);
begin
  if (n>1024*1024*1024)or(n<0) then raise Tttprogexception.create('requested call-stack size exceeds hard limit');
  setlength(callstack,n);
  maxcsp := n;
  if (csp>maxcsp) then raise Tttprogexception.create('redefinition of call depth limit invalidated current call chain');
end;

constructor TTexecCallStack.create();
begin
  inherited create();
  csp := 0;
  set_calldepth_limit(0);
end;












procedure TTexecStack.push_stack(d : dword);
begin
  if (sp>=maxsp) then begin
    raise Tttprogexception.create('max TT stack depth exceeded');
  end;
  stack[sp] := d;
  sp := sp + 1;
end;

function TTexecStack.pop_stack() : dword;
begin
  if (sp>0) then begin
    sp := sp-1;
    pop_stack := stack[sp];
  end else raise Tttprogexception.create('out of TT stack elements');
end;

function TTexecStack.get_current_sp() : dword;
begin
  get_current_sp := sp;
end;

procedure TTexecStack.clear_stack();
begin
  sp := 0;
end;

function TTexecStack.get_stack_element(n : longint) : dword;
begin
  if (n>=0)and(n<sp) then get_stack_element := stack[n]
  else raise Tttprogexception.create('absolute reference to TT stack element out of bounds');
end;

procedure TTexecStack.remove_stack_element(n : longint);
var i,j : longint;
begin
  if (n>=0)and(n<sp) then begin
    j := sp-2;
    for i := n to j do begin
      stack[i] := stack[i+1];
    end;
    sp := sp -1;
  end else raise Tttprogexception.create('abs. reference to TT stack element out of bounds');
end;


procedure TTexecStack.set_stacklimit(n : longint);
begin
  if (n>1024*1024*1024)or(n<0) then raise Tttprogexception.create('requested stack size exceeds hard limit');
  setlength(stack,n);
  maxsp := n;
  if (sp>maxsp) then raise Tttprogexception.create('redefinition of TT stack depth limit invalidated stack elements');
end;

constructor TTexecStack.create();
begin
  inherited create();
  sp := 0;
  set_stacklimit(0);
end;



begin
end.


