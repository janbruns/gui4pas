{$mode objfpc}

uses classes, sysutils,
     FPImage, FPCanvas, FPImgCanv,
     FPWritePNG, FPwriteBMP, ttcontours, ttflaten, ttrender, ot_cmap,
     fontfile,ttgridfit,math, tttile;



const
ptsiz = 16;
dogridfit = true;
usegrey = 0.7;
xres = 1920;
yres = 1080;





procedure paint_glybmp(gp : Pglyphbmp; img : TFPCustomImage; ix,iy : longint);
var col : TFPColor; xr,yr,x,y,ax,ay,dx,dy : longint; p : Pbyte; t,cv : double;
begin
  xr := gp^.xres;
  yr := gp^.yres;
  ix += gp^.refx;
  iy += -gp^.refy +ptsiz;
  p := gp^.dat;
  for y := 1 to yr do for x := 1 to xr do begin
    ax := min(max(ix+x,0),xres-1); // for now, take global clip
    ay := min(max(iy-y,0),yres-1);
    col := img.Colors[ax, ay];
    t := (p^ and 127)/127;
    if (p^ and 128)=0 then begin
      t := usegrey*t +(1-usegrey)*0;
    end else begin
      t := usegrey*t +(1-usegrey)*1;
    end;
    t := 1-t;
    cv := col.red/65535;
    cv := (1-t)*cv + (t)*1;
    col.red := round( 65535*cv );
    inc(p);
    col.green := col.red;
    col.blue := col.red;
    img.Colors[ax, ay] := col;
  end;
end;

const
teststring : UTF8string = 'Dies ist ein Schriftprobentext, der vor Ort dazu genutzt werden könnte, Darstellungsdetails zu visualisieren. Er wurde also nicht dazu ersonnen, dem Leser einen Informationsgehalt darzubieten, so daß hier kaum irgendwelche Inhalte gelesen werden können. Aber immerhin: "Zwölf Boxkämpfer jagen Viktor quer über den Sylter Deich." sowie auch "The quick brown fox jumps over the lazy dog." sind bereits enthalten. Daher gilt: ';








var
p : Pglyphbmp;


var 
canvas : TFPCustomCanvas;
image : TFPCustomImage;
writer : TFPCustomImageWriter;

s : TMemoryStream;
font : TOTcmapfont;
g,gnxt : TTglyph;

tstart : double;
msgcnt : longint;
msg : UnicodeString;
ch,xpos,ypos,dv : longint;
fontfn : ansistring;


begin
  msg := UTF8Decode(UTF8Decode(teststring));
  msgcnt := 1;

  tstart := now();

  image := TFPMemoryImage.Create(xres,yres);
  Canvas := TFPImageCanvas.Create(image);
//  Writer := TFPWriterBMP.Create;
  Writer := TFPWriterPNG.Create;

  s := TMemoryStream.create();
  ///usr/share/fonts/truetype/ttf-dejavu/DejaVuSerif.ttf
  //fontfn := ('./times.ttf');
  //fontfn := ('/usr/share/fonts/truetype/ttf-dejavu/DejaVuSerif.ttf');
  fontfn := ('../../media/fonts/dejavu-fonts-ttf-2.37/ttf/DejaVuSans.ttf');

  s.LoadFromFile(fontfn);

  font := TOTcmapfont.create();
  font.load(s);

  ypos := 0;
  while (ypos<yres) do begin
    xpos := 0;
    while xpos<xres do  begin

      if (msgcnt<=length(msg)) then ch := ord(msg[msgcnt]) else ch := ord(' ');
      inc(msgcnt);
      if msgcnt>length(msg) then msgcnt := 1;

      ch := font.UnicodeToGlyphIndex(ch);
      g := font.getGlyph( ch );
      if (msgcnt<=length(msg)) then ch := ord(msg[msgcnt]) else ch := ord(' ');
      ch := font.UnicodeToGlyphIndex(ch);
      gnxt := font.getGlyph( ch );

      if not(g=nil) then begin
        if gnxt<>nil then begin
          dv := font.lookup_kern(g.idx,gnxt.idx,1);
        end else dv := 0;
        p := get_glybmp(g,ptsiz,ptsiz,dogridfit);
        paint_glybmp(p, image,xpos, ypos);
        xpos := xpos + round((p^.awx + dv*64*ptsiz/g.font.unitsPerEm)/64);
      end else break;
    end;
    ypos += ptsiz;
  end;

  writeln((now()-tstart)*3600*24,' s: done rendering. saving image-file...');
  image.SaveToFile('test_'+ExtractFileName(fontfn)+'_'+IntToStr(ptsiz)+'_'+IntToStr(ord(dogridfit))+'_'+IntToStr(round(100*usegrey))+'.png', writer);
  font.destroy();
  s.destroy();
  Canvas.Free;
  image.Free;
  writer.Free;
  writeln((now()-tstart)*3600*24,' s: halt.');
end.


