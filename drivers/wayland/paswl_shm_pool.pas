{$mode objfpc}
{$unitpath .}
unit paswl_shm_pool;

interface
uses baseunix,wl_basic_types,wl_core,wl_libwl,
paswl_bindable,
paswl_buffer,
mempool,memfdmgr;


type
T_paswl_shm_pool=class;

Tmempool_for_shm = class(Tmempool)
  function grow_pool(s : Tsize) : boolean; override;

  public
  paswl : T_paswl_shm_pool;
end;

T_paswl_shm_pool=class(Tpaswl_bindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;

  function createMemFd(s : Tsize) : Tcint; virtual;
  procedure releaseMemFd(); virtual;
  function get_buffer_adress(oset : Tsize) : pointer; virtual;
  function _create_buffer(oset : Tsize; w, h, bpl : longint; fmt : dword; c : T_paswl_buffer_cls) : T_paswl_buffer; virtual;
  procedure init_size(s : Tsize); virtual;
  
  private
  procedure set_new_size(s : Tsize); virtual;
  
  private
  procedure mapFd(); virtual;
  procedure unmapFd(); virtual;

  private
  _len : Tsize;
  dat : Pbyte;
    
  public
  tmpf : Ttmpfiledescr;
  property len : Tsize read _len; // write set_new_size;
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
end;
T_paswl_shm_pool_cls = class of T_paswl_shm_pool;


Tpaswl_shm_pool=class(T_paswl_shm_pool)
  function create_buffer(oset : Tsize; w, h, bpl : longint; fmt : dword; c : T_paswl_buffer_cls) : T_paswl_buffer; virtual;
end;

Tpaswl_shm_pool_cls = class of Tpaswl_shm_pool;

Tpaswl_shm_managedpool=class(T_paswl_shm_pool)
  public
  constructor create(); override;
  destructor destroy(); override;
  function createMemFd(s : Tsize) : Tcint; override;
  function create_buffer( w, h, bpl : longint; fmt : dword; c : T_paswl_buffer_cls) : T_paswl_buffer; virtual;
  public
  mempool : Tmempool_for_shm;
end;
Tpaswl_shm_managedpool_cls = class of Tpaswl_shm_managedpool;

function fmt_to_bpp(fmt : dword) : longint;





implementation



function Tmempool_for_shm.grow_pool(s : Tsize) : boolean;
var olds,s2 : Tsize;
begin
  olds := paswl.len;
  s2 := olds;
  if s2<1 then s2 := 1;
  while (s2-olds)<s do s2 *= 2;
  paswl.set_new_size(s2);
  { add the fresh memory to freelist }
  release_to_pool(olds,paswl.len-olds);
  grow_pool := (s2=paswl.len);
end;



procedure T_paswl_shm_pool.init_size(s : Tsize);
begin
  if _len=0 then begin
    createMemFd(s);
  end else set_new_size(s);
end;

procedure T_paswl_shm_pool.set_new_size(s : Tsize);
begin
  if _len=0 then begin
    createMemFd(s);
  end;
  if s>_len then begin
    if tmpf=nil then begin
      writeln('tmpf nil!');
    end else begin
      unmapfd();
      tmpf.grow(s);
      wl_shm_pool_resize(wlo,s);
      _len := s;
      mapfd();
    end;
  end;  
end;


constructor T_paswl_shm_pool.create();
begin
  inherited create();
  _len := 0;
end;


constructor Tpaswl_shm_managedpool.create();
begin
  inherited ;
  mempool := Tmempool_for_shm.create();
  mempool.paswl := self;
end;

destructor Tpaswl_shm_managedpool.destroy();
begin
  mempool.destroy();
  inherited;
end;

destructor T_paswl_shm_pool.destroy();
begin
  unmapFd();
  releaseMemFd();
  wl_shm_pool_destroy(wlo);
  wl_proxy_destroy(wlo);
  inherited destroy();
end;

procedure T_paswl_shm_pool.releaseMemFd();
begin
  if tmpf<>nil then begin
    if tmpf.fd>=0 then begin
      fpclose(tmpf.fd);
    end;
    tmpf.destroy();
  end;
  tmpf := nil;
end;


function filesize_fd(fd : cint) : Tsize;
var s : stat; 
begin
  if fd>=0 then begin
    if FpFStat(fd,s)=0 then begin
      filesize_fd := s.st_size;
    end else filesize_fd := 0;
  end else filesize_fd := 0;  
end;

procedure T_paswl_shm_pool.mapFd();
var fd : cint;
begin
  if dat=nil then begin
    if tmpf<>nil then begin
      fd := tmpf.fd;
      if len>0 then begin
        dat := Fpmmap(nil, len,PROT_READ or PROT_WRITE,MAP_SHARED,fd,0);
      end else dat := nil;
    end else dat := nil;
  end;
end;

procedure T_paswl_shm_pool.unmapFd();
begin
  if dat<>nil then begin
    Fpmunmap(dat,len);
    dat := nil;
  end;
end;

function Tpaswl_shm_managedpool.createMemFd(s : Tsize) : Tcint;
var f : Tcint;
begin
  f := inherited createMemFd(s);
  if f>=0 then begin
    {add the intial memblock to memmgr}
    mempool.release_to_pool(0,s);
  end;
  createMemFd := f;
end;

function T_paswl_shm_pool.createMemFd(s : Tsize) : Tcint;
begin
  if (tmpf=nil) then begin
    tmpf := memfdmgr.getMemFd(s);
    _len := s;
    mapfd();
  end;
  if (tmpf=nil) then begin
    createMemFd := -1;
  end else createMemFd := tmpf.fd;
end;

procedure T_paswl_shm_pool.onBind();
begin
  inherited;
end;

procedure T_paswl_shm_pool.onUnbind();
begin
  inherited;
end;





function T_paswl_shm_pool.get_buffer_adress(oset : Tsize) : pointer;
begin
  get_buffer_adress := @dat[oset];
end;

class function T_paswl_shm_pool.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_shm_pool_interface;
end;

class function T_paswl_shm_pool.wl_ifver() : dword;
begin
  wl_ifver := 1;
end;


function T_paswl_shm_pool._create_buffer(oset : Tsize; w, h, bpl : longint; fmt : dword; c : T_paswl_buffer_cls) : T_paswl_buffer;
var o : T_paswl_buffer;
begin
  o := T_paswl_buffer( make_child(c,nil,true) );
  if o<>nil then begin
    o.width := w;
    o.height := h;
    o.bpl := bpl;
    o.format := fmt;
    o.bpp := fmt_to_bpp(fmt);
    o.oset := oset;
    o.pool := self;
  end;
  if not (o is Tpaswl_virtualbuffer) then begin
    o.wlo := wl_shm_pool_create_buffer(wlo,oset,w,h,bpl,fmt);
    o.onBind();
  end;
  _create_buffer := o;
end;


function Tpaswl_shm_managedpool.create_buffer( w, h, bpl : longint; fmt : dword; c : T_paswl_buffer_cls) : T_paswl_buffer;
var o : T_paswl_buffer; oset : Tsize;
begin
  oset := mempool.alloc_from_pool(h*int64(bpl));
  if (oset=high(Tsize)) then begin
    o := nil;
  end else begin
    o := _create_buffer(oset, w, h, bpl, fmt, c) as T_paswl_buffer;
  end;
  create_buffer := o;
end;

function Tpaswl_shm_pool.create_buffer(oset : Tsize; w, h, bpl : longint; fmt : dword; c : T_paswl_buffer_cls) : T_paswl_buffer;
var o : T_paswl_buffer;
begin
  o := _create_buffer(oset, w, h, bpl, fmt, c) as T_paswl_buffer;
  create_buffer := o;
end;


function fmt_to_bpp(fmt : dword) : longint;
begin
  case fmt of
    wl_shm_format_avuy8888,
    wl_shm_format_xvuy8888,
    wl_shm_format_vuy101010,
    wl_shm_format_xyuv8888,
    wl_shm_format_rg1616,
    wl_shm_format_gr1616,
    wl_shm_format_yuyv, // ?
    wl_shm_format_yvyu, // ?
    wl_shm_format_uyvy, // ?
    wl_shm_format_vyuy, // ?
    wl_shm_format_ayuv,
    wl_shm_format_xvyu2101010,

    wl_shm_format_xrgb2101010,
    wl_shm_format_xbgr2101010,
    wl_shm_format_rgbx1010102,
    wl_shm_format_bgrx1010102,
    wl_shm_format_argb2101010,
    wl_shm_format_abgr2101010,
    wl_shm_format_rgba1010102,
    wl_shm_format_bgra1010102,  
    wl_shm_format_xbgr8888,
    wl_shm_format_rgbx8888,
    wl_shm_format_bgrx8888,
    wl_shm_format_abgr8888,
    wl_shm_format_rgba8888,
    wl_shm_format_bgra8888,
    wl_shm_format_argb8888,
    wl_shm_format_xrgb8888 : fmt_to_bpp := 4;

    wl_shm_format_r8,
    wl_shm_format_rgb332,
    wl_shm_format_bgr233,
    wl_shm_format_c8 : fmt_to_bpp := 1;
    
    wl_shm_format_r16,
    wl_shm_format_rg88,
    wl_shm_format_gr88,
    wl_shm_format_xrgb4444,
    wl_shm_format_xbgr4444,
    wl_shm_format_rgbx4444,
    wl_shm_format_bgrx4444,
    wl_shm_format_argb4444,
    wl_shm_format_abgr4444,
    wl_shm_format_rgba4444,
    wl_shm_format_bgra4444,
    wl_shm_format_xrgb1555,
    wl_shm_format_xbgr1555,
    wl_shm_format_rgbx5551,
    wl_shm_format_bgrx5551,
    wl_shm_format_argb1555,
    wl_shm_format_abgr1555,
    wl_shm_format_rgba5551,
    wl_shm_format_bgra5551,
    wl_shm_format_rgb565,
    wl_shm_format_bgr565 : fmt_to_bpp := 2;
    
    wl_shm_format_vuy888,
    wl_shm_format_rgb888,
    wl_shm_format_bgr888 : fmt_to_bpp := 3;


    
    wl_shm_format_xrgb16161616,
    wl_shm_format_xbgr16161616,
    wl_shm_format_argb16161616,
    wl_shm_format_abgr16161616,
    wl_shm_format_xvyu12_16161616,
    wl_shm_format_xvyu16161616,
    wl_shm_format_xrgb16161616f,
    wl_shm_format_xbgr16161616f,
    wl_shm_format_argb16161616f,
    wl_shm_format_abgr16161616f : fmt_to_bpp := 8;

    else fmt_to_bpp := 4;
{
    wl_shm_format_nv12 = $3231564E;
    wl_shm_format_nv21 = $3132564E;
    wl_shm_format_nv16 = $3631564E;
    wl_shm_format_nv61 = $3136564E;
    wl_shm_format_yuv410 = $39565559;
    wl_shm_format_yvu410 = $39555659;
    wl_shm_format_yuv411 = $31315559;
    wl_shm_format_yvu411 = $31315659;
    wl_shm_format_yuv420 = $32315559;
    wl_shm_format_yvu420 = $32315659;
    wl_shm_format_yuv422 = $36315559;
    wl_shm_format_yvu422 = $36315659;
    wl_shm_format_yuv444 = $34325559;
    wl_shm_format_yvu444 = $34325659;
    wl_shm_format_y210 = $30313259;
    wl_shm_format_y212 = $32313259;
    wl_shm_format_y216 = $36313259;
    wl_shm_format_y410 = $30313459;
    wl_shm_format_y412 = $32313459;
    wl_shm_format_y416 = $36313459;
    wl_shm_format_y0l0 = $304C3059;
    wl_shm_format_x0l0 = $304C3058;
    wl_shm_format_y0l2 = $324C3059;
    wl_shm_format_x0l2 = $324C3058;
    wl_shm_format_yuv420_8bit = $38305559;
    wl_shm_format_yuv420_10bit = $30315559;
    wl_shm_format_xrgb8888_a8 = $38415258;
    wl_shm_format_xbgr8888_a8 = $38414258;
    wl_shm_format_rgbx8888_a8 = $38415852;
    wl_shm_format_bgrx8888_a8 = $38415842;
    wl_shm_format_rgb888_a8 = $38413852;
    wl_shm_format_bgr888_a8 = $38413842;
    wl_shm_format_rgb565_a8 = $38413552;
    wl_shm_format_bgr565_a8 = $38413542;
    wl_shm_format_nv24 = $3432564E;
    wl_shm_format_nv42 = $3234564E;
    wl_shm_format_p210 = $30313250;
    wl_shm_format_p010 = $30313050;
    wl_shm_format_p012 = $32313050;
    wl_shm_format_p016 = $36313050;
    wl_shm_format_axbxgxrx106106106106 = $30314241;
    wl_shm_format_nv15 = $3531564E;
    wl_shm_format_q410 = $30313451;
    wl_shm_format_q401 = $31303451;
    wl_shm_format_c1 = $20203143;
    wl_shm_format_c2 = $20203243;
    wl_shm_format_c4 = $20203443;
    wl_shm_format_d1 = $20203144;
    wl_shm_format_d2 = $20203244;
    wl_shm_format_d4 = $20203444;
    wl_shm_format_d8 = $20203844;
    wl_shm_format_r1 = $20203152;
    wl_shm_format_r2 = $20203252;
    wl_shm_format_r4 = $20203452;
    wl_shm_format_r10 = $20303152;
    wl_shm_format_r12 = $20323152;
    wl_shm_format_p030 = $30333050;
}
  end;
end;



initialization
begin
end;

finalization
begin
end;

end.



