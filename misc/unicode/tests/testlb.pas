{$unitpath ..}
{$mode objfpc}
uses 
linebreakalgo,
paragraphmanager,
unicode_bidiralgo,
unicode_UTF8;

var
o : Tparagraphmanager;
i : dword;
s,wj,shy,oe,oee : ansistring;

begin
  wj := UTF8_codepoint_as_UTF8($2060);
  shy := UTF8_codepoint_as_UTF8($AD);
  oe := 'o'+UTF8_codepoint_as_UTF8($030B);
  oee := oe+UTF8_codepoint_as_UTF8($0308);
  s := 'Test a,b 123 12:34   1.2 Hal'+wj+' len '+wj+'h'+oee+oe+'lm'+oe+'. Test'+shy+'osteron';

{ current output (just as expected):

Test |a,b |123 |12:34   |1.2 |Hal⁠ |len ⁠hő̈őlmő. |Test­|osteron|
Test |a,b |123 |12:34   |1.2 |Hal⁠ len ⁠hő̈őlmő. |Test­|osteron|
}

  o := Tparagraphmanager.create();
  o.make_bd(Tunicode_bidir_algo);
  o.make_lb(Tlinebreaker);

  o.load_from_UTF8(s);
  o.linebreaker.make_break();
  for i := 0 to high(o.bidirhandler.chars) do begin
    write(UTF8_codepoint_as_UTF8(o.bidirhandler.chars[i]));
    if (o.linebreaker.breakstatus[i] and break_allowed)>0 then write('|');
  end;
  writeln;
  
  Tlinebreaker(o.linebreaker).WJ_ignores_trailSP := true;
  o.load_from_UTF8(s);
  o.linebreaker.make_break();
  for i := 0 to high(o.bidirhandler.chars) do begin
    write(UTF8_codepoint_as_UTF8(o.bidirhandler.chars[i]));
    if (o.linebreaker.breakstatus[i] and break_allowed)>0 then write('|');
  end;
  writeln;
  
  
  o.destroy();

end.
