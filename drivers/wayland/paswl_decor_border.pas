{$mode objfpc}
{$unitpath .}
unit paswl_decor_border;

interface
uses 
  wl_basic_types,
  wl_core,
  wl_libwl,
  paswl_compositor,
  paswl_subcompositor,
  paswl_viewporter,
  paswl_surface,
  paswl_subsurface,
  paswl_shm_pool,
  paswl_buffer,
  paswl_viewport;


const
wl_module_require_subcompositor = 1;
wl_module_require_shm = 2;
wl_module_require_viewporter = 4;
wl_module_require_multifuncbuf = $2000000000000000;


type
Tborderpainter = class
  public
  procedure repaint(dst : T_paswl_buffer; planwidth,planheight : longint); virtual;
  procedure make(bordersize_ : longint; bordercol : Pdword);

  class function required_wl_modules() : qword;
  
  public
  bordersize : longint;
  bcol : Pdword;
end;

Tborderpartsurf = class(Tpaswl_surface)
  public
  procedure resize(planwidth,planheight : longint); virtual; abstract;

  public
  which,bordersize : longint;
  sub : Tpaswl_subsurface;
end;
Tborderpartsurf_cls = class of Tborderpartsurf;


Tborderpartsurf_using_vb = class(Tborderpartsurf)
  public
  procedure make(which_,bordersize_ : longint; bordercol : Pdword; mpool : Tpaswl_shm_managedpool; subs : Tpaswl_subsurface);
  procedure resize(planwidth,planheight : longint); override;
  destructor destroy(); override;
  constructor create(); override;

  class function required_wl_modules() : qword;

  private
  {used in case even the max width/height needs to grow}
  procedure reorg(planwidth,planheight : longint);

  public
  maxw,maxh : longint;
  buf : Tpaswl_recropablebuffer;
  vbuf : Tpaswl_virtualbuffer;
  pool : Tpaswl_shm_managedpool;
  bcol : Pdword;
end;


{ even better with wl viewporter }
Tborderpartsurf_using_vp = class(Tborderpartsurf)
  public
  procedure make(which_,bordersize_ : longint; bcol : Pdword; mpool : Tpaswl_shm_managedpool; subs : Tpaswl_subsurface; vprt : Tpaswl_viewport);
  procedure resize(planwidth,planheight : longint); override;
  destructor destroy(); override;
  
  class function required_wl_modules() : qword;
  
  public
  buf : Tpaswl_buffer;
  vp : Tpaswl_viewport;
end;


Tpaswl_decorborder = class
  procedure take_compositor(c : Tpaswl_compositor; cs : Tpaswl_subcompositor = nil);
  procedure take_viewporter(vptr : Tpaswl_viewporter = nil);
  procedure take_pool(s : Tpaswl_shm_managedpool);
  procedure take_surface(s : Tpaswl_surface);
  procedure take_colors(n : longint; c : Pdword);
  procedure onResize(b : T_paswl_buffer; planwidth,planheight : longint);
  function make() : qword;
  procedure maybe_remake();
  procedure releaseChilds();
  constructor create();
  destructor destroy(); override;
  
  class function dryMake(have_subcompo, have_vp : boolean) : qword;
  
  public
  compo : Tpaswl_compositor;
  subcompo : Tpaswl_subcompositor;
  viewporter : Tpaswl_viewporter;
  pool : Tpaswl_shm_managedpool;
  surf : Tpaswl_surface;
  cols : array of dword;
  painted : Tborderpainter;
  parted : array[0..7] of Tborderpartsurf;
  made : boolean;
end;




implementation
uses math,sysutils;

class function Tborderpainter.required_wl_modules() : qword;
begin
  required_wl_modules := wl_module_require_shm or wl_module_require_multifuncbuf;
end;

class function Tborderpartsurf_using_vb.required_wl_modules() : qword;
begin
  required_wl_modules := wl_module_require_subcompositor or wl_module_require_shm;
end;

class function Tborderpartsurf_using_vp.required_wl_modules() : qword;
begin
  required_wl_modules := wl_module_require_subcompositor or wl_module_require_shm or wl_module_require_viewporter;
end;


procedure Tborderpainter.repaint(dst : T_paswl_buffer; planwidth,planheight : longint);
var c,x,y : longint; col : dword;
begin
//writeln('repaint_border: ',planwidth,'/',planheight,' buf:',dst.width,'/',dst.height);
  for c := 0 to bordersize-1 do begin
    col := bcol[c];
    for x := c to planwidth-1 -c do begin
      dst.setPixel(x,c,col);
      dst.setPixel(x,planheight-1 -c,col);
    end;
    for y := c to planheight-1 -c do begin
      dst.setPixel(c,y,col);
      dst.setPixel(planwidth-1 -c,y,col);
    end;
  end;
end;

procedure Tborderpainter.make(bordersize_ : longint; bordercol : Pdword);
begin
  bordersize := bordersize_;
  bcol := bordercol;
end;



procedure Tborderpartsurf_using_vb.make(which_,bordersize_ : longint; bordercol : Pdword; mpool : Tpaswl_shm_managedpool; subs : Tpaswl_subsurface);
var is_h,is_v : boolean; x,y,dx,dy : longint;
begin
  which := which_;
  bordersize := bordersize_;
  sub := subs;
  pool := mpool;
  bcol := bordercol;
  while (maxw<3*bordersize) do maxw *= 2;
  while (maxh<3*bordersize) do maxh *= 2;
  
  is_h := false;
  is_v := false;
  case which of
    1,3 : is_h := true;
    0,2 : is_v := true;
  end;
  dx := bordersize;
  dy := bordersize;
  if is_h then dx := maxw -2*bordersize;
  if is_v then dy := maxh -2*bordersize;
  vbuf := pool.create_buffer( dx, dy, 4*dx, wl_shm_format_argb8888, Tpaswl_virtualbuffer) as Tpaswl_virtualbuffer;
  for y := 0 to dy-1 do begin
    for x := 0 to dx-1 do begin
      case which of
        0 : vbuf.setPixel(x,y,bcol[bordersize-1-x]);
        1 : vbuf.setPixel(x,y,bcol[y]);
        2 : vbuf.setPixel(x,y,bcol[x]);
        3 : vbuf.setPixel(x,y,bcol[bordersize-1-y]);
        4 : vbuf.setPixel(x,y,bcol[min(x,y)]);
        5 : vbuf.setPixel(x,y,bcol[min(bordersize-1-x,y)]);
        6 : vbuf.setPixel(x,y,bcol[min(x,bordersize-1-y)]);
        7 : vbuf.setPixel(x,y,bcol[min(bordersize-1-x,bordersize-1-y)]);
      end;
    end;
  end;
end;


procedure Tborderpartsurf_using_vb.reorg(planwidth,planheight : longint);
begin
  if assigned(buf) and (buf.busy) then begin
    buf.release_on_release := true;
    buf := nil;
  end else freeandnil(buf);
  freeandnil(vbuf);
  while (maxw<planwidth ) do maxw *= 2;
  while (maxh<planheight) do maxh *= 2;
  make(which,bordersize,bcol,pool,sub);
end;


procedure Tborderpartsurf_using_vb.resize(planwidth,planheight : longint);
var is_h,is_v : boolean; x,y,dx,dy : longint;
begin
  if (planwidth>maxw)or(planheight>maxh) then reorg(planwidth,planheight);
  is_h := false;
  is_v := false;
  case which of
    1,3 : is_h := true;
    0,2 : is_v := true;
  end;
  dx := bordersize;
  dy := bordersize;
  if is_h then dx := planwidth  -2*bordersize;
  if is_v then dy := planheight -2*bordersize;
  if assigned(buf) then begin
    buf := buf.recrop(0,0,dx,dy);
  end else begin
    buf := vbuf.get_recropable(0,0,dx,dy);
  end;
  x := 0;
  y := 0;
  case which of
    0,1,3,5,7 : x += bordersize;
  end;
  case which of
    0,5,7 : x := planwidth -x;
  end;
  case which of
    0,2,3,6,7 : y += bordersize;
  end;
  case which of
    3,6,7 : y := planheight -y;
  end;
  attach(buf);
  damage(0,0,dx,dy);
  sub.set_position(x,y);
  commit();
end;

constructor Tborderpartsurf_using_vb.create();
begin
  inherited;
  maxh := 256;
  maxw := 256;
end;

destructor Tborderpartsurf_using_vb.destroy();
begin
  freeandnil(buf);
  freeandnil(vbuf);
  freeandnil(sub);
  inherited;
end;






procedure Tborderpartsurf_using_vp.make(which_,bordersize_ : longint; bcol : Pdword; mpool : Tpaswl_shm_managedpool; subs : Tpaswl_subsurface; vprt : Tpaswl_viewport);
var x,y,dx,dy : longint;
begin
  which := which_;
  bordersize := bordersize_;
  sub := subs;
  vp := vprt;
  dx := bordersize;
  dy := bordersize;
  buf := mpool.create_buffer( dx, dy, 4*dx, wl_shm_format_argb8888, Tpaswl_buffer) as Tpaswl_buffer;
  for y := 0 to dy-1 do begin
    for x := 0 to dx-1 do begin
      case which of
        0 : buf.setPixel(x,y,bcol[bordersize-1-x]);
        1 : buf.setPixel(x,y,bcol[y]);
        2 : buf.setPixel(x,y,bcol[x]);
        3 : buf.setPixel(x,y,bcol[bordersize-1-y]);
        4 : buf.setPixel(x,y,bcol[min(x,y)]);
        5 : buf.setPixel(x,y,bcol[min(bordersize-1-x,y)]);
        6 : buf.setPixel(x,y,bcol[min(x,bordersize-1-y)]);
        7 : buf.setPixel(x,y,bcol[min(bordersize-1-x,bordersize-1-y)]);
      end;
    end;
  end;
end;


procedure Tborderpartsurf_using_vp.resize(planwidth,planheight : longint);
var is_h,is_v : boolean; x,y,dx,dy : longint;
begin
  is_h := false;
  is_v := false;
  case which of
    1,3 : is_h := true;
    0,2 : is_v := true;
  end;
  dx := bordersize;
  dy := bordersize;
  if is_h then dx := planwidth  -2*bordersize;
  if is_v then dy := planheight -2*bordersize;
  x := 0;
  y := 0;
  case which of
    0,1,3,5,7 : x += bordersize;
  end;
  case which of
    0,5,7 : x := planwidth -x;
  end;
  case which of
    0,2,3,6,7 : y += bordersize;
  end;
  case which of
    3,6,7 : y := planheight -y;
  end;
  attach(buf);
  vp.set_destination(dx,dy);
  vp.set_source(0,0,256*bordersize,256*bordersize);
  damage(0,0,dx,dy);
  sub.set_position(x,y);
  commit();
end;

destructor Tborderpartsurf_using_vp.destroy();
begin
  freeandnil(buf);
  freeandnil(vp);
  freeandnil(sub);
  inherited;
end;








procedure Tpaswl_decorborder.take_compositor(c : Tpaswl_compositor; cs : Tpaswl_subcompositor = nil);
begin
  compo := c;
  subcompo := cs;
end;

procedure Tpaswl_decorborder.take_viewporter(vptr : Tpaswl_viewporter = nil);
begin
  viewporter := vptr;
end;

procedure Tpaswl_decorborder.take_pool(s : Tpaswl_shm_managedpool);
begin
  pool := s;
end;

procedure Tpaswl_decorborder.take_surface(s : Tpaswl_surface);
begin
  surf := s;
end;

procedure Tpaswl_decorborder.take_colors(n : longint; c : Pdword);
var i : longint;
begin
  setlength(cols,n);
  for i := 0 to n-1 do cols[i] := c[i];
  maybe_remake();
end;

procedure Tpaswl_decorborder.onResize(b : T_paswl_buffer; planwidth,planheight : longint);
var i : longint;
begin
  if assigned(painted) then begin
    if assigned(b) then begin
      painted.repaint(b,planwidth,planheight);
      if assigned(surf) then begin
        i := high(cols)+1;
        surf.damage(0,0,planwidth,i);
        surf.damage(0,0,i,planheight);
        surf.damage(0,planheight-i,planwidth,i);
        surf.damage(planwidth-i,0,i,planheight);
      end;
    end;
  end else begin
    for i := 0 to 7 do begin
      if assigned(parted[i]) then begin
        parted[i].resize(planwidth,planheight);
      end;
    end;
  end;
end;


function Tpaswl_decorborder.make() : qword;
var dm : qword; i : longint; subs : Tpaswl_subsurface; svb : Tborderpartsurf_using_vb; svp : Tborderpartsurf_using_vp; vp : Tpaswl_viewport;
begin
  dm := dryMake(assigned(subcompo), assigned(viewporter) );
  if (dm and wl_module_require_multifuncbuf)>0 then begin
    writeln('make painted border');
    painted := Tborderpainter.create();
    painted.make(1+high(cols),@cols[0]);
  end else begin
    if assigned(viewporter) then begin
      writeln('make vp border ',hexstr(ptruint(viewporter),16));
      for i := 0 to 7 do begin
        svp := compo.create_surface(Tborderpartsurf_using_vp) as Tborderpartsurf_using_vp;
        subs := subcompo.get_subsurface(svp,surf);
        vp := viewporter.get_viewport(svp);
        parted[i] := svp;
        svp.make(i,1+high(cols),@cols[0],pool,subs,vp);
      end;
    end else begin
      writeln('make vb border');
      for i := 0 to 7 do begin
        svb := compo.create_surface(Tborderpartsurf_using_vb) as Tborderpartsurf_using_vb;
        subs := subcompo.get_subsurface(svb,surf);
        parted[i] := svb;
        svb.make(i,1+high(cols),@cols[0],pool,subs);
      end;
    end;
  end;
  made := true;
  make := dm;
end;

procedure Tpaswl_decorborder.releaseChilds();
var i : longint;
begin
  freeandnil(painted);
  for i := 0 to 7 do freeandnil(parted[i]);
  setlength(cols,0);
end;


procedure Tpaswl_decorborder.maybe_remake();
begin
  if made then begin
    releaseChilds();
    make();
  end;
end;

constructor Tpaswl_decorborder.create();
begin
  inherited;
  setlength(cols,0);
  made := false;
end;

destructor Tpaswl_decorborder.destroy();
begin
  releaseChilds();
  inherited;
end;

class function Tpaswl_decorborder.dryMake(have_subcompo, have_vp : boolean) : qword;
begin
  if have_subcompo then begin
    if have_vp
    then dryMake := Tborderpartsurf_using_vp.required_wl_modules()
    else dryMake := Tborderpartsurf_using_vb.required_wl_modules();    
  end else dryMake := Tborderpainter.required_wl_modules();
end;

begin
end.
