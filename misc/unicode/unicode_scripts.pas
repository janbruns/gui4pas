{$mode objfpc}
{$unitpath .}
unit unicode_scripts;

interface

const
unicode_scr_UNKNOWN = 255; // might change to 65535 some day
unicode_scr_Common = 0;
unicode_scr_Inherited = 1;
unicode_scr_Adlam = 2;
unicode_scr_Ahom = 3;
unicode_scr_Anatolian_Hieroglyphs = 4;
unicode_scr_Arabic = 5;
unicode_scr_Armenian = 6;
unicode_scr_Avestan = 7;
unicode_scr_Balinese = 8;
unicode_scr_Bamum = 9;
unicode_scr_Bassa_Vah = 10;
unicode_scr_Batak = 11;
unicode_scr_Bengali = 12;
unicode_scr_Bhaiksuki = 13;
unicode_scr_Bopomofo = 14;
unicode_scr_Brahmi = 15;
unicode_scr_Braille = 16;
unicode_scr_Buginese = 17;
unicode_scr_Buhid = 18;
unicode_scr_Canadian_Aboriginal = 19;
unicode_scr_Carian = 20;
unicode_scr_Caucasian_Albanian = 21;
unicode_scr_Chakma = 22;
unicode_scr_Cham = 23;
unicode_scr_Cherokee = 24;
unicode_scr_Chorasmian = 25;
unicode_scr_Coptic = 26;
unicode_scr_Cuneiform = 27;
unicode_scr_Cypriot = 28;
unicode_scr_Cypro_Minoan = 29;
unicode_scr_Cyrillic = 30;
unicode_scr_Deseret = 31;
unicode_scr_Devanagari = 32;
unicode_scr_Dives_Akuru = 33;
unicode_scr_Dogra = 34;
unicode_scr_Duployan = 35;
unicode_scr_Egyptian_Hieroglyphs = 36;
unicode_scr_Elbasan = 37;
unicode_scr_Elymaic = 38;
unicode_scr_Ethiopic = 39;
unicode_scr_Georgian = 40;
unicode_scr_Glagolitic = 41;
unicode_scr_Gothic = 42;
unicode_scr_Grantha = 43;
unicode_scr_Greek = 44;
unicode_scr_Gujarati = 45;
unicode_scr_Gunjala_Gondi = 46;
unicode_scr_Gurmukhi = 47;
unicode_scr_Han = 48;
unicode_scr_Hangul = 49;
unicode_scr_Hanifi_Rohingya = 50;
unicode_scr_Hanunoo = 51;
unicode_scr_Hatran = 52;
unicode_scr_Hebrew = 53;
unicode_scr_Hiragana = 54;
unicode_scr_Imperial_Aramaic = 55;
unicode_scr_Inscriptional_Pahlavi = 56;
unicode_scr_Inscriptional_Parthian = 57;
unicode_scr_Javanese = 58;
unicode_scr_Kaithi = 59;
unicode_scr_Kannada = 60;
unicode_scr_Katakana = 61;
unicode_scr_Kawi = 62;
unicode_scr_Kayah_Li = 63;
unicode_scr_Kharoshthi = 64;
unicode_scr_Khitan_Small_Script = 65;
unicode_scr_Khmer = 66;
unicode_scr_Khojki = 67;
unicode_scr_Khudawadi = 68;
unicode_scr_Lao = 69;
unicode_scr_Latin = 70;
unicode_scr_Lepcha = 71;
unicode_scr_Limbu = 72;
unicode_scr_Linear_A = 73;
unicode_scr_Linear_B = 74;
unicode_scr_Lisu = 75;
unicode_scr_Lycian = 76;
unicode_scr_Lydian = 77;
unicode_scr_Mahajani = 78;
unicode_scr_Makasar = 79;
unicode_scr_Malayalam = 80;
unicode_scr_Mandaic = 81;
unicode_scr_Manichaean = 82;
unicode_scr_Marchen = 83;
unicode_scr_Masaram_Gondi = 84;
unicode_scr_Medefaidrin = 85;
unicode_scr_Meetei_Mayek = 86;
unicode_scr_Mende_Kikakui = 87;
unicode_scr_Meroitic_Cursive = 88;
unicode_scr_Meroitic_Hieroglyphs = 89;
unicode_scr_Miao = 90;
unicode_scr_Modi = 91;
unicode_scr_Mongolian = 92;
unicode_scr_Mro = 93;
unicode_scr_Multani = 94;
unicode_scr_Myanmar = 95;
unicode_scr_Nabataean = 96;
unicode_scr_Nag_Mundari = 97;
unicode_scr_Nandinagari = 98;
unicode_scr_Newa = 99;
unicode_scr_New_Tai_Lue = 100;
unicode_scr_Nko = 101;
unicode_scr_Nushu = 102;
unicode_scr_Nyiakeng_Puachue_Hmong = 103;
unicode_scr_Ogham = 104;
unicode_scr_Ol_Chiki = 105;
unicode_scr_Old_Hungarian = 106;
unicode_scr_Old_Italic = 107;
unicode_scr_Old_North_Arabian = 108;
unicode_scr_Old_Permic = 109;
unicode_scr_Old_Persian = 110;
unicode_scr_Old_Sogdian = 111;
unicode_scr_Old_South_Arabian = 112;
unicode_scr_Old_Turkic = 113;
unicode_scr_Old_Uyghur = 114;
unicode_scr_Oriya = 115;
unicode_scr_Osage = 116;
unicode_scr_Osmanya = 117;
unicode_scr_Pahawh_Hmong = 118;
unicode_scr_Palmyrene = 119;
unicode_scr_Pau_Cin_Hau = 120;
unicode_scr_Phags_Pa = 121;
unicode_scr_Phoenician = 122;
unicode_scr_Psalter_Pahlavi = 123;
unicode_scr_Rejang = 124;
unicode_scr_Runic = 125;
unicode_scr_Samaritan = 126;
unicode_scr_Saurashtra = 127;
unicode_scr_Sharada = 128;
unicode_scr_Shavian = 129;
unicode_scr_Siddham = 130;
unicode_scr_SignWriting = 131;
unicode_scr_Sinhala = 132;
unicode_scr_Sogdian = 133;
unicode_scr_Sora_Sompeng = 134;
unicode_scr_Soyombo = 135;
unicode_scr_Sundanese = 136;
unicode_scr_Syloti_Nagri = 137;
unicode_scr_Syriac = 138;
unicode_scr_Tagalog = 139;
unicode_scr_Tagbanwa = 140;
unicode_scr_Tai_Le = 141;
unicode_scr_Tai_Tham = 142;
unicode_scr_Tai_Viet = 143;
unicode_scr_Takri = 144;
unicode_scr_Tamil = 145;
unicode_scr_Tangsa = 146;
unicode_scr_Tangut = 147;
unicode_scr_Telugu = 148;
unicode_scr_Thaana = 149;
unicode_scr_Thai = 150;
unicode_scr_Tibetan = 151;
unicode_scr_Tifinagh = 152;
unicode_scr_Tirhuta = 153;
unicode_scr_Toto = 154;
unicode_scr_Ugaritic = 155;
unicode_scr_Vai = 156;
unicode_scr_Vithkuqi = 157;
unicode_scr_Wancho = 158;
unicode_scr_Warang_Citi = 159;
unicode_scr_Yezidi = 160;
unicode_scr_Yi = 161;
unicode_scr_Zanabazar_Square = 162;

function get_unicode_script(cp : dword) : byte;


implementation
uses sysutils,unicode_tabinit;


const
{$include scripts.inc}

var scr : array of byte;



function get_unicode_script(cp : dword) : byte;
begin
  if cp<=high(scr) then result := scr[cp] else result := 0;
end;


procedure do_init();
var s : ansistring;
begin
  setlength(s,high(sinc)+1);
  move(sinc[0],s[1],high(sinc)+1);
  unicode_initvaldecode(s,scr);
end;


begin
  do_init();
end.
