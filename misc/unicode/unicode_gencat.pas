{$mode objfpc}
{$unitpath .}
unit unicode_gencat;

interface

const
{ reordering these symbols (like done here) reduced exe size by about 0,5kB 
  (about 10% of 5KB used for this tab, as genrated by aux/make_gencat.pas).
}
gencat_unknown = 0;
gencat_Ll = 1;
gencat_Lu = 2;
gencat_Lo = 3;
gencat_Mn = 4;
gencat_Po = 5;
gencat_So = 6;
gencat_Mc = 7;
gencat_Ps = 8;
gencat_Pe = 9;
gencat_No = 10;
gencat_Lm = 11;
gencat_Nd = 12;
gencat_Sm = 13;
gencat_Sk = 14;
gencat_Sc = 15;
gencat_Cf = 16;
gencat_Pd = 17;
gencat_Nl = 18;
gencat_Pi = 19;
gencat_Pf = 20;
gencat_Lt = 21;
gencat_Zs = 22;
gencat_Pc = 23;
gencat_Me = 24;
gencat_Cs = 25;
gencat_Co = 26;
gencat_Zl = 27;
gencat_Zp = 28;
gencat_Cc = 29;
gencat_Cn = 30;





function get_unicode_gencat(cp : dword) : byte;

implementation
uses sysutils,unicode_tabinit;


const
{$include gencat.inc}

var catdat : array of byte;



function get_unicode_gencat(cp : dword) : byte;
begin
  if cp<=high(catdat) then result := catdat[cp] else result := 0;
end;


procedure do_init();
var s : ansistring;
begin
  setlength(s,high(sinc)+1);
  move(sinc[0],s[1],high(sinc)+1);
  unicode_initvaldecode(s,catdat);
end;


begin
  do_init();
end.
