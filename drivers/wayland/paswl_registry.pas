{$mode objfpc}
{$unitpath .}
unit paswl_registry;

interface
uses wl_basic_types,wl_core,wl_libwl,paswl_basebindable;


type



Tpaswl_registry=class(Tpaswl_regbasebindable)

  public
  constructor create(); override;
  destructor destroy(); override;
  procedure onBind(); override;
  procedure onUnbind(); override;
  
  protected
  procedure cb_registry_global(name : dword; iface : Pchar; version : dword); virtual;
  procedure cb_registry_global_remove(name : dword); virtual;
  
  public
  class function wl_ifdef() : Pwl_interface; override; // the header at the time the scanner tool was used
  class function wl_ifver() : dword; override; // wl_ifdef()^.version at the time the pas object code was written
  
end;

Tpaswl_registry_cls = class of Tpaswl_registry;




implementation
uses strings,math;


procedure registry_global(dat:pointer; o:wl_registry; name : dword; iface : Pchar; version : dword); cdecl;
var d : Tpaswl_registry;
begin
  if dat<>nil then begin
    d := Tpaswl_registry(dat);
    if d.wlo=o then d.cb_registry_global(name, iface, version)
    else ;//
  end;
end;

procedure registry_global_remove(dat:pointer; o:wl_registry; name : dword); cdecl;
var d : Tpaswl_registry;
begin
  if dat<>nil then begin
    d := Tpaswl_registry(dat);
    if d.wlo=o then d.cb_registry_global_remove(name)
    else ;//
  end;
end;

var
imp_if_registry_listener : wl_registry_listener = (
  global : @registry_global;
  global_remove : @registry_global_remove;
);



constructor Tpaswl_registry.create();
begin
  inherited create();
end;

destructor Tpaswl_registry.destroy();
begin
  inherited destroy();
  wl_proxy_destroy(wlo);
end;

procedure Tpaswl_registry.onBind();
begin
  inherited;
  wl_registry_add_listener(wlo,imp_if_registry_listener,self);
end;

procedure Tpaswl_registry.onUnbind();
begin
  inherited;
end;

procedure Tpaswl_registry.cb_registry_global(name : dword; iface : Pchar; version : dword);
begin
  writeln('Tpaswl_registry.cb_registry_global');
end;

procedure Tpaswl_registry.cb_registry_global_remove(name : dword);
begin
end;

class function Tpaswl_registry.wl_ifdef() : Pwl_interface;
begin
  wl_ifdef := wl_registry_interface;
end;

class function Tpaswl_registry.wl_ifver() : dword;
begin
  wl_ifver := 1;
end;


initialization
begin
end;

finalization
begin
end;

end.



