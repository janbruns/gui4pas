{$mode objfpc}
{$unitpath .}
unit unicode_bidicls;

interface

const
bidicls_Left_To_Right = 1;
bidicls_Right_To_Left = 2;
bidicls_Arabic_Letter = 3;
bidicls_European_Number = 4;
bidicls_European_Separator = 5;
bidicls_European_Terminator = 6;
bidicls_Arabic_Number = 7;
bidicls_Common_Separator = 8;
bidicls_Paragraph_Separator = 9;
bidicls_Segment_Separator = 10;
bidicls_White_Space = 11;
bidicls_Other_Neutral = 12;
bidicls_Boundary_Neutral = 13;
bidicls_Nonspacing_Mark = 14;
bidicls_Left_To_Right_Override = 15;
bidicls_Right_To_Left_Override = 16;
bidicls_Left_To_Right_Embedding = 17;
bidicls_Right_To_Left_Embedding = 18;
bidicls_Pop_Directional_Format = 19;
bidicls_Left_To_Right_Isolate = 20;
bidicls_Right_To_Left_Isolate = 21;
bidicls_First_Strong_Isolate = 22;
bidicls_Pop_Directional_Isolate = 23;



function get_unicode_bidi_class(cp : dword) : byte;

implementation
uses sysutils,unicode_tabinit;


const
{$include bidicls.inc}

var bidicls : array of byte;



function get_unicode_bidi_class(cp : dword) : byte;
begin
  if cp<=high(bidicls) then result := bidicls[cp] else result := 0;
end;


procedure do_init();
var s : ansistring;
begin
  setlength(s,high(sinc)+1);
  move(sinc[0],s[1],high(sinc)+1);
  unicode_initvaldecode(s,bidicls);
end;


begin
  do_init();
end.
