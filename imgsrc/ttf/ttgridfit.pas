UNIT ttgridfit;
{$mode objfpc}
{$COPERATORS ON}



{ The heavily font-data/render-engine specific parts of the
  TrueType code interpreter.


  procedure set_font(f : TTfont; emumode : longint);

  emumode : 
    0 : use TrueType flavoured OpenType interpreter
    1 : use signed compares in interpreter
    2 : activate some TrueType pointmove compatibility mode
    3 : TrueType compatibility mode 1+2
  


}

{_$DEFINE support_scanrule56}

interface
uses ttptinst,ttstack,ttprog,ttcalc,ttdata,fontfile;

const
version_for_GETINFO_instruction = 30;
init_glyphs_use_defaults = false;
glyphprog_zone0_from_prep = false;
use_storage_chain = true;


{ set the default angle in degree that projection and freedom 
  vector are allowed befores move get filtered out.
}
max_acceptable_pv_fv_angle       = 89.9790;  
{ 89.9965 = 1 F2DOT14 unit, 
  89.9930 = 2 F2DOT14 units, 
  89.9895 = 3 F2DOT14 units, 
  89.9860 = 4 F2DOT14 units, 
  89.9790 = 6 F2DOT14 units, 
  89.9720 = 8 F2DOT14 units, 
}

type
Tenginecompensator = function(dist :  double; up : pointer) : double;


Tgridfitter = class(Tpointinstructor)
  function do_exec(prog : TTprogram) : boolean; override;

  function apply_engine_compensation(x,ctyp : longint) : longint; override;
  function get_org_zonepoint_x(ptidx,zone : longint) : longint; override;
  function get_org_zonepoint_y(ptidx,zone : longint) : longint; override;
  function get_zonepoint_x(ptidx,zone : longint) : longint; override;
  function get_zonepoint_y(ptidx,zone : longint) : longint; override;
  procedure set_org_zonepoint_x(ptidx,zone,v : longint); override;
  procedure set_org_zonepoint_y(ptidx,zone,v : longint); override;
  procedure set_zonepoint_x(ptidx,zone,v : longint); override;
  procedure set_zonepoint_y(ptidx,zone,v : longint); override;
  procedure chkZonePtIndex(ptidx,zone : longint);
  function get_last_zone_point_idx(z : longint) : longint; override;
  procedure set_touched_z1x(p : longint; t : boolean); override;
  procedure set_touched_z1y(p : longint; t : boolean); override;
  function is_touched_z1x(p : longint) : boolean; override;
  function is_touched_z1y(p : longint) : boolean; override;

  procedure do_delta_instr(b : byte); override;
  procedure set_onoffcurve(b : byte); override;
  procedure do_readwrite_store(b : byte); override;

  procedure do_WCVTP(b : byte); override;

  procedure do_SCANCTRL(b : byte); override;
  procedure do_GETINFO(b : byte); override;
  procedure do_SCANTYPE(b : byte); override;
  procedure do_MP_S_PPEM(b : byte); override;
  procedure do_INSTCTRL(b : byte); override;


  procedure set_font(f : TTfont; emumode : longint);
  procedure set_ppem(x,y,ps : longint);


  function gridfit_glyph(g : TTglyph) : boolean;
  constructor create();
  destructor destroy(); override;

  function getScantype() : longint;
  function getFitStatus() : boolean; // instructions executed and success?


  procedure onEnterGlyphInstructions(g,g0 : TTglyph); virtual;

  procedure onExec(); virtual;


  protected
  function doscale(v : longint) : longint;

  procedure load_cvt_from_font();
  procedure load_cvt_prog_from_font();
  procedure load_font_prog_from_font();
  procedure restore_cvt();
  procedure execute_cvt_prog();
  procedure execute_glyph_prog(g0,g : TTglyph; p0,np, c0,nc : longint);

  procedure take_glyphdata(g0,g : TTglyph; p0,np,c0,nc : longint); virtual;
  procedure untake_glyphdata(g0,g : TTglyph; p0,np,c0,nc : longint); virtual;

  procedure initzone0;
  procedure zonecopy_0_to_C();
  procedure zonecopy_C_to_0();
  procedure dimzone(z,n : longint);

  function calc_pv_em() : longint;

  private
  procedure _disable_hinting(); // to be called from instructions
  procedure _enable_hinting(); // to be called from instructions



  public
  whitecompensator, blackcompensator : Tenginecompensator;
  wcompensatorDATA, bcompensatorDATA : pointer;

  version_for_GETINFO : byte;
  render_greyscale : boolean;


  protected
  font : TTfont;

  cvt_tab, // cvt as read from font-file
  cvt_prog : Ttdatastore; // cvt after executing cvt-prog

  stor_font, stor_prep : Ttdatastore; // storage area as left by font/prep-prog

  fontprog,
  cvtprog : TTprogram;

  zoneCorgx,zoneCorgy,
  zone0curx,zone0cury,
  zone0orgx,zone0orgy,
  zone1curx,zone1cury,
  zone1orgx,zone1orgy : array of F26DOT6;
  zone1touchX,zone1touchY,zone1onoff : array of boolean;

  ppem_x,  ppem_y,  pointsize : double;
  unitsPerEm, last_z1_pt : longint;

  glyph_streched,glyph_rotated : boolean;

  current_prog_type : longint; // 0:font, 1:prep, 2:glyph 

  glyphs_use_defaults, // g-progs can get gs inititalized to cvt or defaults
  hinting_disabled_by_font,
  hinting_disabled_by_prep,
  all_subglyphprogs_ok : boolean;

  gscvt : Tgraphicstate;



end;


function defaultcompensator(dist : double; up : pointer) : double;



implementation
uses sysutils,math;

const
CALLDEPTH_LIMIT = 128;

scanrule1 = 1;
scanrule2 = 2;
scanrule3 = 4;
scanrule4 = 8;
scanrule5 = 16;
scanrule6 = 32;



procedure Tgridfitter.onEnterGlyphInstructions(g,g0 : TTglyph);
begin
end;


function Tgridfitter.calc_pv_em() : longint;
var sx,sy : double; 
begin
  sx := ppem_x * sqr(gs.pvx) / sqr(16384);
  sy := ppem_y * sqr(gs.pvy) / sqr(16384);
  calc_pv_em := round(64*(sx+sy));
end;

function defaultcompensator(dist : double; up : pointer) : double;
begin
  defaultcompensator := dist;
end;

function Tgridfitter.apply_engine_compensation(x,ctyp : longint) : longint;
begin
  apply_engine_compensation := x;
  if (ctyp=2) then apply_engine_compensation := round(64*whitecompensator(x/64,wcompensatorDATA));
  if (ctyp=1) then apply_engine_compensation := round(64*blackcompensator(x/64,bcompensatorDATA));
end;


procedure Tgridfitter.chkZonePtIndex(ptidx,zone : longint);
begin
  if (ptidx<0)or(ptidx>get_last_zone_point_idx(zone)) then begin
    raise Tttprogexception.create(
      'zone'+inttostr(zone)+'-index '+
       inttostr(ptidx)+' out of bounds'    );
  end;
end;

function Tgridfitter.get_last_zone_point_idx(z : longint) : longint;
begin
  if (z=0) then get_last_zone_point_idx := high(zone0curx)
  else get_last_zone_point_idx := last_z1_pt;
end;

function Tgridfitter.get_org_zonepoint_x(ptidx,zone : longint) : F26DOT6;
begin
  chkZonePtIndex(ptidx,zone);
  if (zone=0) then get_org_zonepoint_x := zone0orgx[ptidx]
              else get_org_zonepoint_x := zone1orgx[ptidx];
end;

function Tgridfitter.get_org_zonepoint_y(ptidx,zone : longint) : F26DOT6;
begin
  chkZonePtIndex(ptidx,zone);
  if (zone=0) then get_org_zonepoint_y := zone0orgy[ptidx]
              else get_org_zonepoint_y := zone1orgy[ptidx];
end;

function Tgridfitter.get_zonepoint_x(ptidx,zone : longint) : F26DOT6;
begin
  chkZonePtIndex(ptidx,zone);
  if (zone=0) then get_zonepoint_x := zone0curx[ptidx]
              else get_zonepoint_x := zone1curx[ptidx];
end;

function Tgridfitter.get_zonepoint_y(ptidx,zone : longint) : F26DOT6;
begin
  chkZonePtIndex(ptidx,zone);
  if (zone=0) then get_zonepoint_y := zone0cury[ptidx]
              else get_zonepoint_y := zone1cury[ptidx];
end;

procedure Tgridfitter.set_zonepoint_x(ptidx,zone : longint; v : F26DOT6);
begin
  chkZonePtIndex(ptidx,zone);
  if (zone=0) then zone0curx[ptidx] := v
              else zone1curx[ptidx] := v;
  if (zone=1) then begin
    if not(gs.fvx=0) then zone1touchX[ptidx] := true;
  end;
end;

procedure Tgridfitter.set_zonepoint_y(ptidx,zone : longint; v : F26DOT6);
begin
  chkZonePtIndex(ptidx,zone);
  if (zone=0) then zone0cury[ptidx] := v
              else zone1cury[ptidx] := v;
  if (zone=1) then begin
    if not(gs.fvy=0) then zone1touchY[ptidx] := true;
  end;
end;

procedure Tgridfitter.set_org_zonepoint_x(ptidx,zone,v : longint);
begin
  chkZonePtIndex(ptidx,zone);
  if not(zone=0) then raise Tttprogexception.create('set glyph-zone org x');
  zone0orgx[ptidx] := v
end;

procedure Tgridfitter.set_org_zonepoint_y(ptidx,zone,v : longint);
begin
  chkZonePtIndex(ptidx,zone);
  if not(zone=0) then raise Tttprogexception.create('set glyph-zone org y');
  zone0orgy[ptidx] := v
end;




procedure Tgridfitter.set_touched_z1x(p : longint; t : boolean);
begin
  chkZonePtIndex(p,1);
  zone1touchX[p] := t;
end;

procedure Tgridfitter.set_touched_z1y(p : longint; t : boolean);
begin
  chkZonePtIndex(p,1);
  zone1touchY[p] := t;
end;

function Tgridfitter.is_touched_z1x(p : longint) : boolean;
begin
  chkZonePtIndex(p,1);
  is_touched_z1x := zone1touchX[p];
end;

function Tgridfitter.is_touched_z1y(p : longint) : boolean;
begin
  chkZonePtIndex(p,1);
  is_touched_z1y := zone1touchY[p];
end;








procedure Tgridfitter.do_delta_instr(b : byte);
var i,n,em,k,a,aem,refem,stp : longint; onpt : boolean;
begin
  onpt := true;
  if (b>=$73)and(b<=$75) then onpt := false; // use cvt
  em := 0;
  case b of
    $71 : em := 1;    
    $72 : em := 2;
    $74 : em := 1;    
    $75 : em := 2;
  end;
  em := em*16 + gs.delta_base;
  n := pop_stack();
  if (n>0) then begin
    { SPECUNCLEAR: should refem really use pv_em? or should it use pointsize? }
    refem := calc_pv_em() DIV 64; 
    stp := 64 shr gs.delta_shift;
    for i := 1 to n do begin
      k := pop_stack();
      a := pop_stack();
      aem := em+(a shr 4);
      if (aem=refem) then begin
        a := a and 15;
        a := a-8;
        if (a>=0) then a := a+1;
        a := a*stp;
        if (onpt) then mov_point_fv(k,gs.zp0,a)
                  else mov_cvt(k,a);
      end;
    end;
  end;
end;



procedure Tgridfitter.set_onoffcurve(b : byte);
var i,j : longint;
begin
  if (b=$80) then begin
    // FLIPPT instruction
    while (gs.loopcount>0) do begin
      i := pop_stack();
      if (gs.zp0=1) then begin
        chkZonePtIndex(i,gs.zp0);
        zone1onoff[i] := not(zone1onoff[i]);
      end;
      dec(gs.loopcount);
    end;
    gs.loopcount := 1;
  end else begin
    // b=$82 : FLIPRGOFF-instruction
    // b=$81 : FLIPRGON-instruction
    j := pop_stack();
    i := pop_stack();
    if (gs.zp0=1) then begin
      for i := i to j do begin
        chkZonePtIndex(i,gs.zp0);
        zone1onoff[i] := ((b and 1)>0); 
      end;
    end;
  end;
end;

procedure Tgridfitter.do_readwrite_store(b : byte);
begin
  inherited ;
end;




procedure Tgridfitter.do_WCVTP(b : byte);
var i,j : longint; 
begin
  i := pop_stack();
  j := pop_stack();
  if (b=$70) then begin
    i := doscale(i);
  end;
  cvt.setValue(j,i);
end;








procedure Tgridfitter.do_SCANCTRL(b : byte);
var v,ppemthresh,ppem : longint; 
begin
  v := pop_stack() and 65535;
  ppemthresh := v and 255;
  ppem := calc_pv_em() DIV 64; 
  if (((v shr  8) and 1)=1) then begin
    if (ppem<=ppemthresh) then gs.scanctrl := true;
  end;
  if (((v shr  9) and 1)=1) then begin
//    if (glyph rotated) then gs.scanctrl := true;
  end;
  if (((v shr 10) and 1)=1) then begin
//    if (glyph streched) then gs.scanctrl := true;
  end;
  if (((v shr 11) and 1)=1) then begin
    if (ppem>ppemthresh) then gs.scanctrl := false;
  end;
  if (((v shr 12) and 1)=1) then begin
//    if (glyph rotated) then gs.scanctrl := false;
  end;
  if (((v shr 13) and 1)=1) then begin
//    if (glyph streched) then gs.scanctrl := false;
  end;
end;


procedure Tgridfitter.do_GETINFO(b : byte);
var i,r : dword;
begin 
  i := pop_stack();
//writeln('GETINFO:',hexstr(i,8)); readln;
  r := 0;
  if (((i shr 0) and 1)=1) then begin
    r := r or (version_for_GETINFO and 255);
  end;
  if (((i shr 1) and 1)=1) then begin
    if (glyph_rotated) then r := r or (1 shl 8);
  end;
  if (((i shr 2) and 1)=1) then begin
    if (glyph_streched) then r := r or (1 shl 9);
  end;
  if (((i shr 3) and 1)=1) then begin
// if (variations) then r := r or (1 shl 10);
  end;
  if (((i shr 4) and 1)=1) then begin
    r := r or (1 shl 11);
  end;
  if (((i shr 5) and 1)=1) then begin
    if (render_greyscale) then r := r or (1 shl 12);
  end;
  if (((i shr 6) and 1)=1) then begin
//    if (clear type) then r := r or (1 shl 13);
  end;
  if (((i shr 7) and 1)=1) then begin
//    if (clear type) then r := r or (1 shl 14);
  end;
  if (((i shr 8) and 1)=1) then begin
//    if (clear type) then r := r or (1 shl 15);
  end;
  if (((i shr 9) and 1)=1) then begin
//    if (clear type) then r := r or (1 shl 16);
  end;
  if (((i shr 10) and 1)=1) then begin
//    if (clear type) then r := r or (1 shl 17);
  end;
  if (((i shr 11) and 1)=1) then begin
//    if (clear type) then r := r or (1 shl 18);
  end;
  if (((i shr 12) and 1)=1) then begin
//    if (clear type) then r := r or (1 shl 19);
  end;
  push_stack(r);
end;

procedure Tgridfitter.do_SCANTYPE(b : byte);
var v : longint;
begin
  v := pop_stack() and 65535;
  case v of
    0 : gs.scantype := scanrule1 or scanrule2 or scanrule3;
    1 : gs.scantype := scanrule1 or scanrule2 or scanrule4;
    2,3 : gs.scantype := scanrule1 or scanrule2;
{$IFDEF support_scanrule56}
    4 : gs.scantype := scanrule1 or scanrule2 or scanrule5;
    5 : gs.scantype := scanrule1 or scanrule2 or scanrule6;
    6,7 : gs.scantype := scanrule1 or scanrule2;
{$endif}
  end;
end;

procedure Tgridfitter.do_MP_S_PPEM(b : byte);
begin
  push_stack(calc_pv_em() DIV 64);
end;

procedure Tgridfitter.do_INSTCTRL(b : byte);
var s,v : dword;
begin
  s := pop_stack();
  v := pop_stack();
  if (s=1) then begin
    if (v=0) then _enable_hinting()
    else _disable_hinting();
  end;
  if (s=2)and(current_prog_type=1) then begin
    if (v=0) then glyphs_use_defaults := false
             else glyphs_use_defaults := true;
  end;
end;







procedure Tgridfitter.load_cvt_from_font();
var i : longint;
begin
  cvt_tab.setLength(0);
  cvt_tab.setLength(high(font.cvt)+1);
  for i := 0 to high(font.cvt) do begin
    cvt_tab.setValue(i, longint(font.cvt[i]) );
  end;
end;

procedure Tgridfitter.load_cvt_prog_from_font();
begin
  if not(cvtprog=nil) then cvtprog.destroy();
  cvtprog := TTprogram.create(fontprog);
  setlength(cvtprog.instructions,high(font.prepprog)+1);
  move( font.prepprog[0],
        cvtprog.instructions[0],
        high(font.prepprog)+1   );
end;

procedure Tgridfitter.load_font_prog_from_font();
begin
  if not(fontprog=nil) then fontprog.destroy();
  fontprog := TTprogram.create(nil);
  setlength(fontprog.instructions,high(font.fontprog)+1);
  move( font.fontprog[0],
        fontprog.instructions[0],
        high(font.fontprog)+1   );
end;

procedure Tgridfitter.restore_cvt();
begin
  if cvt.is_dirty() then cvt.copyFrom(cvt_prog);
end;

procedure Tgridfitter.set_font(f : TTfont; emumode : longint);
begin
  use_signed_compare  := true;

  current_prog_type := 0;
  hinting_disabled_by_font := false;
  hinting_disabled_by_prep := false;
  font := f;
  unitsPerEm := font.unitsPerEm;
  ppem_x := 0;
  ppem_y := 0;
  pointsize := 0;
  progstorage.setLength(font.maxprofile.maxStorage);

  set_calldepth_limit(CALLDEPTH_LIMIT);
  set_stacklimit(font.maxprofile.maxStackElements);
  dimzone(0,font.maxprofile.maxTwilightPoints);
  dimzone(1,max(font.maxprofile.maxPoints,font.maxprofile.maxCompositePoints)+4);

  load_cvt_from_font();

  load_font_prog_from_font();
  load_cvt_prog_from_font();
  do_exec(fontprog);
end;

function Tgridfitter.doscale(v : longint) : longint;
begin
  doscale := round( v*64*pointsize/unitsPerEm );
end;


procedure Tgridfitter.execute_cvt_prog();
var i,j,v,v2 : longint;
begin
  current_prog_type := 1;
  cvt.copyFrom(cvt_tab);
  j := cvt.getLength()-1;
  for i := 0 to j do begin
    v := cvt.getValue(i);
    v2 := doscale(v);
    cvt.setValue(i,v2);
  end;
  hinting_disabled_by_prep := hinting_disabled_by_font;
  if not(hinting_disabled_by_font) then begin
    if not(cvtprog=nil) then do_exec(cvtprog);
  end;
  cvt.mark_clean();
  cvt_prog.copyFrom(cvt);
end;

function subglyph_rotated(sg : Psubglyph) : boolean;
begin
  subglyph_rotated := (sg^.sxy<>0)or(sg^.syx<>0);
end;

function subglyph_streched(sg : Psubglyph) : boolean;
begin
  subglyph_streched := (abs(sg^.sxy)<>16384) or (abs(sg^.syx)<>16384);
end;


procedure Tgridfitter.execute_glyph_prog(g0,g : TTglyph; p0,np, c0,nc : longint);
var sg : Psubglyph; gsg : TTglyph; p,c,pc,cc : longint; gprog : TTprogram;
var saved_x,saved_y, saved_p : double; saved_rot, saved_str : boolean;
begin
  if not(all_subglyphprogs_ok) then exit;
  saved_x := ppem_x;
  saved_y := ppem_y;
  saved_p := pointsize;
  saved_rot := glyph_rotated;
  saved_str := glyph_streched;

  sg := g.subglyphs;
  p := p0;
  c := c0;
    // gridfit the subglyphs, if any
  while not(sg=nil) do begin
    ppem_x := saved_x * sg^.sx/16384;
    ppem_y := saved_y * sg^.sy/16384;
    pointsize := saved_p * sg^.sx/16384; // ??
    glyph_rotated  := saved_rot or subglyph_rotated(sg);
    glyph_streched := saved_str or subglyph_streched(sg);
    gsg := font.getGlyph(sg^.idx);
    pc := high(gsg.coordx)+1;
    cc := gsg.numberOfContours;
    execute_glyph_prog(g0,gsg,p,pc,c,cc);
    c := c + cc;
    p := p + pc;
    sg := sg^.next;
  end;
  ppem_x := saved_x;
  ppem_y := saved_y;
  pointsize := saved_p;
  glyph_rotated  := saved_rot;
  glyph_streched := saved_str;


  // copy scaled points to gridfitter and generate advance points
  take_glyphdata(g0,g,p0,np,c0,nc);
  if (high(g.instructions)>=0) then begin
    // exec
    restore_cvt();
    gprog := TTprogram.create(cvtprog, g.instructions);
    zone1glyph := g;
    mainglyph := g0;

    onEnterGlyphInstructions(g,g0);


    try do_exec(gprog);
    Except
      on Tttprogexception do all_subglyphprogs_ok := false;
    end;

    gprog.destroy();
    zone1glyph := nil;
  end;
  // copy gridfitted points back to scaled
  if all_subglyphprogs_ok then untake_glyphdata(g0,g,p0,np,c0,nc);
  if (g0=g) then begin
    // copy advance points to glyph
  end;
end;

procedure Tgridfitter.take_glyphdata(g0,g : TTglyph; p0,np,c0,nc : longint);
var i,j,len : longint; lsb,aw : F26DOT6;
begin
  g.scale(ppem_x,ppem_y,unitsPerEm);

  len := high(g.coordx)+1;
  if ((high(zone1orgx)+1)<(len+4))
  then raise Tttprogexception.create('insufficient pt-zone1');
  j := 0;
  for i := 0 to len-1 do begin
    if (i>high(g.scoordx)) then begin
      zone1orgx[i] := g0.scoordx[i+p0];
      zone1orgy[i] := g0.scoordy[i+p0];
      zone1onoff[i] := g0.sonoffcurve[i+p0];
    end else begin
      zone1orgx[i] := g.scoordx[i];
      zone1orgy[i] := g.scoordy[i];
      zone1onoff[i] := g.sonoffcurve[i];
    end;
    inc(j);
  end;
  i := g.idx;

  { horizontal phantom points }
  g.scale_metrics(ppem_x,ppem_y,unitsPerEm);
  lsb := g.sxmin -g.slsb;
  zone1orgx[j] := lsb;
  zone1orgy[j] := 0;
  zone1onoff[j] := false;
  inc(j);
  zone1orgx[j] := lsb+g.saw;
  zone1orgy[j] := 0;
  zone1onoff[j] := false;
  inc(j);

  { vertical phantom points }
  lsb := round(pointsize*64);
  aw  := 0;
  zone1orgx[j] := 0;
  zone1orgy[j] := lsb;
  zone1onoff[j] := false;
  inc(j);
  zone1orgx[j] := 0;
  zone1orgy[j] := aw;
  zone1onoff[j] := false;

  for i := j downto 0 do begin
    zone1curx[i] := zone1orgx[i];
    zone1cury[i] := zone1orgy[i]; 
    zone1touchX[i] := false;
    zone1touchy[i] := false;
  end;
  zone1curx[len+1] := round(zone1orgx[len+1]/64)*64;  
  last_z1_pt := j;
end;

procedure Tgridfitter.untake_glyphdata(g0,g : TTglyph; p0,np,c0,nc : longint);
var i,c,len : longint; { lsb,aw : F26DOT6;}
begin
  c := 0;
  // discard "ugly" computations
  len := high(g.coordx)+1;
  for i := 0 to len-1 do begin
    if (zone1curx[i]<-640*ppem_x)
    or (zone1cury[i]<-640*ppem_y)
    or (zone1curx[i]>640*ppem_x)
    or (zone1cury[i]>640*ppem_y) then begin
      all_subglyphprogs_ok := false;
      exit;
    end;
  end;
  for i := 0 to len-1 do begin
    g0.scoordx[i+p0] := zone1curx[i];
    g0.scoordy[i+p0] := zone1cury[i];
    g0.sonoffcurve[i+p0] := zone1onoff[i];
    if (i=g.endPtsOfContours[c]) then begin
      inc(c); 
    end;
  end;
  g.saw := zone1curx[len+1]-zone1curx[len];
  g.slsb := g.sxmin -zone1curx[len];
  last_z1_pt := -1;
end;


function Tgridfitter.gridfit_glyph(g : TTglyph) : boolean;
begin
  glyph_rotated  := false;
  glyph_streched := false;
  current_prog_type := 2;
  { don't instruct empty glyphs.
    also don't instruct glyphs if the prep disabled hinting   }
  all_subglyphprogs_ok := (high(g.coordX)>=0) and not(hinting_disabled_by_prep);
  if all_subglyphprogs_ok then begin
    g.scale(ppem_x,ppem_y,unitsPerEm);
    execute_glyph_prog(g,g,0,high(g.coordx)+1,0,g.numberOfContours);
  end;
  if not(all_subglyphprogs_ok) then begin
    { after instructions, if there went something wrong,
      take original outline  }
    g.scale(ppem_x,ppem_y,unitsPerEm);
  end;
  zone1glyph := nil;
  mainglyph := nil;
  gridfit_glyph := all_subglyphprogs_ok;
end;


procedure Tgridfitter.set_ppem(x,y,ps : longint);
begin
  if (x<>ppem_x)or(y<>ppem_y)or(ps<>pointsize) then begin 
    ppem_x := x;
    ppem_y := y;
    pointsize := ps;
    execute_cvt_prog();
  end;
end;

function Tgridfitter.do_exec(prog : TTprogram) : boolean;
begin
  clear_stack();
  if (current_prog_type=2) then begin
    if (glyphs_use_defaults) then begin
      reset_gs();
      gs.scantype := gscvt.scantype;
      gs.scanctrl := gscvt.scanctrl;
//      gs.control_value_cut_in := gscvt.control_value_cut_in;
    end else begin 
      gs := gscvt;
      gs.zp0 := 1;
      gs.zp1 := 1;
      gs.zp2 := 1;
      gs.pvx := 16384;
      gs.dvx := gs.pvx;
      gs.fvx := gs.pvx;
      gs.pvy := 0;
      gs.dvy := 0;
      gs.fvy := 0;
      gs.rp0 := 0;
      gs.rp1 := 0;
      gs.rp2 := 0;
    end;
  end else reset_gs();
  initzone0;
  if (use_storage_chain) then begin
    case current_prog_type of
      0 : progstorage.initData(0);
      1 : progstorage.copyFrom(stor_font);
      2 : progstorage.copyFrom(stor_prep);
    end;
  end {else progstorage.initData(0)};
  onExec();
  do_exec := inherited do_exec(prog);
  if (current_prog_type < 2) then gscvt := gs;
  if (current_prog_type = 1) then zonecopy_0_to_C();
  if (current_prog_type = 0) then stor_font.copyFrom(progstorage);
  if (current_prog_type = 1) then stor_prep.copyFrom(progstorage);
end;


procedure Tgridfitter.onExec();
begin
end;

procedure Tgridfitter.initzone0;
var i : longint;
begin
  {if ((current_prog_type=2) and glyphprog_zone0_from_prep) then begin
    zonecopy_C_to_0();
  end else begin }
    for i := 0 to high(zone0curx) do begin
      zone0orgx[i] := 0;
      zone0orgy[i] := 0;
    end;
  {end;}
  for i := 0 to high(zone0curx) do begin
    zone0curx[i] := zone0orgx[i];
    zone0cury[i] := zone0orgy[i];
  end;
end;


procedure Tgridfitter.zonecopy_0_to_C();
var i : longint;
begin
  for i := 0 to high(zone0orgx) do begin
    zoneCorgx[i] := zone0curx[i];
    zoneCorgy[i] := zone0cury[i];
  end;
end;

procedure Tgridfitter.zonecopy_C_to_0();
var i : longint;
begin
  for i := 0 to high(zone0orgx) do begin
    zone0orgx[i] := zoneCorgx[i];
    zone0orgy[i] := zoneCorgy[i];
  end;
end;






procedure Tgridfitter.dimzone(z,n : longint);
begin
  if (z=0) then begin
    setlength(zone0curx,n);
    setlength(zone0cury,n);
    setlength(zone0orgx,n);
    setlength(zone0orgy,n);
    setlength(zoneCorgx,n);
    setlength(zoneCorgy,n);
  end else begin
    setlength(zone1curx,n);
    setlength(zone1cury,n);
    setlength(zone1orgx,n);
    setlength(zone1orgy,n);
    setlength(zone1touchX,n);
    setlength(zone1touchY,n);
    setlength(zone1onoff,n);
  end;
end;

constructor Tgridfitter.create();
begin
  inherited create();
  fontprog := nil;
  cvtprog  := nil;
  cvt      := Ttdatastore.create();
  cvt_prog := Ttdatastore.create();
  cvt_tab  := Ttdatastore.create();
  progstorage := Ttdatastore.create();
  stor_font   := Ttdatastore.create();
  stor_prep   := Ttdatastore.create();
  unitsPerEm := 0;
  whitecompensator := @defaultcompensator;
  blackcompensator := @defaultcompensator;
  wcompensatorDATA := nil;
  bcompensatorDATA := nil;

  setlength(zone0curx,0);
  setlength(zone0cury,0);
  setlength(zone0orgx,0);
  setlength(zone0orgy,0);
  setlength(zoneCorgx,0);
  setlength(zoneCorgy,0);
  setlength(zone1curx,0);
  setlength(zone1cury,0);
  setlength(zone1orgx,0);
  setlength(zone1orgy,0);
  setlength(zone1touchX,0);
  setlength(zone1touchY,0);
  setlength(zone1onoff,0);
  zone1glyph := nil;
  mainglyph := nil;
  allow_mindist_autoflip := false;
  allow_mindist_autoflip := true;
  last_z1_pt := -1;
  glyphs_use_defaults := init_glyphs_use_defaults;
  setAcceptablePVFVmismatch(max_acceptable_pv_fv_angle);
  version_for_GETINFO := version_for_GETINFO_instruction;
end;

destructor Tgridfitter.destroy();
begin
  if not( cvtprog=nil) then  cvtprog.destroy();
  if not(fontprog=nil) then fontprog.destroy();
  cvt.destroy();
  cvt_tab.destroy();
  cvt_prog.destroy();
  progstorage.destroy();
  stor_font.destroy();
  stor_prep.destroy();
  inherited destroy();
end;

procedure Tgridfitter._disable_hinting(); // to be called from instructions
begin
  case current_prog_type of
    0 : hinting_disabled_by_font := true;
    1 : hinting_disabled_by_prep := true;
    2 : all_subglyphprogs_ok := false;
  end;
end;

procedure Tgridfitter._enable_hinting(); // to be called from instructions
begin
  case current_prog_type of
    0 : hinting_disabled_by_font := false;
    1 : hinting_disabled_by_prep := false;
//  2 : all_subglyphprogs_ok := false; 
// glpyhs aren't allowed to  take back a decision 
// to disable their instructions
  end;
end;

function Tgridfitter.getScantype() : longint;
begin
  if gs.scanctrl then begin
    getScantype := gs.scantype;
  end else getScantype := 3;
end;

function Tgridfitter.getFitStatus() : boolean;
begin
  getFitStatus := all_subglyphprogs_ok;
end;



begin
end.


