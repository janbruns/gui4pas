{$mode objfpc}

UNIT ttrender;




{ render 1st order polyline-polygons


  When rasterizing contours this implementation works on a 1/256'th 
  subpixel raster for improved line-darwing precision, coverage 
  guessing, so the incoming stream of points coordinates need
  to be prescaled by 256 to match the pixel-raster.

  
}


interface
uses ttcontours;

type
Tint32arr = array of longint;
Tintarr2d = array of Tint32arr;


ttrenderer = class
  constructor create();
  destructor destroy(); override;

  procedure draw_contours(cl : TcontourList); virtual;

  procedure sumup();
  procedure setres(x,y : longint); virtual;
  procedure clear(); virtual;

  procedure setzero(x,y : longint);
  procedure setClip(x0,y0,dx,dy : TTcoord);


  { Guess how much of a pixel is covered by a glyph.
    Return value 0.0 means 100% covered, fully black
    Return value 0.5 means  50% covered, 
    Return value 1.0 means   0% covered, totally white
  }
  function guess_pixel_coverage(x,y : longint) : double;

  function scan1a(x,y : longint) : boolean;
  function scan1b(x,y : longint) : boolean;
  function scan1(x,y : longint) : boolean;
  function scan2(x,y : longint) : boolean;


  function dropout3a(x,y : longint) : boolean;
  function dropout3b(x,y : longint) : boolean;
  function dropout4a(x,y : longint) : boolean;
  function dropout4b(x,y : longint) : boolean;

  function decide_pixel(x,y,scanmode : longint) : boolean;


  procedure set_subpixel_offset(dx,dy : longint);


  private

  procedure take_xy(x,y : TTcoord);
  procedure take_yx(x,y : TTcoord);
  procedure take_x(x,y : TTcoord);
  function calc_x(y : TTcoord) : TTcoord;
  function do_floor(a : TTcoord) : TTcoord;
  function do_ceil(a : TTcoord) : TTcoord;

  function firsty() : TTcoord;
  function nexty(y : TTcoord) : TTcoord;
  function doney(y : TTcoord) : boolean;
  function doneyneq(y : TTcoord) : boolean;
  procedure draw_segment(a,b : TTvec);
  procedure draw_segment_xy(a,b : TTvec);
  procedure draw_segment_yx(a,b : TTvec);
  procedure draw_contour(c : Tcontour);

  procedure apply_subpixel_mask(var v : TTvec);
  procedure apply_subpixel_offset(var v : TTvec);


  public dbgprint : boolean;

  public
  guess_maxblack, guess_minwhite : double;

  private
  px_count, 
  py_count : array of shortint;

  px_exakt,
  px_upcnt,
  py_upcnt, 
  px_dist, 
  py_dist : array of byte;

  xres,yres : longint;

  spox,spoy,
  zerox,zeroy : TTcoord;

  start,dirvec : TTvec;
  clipxmax,clipxmin,
  clipymax,clipymin,
  y_per_line,
  pixelsize,pixelsize2,pixelsize22 : TTcoord;

end;


ttmultirenderer = class(ttrenderer)
  constructor create();

  procedure setres(x,y : longint); override;
  procedure check_size(x,y : TTcoord); 

  procedure clearall();
  procedure make_bwcol(scanmode : longint);
  procedure addColGuess();
  procedure draw(scanmode : longint; cl : TcontourList);

  procedure setMultisample(multi : longint);
  function getgrey(x,y : longint) : longint;
  function getbw(x,y : longint) : byte;

  public
  pixelsg  : array of longint;
  pixelsbw : array of byte;

  maxcolsum,
  maxwhite, gminwhite, ginvminwhite, colerr : longint;
  private ms : longint;
end;


var weird_secret_bit : boolean;


implementation
uses math;

{$COPERATORS ON}





constructor ttrenderer.create();
begin
  inherited create();
  setlength(px_count,0);
  setlength(py_count,0);
  setlength(px_upcnt,0);
  setlength(py_upcnt,0);
  setlength(px_dist,0);
  setlength(py_dist,0);
  setlength(px_exakt,0);
  xres := 0;
  yres := 0;
  pixelsize  := 256;
  pixelsize2 := pixelsize DIV 2;
  pixelsize22 := pixelsize2*pixelsize2;
  dbgprint := true;
end;

destructor ttrenderer.destroy();
begin
  inherited destroy();
end;




procedure ttrenderer.setres(x,y : longint);
begin
  xres := x;
  yres := y;
  setlength(px_count,y*x);
  setlength(py_count,y*x);
  setlength(px_upcnt,y*x);
  setlength(py_upcnt,y*x);
  setlength(px_dist,y*x);
  setlength(py_dist,y*x);
  setlength(px_exakt,y*x);
  setClip(0,0,xres,yres);
  clear();
end;

procedure ttrenderer.setClip(x0,y0,dx,dy : TTcoord);
begin
  clipxmin := min(max(x0,0),xres-1);
  clipymin := min(max(y0,0),yres-1);
  clipxmax := min(max(x0+dx,clipxmin),xres-1);
  clipymax := min(max(y0+dy,clipymin),yres-1);
end;

procedure ttrenderer.clear();
var y,x,c : longint;
begin
  c := clipxmax-clipxmin+1;
  for y := clipymin to clipymax do begin
    x := y*xres+clipxmin;
    fillbyte(px_count[x],c,0);
    fillbyte(py_count[x],c,0);
    fillbyte(px_upcnt[x],c,0);
    fillbyte(py_upcnt[x],c,0);
    fillbyte(px_exakt[x],c,0);
    fillbyte(px_dist[x],c,128);
    fillbyte(py_dist[x],c,128);
  end;
end;


procedure ttrenderer.sumup();
var y,x,xr,c,w : longint; p : Pshortint;
begin
  xr := xres;
  c := (clipxmax-clipxmin);
  for y := clipymax downto clipymin do begin
    p := @px_count[y*xr+0+clipxmin];
    w := p^;
    inc(p);
    for x := 1 to c do begin
      w := w + p^;
      p^ := w;
      inc(p);
    end;
  end;
  c := (clipymax-clipymin);
  for x := clipxmin to clipxmax do begin
    p := @py_count[clipymin*xr +x];
    w := p^;
    inc(p,xr);
    for y := 1 to c do begin
      w := w + p^;
      p^ := w;
      inc(p,xr);
    end;
  end;
end;


procedure ttrenderer.take_xy(x,y : TTcoord);
var d,dist : longint; 
begin
  if (y_per_line>0) then d := 1 else d := -1;
  dist := x-do_floor(x);
  x := (x+pixelsize-1) div pixelsize;
  y := y div pixelsize;
  x := x + zerox;
  y := y + zeroy;
  x := max(min(x,clipxmax),clipxmin);
  y := max(min(y,clipymax),clipymin);
//  x := max(0,min(x,xres-1));
//  y := max(0,min(y,yres-1));
  inc(px_upcnt[y*xres+x]);
  inc(px_count[y*xres+x],d);
  dist -= pixelsize div 2;
  dec(x);
  if (dist>=0) then inc(x);
  x := max(min(x,clipxmax),clipxmin);
//  x := max(0,min(x,xres-1));
  px_dist[y*xres+x] := min(px_dist[y*xres+x], abs(dist));  
end;

procedure ttrenderer.take_yx(x,y : TTcoord);
var d,dist : longint; 
begin
  if (y_per_line>0) then d := 1 else d := -1;
  dist := y-do_floor(y);
  x := x div pixelsize;
  y := (y + pixelsize-1) div pixelsize;
  x := x + zerox;
  y := y + zeroy;
  x := max(min(x,clipxmax),clipxmin);
  y := max(min(y,clipymax),clipymin);
//  x := max(0,min(x,xres-1));
//  y := max(0,min(y,yres-1));
  inc(py_upcnt[y*xres+x]);
  inc(py_count[y*xres+x],d);
  dist -= pixelsize div 2;
  dec(y);
  if (dist>=0) then inc(y);
  y := max(min(y,clipymax),clipymin);
//  y := max(0,min(y,yres-1));
  py_dist[y*xres+x] := min(py_dist[y*xres+x], abs(dist));  
end;

procedure ttrenderer.take_x(x,y : TTcoord);
begin
  if (x-do_floor(x))>0 then exit;
  if (y-do_floor(y))>0 then exit;
  x := x div pixelsize;
  y := y div pixelsize;
  x := x + zerox;
  y := y + zeroy;
  x := max(min(x,clipxmax),clipxmin);
  y := max(min(y,clipymax),clipymin);
//  x := max(0,min(x,xres-1));
//  y := max(0,min(y,yres-1));
  inc(px_exakt[y*xres+x]);
end;


function ttrenderer.calc_x(y : TTcoord) : TTcoord;
var x : int64;
begin
  x := y-start.y;
  x := x * dirvec.x;
  x := x DIV dirvec.y;
  calc_x := x +start.x;  
end;

function ttrenderer.do_floor(a : TTcoord) : TTcoord;
var rem : TTcoord;
begin
  rem := a and (pixelsize-1);
  do_floor := a-rem;
end;

function ttrenderer.do_ceil(a : TTcoord) : TTcoord;
begin
  do_ceil := do_floor(a+pixelsize-1);
end;


function ttrenderer.firsty() : TTcoord;
begin
  if (y_per_line<0) then begin
    firsty := do_floor(start.y);
  end else begin
    firsty := do_ceil(start.y);
  end;
end;

function ttrenderer.nexty(y : TTcoord) : TTcoord;
begin
  nexty := y + y_per_line;
end;

function ttrenderer.doneyneq(y : TTcoord) : boolean;
begin
  if (y_per_line>0) then begin
    doneyneq := (y>=(start.y+dirvec.y));
  end else begin
    doneyneq := (y<=(start.y+dirvec.y));
  end;
end;

function ttrenderer.doney(y : TTcoord) : boolean;
begin
  if (y_per_line>0) then begin
    doney := (y>(start.y+dirvec.y));
  end else begin
    doney := (y<(start.y+dirvec.y));
  end;
end;


procedure ttrenderer.draw_segment_xy(a,b : TTvec);
var y,x : TTcoord;
begin
{ reduce y-resolution by 1 bit, so there's no
  care needed about exactly pixel-aligned data.
  no prob, just superscale about 1 one more bit
  at no memory expense. }
  apply_subpixel_offset(a);
  apply_subpixel_offset(b);
  apply_subpixel_mask(a);
  apply_subpixel_mask(b);
  dirvec.x := b.x-a.x;
  dirvec.y := b.y-a.y;
//  if dbgprint then writeln('render segment (',a.x,'/',a.y,',',b.x,'/',b.y,') dy=',dirvec.y);
  start.x := a.x;
  start.y := a.y;
  y_per_line := pixelsize;
  if (dirvec.y<0) then y_per_line := -y_per_line;
  if not(dirvec.y=0) then begin
    y := firsty();
    while not(doney(y)) do begin
      x := calc_x(y);
      take_xy(x,y);
      y := nexty(y);
    end;
  end;
end;

procedure ttrenderer.draw_segment_yx(a,b : TTvec);
var y,x : TTcoord;
begin
{ Similar to normal draw segment, but for vertical scanlines.
}
  apply_subpixel_offset(a);
  apply_subpixel_offset(b);
  apply_subpixel_mask(a);
  apply_subpixel_mask(b);
  dirvec.x := b.y-a.y;
  dirvec.y := b.x-a.x;
//  if dbgprint then writeln('render segment (',a.x,'/',a.y,',',b.x,'/',b.y,') dy=',dirvec.y);
  start.x := a.y;
  start.y := a.x;
  y_per_line := pixelsize;
  if (dirvec.y<0) then y_per_line := -y_per_line;
  if not(dirvec.y=0) then begin
    y := firsty();
    while not(doney(y)) do begin
      x := calc_x(y);
      take_yx(y,x);
      y := nexty(y);
    end;
  end;
end;

procedure ttrenderer.draw_segment(a,b : TTvec);
var y,x : TTcoord;
begin
  apply_subpixel_offset(a);
  apply_subpixel_offset(b);
  dirvec.x := b.x-a.x;
  dirvec.y := b.y-a.y;
  start.x := a.x;
  start.y := a.y;
  y_per_line := pixelsize;
  if (dirvec.y<0) then y_per_line := -y_per_line;
  if not(dirvec.y=0) then begin
    y := firsty();
    while not(doney(y)) do begin
      x := calc_x(y);
      take_x(x,y);
      y := nexty(y);
    end;
  end;
end;



procedure ttrenderer.draw_contour(c : Tcontour);
var i,len : longint;
begin
  len := c.pointCount();
  if dbgprint then writeln('render contour with ',len,' points');
  if (len<2) then exit;
  for i := 0 to len-2 do draw_segment_xy(c.getPoint(i),c.getPoint(i+1));
  draw_segment_xy(c.getPoint(len-1),c.getPoint(0));
  for i := 0 to len-2 do draw_segment_yx(c.getPoint(i),c.getPoint(i+1));
  draw_segment_yx(c.getPoint(len-1),c.getPoint(0));
  for i := 0 to len-2 do draw_segment(c.getPoint(i),c.getPoint(i+1));
  draw_segment(c.getPoint(len-1),c.getPoint(0));
end;





procedure ttrenderer.draw_contours(cl : TcontourList);
var c : Tcontour;
begin
  if dbgprint then writeln('render contours');
  c := cl.first_contour();
  while not(c=nil) do begin
    draw_contour( c );
    c := cl.next_contour(c);
  end;
end;


procedure ttrenderer.apply_subpixel_mask(var v : TTvec);
begin
  v.x := v.x or 1;
  v.y := v.y or 1;
end;

procedure ttrenderer.apply_subpixel_offset(var v : TTvec);
begin
  v.x += spox;
  v.y += spoy;
end;

procedure ttrenderer.set_subpixel_offset(dx,dy : longint);
begin
  spox := dx;
  spoy := dy;
end;

procedure ttrenderer.setzero(x,y : longint);
begin
  zerox := x;
  zeroy := y;
end;





function ttrenderer.scan1a(x,y : longint) : boolean;
begin
  x := max(0,min(x,xres-1));
  y := max(0,min(y,yres-1));
  scan1a := (px_count[y*xres+x]<>0);
end;

function ttrenderer.scan1b(x,y : longint) : boolean;
begin
  x := max(0,min(x,xres-1));
  y := max(0,min(y,yres-1));
  scan1b := (py_count[y*xres+x]<>0);
end;

function ttrenderer.scan1(x,y : longint) : boolean;
begin
  scan1 := scan1a(x,y) or scan1b(x,y);
end;

function ttrenderer.scan2(x,y : longint) : boolean;
begin
  x := max(0,min(x,xres-1));
  y := max(0,min(y,yres-1));
  scan2 := (px_exakt[y*xres+x]<>0);
end;


function ttrenderer.dropout3a(x,y : longint) : boolean;
begin
  x := max(0,min(x,xres-1));
  y := max(0,min(y,yres-1));
  dropout3a := (px_upcnt[y*xres+x]>1);
end;

function ttrenderer.dropout3b(x,y : longint) : boolean;
begin
  x := max(0,min(x,xres-1));
  y := max(0,min(y,yres-1));
  dropout3b := (py_upcnt[y*xres+x]>1);
end;



function ttrenderer.dropout4a(x,y : longint) : boolean;
var u,d : boolean;
begin
  dropout4a := false;
  x := max(1,min(x,xres-2));
  y := max(1,min(y,yres-2));
  if (px_upcnt[y*xres+x]>1) then begin
    if (px_count[y*xres+x+1]=0) then begin
      u := ((px_upcnt[(y-1)*xres+x] +px_upcnt[(y-1)*xres+x-1] +px_upcnt[(y-1)*xres+x+1])>0);
      d := ((px_upcnt[(y+1)*xres+x] +px_upcnt[(y+1)*xres+x-1] +px_upcnt[(y+1)*xres+x+1])>0);
      dropout4a := u or d;
    end;
  end;
end;

function ttrenderer.dropout4b(x,y : longint) : boolean;
var u,d : boolean;
begin
  dropout4b := false;
  x := max(1,min(x,xres-2));
  y := max(1,min(y,yres-2));
  if (py_upcnt[y*xres+x]>1) then begin
    if (py_count[(y+1)*xres+x]=0) then begin
      u := ((py_upcnt[y*xres+x-1] +py_upcnt[(y-1)*xres+x-1] +py_upcnt[(y+1)*xres+x-1])>0);
      d := ((py_upcnt[y*xres+x+1] +py_upcnt[(y-1)*xres+x+1] +py_upcnt[(y+1)*xres+x+1])>0);
      dropout4b := u or d;
    end;
  end;
end;


function ttrenderer.decide_pixel(x,y,scanmode : longint) : boolean;
var b : boolean;
begin
  b := false;
  if ((scanmode and 1)>0) then b := b or scan1(x,y);
  if ((scanmode and 2)>0) then b := b or scan2(x,y);
  if ((scanmode and 4)>0) then b := b or dropout3a(x,y);
  if ((scanmode and 4)>0) then b := b or dropout3b(x,y);
  if ((scanmode and 8)>0) then b := b or dropout4a(x,y);
  if ((scanmode and 8)>0) then b := b or dropout4b(x,y);
  decide_pixel := b;
end;


function ttrenderer.guess_pixel_coverage(x,y : longint) : double;
var i,j,a : longint;
begin
  weird_secret_bit := false;
  x := max(0,min(x,xres-1));
  y := max(0,min(y,yres-1));
  a := y*xres+x;
  if (px_count[a]=0)and(py_count[a]=0) then begin
    { pixel-center not covered by filled polygons,
      but there mey be contours crossing the pixel border. }
    i := px_dist[a];
    j := py_dist[a];
    if (i<128)and(j<128) then begin
      i := max(i,j)+(min(i,J) div 4); j := 0;
    end else begin
      if (i=128)and(j=128) then begin
        guess_pixel_coverage := 1.0;
        exit;
      end;
      i := min(i,j);
      j := i;
    end;
    { yes, we have near contours.
       guess_minwhite = 0 : looks pretty, but
       guess_minwhite = 0.5..0.75 might be more accurate }
    i := 128-i;
    guess_pixel_coverage := max(0,( guess_minwhite + (1-guess_minwhite)*i/158 ));
  end else begin
      { yes, we have near contours.
         guess_maxblack = 0..0.125 : looks pretty, but
         guess_maxblack = 0.25..0.5 might be more accurate }
    i := 1+px_dist[a];
    i *= 1+py_dist[a];
    if (i=129*129) then i := 0;
    guess_pixel_coverage := guess_maxblack*i/129/129;
  end;
end;





constructor ttmultirenderer.create();
begin
  setlength(pixelsg ,0);
  setlength(pixelsbw,0);
  inherited create();
end;

procedure ttmultirenderer.setres(x,y : longint);
begin
  setlength(pixelsg ,x*y);
  setlength(pixelsbw,x*y);
  inherited setres(x,y);
  clearall();
end;

procedure ttmultirenderer.clearall(); 
var y,x,c : longint;
begin
  c := clipxmax-clipxmin+1;
  for y := clipymin to clipymax do begin
    x := y*xres+clipxmin;
    filldword(pixelsg[x],c,0);
    fillbyte(pixelsbw[x],c,255);
  end;
  clear();
end;

procedure ttmultirenderer.draw(scanmode : longint; cl : TcontourList);
var x,y,s : longint;
begin
  clearall();
  s := 127 DIV (ms+1);
  for y := -ms to ms do begin
    for x := -ms to ms do begin
      set_subpixel_offset(x*s,y*s);
      draw_contours(cl);
      sumup();
      addColGuess();
      if (x=0)and(y=0)and(scanmode<>0) then make_bwcol(scanmode);
      if not((x=ms)and(y=x)) then clear();
    end;
  end;
end;

procedure ttmultirenderer.addColGuess();
var x,y,a,i,j : longint;
begin
  for y := clipymin to clipymax do begin
    for x := clipxmin to clipxmax do begin
      a := xres*y+x;

      if (px_count[a]=0)and(py_count[a]=0) then begin
        { pixel-center not covered by filled polygons,
          but there mey be contours crossing the pixel border. }
        i := px_dist[a];
        j := py_dist[a];
        if (i=128)and(j=128) then begin
          pixelsg[a] += maxwhite;
        end else begin
          if (i<128)and(j<128) then begin
            i := max(i,j)+(min(i,J) div 4);
            j := 0;
          end else begin
            i := min(i,j);
            j := i;      
          end;
          i := 128-i;
          pixelsg[a] += gminwhite +i*ginvminwhite;
        end;
      end else begin
        // if the pixel is in contour-area, fill black without
        // further guess, since there might be black lines on the
        // area which would confuse the guessing algorithm and
        // make it think it's near the outside world. Nothing to do.
      end;

    end;
  end;
end;


procedure ttmultirenderer.make_bwcol(scanmode : longint);
var x,y,a : longint;
begin
  for y := clipymin to clipymax do begin
    for x := clipxmin to clipxmax do begin
      a := xres*y+x;
      if decide_pixel(x,y,scanmode) 
      then pixelsbw[a] := 0
      else pixelsbw[a] := 255;
    end;
  end;
end;

procedure ttmultirenderer.setMultisample(multi : longint);
var i : longint;
begin
  ms := multi;
  i := 2*ms+1;
  i := i*i;
  maxwhite := $3FFFFFFF div i;
  gminwhite := round(guess_minwhite * maxwhite);
  ginvminwhite := ((maxwhite-gminwhite) div 158);
  gminwhite := maxwhite-ginvminwhite*158;
  colerr := $3FFFFFFF -i*maxwhite;
  maxcolsum := i*maxwhite;

  writeln('maxwhite:',maxwhite);
  writeln('gminwhite:',gminwhite);
  writeln('ginvminwhite:',ginvminwhite);
  writeln('ginvminwhite*ms:',ginvminwhite*i);
  writeln('colerr:',colerr);
  writeln('werr=',$3FFFFFFF-(gminwhite +ginvminwhite*158)*i);
end;

function ttmultirenderer.getgrey(x,y : longint) : longint;
begin
  if (pixelsg[y*xres+x]<0) then getgrey := 0 else
  getgrey := pixelsg[y*xres+x];
end;

function ttmultirenderer.getbw(x,y : longint) : byte;
begin
  getbw := pixelsbw[y*xres+x];
end;


procedure ttmultirenderer.check_size(x,y : TTcoord); 
begin
  if (xres<x+3)or(yres<y+3) then begin
    setres(2*(x+3),2*(y+3));
  end;
end;

begin
end.












